package com.ge.hc.lcs.axone.security.iam.mua.pageobjects;

import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.ge.hc.lcs.axone.security.iam.mua.constants.UIComponentConstants;
import com.ge.hc.lcs.axone.security.iam.mua.utilities.Utility;

import net.serenitybdd.core.pages.PageObject;

public class UserPage extends PageObject {

	@FindBy(xpath = ".//*[@id='select2-metadataTypeId-container']")
	private WebElement dropDownMetaDataType;

	@FindBy(xpath = ".//*[@id='js']/body/span/span/span[1]/input")
	private WebElement textMetaDataType;

	@FindBy(xpath = "//input[@id='firstName']")
	private WebElement textFirstName;

	@FindBy(xpath = "//input[@id='lastName']")
	private WebElement textLastName;

	@FindBy(id = "emailAddress")
	private WebElement txtEmail;	

	@FindBy(id = "saveBtn")
	private WebElement buttonSave;

	@FindBy(id = "login")
	private WebElement userLogin;

	@FindBy(id = "notifyUserViaEmail")
	private WebElement chkNotifyUserViaEmail;

	@FindBy(id = "notifySupervisorViaEmail")
	private WebElement chkNotifySupervisorViaEmail;

	@FindBy(id = "provisionOnStartDate")
	private WebElement chkProvisionOnStartDate;

	@FindBy(className = "title")
	private WebElement editUserlabel;

	@FindBy(id = "principal")
	private WebElement txtPrincipal;

	@FindBy(id = "submitUserSearch")
	private WebElement submitUserSearch;

	@FindBy(xpath = "//i[contains(text(),'mode_edit')]")
	private WebElement editUserButton;

	@FindBy(xpath = "//*[@id='REMOVE_USER']")
	private WebElement deleteUserButton;

	@FindBy(id = "save")
	private WebElement buttonSaveDeleteConfirm;

	@FindBy(id = "resultsArea")
	private WebElement searchResult;

	@FindBy(xpath = "//*[@id='js']/body/div[3]")
	private WebElement alertPopup;

	@FindBy(id = "password")
	private WebElement txtPassword;

	@FindBy(id = "confirmPassword")
	private WebElement txtConfirmPassword;

	@FindBy(id = "submit")
	private WebElement btnResetPassword;

	@FindBy(xpath = "//*[@id='resultsArea']/div/table/tbody")
	private WebElement tblSearchResults;

	@FindBy(id = "logoutBtn")
	private WebElement btnLogout;

	@FindBy(xpath = "//*[@id='topMenu']/li[1]/a")
	private WebElement userSettings;

	@FindBy(id = "change_password_dropdown")
	private WebElement btnChangePassword;

	@FindBy(id = "currentPassword")
	private WebElement txtCurrentPassword;

	@FindBy(id = "newPassword")
	private WebElement txtNewPassword;

	@FindBy(id = "newPasswordConfirm")
	private WebElement txtNewPasswordConfirm;

	@FindBy(id = "submit")
	private WebElement btnChangePasswordSubmit;

	@FindBy(xpath = "//*[@id='js']/body/div[5]/div[1]")
	private WebElement divPasswordTooltips;

	@FindBy(id = "adminActions")
	private WebElement divAdminActions;

	@FindBy(xpath = "//*[@id='userRoleId']/div[2]/div/span/span[1]/span/span[1]")
	private WebElement roleInputAutoComplete;

	@FindBy(id = "unlockPswd")
	private WebElement forgotPswd;

	@FindBy(xpath = ".//*[@id='select2-resetPasswordMethod-container']")
	private WebElement resetPwdViaDropDown;

	@FindBy(xpath = "//*[@id='js']/body/span/span/span[1]/input")
	private WebElement txtResetPwd;

	@FindBy(id = "unlockUserForm")
	private WebElement divChallengeResponseQA;

	@FindBy(id = "save")
	private WebElement btnSaveChallengeRespAns;

	@FindBy(id = "userNameDisplay")
	private WebElement lblUserName;

	@FindBy(xpath = "//*[@id='js']/body/div/div[2]")
	private WebElement divChallengeRespQuestions;

	@FindBy(xpath = "//*[@id='js']/body/span/span/span[1]/input")
	private WebElement txtChallengeRespQuestion;

	@FindBy(xpath = "//*[@id='js']/body/div[1]/div/div[2]/form/div[8]/div/h4")
	private WebElement userIdentitiesExpand;

	@FindBy(xpath = "//*[@id='userIdentitiesContainer']/div/table/tbody")
	private WebElement tblUserIdentityDetails;

	@FindBy(xpath = "//*[@id='formContainer']/div/div[1]/div/div/span/span[1]/span")
	private WebElement addSearchField;

	@FindBy(id = "firstName")
	private WebElement txtFirstName;

	@FindBy(xpath = "//*[@id='error']")
	private WebElement divChallengeResError;

	
	private WebDriver driver;
	LoginPage loginPage;
	private String xpathMetaDataType = "//ul[@id='select2-metadataTypeId-results']//li[contains(text(),'";
	private String searchColumnAttribute;

	public UserPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * creating new user without role.
	 * 
	 * @param firstName
	 *            the first name of the user
	 * @param lastName
	 *            the last name of the user
	 * @param userLoginName
	 *            the user login name
	 * @throws Exception
	 *             the exception
	 */
	public void createUser(String firstName, String lastName, String userLoginName) throws Exception {
		// Last parameter for the below method is empty because user is created without
		// role.
		setUserInfo(firstName, lastName, userLoginName, "");
		validateSuccessMsg();
		Utility.waitForLoader(driver);
	}

	public void validateSuccessMsg() {
		Utility.switchToFrame(driver);
		Utility.assertAjaxSuccess(driver);
		Utility.switchToParentFrame(driver);

	}

	/**
	 * Sets the user information with invalid attributes.
	 *
	 * @param firstName
	 *            the first name
	 * @param lastName
	 *            the last name
	 * @param userLoginName
	 *            the user login name
	 * @throws Exception
	 *             the exception
	 */
	public void createInvalidUser(String firstName, String lastName, String userLoginName) throws Exception {
		// Last parameter for the below method is empty because user is created without
		// role.
		setUserInfo(firstName, lastName, userLoginName, "");
		validateFailureMsg();
	}

	/**
	 * Click on Edit user after searching user by principal.
	 *
	 * @throws Exception
	 *             the exception
	 */
	public void clickEditUser() throws Exception {
		Utility.switchToFrame(driver);
		Utility.click(driver, editUserButton);
		Utility.switchToParentFrame(driver);
		Utility.waitForPageToLoad(driver);
	}

	/**
	 * Search user by principal.
	 *
	 * @param principal
	 *            the principal name of the user
	 * @throws Exception
	 *             the exception
	 */
	public void searchUserByPrincipal(String principal) throws Exception {
		searchColumnAttribute="principal";
		searchUser(principal, txtPrincipal);
	}

	/**
	 * Delete user.
	 */
	public void deleteUser() {
		Utility.switchToFrame(driver);
		Utility.click(driver, deleteUserButton);
		driver.switchTo().activeElement();
		Utility.click(driver, buttonSaveDeleteConfirm);
		driver.switchTo().defaultContent();
		Utility.switchToParentFrame(driver);
	}

	/**
	 * Delete user.This method is called in adhoc test scenarios.
	 */
	public void executeDelete() {
		Utility.click(driver, deleteUserButton);
		driver.switchTo().activeElement();
		Utility.click(driver, buttonSaveDeleteConfirm);
		driver.switchTo().defaultContent();
		Utility.switchToParentFrame(driver);
		Utility.assertAjaxSuccess(driver);
	}

	/**
	 * Search the user by login id and verify user exists in search results.
	 *
	 * @param principal
	 *            the principal of the user
	 * @return true, if the user exists
	 * @throws Exception
	 *             the exception
	 */
	public boolean verifyUserExists(String principal) throws Exception {
		searchUserByPrincipal(principal);
		boolean exist = false;
		Utility.switchToFrame(driver);
		if (!(driver.getPageSource().contains("No Users were found for your search"))) {
			exist = editUserButton.isDisplayed();
		}
		Utility.switchToParentFrame(driver);
		return exist;
	}

	/**
	 * Below method is to verify the user name in edit page after creating a
	 * user.Once the user click on submit while creating new user, page will be
	 * navigated to edit user.
	 *
	 * @param firstName
	 *            the first name of the user
	 * @param lastName
	 *            the last name of the user
	 * @return true, if the user name is displayed in the edit user page
	 * @throws Exception
	 *             the exception
	 */
	public boolean verifyUserNameInEditPage(String firstName, String lastName) throws Exception {
		boolean exist = false;
		Utility.waitForAjax(driver);
		Utility.waitForPageToLoad(driver);
		Utility.switchToFrame(driver);
		Utility.waitForElementToBeVisible(driver, divAdminActions);
		Utility.waitForElementToBeVisible(driver, editUserlabel);
		if (editUserlabel.getText().contains(firstName + " " + lastName))
			exist = true;
		Utility.switchToParentFrame(driver);
		return exist;
	}

	/**
	 * Gets the first name of the user by passing principal.
	 *
	 * @param principal
	 *            the principal name of the user
	 * @return the first name of the user
	 * @throws Exception
	 *             the exception
	 */
	public String getFirstName() throws Exception {
		return getUserAttribute(textFirstName);
	}

	/**
	 * Gets the last name of the user by passing principal.
	 *
	 * @param principal
	 *            the principal name of the user
	 * @return the last name of the user
	 * @throws Exception
	 *             the exception
	 */
	public String getLastName() throws Exception {
		return getUserAttribute(textLastName);
	}

	private String getUserAttribute(WebElement element) throws Exception {
		String attribute = "";
		Utility.switchToFrame(driver);
		Utility.waitForAjax(driver);
		Utility.waitForElementToBeVisible(driver, divAdminActions);
		Utility.waitForElementToBeVisible(driver, element);
		attribute = element.getAttribute("value");
		Utility.switchToParentFrame(driver);
		return attribute;
	}

	/**
	 * Update first name of the user in the edit page of user information.
	 * 
	 * @param newAttributeValue
	 *            the new value for first name
	 * @throws Exception
	 *             the exception
	 */
	public void updateFirstName(String newAttributeValue) throws Exception {
		updateUserAttribute(newAttributeValue, textFirstName);
	}

	/**
	 * Update last name of the user in the edit page of user information.
	 * 
	 * @param newAttributeValue
	 *            the new value for last name
	 * @throws Exception
	 *             the exception
	 */
	public void updateLastName(String newAttributeValue) throws Exception {
		updateUserAttribute(newAttributeValue, textLastName);
	}

	private void updateUserAttribute(String attributeValue, WebElement element) throws Exception {
		Utility.switchToFrame(driver);
		Utility.waitForElementToBeVisible(driver, divAdminActions);
		Utility.type(driver, attributeValue, element);
		Utility.switchToParentFrame(driver);
	}
	
	public void saveUser() throws Exception {
		Utility.switchToFrame(driver);
		Utility.click(driver, buttonSave);
		Utility.switchToParentFrame(driver);
	}

	/**
	 * Validates the failure popup window is displayed when the user performs any
	 * invalid action.
	 */
	public void validateFailureMsg() throws Exception {
		Utility.switchToFrame(driver);
		Utility.assertAjaxFailure(driver);
		Utility.switchToParentFrame(driver);
	}

	/**
	 * Search user by passing the search text.Search text can be any of the
	 * following fields(LASTNAME,EMAIL,PRINCIPAL,EMPLOYEEID).
	 *
	 * @param field
	 *            the search field
	 * @param searchValue
	 *            the search text
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public boolean searchUserByValue(UIComponentConstants.SearchBy field, String searchValue) throws Exception {
		boolean result = false;
		switch (field) {
		case LASTNAME:
			searchUser(searchValue, textLastName);
			searchColumnAttribute="name";
			result = true;
			break;
		case EMAIL:
			searchUser(searchValue, txtEmail);
			searchColumnAttribute="email";
			result = true;
			break;
		case PRINCIPAL:
			searchUser(searchValue, txtPrincipal);
			searchColumnAttribute="principal";
			result = true;
			break;		
		case FIRSTNAME:
			searchUser(searchValue, txtFirstName);
			searchColumnAttribute="name";
			result = true;
			break;
		default:
			result = false;
		}
		return result;
	}

	private void searchUser(String inputText, WebElement element) throws Exception {
		Utility.switchToFrame(driver);
		Utility.type(driver, inputText, element);
		Utility.click(driver, submitUserSearch);
		Utility.waitForElementToBeVisible(driver, searchResult);
		Utility.switchToParentFrame(driver);
	}

	/**
	 * Verify user in search results table.
	 *
	 * @param searchValue the search text value
	 * @param searchColumnAttribute the column attribute name of the search field
	 * @return true, if the user exists
	 * @throws Exception             the exception
	 */
	public boolean verifySearchValueExistsInSearchResults(String searchValue) throws Exception {
		boolean exists = false;
		Utility.switchToFrame(driver);
		Utility.waitForElementToBeVisible(driver, tblSearchResults);
		if (!(driver.getPageSource().contains("No Users were found for your search"))) {
			List<WebElement> rows = tblSearchResults.findElements(By.xpath("tr"));
			for (WebElement row : rows) {
				List<WebElement> columns = row.findElements(By.xpath("td"));
				for (WebElement col : columns) {
					if (col.getAttribute("openiam-row").equals(searchColumnAttribute)) {
						exists = col.getText().contains(searchValue);
						if (exists) {
							break;
						}
					}
				}
				if (exists)
					break;
			}
		}
		Utility.switchToParentFrame(driver);
		return exists;

	}

	/**
	 * Below method is to enter the values for Password and the Confirm Password
	 * text boxes.This method should be called when the admin is resetting the
	 * password for a user.
	 *
	 * @param password
	 *            the password
	 * @throws Exception
	 *             the exception
	 */

	public void enterPasswordValuesByAdmin(String password) throws Exception {
		Utility.switchToFrame(driver);
		Utility.type(driver, password, txtPassword);
		Utility.type(driver, password, txtConfirmPassword);
		Utility.switchToParentFrame(driver);
	}

	/**
	 * Verify the success message while resetting password.
	 *
	 * @throws Exception
	 *             the exception
	 */
	public void verifyResetPassword() throws Exception {
		Utility.switchToFrame(driver);
		Utility.click(driver, btnResetPassword);
		Utility.assertAjaxSuccess(driver);
		Utility.switchToParentFrame(driver);
	}

	/**
	 * Verifying the user is able to login to Selfservice portal once after the
	 * admin resetting the password.
	 *
	 * @param username
	 *            the username
	 * @param password
	 *            the password
	 * @return true, if the user is able to login to Selfservice portal
	 * @throws Exception
	 *             the exception
	 */
	public boolean verifyUserLoginAfterPasswordReset(String username, String password) throws Exception {
		boolean result = false;
		loginPage.login(username, password);
		if (driver.getCurrentUrl().contains(UIComponentConstants.CHANGEPASSWORD_PAGE)) {
			result = true;
		}
		return result;
	}

	/**
	 * Below method is to change the password by the user in self service portal.
	 *
	 * @param oldPassword
	 *            the old password
	 * @param newPassword
	 *            the new password
	 * @throws Exception
	 *             the exception
	 */
	public void changePassword(String oldPassword, String newPassword) throws Exception {
		Utility.switchToParentFrame(driver);
		Utility.click(driver, userSettings);
		Utility.click(driver, btnChangePassword);
		Utility.switchToFrame(driver);
		Utility.waitForPageToLoad(driver);
		Utility.type(driver, oldPassword, txtCurrentPassword);
		Utility.type(driver, newPassword, txtNewPassword);
		Utility.type(driver, newPassword, txtNewPasswordConfirm);
		Utility.switchToParentFrame(driver);
	}

	/**
	 * Verify change password by user. Note: Clicking of Change Password button is
	 * done here as we need to validate the success messgae which will be hided
	 * immediately after clicking of Change Password button
	 *
	 * @throws Exception
	 *             the exception
	 */
	public void verifyChangePasswordByUser() throws Exception {
		Utility.switchToFrame(driver);
		Utility.click(driver, btnChangePasswordSubmit);
		Utility.assertAjaxSuccess(driver);
		Utility.waitForAjax(driver);
		Utility.switchToParentFrame(driver);
	}

	/**
	 * Below method is to validate password is not changed when attempt to give non
	 * compliance password.
	 *
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public boolean verifyPasswordNotChanged() throws Exception {
		boolean result = false;
		Utility.switchToFrame(driver);
		result = !(btnChangePasswordSubmit.isEnabled()) ? true : false;
		Utility.switchToParentFrame(driver);
		return result;
	}

	/**
	 * Creates a user and assign the role to the user.
	 * 
	 * @param firstName
	 *            the first name of the user
	 * @param lastName
	 *            the last name of the user
	 * @param userLoginName
	 *            the user login name (principal name)
	 * @param role
	 *            the role name to be entitled to the user
	 * @throws Exception
	 *             the exception
	 */
	public void createUserWithRole(String firstName, String lastName, String userLoginName, String role)
			throws Exception {
		setUserInfo(firstName, lastName, userLoginName, role);
	}

	/**
	 * Setting user information while creation.
	 *
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param userLoginName the user login name
	 * @param role the role
	 * @throws Exception the exception
	 */
	private void setUserInfo(String firstName, String lastName, String userLoginName, String role) throws Exception {
		Utility.waitForPageToLoad(driver);
		Utility.waitForAjax(driver);
		Utility.waitForLoader(driver);
		Utility.switchToFrame(driver);
		Utility.type(driver, userLoginName, userLogin);
		Utility.type(driver, firstName, textFirstName);
		Utility.type(driver, lastName, textLastName);
		if (role != "") {
			Utility.select2Autocomplete(driver, roleInputAutoComplete, role);
			Utility.waitForAjax(driver);
		}
		// Uncheck all the notification check boxes to avoid giving input to all the
		// fields while creating user
		Utility.uncheckCheckBox(chkNotifyUserViaEmail);
		Utility.uncheckCheckBox(chkNotifySupervisorViaEmail);
		Utility.uncheckCheckBox(chkProvisionOnStartDate);
		Utility.click(driver, buttonSave);
		Utility.switchToParentFrame(driver);
	}

	/**
	 * Navigate to login page.
	 *
	 * @param URL
	 *            the url
	 * @throws Exception
	 *             the exception
	 */
	public void navigateToLoginPage(String URL) throws Exception {
		driver.navigate().to(URL);
	}

	/**
	 * Click forgot password link.
	 *
	 * @throws Exception
	 *             the exception
	 */
	public void clickForgotPasswordLink() throws Exception {
		Utility.waitForPageToLoad(driver);
		Utility.click(driver, forgotPswd);
	}

	private void submitChallenegeRespAnswers(JSONObject jsonchallengeResponseQA, String loginId) throws Exception {
		Utility.waitForPageToLoad(driver);
		Utility.waitForAjax(driver);
		Utility.waitForElementToBeClickable(driver, resetPwdViaDropDown);
		Utility.click(driver, resetPwdViaDropDown);
		Utility.select2Autocomplete(driver, txtResetPwd, UIComponentConstants.RESETPWD_BY_CHALLENGERESPONSE);
		Utility.waitForAjax(driver);
		Utility.type(driver, loginId, txtPrincipal);
		Utility.click(driver, btnChangePasswordSubmit);
		Utility.waitForPageToLoad(driver);
		String question = "";
		String answer = "";
		WebElement txtAnswer;
		List<WebElement> divs = divChallengeResponseQA.findElements(By.cssSelector(".form-group.has-feedback"));
		for (WebElement div : divs) {
			question = div.findElement(By.xpath("./input")).getAttribute("oiamquestiontext").trim();
			answer = jsonchallengeResponseQA.get(question).toString();
			txtAnswer = div.findElement(By.xpath("./input"));
			Utility.type(driver, answer, txtAnswer);
		}
		Utility.click(driver, btnSaveChallengeRespAns);
		Utility.waitForPageToLoad(driver);
		Utility.waitForAjax(driver);
	}

	/**
	 * Reset password by challenge response questions and answers.
	 *
	 * @param jsonchallengeResponseQA
	 *            the json string of challenge response questions and answers
	 * @param loginId
	 *            the login id
	 * @param newPassword
	 *            the new password
	 * @throws Exception
	 *             the exception
	 */
	public void resetPasswordByChallengeResponse(JSONObject jsonchallengeResponseQA, String loginId, String newPassword)
			throws Exception {
		submitChallenegeRespAnswers(jsonchallengeResponseQA, loginId);
		resetNewPassword(newPassword);
		
	}
	
	public void resetNewPassword(String newPassword) {
		Utility.waitForPageToLoad(driver);
		Utility.type(driver, newPassword, txtNewPassword);
		Utility.type(driver, newPassword, txtNewPasswordConfirm);
	}

	/**
	 * Verify reset password is success after answering the challenge response
	 * questions.
	 *
	 * @throws Exception
	 *             the exception
	 */
	public void verifyResetPasswordAfterChallengeRespAnswers() throws Exception {
		Utility.click(driver, btnResetPassword);
		Utility.assertAjaxSuccess(driver);
	}

	/**
	 * Verify user is in home page.
	 *
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public boolean verifyUserIsInHomePage() {
		boolean exists = false;
		Utility.waitForAjax(driver);
		Utility.waitForPageToLoad(driver);
		Utility.waitForElementToBeVisible(driver, lblUserName);
		exists = lblUserName.isDisplayed();
		return exists;
	}

	/**
	 * Below method is to change the password by the user during first time login in
	 * self service portal once after admin resets the password.
	 *
	 * @param oldPassword
	 *            the old password
	 * @param newPassword
	 *            the new password
	 * @throws Exception
	 *             the exception
	 */
	public void changePasswordAfterAdminResetsPwd(String oldPassword, String newPassword) throws Exception {
		Utility.type(driver, oldPassword, txtCurrentPassword);
		Utility.type(driver, newPassword, txtNewPassword);
		Utility.type(driver, newPassword, txtNewPasswordConfirm);
		Utility.click(driver, btnChangePasswordSubmit);
		Utility.assertAjaxSuccess(driver);
	}

	/**
	 * Verify user is prompted for challenge response questions and answers page.
	 *
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public boolean verifyUserPromptedForChallengeRespQA() throws Exception {
		Utility.waitForPageToLoad(driver);
		boolean result = false;
		Utility.waitForElementToBeVisible(driver, divChallengeRespQuestions);
		List<WebElement> divs = divChallengeRespQuestions.findElements(By.cssSelector(".row"));
		if (divs.size() > 0)
			result = true;
		return result;

	}

	/**
	 * Sets the challenge response questions and answers.
	 *
	 * @param jsonchallengeResponseQA
	 *            the json string with predefined challenge response questions and
	 *            answers
	 * @param customQA
	 *            the custom challenge response questions and answers
	 * @throws Exception
	 *             the exception
	 */
	public void setChallengeResponseQA(JSONObject jsonchallengeResponseQA, JSONObject customQA) throws Exception {
		Utility.waitForPageToLoad(driver);
		Iterator<String> keysItr = jsonchallengeResponseQA.keys();
		Iterator<String> custKeysItr = customQA.keys();
		Utility.waitForElementToBeVisible(driver, divChallengeRespQuestions);
		List<WebElement> divs = divChallengeRespQuestions.findElements(By.cssSelector(".row"));
		String question = "";
		String answer = "";
		String custom_question = "";
		String custom_answer = "";

		for (WebElement div : divs) {
			if (div.findElement(By.xpath("./div")).getAttribute("class").trim().equals("col-sm-12")&&div.findElement(By.xpath("./div/div")).getAttribute("class").trim().equals("question well")) {
				if (keysItr.hasNext()) {
					question = keysItr.next();
					answer = jsonchallengeResponseQA.get(question).toString();
					keysItr.remove();
				}
				WebElement txtAnswer = div.findElement(By.xpath("./div/div/div[2]/div/input"));
				WebElement txtCnfAnswer = div.findElement(By.xpath("./div/div/div[3]/div/input"));

				if (div.findElement(By.xpath("./div/div/div[1]")).getAttribute("class").trim().equals("form-group")) {
					div.findElement(By.xpath("./div/div/div[1]")).click();
					Utility.select2Autocomplete(driver, txtChallengeRespQuestion, question);
					Utility.type(driver, answer, txtAnswer);
					Utility.type(driver, answer, txtCnfAnswer);

				} else {
					if (custKeysItr.hasNext()) {
						custom_question = custKeysItr.next();
						custom_answer = customQA.get(custom_question).toString();
						custKeysItr.remove();
					}
					div.findElement(By.xpath("./div/div/div[1]/input")).sendKeys(custom_question);
					Utility.type(driver, custom_answer, txtAnswer);
					Utility.type(driver, custom_answer, txtCnfAnswer);
				}
			}
		}
	}

	/**
	 * Verify challenge response QA are set.
	 *
	 * @throws Exception
	 *             the exception
	 */
	public void verifyChallengeResponseQAAreSet() throws Exception {
		Utility.click(driver, btnSaveChallengeRespAns);
		Utility.assertAjaxSuccess(driver);
	}

	/**
	 * Adds the search field.
	 *
	 * @param fieldValue
	 *            the field value to be searched for autocomplete text box
	 * @throws Exception
	 *             the exception
	 */
	public void addSearchField(String fieldValue) throws Exception {
		Utility.switchToFrame(driver);
		Utility.select2Autocomplete(driver, addSearchField, fieldValue);
		Utility.waitForAjax(driver);
		Utility.waitForElementToBeVisible(driver, txtFirstName);
		Utility.switchToParentFrame(driver);
	}

	/**
	 * Submit incorrect challenge response QA.
	 *
	 * @param jsonchallengeResponseQA
	 *            the incorrect jsonchallenge response QA
	 * @param loginId
	 *            the login id of the user
	 * @throws Exception
	 *             the exception
	 */
	public void submitIncorrectChallengeResponseQA(JSONObject jsonchallengeResponseQA, String loginId)
			throws Exception {
		submitChallenegeRespAnswers(jsonchallengeResponseQA, loginId);
		Utility.assertChallengeRespQAFailure(driver, divChallengeResError);
	}

	/**
	 * Gets the error message for incorrect challenge response QA.
	 *
	 * @return the error message for incorrect challenge response QA
	 * @throws Exception
	 *             the exception
	 */
	public String getErrorMsgForIncorrectChallengeResponseQA() throws Exception {
		return divChallengeResError.getText();
	}

	/**
	 * Verify user in unlock user account page.
	 *
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	public boolean verifyUserInUnlockUserAccountPage() throws Exception {
		boolean result = false;
		if (driver.getCurrentUrl().contains(UIComponentConstants.UNLOCKRESETPASSWORD_PAGE)) {
			result = true;
		}
		return result;
	}

	/**
	 * Setting user information while creation.
	 *
	 * @param metadataType
	 *            the metadata type
	 * @param firstName
	 *            the first name
	 * @param lastName
	 *            the last name
	 * @param userLoginName
	 *            the user login name
	 * @param role
	 *            the role
	 * @throws Exception
	 *             the exception
	 */
	public void setUserInfo(String metadataType, String firstName, String lastName, String userLoginName, String role)
			throws Exception {
		Utility.type(driver, userLoginName, userLogin);
		setMetaDataType(metadataType);
		Utility.type(driver, firstName, textFirstName);
		Utility.type(driver, lastName, textLastName);
		if (role != "") {
			Utility.select2Autocomplete(driver, roleInputAutoComplete, role);
			Utility.waitForAjax(driver);
		}
		// Uncheck all the notification check boxes to avoid giving input to all the
		// fields while creating user
		Utility.uncheckCheckBox(chkNotifyUserViaEmail);
		Utility.uncheckCheckBox(chkNotifySupervisorViaEmail);
		Utility.uncheckCheckBox(chkProvisionOnStartDate);
		Utility.click(driver, buttonSave);
		Utility.switchToParentFrame(driver);
		Utility.assertAjaxSuccess(driver);
	}

	/**
	 * Below method is to choose the user metadata type in the user information
	 * while creating new user.
	 *
	 * @param metaDataType
	 *            the meta data type
	 * @throws Exception
	 *             the exception
	 */
	private void setMetaDataType(String metaDataType) throws Exception {
		Utility.click(driver, dropDownMetaDataType);
		Utility.type(driver, metaDataType, textMetaDataType);
		Utility.click(driver, By.xpath(xpathMetaDataType + metaDataType + "')]"));
	}
	
	/**
	 * Checks whether the user is at reset password window
	 * @throws Exception
	 *             the exception
	 *
	*/
	public boolean isChangePasswordPageDisplayed() throws Exception {
 		boolean result = false;
 		if (driver.getCurrentUrl().contains(UIComponentConstants.CHANGEPASSWORD_PAGE)) {
 			result = true;
 		}
 		return result;
 	}
}
