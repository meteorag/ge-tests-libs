package com.ge.hc.lcs.axone.security.iam.mua.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.ge.hc.lcs.axone.security.iam.mua.utilities.Utility;

import net.serenitybdd.core.pages.PageObject;

public class MenuPage extends PageObject {

	@FindBy(id = "outerMenuACC_CONTROL")
	private WebElement linkAccessControl;

	@FindBy(id = "outerMenuSECURITY_ROLE")
	private WebElement linkRole;

	@FindBy(id = "outerMenuIDMAN")
	private WebElement linkUserAdmin;

	@FindBy(id = "outerMenuUSER_SEARCH")
	private WebElement linkUser;

	@FindBy(linkText = "User Entitlements")
	private WebElement linkUserEntitlements;

	@FindBy(css = "div.entitlement_menu")
	private WebElement userEntitlementMenu;

	@FindBy(id = "outerMenuSECURITY_POLICY")
	private WebElement policy;

	@FindBy(id = "outerMenuAUTHENTICATION_POLICY")
	private WebElement authenticationPolicy;

	@FindBy(linkText = "Reset Password")
	private WebElement linkResetPassword;

	@FindBy(id = "resetTypeSelect")
	private WebElement dropdownResetType;

	@FindBy(id = "outerMenuADMIN")
	private WebElement administrationMenu;

	@FindBy(id = "outerMenuSYNCLOG")
	private WebElement linkLogViewer;

	@FindBy(xpath = "//*[@id='submenu']//a[contains(text(),'Create New User')]")
	private WebElement createNewUserMenu;

	@FindBy(id = "outerMenuPASSWORD_POLICY")
	private WebElement passwordPolicy;


	private WebDriver driver;
	public MenuPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Select roles menu.
	 */
	public void selectRolesMenu() {
		selectMenu(linkAccessControl, linkRole);
	}

	/**
	 * Select user admin menu.
	 */
	public void selectUserAdminMenu() {
		selectMenu(linkUserAdmin, linkUser);
	}

	/**
	 * Select authentication policy menu.
	 */
	public void selectAuthenticationPolicyMenu() {
		selectMenu(policy, authenticationPolicy);
	}

	private void selectMenu(WebElement mainTab, WebElement subTab) {
		Utility.waitForPageToLoad(driver);
		Utility.waitForAjax(driver);
		Utility.switchToParentFrame(driver);
		Utility.waitForElementToBeClickable(driver, mainTab);
		Utility.click(driver, mainTab);
		Utility.click(driver, subTab);
	}

	/**
	 * Select user entitlement menu.
	 */
	public void selectUserEntitlementMenu() {
		selectSubMenu(linkUserEntitlements, userEntitlementMenu);
	}

	/**
	 * Click reset password menu.
	 */
	public void clickResetPasswordMenu() {
		selectSubMenu(linkResetPassword, dropdownResetType);
	}

	/**
	 * Select sub menu after clicking the main menu.
	 *
	 * @param subMenu the sub menu
	 * @param waitForElement wait till this web element to be loaded
	 */
	public void selectSubMenu(WebElement subMenu, WebElement waitForElement) {
		Utility.waitForPageToLoad(driver);
		Utility.switchToParentFrame(driver);
		Utility.click(driver, subMenu);
		Utility.switchToFrame(driver);
		Utility.waitForElementToBeVisible(driver, waitForElement);
		Utility.switchToParentFrame(driver);
	}

	/**
	 * Select log viewer menu.
	 */
	public void selectLogViewerMenu() {
		Utility.waitForLoader(driver);
		selectMenu(administrationMenu, linkLogViewer);
	}

	/**
	 * Select create user menu.
	 */
	public void selectCreateUserMenu() {
		Utility.click(driver,createNewUserMenu);
	}

	/**
	 * Select password policy menu.
	 */
	public void selectPasswordPolicyMenu() {
		Utility.waitForLoader(driver);
		selectMenu(policy, passwordPolicy);
	}

	/**
 	 * Checks if create user button is enabled.
 	 *
 	 * @return true, if it is enabled
 	 * @throws Exception the exception
 	 */
 	public boolean isCreateUserEnabled()
 			throws Exception {
 		return createNewUserMenu.isEnabled();
 	}

}
