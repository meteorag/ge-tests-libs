package com.ge.hc.lcs.axone.security.iam.mua.pageobjects;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.ge.hc.lcs.axone.security.iam.mua.constants.UIComponentConstants;
import com.ge.hc.lcs.axone.security.iam.mua.utilities.Utility;

import net.serenitybdd.core.pages.PageObject;

/**
 * @author 503014680
 *
 */
public class PasswordPolicyPage extends PageObject {

	@FindBy(xpath = "//input[@id='searchInput']")
	private WebElement searchInput;

	@FindBy(id = "search")
	private WebElement search;

	@FindBy(xpath = "//i[.='mode_edit']")
	private WebElement editPasswordPolicy;

	@FindBy(id = "attribute-value-from-PWD_LEN")
	private WebElement PwdLengthFrom;

	@FindBy(id = "attribute-value-to-PWD_LEN")
	private WebElement PwdLengthTo;

	@FindBy(id = "attribute-value-from-NUMERIC_CHARS")
	private WebElement NumCharsFrom;

	@FindBy(id = "attribute-value-to-NUMERIC_CHARS")
	private WebElement NumCharsTo;

	@FindBy(id = "attribut-value-PWD_HIST_VER")
	private WebElement histVersion;

	@FindBy(id = "attribut-value-REJECT_CHARS_IN_PSWD")
	private WebElement rejectChars;

	@FindBy(id = "attribut-value-QUEST_ANSWER_CORRECT")
	private WebElement qaCorrect;

	@FindBy(id = "attribute-value-CHNG_PSWD_ON_RESET")
	private WebElement changePwdOnReset;

	@FindBy(id = "attribut-value-QUEST_COUNT")
	private WebElement questCount;

	@FindBy(id = "attribute-value-from-PWD_EXP_WARN")
	private WebElement PwdExpWarningFrom;

	@FindBy(id = "attribute-value-to-PWD_EXP_WARN")
	private WebElement PwdExpWarningTo;

	@FindBy(id = "attribut-value-PWD_EXPIRATION")
	private WebElement pwdExp;
	
	@FindBy(id = "priority")
	private WebElement pwdPriority;
	
	@FindBy(id = "name")
	private WebElement pwdPolicyName;	

	@FindBy(id = "saveBtn")
	private WebElement btnSave;

	private WebDriver driver;

	public PasswordPolicyPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Update the field/fields of password policy to a new value and assert success message.
	 *
	 * @param attribute the attribute name of the Password policy
	 * @param value1 the from range value of the attribute field
	 * @param value2 the to range value of the attribute field
	 * @return true, if successful
	 * @throws Exception             the exception
	 */
	public boolean updatePasswordPolicy(UIComponentConstants.PasswordPolicyAttribute attribute, String value1,
			String value2) throws Exception {
		boolean result = false;		
		result=enterAttributeValues(attribute, value1,value2);
		savePolicy();
		Utility.switchToFrame(driver);
		Utility.assertAjaxSuccess(driver);		
		Utility.switchToParentFrame(driver);
		Utility.waitForAjax(driver);
		Utility.waitForPageToLoad(driver);
		return result;
	}
	
	/**
	 * Update the field/fields of password policy to an invalid value and assert failure message.
	 *
	 * @param attribute  the attribute name of the Password policy
	 * @param value1 the from range value of the attribute field
	 * @param value2 the to range value of the attribute field
	 * @return true, if successful
	 * @throws Exception             the exception
	 */
	public boolean updatePasswordPolicyWithInvalidValues(UIComponentConstants.PasswordPolicyAttribute attribute, String value1,
			String value2) throws Exception {
		boolean result = false;		
		result=enterAttributeValues(attribute, value1,value2);
		savePolicy();
		Utility.switchToFrame(driver);
		Utility.assertAjaxFailure(driver);
		Utility.waitForAjax(driver);
		Utility.switchToParentFrame(driver);
		return result;
	}
	
	/**
	 * Enter attribute values in password policy.
	 *
	 * @param attribute the attribute name in password policy
	 * @param value1 the from range value of the attribute
	 * @param value2 the to range value of the attribute
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public boolean enterAttributeValues(UIComponentConstants.PasswordPolicyAttribute attribute, String value1,
			String value2) throws Exception {
		boolean result = false;
		Utility.switchToFrame(driver);
		switch (attribute) {
		case PWD_LEN:
			typeAttributeValue(PwdLengthFrom, value1);
			typeAttributeValue(PwdLengthTo, value2);
			result = true;
			break;
		case NUMERIC_CHARS:
			typeAttributeValue(NumCharsFrom, value1);
			typeAttributeValue(NumCharsTo, value2);
			result = true;
			break;
		case PWD_HIST_VER:
			typeAttributeValue(histVersion, value1);
			result = true;
			break;
		case REJECT_CHARS_IN_PSWD:
			typeAttributeValue(rejectChars, value1);
			result = true;
			break;
		case QUEST_ANSWER_CORRECT:
			typeAttributeValue(qaCorrect, value1);
			result = true;
			break;
		case QUEST_COUNT:
			typeAttributeValue(questCount, value1);
			result = true;
			break;
		case CHNG_PSWD_ON_RESET:			
			Utility.waitForElementToBeClickable(driver, changePwdOnReset);			
			if(!((value1.equals("false") && !(changePwdOnReset.isSelected())) || (value1.equals("true") && (changePwdOnReset.isSelected()))))
			Utility.click(driver, changePwdOnReset);
			result = true;
			break;
		case PWD_EXP_WARN:
			typeAttributeValue(PwdExpWarningFrom, value1);
			typeAttributeValue(PwdExpWarningTo, value2);
			result = true;
			break;
		case PWD_EXPIRATION:
			typeAttributeValue(pwdExp, value1);
			result = true;
			break;
		case PRIORITY:
			typeAttributeValue(pwdPriority, value1);
			result = true;			
			break;
		case POLICY_NAME:
			typeAttributeValue(pwdPolicyName, value1);
			result = true;			
			break;
		default:
			result = false;
		}			
		Utility.switchToParentFrame(driver);
		return result;
	}
	
	/**
	 * Save password policy.
	 *
	 * @throws Exception the exception
	 */
	public void savePolicy() throws Exception {
		Utility.switchToFrame(driver);
		Utility.click(driver, btnSave);		
		Utility.switchToParentFrame(driver);		
	}

	private void typeAttributeValue(WebElement element, String value) throws Exception {
		Utility.waitForElementToBeVisible(driver, element);
		Utility.type(driver, value, element);
	}

	/**
	 * Search password policy and edit the same.
	 *
	 * @param policy the policy name
	 */
	public void searchAndEditPasswordPolicy(String policy) {
		Utility.switchToFrame(driver);
		Utility.type(driver, policy, searchInput);
		Utility.click(driver, search);
		Utility.click(driver, editPasswordPolicy);
		Utility.switchToParentFrame(driver);
	}

	/**
	 * Verify password policy attribute.
	 *
	 * @param attribute the attribute name in the password policy
	 * @param value1 the from range value of the attribute
	 * @param value2 the to range value of the attribute
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public boolean verifyPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute attribute, String value1,
			String value2) throws Exception {
		boolean result = false;
		Utility.switchToFrame(driver);
		switch (attribute) {
		case PWD_LEN:
			System.out.println("Expected Password length from :" + value1);
			assertAttributeValue(PwdLengthFrom, value1);
			System.out.println("Expected Password length to :" + value2);
			assertAttributeValue(PwdLengthTo, value2);
			result = true;
			break;
		case NUMERIC_CHARS:
			System.out.println("Expected Password Numeric character from :" + value1);
			assertAttributeValue(NumCharsFrom, value1);
			System.out.println("Expected Password Numeric character to :" + value2);
			assertAttributeValue(NumCharsTo, value2);
			result = true;
			break;
		case PWD_HIST_VER:
			System.out.println("Expected Password history version :" + value1);
			assertAttributeValue(histVersion, value1);
			result = true;
			break;
		case REJECT_CHARS_IN_PSWD:
			System.out.println("Expected password reject character :" + value1);
			assertAttributeValue(rejectChars, value1);
			result = true;
			break;
		case QUEST_ANSWER_CORRECT:
			System.out.println("Expected number of answers to be correct :" + value1);
			assertAttributeValue(qaCorrect, value1);
			result = true;
			break;
		case QUEST_COUNT:
			System.out.println("Expected number of questions to display :" + value1);
			assertAttributeValue(questCount, value1);
			result = true;
			break;
		case CHNG_PSWD_ON_RESET:
			System.out.println("Expected value for change password after reset :" + value1);			
			Utility.waitForElementToBeVisible(driver, changePwdOnReset);
			if (value1.equals("true"))
				Assert.assertTrue(changePwdOnReset.isSelected());
			else if (value1.equals("false"))
				Assert.assertFalse(changePwdOnReset.isSelected());
			result = true;
			break;
		case PWD_EXP_WARN:
			System.out.println("Expected Password expiration warning from value :" + value1);
			assertAttributeValue(PwdExpWarningFrom, value1);
			System.out.println("Expected Password expiration warning to value :" + value2);
			assertAttributeValue(PwdExpWarningTo, value2);
			result = true;
			break;
		case PWD_EXPIRATION:
			System.out.println("Expected password expiration days :" + value1);
			assertAttributeValue(pwdExp, value1);
			result = true;
			break;
		case PRIORITY:
			System.out.println("Expected password priority :" + value1);
			assertAttributeValue(pwdPriority, value1);
			result = true;
			break;
		case POLICY_NAME:
			System.out.println("Expected password policy name :" + value1);
			assertAttributeValue(pwdPolicyName, value1);
			result = true;
			break;
		default:
			result = false;
		}
		Utility.switchToParentFrame(driver);
		return result;
	}

	private void assertAttributeValue(WebElement element, String value) throws Exception {
		Utility.waitForElementToBeVisible(driver, element);
		String actValue = Utility.getValByJavaScript(driver, element);
		System.out.println("Actual value :" + actValue);
		Assert.assertTrue(actValue.trim().equals(value.trim()));
	}
}
