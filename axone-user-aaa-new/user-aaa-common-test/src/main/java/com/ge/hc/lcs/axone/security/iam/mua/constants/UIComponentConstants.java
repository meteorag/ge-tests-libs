package com.ge.hc.lcs.axone.security.iam.mua.constants;

public final class UIComponentConstants {
	public static final String CONTENT_FRAME = "contentFrame";
	public static final String OPENIAM_SOAPSVC="/openiam-esb/idmsrvc";
	public static final String HOSTNAME = "hostname";
	// UI pages
	public static final String LOGIN_PAGE = "login.html";
	public static final String WEBCONSOLE = "/webconsole";

	public static final Double AJAXSLEEP_TIMEOUT = 0.5;
	public static final Integer TIMEOUT_30 = 30;
	public static final Integer TIMEOUT_10 = 10;
	public static final Integer TIMEOUT_120 = 120;
	public static final String SELFSERVICE = "/selfservice";
	public static final String CHANGEPASSWORD_PAGE = "changePassword.html";
	public static final String RESETPWD_BY_CHALLENGERESPONSE = "Challenge Response Questions";
	public static final String AUTHENTICATIONPOLICY_NAME = "Default Authn Policy";
	public static final String UNLOCKRESETPASSWORD_PAGE = "unlockUserResetPasswordForm.html";
	public static final String PASSWORDPOLICY_NAME = "Default Pswd Policy";
	public static final Integer TIMEOUT_1 = 1;
    public static final String DEFAULT_TOKEN_LIFE = "30";
	public static final Integer MINIMUM_TOKEN_LIFE = 1;
	public enum SearchBy {
	    LASTNAME,
	    EMAIL,
	    PRINCIPAL,
	    EMPLOYEEID, 
	    FIRSTNAME
	}

	public enum AuthenticationPolicyAttribute {
		FAILED_AUTH_COUNT,AUTO_UNLOCK_TIME,TOKEN_LIFE
	}

	public enum PasswordPolicyAttribute {
		PWD_LEN,
		NUMERIC_CHARS,
		PWD_HIST_VER,
		REJECT_CHARS_IN_PSWD,
		QUEST_ANSWER_CORRECT,
		QUEST_COUNT,
		CHNG_PSWD_ON_RESET,
		PWD_EXP_WARN,
		PWD_EXPIRATION,
		PRIORITY,
		POLICY_NAME
	}
}
