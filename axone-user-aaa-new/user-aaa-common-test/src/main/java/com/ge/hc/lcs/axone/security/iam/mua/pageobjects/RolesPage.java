package com.ge.hc.lcs.axone.security.iam.mua.pageobjects;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.ge.hc.lcs.axone.security.iam.mua.utilities.Utility;

import net.serenitybdd.core.pages.PageObject;

public class RolesPage extends PageObject {

	@FindBy(id = "_easyui_textbox_input1")
	private WebElement role_search_input;

	@FindBy(className = "datagrid-mask-msg")
	private WebElement progess_status;

	@FindBy(id = "saveRow")
	private WebElement saveRow;

	@FindBy(xpath = "//span[@class='tree-title' and contains(.,'Roles')]")
	private WebElement roleEntitlement;

	@FindBy(xpath = "//div[@class='menu-text' and .='Add']")
	private WebElement roleToBeEntitled;

	@FindBy(id = "select2-roleEntitlementSearchResultAutocomplete1-container")
	private WebElement roleEntitlementInput;

	@FindBy(css = ".datagrid-body [field='name']")
	private WebElement roleSearchOutput;

	@FindBy(xpath = "//div[@class='menu-text' and .='Add']")
	private WebElement addRole;

	@FindBy(xpath = "//div[@class='menu-text' and .='Remove']")
	private WebElement removeRole;

	@FindBy(xpath = "//div[@class='modal-content']")
	private WebElement removeRolePopup;

	@FindBy(xpath = "//div[@class='modal-content']//div[@class='OK pull-right']/a")
	private WebElement removeRolePopupOK;

	@FindAll({ @FindBy(xpath = ".//table[@class='datagrid-btable']/tbody/tr/td[1]") })
	public List<WebElement> column1RoleList;

	private WebDriver driver;

	public RolesPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Verify roles are listed in UI.
	 *
	 * @param roleNames the role names
	 */
	public void verifyRoles(final List<String> roleNames) {
		Utility.switchToFrame(driver);
		Utility.waitForAjax(driver);
		List<String> roleNamesUI = new ArrayList<String>();
		for (WebElement row : column1RoleList) {
			roleNamesUI.add(row.getText());
		}
		Assert.assertTrue(roleNames.size()==roleNamesUI.size() && roleNamesUI.containsAll(roleNames));
		Utility.switchToParentFrame(driver);
	}

	/**
	 * Adds the role to the user.
	 *
	 * @param role the role
	 */
	public void addRoletoUser(String role) {
		Utility.switchToFrame(driver);
		Utility.rightClick(driver, roleEntitlement);
		Utility.click(driver, addRole);
		Utility.select2Autocomplete(driver, roleEntitlementInput, role);
		Utility.waitForAjax(driver);
		Utility.click(driver, saveRow);
		Utility.assertAjaxSuccess(driver);
		Utility.waitForAjax(driver);
		Utility.switchToParentFrame(driver);
	}

	/**
	 * Removes the role from user.
	 *
	 * @param roleName the role
	 */
	public void removeRoleFromUser(String roleName) {
		Utility.switchToFrame(driver);
		By roleElement = By.xpath(
				"//span[@class='tree-title' and contains( translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'"
						+ roleName.toLowerCase() + "')]");
		Utility.rightClick(driver, driver.findElement(roleElement));
		Utility.click(driver, removeRole);
		Utility.waitForElementToBeVisible(driver, removeRolePopup);
		Utility.click(driver, removeRolePopupOK);
		Utility.assertAjaxSuccess(driver);
		Utility.switchToParentFrame(driver);
	}

	/**
	 * Checks if role is entitled for user.
	 *
	 * @param user the user
	 * @param roleName the role name
	 * @return true, if role is entitled for user
	 */
	public boolean isRoleEntitledForUser(String user, String roleName) {
		Utility.switchToFrame(driver);
		boolean isPresent = Utility.isPresent(driver, By.xpath(
				"//span[@class='tree-title' and contains( translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'"
						+ roleName.toLowerCase() + "')]"));
		Utility.switchToParentFrame(driver);
		return isPresent;
	}

	/**
	 * Verify all the roles assigned for a user.
	 *
	 * @param user the user
	 * @param roleNames the role names
	 */
	public void verifyRolesForUser(String user, List<String> roleNames) {
		roleNames.forEach(roleName -> Assert.assertTrue(isRoleEntitledForUser(user, roleName)));
	}

}
