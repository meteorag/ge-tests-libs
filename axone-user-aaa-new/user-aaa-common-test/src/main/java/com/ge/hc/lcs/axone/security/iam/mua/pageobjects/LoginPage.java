package com.ge.hc.lcs.axone.security.iam.mua.pageobjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.ge.hc.lcs.axone.security.iam.mua.constants.UIComponentConstants;
import com.ge.hc.lcs.axone.security.iam.mua.utilities.Utility;

import net.serenitybdd.core.pages.PageObject;

public class LoginPage extends PageObject {

	@FindBy(id = "principal")
	private WebElement principal;

	@FindBy(id = "input_password")
	private WebElement password_element;

	@FindBy(id = "submit")
	private WebElement submit;

	@FindBy(id = "userNameDisplay")
	private WebElement lblUserName;

	private final String idLogOutBtn = "logoutBtn";
	@FindBy(id = idLogOutBtn)
	private WebElement logOut;

	@FindBy(className = "error")
	private WebElement errorMsg;

	@FindBy(id = "loginAgain")
	private WebElement loginAgain;
	
	@FindBy(css = ".btn.btn-success.btn-sm")
 	private WebElement logOutConfirm;

	@FindBy(id = "newPasswordConfirm")
	private WebElement txtNewPasswordConfirm;

	@FindBy(id = "currentPassword")
	private WebElement txtCurrentPassword;

	@FindBy(id = "newPassword")
	private WebElement txtNewPassword;
	
	@FindBy(xpath = "//*[@id='topMenu']/li[1]/a")
	private WebElement userSettings;

	@FindBy(id = "error")
	private WebElement error;

	@FindBy(id = "change_password_dropdown")
	private WebElement btnChangePassword;

	private WebDriver driver;

	public LoginPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Login as admin in webconsole portal.
	 *
	 * @param adminUsername the admin username
	 * @param adminPassword the admin password
	 * @return the login page
	 */
	public LoginPage loginAsAdmin(String adminUsername, String adminPassword) {
		String url = System.getProperty(UIComponentConstants.HOSTNAME) + UIComponentConstants.WEBCONSOLE;
		openURL(url);
		login(adminUsername, adminPassword);
		return this;
	}

	/**
	 * Login as user in selfservice portal.
	 *
	 * @param username the username
	 * @param password the password
	 * @return the login page
	 */
	public LoginPage loginAsUser(String username, String password) {
		String url = System.getProperty(UIComponentConstants.HOSTNAME) + UIComponentConstants.SELFSERVICE;
		openURL(url);
		login(username, password);
		return this;
	}

	/**
	 * Open URL.
	 *
	 * @param URL the url of the page
	 */
	public void openURL(final String URL) {
		openAt(URL);
	}

	/**
	 * Below method perform login action.
	 *
	 * @param login the login id (principal name) of the user
	 * @param password the password
	 */
	public void login(final String login, final String password) {
		verifyUserIsInLoginPage();
		enterLoginCredentials(login, password);
		clickOnSubmit();
		changePasswordOnFirstTimeLogin(password, Utility.getDefaultNewPassword());
	}
	
	public void enterLoginCredentials(String login, String password) {
		Utility.type(driver, login, principal);
		Utility.type(driver, password, password_element);
	}
	
	public void clickOnSubmit() {
		Utility.click(driver, submit);
	}

	/**
	 * Perform Log out action.
	 */
	public void logOut() {
		if (driver.findElements(By.id(idLogOutBtn)).size() > 0) {
			Utility.switchToParentFrame(driver);
			Utility.click(driver, logOut);
			driver.switchTo().activeElement();
 			Utility.click(driver, logOutConfirm);
 			driver.switchTo().defaultContent();
		}
	}

	/**
	 * Verifies user has logged out and login again page is displayed
	 */
	public void verifyUserLogout() {
		Utility.waitForAjax(driver);
		Utility.waitForPageToLoad(driver);
		Utility.waitForElementToBeVisible(driver, loginAgain);
	}

	/**
	 * Maximize window.
	 *
	 * @throws Exception the exception
	 */
	public void maximizeWindow() throws Exception {
		Utility.waitForPageToLoad(driver);
		Utility.waitForAjax(driver);
		driver.manage().window().maximize();
		Utility.waitForPageToLoad(driver);
	}

	/**
	 * Verifying the user is able to login to Selfservice portal once after the
	 * admin resetting the password.
	 *
	 * @param username the username
	 * @param password the password
	 * @return true, if the user is able to login to Selfservice portal
	 * @throws Exception the exception
	 */
	public boolean verifyUserLoginAfterPasswordReset(String username, String password) throws Exception {
		boolean result = false;
		String currentURL = driver.getCurrentUrl();
		String URL = System.getProperty(UIComponentConstants.HOSTNAME) + UIComponentConstants.SELFSERVICE;
		driver.navigate().to(URL);
		login(username, password, URL);
		if (driver.getCurrentUrl().contains(UIComponentConstants.CHANGEPASSWORD_PAGE)) {
			result = true;
		}
		driver.navigate().to(currentURL);
		return result;
	}

	/**
	 * Below method perform login action.
	 *
	 * @param login the login id (principal name) of the user
	 * @param password the password
	 */
	public void login(final String login, final String password, final String URL) {
		openAt(URL);
		verifyUserIsInLoginPage();
		enterLoginCredentials(login, password);
		clickOnSubmit();
		changePasswordOnFirstTimeLogin(password, Utility.getDefaultNewPassword());

	}
	public boolean verifyUserIsInHomePage() throws Exception {
		//Below code is just a hack to refresh the UserSearch page and will be removed once OpenIAM 4.1.3 got released
		try
		{
			Thread.sleep(1000);
			driver.navigate().refresh();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		Utility.waitForElementToBeVisible(driver, lblUserName);
		return lblUserName.isDisplayed();
	}
	
	public boolean verifyUserLoggedIntoWebConsole() {
		return driver.getTitle().contains("WebConsole");
	}

	public void verifyErrorMessage(String message) {
		Utility.waitForAjax(driver);
		Utility.waitForPageToLoad(driver);
		Utility.waitForElementToBeVisible(driver, errorMsg);
		Assert.assertTrue(errorMsg.getText().contains(message));
	}
	
	/**
	 * Navigate to login page.
	 *
	 * @param URL
	 *            the url
	 * @throws Exception
	 *             the exception
	 */
	public void navigateToLoginPage(String URL) {
		driver.navigate().to(URL);
	}
	
	/**
	 * Verify User is on login page
	 */
	public void verifyUserIsInLoginPage() {
		driver.manage().window().maximize();
		Utility.waitForPageURLToContain(driver, UIComponentConstants.LOGIN_PAGE);
	}
	
	/**
	 * Click on user settings option. This method is invoked after user session is
	 * expired and clicking this will automatically prompt for login.
	 */
	public void clickChangePassword() {
		Utility.switchToParentFrame(driver);
		Utility.waitForElementToBeClickable(driver, userSettings);
		Utility.click(driver, userSettings);
		Utility.click(driver, btnChangePassword);
		Utility.switchToParentFrame(driver);
	}
	
	/**
	 * Verify login button is displayed. After inactivity logout verify user is
	 * prompted for login again.
	 * 
	 * @return
	 */
	public boolean verifyLoginButtonIsDisplayed() {
		Utility.switchToParentFrame(driver);
		Utility.waitForElementToBeVisible(driver, principal);
		return principal.isDisplayed();
	}
	
	/**
	 * Verifies whether invalid redirect URI/client Id message is displayed
	 */
	public void verifyErrorMessageForInvalidRedirectURIOrClientId(String message) {
		Utility.waitForPageToLoad(driver);
		Utility.waitForElementToBeVisible(driver, error);
		Assert.assertTrue(error.getText().matches(message));
	}
	
	/**
	 * Verifies the authenticated user has access to Admin portal/webconsole
	 * 
	 * @return
	 */
	public boolean verifyUserUnauthorizedForWebConsole() {
		Utility.waitForPageToLoad(driver);
		return driver.getTitle().trim().equals("OpenIAM - Unauthorized");
	}

	public void changePasswordOnFirstTimeLogin(String oldPassword, String newPassword ) {
		if (driver.getCurrentUrl().contains(UIComponentConstants.CHANGEPASSWORD_PAGE)) {
			Utility.type(driver, oldPassword, txtCurrentPassword);
			Utility.type(driver, newPassword, txtNewPassword);
			Utility.type(driver, newPassword, txtNewPasswordConfirm);
			clickOnSubmit();
			Utility.assertAjaxSuccess(driver);
		}
	}
	
	public void redirectToURLAfterPasswordChange(String URL) {
		Utility.waitForPageToLoad(driver);
		if (driver.getCurrentUrl().contains("selfservice")) {
			driver.navigate().to(URL);
		}
	}	
}
