package com.ge.hc.lcs.axone.security.iam.mua.pageobjects;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.ge.hc.lcs.axone.security.iam.mua.constants.UIComponentConstants;
import com.ge.hc.lcs.axone.security.iam.mua.utilities.Utility;

import net.serenitybdd.core.pages.PageObject;

/**
 * @author 503014680
 *
 */
public class AuthenticationPolicyPage extends PageObject {

	@FindBy(xpath = "//input[@id='searchInput']")
	private WebElement searchInput;

	@FindBy(id = "search")
	private WebElement search;

	@FindBy(xpath = "//i[.='mode_edit']")
	private WebElement editAuthenticationPolicy;

	@FindBy(id = "FAILED_AUTH_COUNT")
	private WebElement FAILED_AUTH_COUNT;

	@FindBy(id = "AUTO_UNLOCK_TIME")
	private WebElement AUTO_UNLOCK_TIME;

	@FindBy(id = "TOKEN_LIFE")
	private WebElement TOKEN_LIFE;

	@FindBy(id = "save")
	private WebElement save;

	private WebDriver driver;

	public AuthenticationPolicyPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Update the field/fields of authentication policy to a new value.
	 *
	 * @param attribute the attribute name of the AuthenticationPolicy
	 * @param value the value for the attribute
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public boolean updateAuthenticationPolicy(UIComponentConstants.AuthenticationPolicyAttribute attribute, String value) throws Exception {
		// TBD search is required or not, Since only one default policy will be there
		// going forward
		boolean result=false;
		Utility.switchToFrame(driver);
		result=updateAuthenticationPolicyAttribute(attribute,value);
		Utility.click(driver, save);
		Utility.assertAjaxSuccess(driver);
		Utility.waitForAjax(driver);
		Utility.waitForLoader(driver);
		Utility.switchToParentFrame(driver);
		return result;
	}

	/**
	 * Note:For now we are updating only FAILED_AUTH_COUNT. We can add the appropriate case statement if we want to update for other attributes.
	 */
	private boolean updateAuthenticationPolicyAttribute(UIComponentConstants.AuthenticationPolicyAttribute attribute,String value) throws Exception {
		boolean result=false;
		switch (attribute) {
		case FAILED_AUTH_COUNT:
			typeAttributeValue(value, FAILED_AUTH_COUNT);
			result=true;
			break;
		case AUTO_UNLOCK_TIME:
			typeAttributeValue(value,AUTO_UNLOCK_TIME);
			result=true;
			break;
		case TOKEN_LIFE:
			typeAttributeValue(value,TOKEN_LIFE);
			result=true;
			break;
		default:
			result=false;
		}
		return result;
	}

	private void typeAttributeValue(String value, WebElement element) throws Exception {
		Utility.waitForElementToBeVisible(driver, element);
		Utility.type(driver, value, element);
	}

	/**
	 * Below method is to verify the attribute of authentication policy is updated with the new value.
	 *
	 * @param attribute the attribute name of authentication policy
	 * @param value the value for the attribute
	 * @param policyName the authentication policy name
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public boolean verifyAttributeValue(UIComponentConstants.AuthenticationPolicyAttribute attribute, String value,
			String policyName) throws Exception {
		// TBD search is required or not, Since only one default policy will be there
		// going forward
		boolean result=false;
		Utility.switchToFrame(driver);
		result=verifyAttribute(attribute,value);
		Utility.switchToParentFrame(driver);
		return result;
	}

	/**
	 * Note:For now we are validating only FAILED_AUTH_COUNT. We can add the appropriate case statement if we want to validate for other attributes.
	 */
	private boolean verifyAttribute(UIComponentConstants.AuthenticationPolicyAttribute attribute,String value) throws Exception {
		boolean result=false;
		switch (attribute) {
		case FAILED_AUTH_COUNT:
			System.out.println("Expected and Actual Values of failed authentication count are below");
			assertAttributeValue(value,FAILED_AUTH_COUNT);
			result=true;
			break;
		case AUTO_UNLOCK_TIME:
			System.out.println("Expected and Actual Values of auto unlock time are below");
			assertAttributeValue(value,AUTO_UNLOCK_TIME);
			result=true;
			break;
		case TOKEN_LIFE:
			System.out.println("Expected and Actual Values of token life are below");
			assertAttributeValue(value,TOKEN_LIFE);
			result=true;
			break;
		default:
			result=false;
		}
		return result;
	}

	private void assertAttributeValue(String value, WebElement element) throws Exception {
		Utility.waitForElementToBeVisible(driver, element);
		System.out.println("Expected Value is :" + value.trim());
		System.out.println("Actual Value is:" +element.getAttribute("value").trim());
		Assert.assertTrue(element.getAttribute("value").trim().equals(value.trim()));
	}

	/**
	 * Below method is search for the authentication policy
	 *
	 * @param policyName the authentication policy name
	 * @return none, if successful
	 * @throws Exception the exception
	 */
	public void searchAuthenticationPolicy(String policy) {
		Utility.switchToFrame(driver);
		Utility.type(driver, policy, searchInput);
		Utility.click(driver, search);
		Utility.click(driver, editAuthenticationPolicy);
		Utility.switchToParentFrame(driver);
	}
	
	/**
	 * Update authentication policy with invalid values.
	 *
	 * @param attribute the name of the attribute
	 * @param value the value of the attribute
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public boolean updateAuthenticationPolicyWithInvalidValues(UIComponentConstants.AuthenticationPolicyAttribute attribute, String value) throws Exception {
		boolean result=false;
		Utility.switchToFrame(driver);
		result=updateAuthenticationPolicyAttribute(attribute,value);
		Utility.click(driver, save);
		Utility.assertAjaxFailure(driver);
		Utility.waitForAjax(driver);
		Utility.switchToParentFrame(driver);
		return result;
	}

}
