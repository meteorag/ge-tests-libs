package com.ge.hc.lcs.axone.security.iam.mua.pageobjects;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.ge.hc.lcs.axone.security.iam.mua.utilities.Utility;

import net.serenitybdd.core.pages.PageObject;

/**
 * @author 503014680
 *
 */
public class LogViewerPage extends PageObject {

	@FindBy(id = "select2-searchFilter-result-l02q-userId")
	private WebElement targetUserSearchCriteria;

	@FindBy(id = "fromDate")
	private WebElement fromDateTime;

	@FindBy(id = "toDate")
	private WebElement toDateTime;

	@FindBy(xpath = "//*[@id=\"formContainer\"]/div/div[1]/div/div/span/span[1]/span")
	private WebElement searchCriteria;

	@FindBy(id = "select2-action-container")
	private WebElement selectAction;

	@FindBy(xpath = "//*[@id='js']/body/span/span/span[1]/input")
	private WebElement txtSelectAction;

	@FindBy(id = "submitAuditSearch")
	private WebElement searchButton;

	@FindBy(id = "resultsArea")
	private WebElement logResults;

	@FindBy(xpath = "//*[@id='resultsArea']/table/tbody")
	private WebElement tblLogResults;

	@FindBy(id = "result")
	private WebElement result;
	
	private WebDriver driver;

	public LogViewerPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Verify log for the user action.
	 *
	 * @param targetUser
	 *            the target user
	 * @param starDateTime
	 *            the star date time when the action is performed
	 * @param endDateTime
	 *            the end date time when the action is completed
	 * @param action
	 *            the user action
	 * @param logCount
	 *            number of logs generated for the corresponding user action.For
	 *            example, if two roles are assigned to the same user, then two logs
	 *            will be generated for the action ADD_USER_TO_ROLE.
	 * @return true, if the log is available
	 * @throws Exception
	 *             the exception
	 */
	public boolean verifyLogForTheAction(String targetUser, String starDateTime, String endDateTime, String action,
			int logCount) throws Exception {
		boolean exist = false;
		Utility.switchToFrame(driver);
		// Retry the logs every 10 seconds for 9 times
		for (int i = 0 ; i < 9 ; i++) {
			Utility.sleep(10);
			Utility.click(driver, searchButton);
			Utility.waitForElementToBeVisible(driver, tblLogResults);
			boolean isTableEmpty = driver.getPageSource().contains("No Audit Log Records were found for your search");
			if (!isTableEmpty) {
				exist = verifyLogResults(targetUser, action, logCount, starDateTime, endDateTime);
			} 
			if (exist) {
				break;
			}
		}
		Utility.switchToParentFrame(driver);
		return exist;
	}
	
	/**
	 *  Verifies the log is available in the search results. Here the default log count is 1.
	 * @param targetUser
	 * @param action
	 * @param status
	 * @param startDate
	 * @param toDate
	 * @return
	 * @throws Exception 
	 */
	public boolean verifyLogs(String targetUser, String action, String status, String startDate, String toDate) throws Exception {
		boolean exist = false;
		Utility.switchToFrame(driver);
		boolean retrySearch = false;
		// Retry the logs every 10 seconds for 9 times
		for (int i = 0; i < 9; i++) {
			Utility.sleep(10);
			// Retry search if the result is not available
			if (retrySearch) {
				Utility.click(driver, searchButton);
			}
			Utility.waitForElementToBeVisible(driver, tblLogResults);
			boolean isTableEmpty = driver.getPageSource().contains("No Audit Log Records were found for your search");
			if (!isTableEmpty) {
				exist = verifyLogResults(targetUser, action, status, startDate, toDate);
			}
			if (exist) {
				break;
			}
			retrySearch = true;
		}
		Utility.switchToParentFrame(driver);
		return exist;
	}

	/**
	 * Search the logs in log viewer using the search criteria provided
	 * 
	 * @param targetUser
	 * @param starDateTime
	 *            the start date time when the action is performed
	 * @param endDateTime
	 *            the end date time when the action is completed
	 * @param action
	 */
	public void searchLogs(String starDateTime, String endDateTime, String action, String status) {
		// Log synchronization in OpenIAM takes time. So the explicit wait is added before searching the logs.
		Utility.sleep(10);
		Utility.switchToFrame(driver);
		Utility.click(driver, searchButton);
		Utility.switchToParentFrame(driver);
	}
	
	private boolean verifyLogResults(String user, String action, String status, String startDate, String endDate) throws Exception {
		boolean result = false;
		Utility.waitForElementToBeVisible(driver, tblLogResults);
		List<WebElement> rows = tblLogResults.findElements(By.xpath("tr"));
		for (WebElement row : rows) {
			List<WebElement> columns = row.findElements(By.xpath("td"));
			if (verifyUserExistsInLogResults(columns, user) && verifyActionExistsInLogResults(columns, action)
					&& (status.equals("") || verifyActionStatusInLogResults(columns, status)) && (verifyDateInResults(columns, startDate, endDate))) {
				result = true;
				break;
			}
		}
		return result;
	}
	
	private boolean verifyActionStatusInLogResults(List<WebElement> columns, String status) {
		boolean statusExists = false;
		for (WebElement col : columns) {
			if (col.getAttribute("openiam-row").equals("result")) {
				statusExists = col.getText().contains(status);
				break;
			}
		}
		return statusExists;
	}
	
	/**
	 * Verify the principal name of the user and the action performed in log
	 * results.
	 */
	private boolean verifyLogResults(String user, String action, int logCount , String startDate, String endDate) throws Exception {
		boolean result = false;
		Utility.waitForElementToBeVisible(driver, tblLogResults);
		List<WebElement> rows = tblLogResults.findElements(By.xpath("tr"));		
		int actualLogCount = 0;
		for (WebElement row : rows) {
			List<WebElement> columns = row.findElements(By.xpath("td"));
			if (verifyUserExistsInLogResults(columns, user) && verifyActionExistsInLogResults(columns, action) && (verifyDateInResults(columns, startDate, endDate))) {
				actualLogCount = actualLogCount+1;
			}
			// If the logs to be searched are found then skip verifying the remaining 
			if (logCount == actualLogCount) {
				result = true;
				break;
			}
		}
		return result;

	}

	private boolean verifyUserExistsInLogResults(List<WebElement> columns, String user) {
		boolean userExists = false;
		for (WebElement col : columns) {
			if (col.getAttribute("openiam-row").equals("principal")) {
				userExists = col.getText().contains(user);
				break;
			}
		}
		return userExists;
	}

	private boolean verifyActionExistsInLogResults(List<WebElement> columns, String action) {
		boolean actionExists = false;
		for (WebElement col : columns) {
			if (col.getAttribute("openiam-row").equals("action")) {
				actionExists = col.getText().contains(action);
				break;
			}
		}
		return actionExists;
	}
	
	private boolean verifyDateInResults(List<WebElement> columns, String startDate, String endDate) throws Exception {
		boolean isDateExists = false;
		for (WebElement col : columns) {
			if (col.getAttribute("openiam-row").equals("formattedDate")) {
				isDateExists = isDateBetweenStartandEnd(startDate, endDate,col.getText());
				break;
			}
		}
		return isDateExists;
	}

	private boolean isDateBetweenStartandEnd(String startDate, String endDate, String inputDate)
			throws Exception {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date start = dateFormatter.parse(startDate);
		Date end = dateFormatter.parse(endDate);
		Date input = dateFormatter.parse(inputDate);
		if ((input.after(start) && input.before(end)) || ((input.equals(start)) || (input.equals(end)))) {
			return true;
		}
		return false;
	}
}
