package com.ge.hc.lcs.axone.security.iam.mua.utilities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

import com.ge.hc.lcs.axone.security.iam.mua.constants.UIComponentConstants;
import com.google.common.base.Predicate;

public class Utility {

	private Utility() {
	}

	static final Integer TIMEOUT_LEVEL;
	static EnvironmentVariables variables;

	static {
		variables = SystemEnvironmentVariables.createEnvironmentVariables();
		if (System.getProperty("timeout_level") == null) {
			TIMEOUT_LEVEL = 3;
		} else {
			TIMEOUT_LEVEL = Integer.valueOf(System.getProperty("timeout_level"));
		}
	}

	/**
	 * Wait for page URL to contain.
	 *
	 * @param driver the webdriver
	 * @param url the url of the page
	 */
	public static void waitForPageURLToContain(final WebDriver driver, final String url) {
		new WebDriverWait(driver, UIComponentConstants.TIMEOUT_30).until(ExpectedConditions.urlContains(url));
		waitForAjax(driver);
	}

	/**
	 * Wait for ajax call to be completed.
	 *
	 * @param driver the webdriver
	 */
	public static void waitForAjax(final WebDriver driver) {
		new WebDriverWait(driver, UIComponentConstants.TIMEOUT_30).until(new Predicate<WebDriver>() {

			@Override
			public boolean apply(WebDriver input) {
				final Boolean retval = (Boolean) ((JavascriptExecutor) driver)
						.executeScript("return window.jQuery != undefined && jQuery.active === 0");
				if (Boolean.TRUE.equals(retval)) {
					return true;
				} else {
					return false;
				}
			}
		});
		sleep(UIComponentConstants.AJAXSLEEP_TIMEOUT);
	}

	public static void sleep(final double seconds) {
		try {
			Thread.sleep((long) (seconds * 1000));
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	private static void removeLoader(final WebDriver driver) {
		((JavascriptExecutor) driver).executeScript("jQuery('.openiam-loader1').remove()"); /* remove loader */
	}

	/**
	 * Wait for element to be visible.
	 *
	 * @param driver the webdriver
	 * @param by the By locator
	 */
	public static void waitForElementToBeVisible(final WebDriver driver, final By by) {
		waitForElementToBeVisible(driver, by, UIComponentConstants.TIMEOUT_30 * TIMEOUT_LEVEL);
		waitForAjax(driver);
	}

	/**
	 * Wait for element to be visible.
	 *
	 * @param driver the webdriver
	 * @param webElement the web element of the page
	 */
	public static void waitForElementToBeVisible(final WebDriver driver, final WebElement webElement) {
		waitForElementToBeVisible(driver, webElement, UIComponentConstants.TIMEOUT_30 * TIMEOUT_LEVEL);
		waitForAjax(driver);
	}

	private static void waitForElementToBeVisible(final WebDriver driver, final By by, final int seconds) {
		new WebDriverWait(driver, seconds).until(ExpectedConditions.visibilityOfElementLocated(by));
		waitForAjax(driver);
	}

	private static void waitForElementToBeVisible(final WebDriver driver, final WebElement webElement,
			final int seconds) {
		new WebDriverWait(driver, seconds).until(ExpectedConditions.visibilityOf(webElement));
		waitForAjax(driver);
	}

	/**
	 * Wait for element to be clickable.
	 *
	 * @param driver the webdriver
	 * @param webElement the web element of the page
	 */
	public static void waitForElementToBeClickable(final WebDriver driver, final WebElement webElement) {
		new WebDriverWait(driver, UIComponentConstants.TIMEOUT_30 * TIMEOUT_LEVEL)
				.until(ExpectedConditions.elementToBeClickable(webElement));
		waitForAjax(driver);
	}

	/**
	 * Wait for element to not be visible.
	 *
	 * @param driver the webdriver
	 * @param by the By locator
	 * @param seconds the seconds
	 */
	public static void waitForElementToNotBeVisible(final WebDriver driver, final By by, final int seconds) {
		new WebDriverWait(driver, seconds).until(ExpectedConditions.invisibilityOfElementLocated(by));
		waitForAjax(driver);
	}

	/**
	 * Wait for element to not be visible.
	 *
	 * @param driver the webdriver
	 * @param webElement the web element
	 */
	public static void waitForElementToNotBeVisible(final WebDriver driver, final WebElement webElement) {
		new WebDriverWait(driver, UIComponentConstants.TIMEOUT_30 * TIMEOUT_LEVEL).until(
				ExpectedConditions.invisibilityOfAllElements(Stream.of(webElement).collect(Collectors.toList())));
		waitForAjax(driver);
	}

	/**
	 * Click event of web element.
	 *
	 * @param driver the webdriver
	 * @param by the By locator
	 */
	public static void click(final WebDriver driver, final By by) {
		waitForElementToBeVisible(driver, by, UIComponentConstants.TIMEOUT_30 * TIMEOUT_LEVEL);
		removeLoader(driver);
		driver.findElements(by).forEach(e -> {
			e.click();
		});
		waitForAjax(driver);
	}

	/**
	 * Click event of web element.
	 *
	 * @param driver the webdriver
	 * @param webElement the web element
	 */
	public static void click(final WebDriver driver, final WebElement webElement) {
		waitForElementToBeVisible(driver, webElement);
		waitForElementToBeClickable(driver, webElement);
		removeLoader(driver);
		webElement.click();
		waitForAjax(driver);
	}

	/**
	 * Perform right click of web element.
	 *
	 * @param driver the webdriver
	 * @param by the By locator
	 */
	public static void rightClick(final WebDriver driver, By by) {
		Actions action = new Actions(driver).contextClick(driver.findElement(by));
		action.build().perform();
	}

	/**
	 * Perofrm right click of web element.
	 *
	 * @param driver the webdriver
	 * @param webElement the web element
	 */
	public static void rightClick(final WebDriver driver, WebElement webElement) {
		Actions action = new Actions(driver).contextClick(webElement);
		action.build().perform();
	}

	/**
	 * Enter values in text box.
	 *
	 * @param driver the webdriver
	 * @param input the input text
	 * @param by the By locator
	 */
	public static void type(final WebDriver driver, final String input, final By by) {
		driver.findElements(by).forEach(element -> {
			type(driver, input, element);
		});
	}

	/**
	 * Enter values in text box.
	 *
	 * @param driver the webdriver
	 * @param input the input text
	 * @param element the web element
	 */
	public static void type(final WebDriver driver, final String input, final WebElement element) {
		waitForElementToBeVisible(driver, element);
		waitForElementToBeClickable(driver, element);
		removeLoader(driver);
		element.click();
		element.clear();
		element.sendKeys(input);
	}

	/**
	 * Switch to frame.
	 *
	 * @param driver the webdriver
	 */
	public static void switchToFrame(final WebDriver driver) {
		try {
			driver.switchTo().frame(driver.findElement(By.id("contentFrame")));
		} catch (NoSuchFrameException w) {
		}
	}

	/**
	 * Switch to parent frame.
	 *
	 * @param driver the webdriver
	 */
	public static void switchToParentFrame(final WebDriver driver) {
		try {
			driver.switchTo().parentFrame();
		} catch (NoSuchFrameException w) {

		}
	}

	/**
	 * Assert equals.
	 *
	 * @param webElement the web element
	 * @param value the value
	 */
	public static void assertEquals(final WebElement webElement, Object value) {
		if (value == null || StringUtils.isBlank(value.toString())) {
			Assert.assertTrue(StringUtils.isBlank(getValueOrText(webElement)));
		} else {
			if (value instanceof Date) {
				value = new SimpleDateFormat("MM/dd/yyyy").format((Date) value);
			}
			Assert.assertEquals(getValueOrText(webElement), value.toString());
		}
	}

	private static String getValueOrText(final WebElement webElement) {
		switch (webElement.getTagName().toLowerCase()) {
		case "input":
		case "select":
		case "textarea":
			return webElement.getAttribute("value");
		default:
			return StringUtils.trimToNull(webElement.getText());
		}
	}

	/**
	 * Select the value in autocomplete drop down menu.
	 *
	 * @param driver the webdriver
	 * @param autocomplete the autocomplete web element
	 * @param value the value to be selected
	 */
	public static void select2Autocomplete(final WebDriver driver, final WebElement autocomplete, final String value) {
		click(driver, autocomplete);
		waitForAjax(driver);
		final By by = By.cssSelector(".select2-container .select2-search__field");
		waitForElementToBeVisible(driver, by);
		WebElement el=driver.findElement(by);
		waitForElementToBeClickable(driver, el);
		el.sendKeys(value);
		waitForAjax(driver);
		waitForElementToBeVisible(driver, By.cssSelector("ul.select2-results__options li"),
				UIComponentConstants.TIMEOUT_30 * TIMEOUT_LEVEL);
		final List<WebElement> options = driver.findElements(By.cssSelector("ul.select2-results__options li"));
		Assert.assertTrue(CollectionUtils.isNotEmpty(options));
		final WebElement element = options.stream().filter(e -> StringUtils.containsIgnoreCase(e.getText(), value))
				.findFirst().orElse(null);
		Assert.assertNotNull(element);
		element.click();
		waitForAjax(driver);
	}

	/**
	 * Assert ajax success.
	 *
	 * @param driver the webdriver
	 */
	public static void assertAjaxSuccess(final WebDriver driver) {
		waitForElementToBeVisible(driver, By.cssSelector("div[data-notify='container'].alert-success"),
				UIComponentConstants.TIMEOUT_30 * TIMEOUT_LEVEL);
	}

	protected static int size(final WebDriver driver, final By by) {
		return driver.findElements(by).size();
	}

	public static boolean isPresent(final WebDriver driver, final By by) {
		return size(driver, by) > 0;
	}
	
	/**
	 * Perform Click build execute events through Action class.
	 *
	 * @param driver the webdriver
	 * @param element the web element
	 */
	public static void clickBuildExecute(final WebDriver driver,final WebElement element) {		
		Actions actions = new Actions(driver);		
		actions.moveToElement(element);
		actions.click().build().perform();
		waitForAjax(driver);
	}
	
	/**
	 * Assert ajax failure.
	 *
	 * @param driver the webdriver
	 */
	public static void assertAjaxFailure(final WebDriver driver) {
		waitForElementToBeVisible(driver,By.cssSelector("div[data-notify='container'].alert-danger"), UIComponentConstants.TIMEOUT_30 * TIMEOUT_LEVEL);
	}
	
	/**
	 * Wait for page to load.
	 *
	 * @param driver the webdriver
	 */
	public static void waitForPageToLoad(final WebDriver driver) {
		new WebDriverWait(driver, UIComponentConstants.TIMEOUT_30 * TIMEOUT_LEVEL).until(new Predicate<WebDriver>() {
			@Override
			public boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		});
	}
	
	/**
	 * Uncheck check box.
	 *
	 * @param element the webelement
	 * @throws Exception the exception
	 */
	public static void uncheckCheckBox(WebElement element) throws Exception {
		if (element.isSelected()) {
			element.click();
		}
	}
	
	/**
	 * Gets the current time stamp.
	 *
	 * @return the current time stamp
	 */
	public static String getCurrentTimeStamp() {
	    SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
	    return strDate;
	}
	
	/**
	 * Assert failure of Incorrect challenge response QA.
	 *
	 * @param driver the webdriver
	 */
	public static void assertChallengeRespQAFailure(final WebDriver driver,final WebElement element) {
		waitForElementToBeVisible(driver,element, UIComponentConstants.TIMEOUT_30 * TIMEOUT_LEVEL);
	}
	
	public static void selectDropDownValue(final WebElement dropDownElement, final String value ) {
		Select resultDropdown = new Select(dropDownElement);
		resultDropdown.selectByValue(value);
	}
	
	/**
	 * Gets the val by java script executor.
	 *
	 * @param driver the webdriver
	 * @param element the webelement
	 * @return the value by java script
	 */
	public static String getValByJavaScript(final WebDriver driver,final WebElement element) {
	    JavascriptExecutor e = (JavascriptExecutor) driver;
	    return e.executeScript("return arguments[0].value", element).toString();	    
	}
	
	/**
	 * Wait for OpenIAM loader overlay to disappear
	 * @param driver
	 */
	public static void waitForLoader(final WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver,UIComponentConstants.TIMEOUT_10 * TIMEOUT_LEVEL);
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				/**
				 * Waits till the openiam-loader overlay is hidden. 
				 * Which is determined using the change in the class attribute value 
				 * to openiam-loader1 text-center m-hidden. 
				 */
			    public Boolean apply(WebDriver driver) {
			    	WebElement element = driver.findElement(By.xpath("//div[text()='Processing Request...']"));
			        String classVal = element.getAttribute("class");
			        if(classVal.equals("openiam-loader1 text-center m-hidden"))  
			        	return true;
			        else 
			            return false;
			    }
			}); 
		} catch (Exception e) {
		}
	}
	
	public static String getTestAdminUser() {
		return variables.getProperty("testAdminUserName");
	}

	public static String getTestAdminPassword() {
		return variables.getProperty("testAdminPassword");
	}
	
	public static String getDefaultNewPassword() {
		return variables.getProperty("defaultNewPassword");
	}
}