User AAA Common Test Jar
======================================
* Provides the java wrapper methods which are common across design and subsystem test cases of IDM and IAM. 

Build Scripts
---------------------------

**Gradle**

`gradle clean build publish`

**publish**

Publish artifacts generated in this build to the configured remote repository

Usage
-------------
Add below line in your build.gradle 

compile group: 'com.ge.hc.lcs.axone.security', name: 'user-aaa-common-test', version: '0.X.0'
