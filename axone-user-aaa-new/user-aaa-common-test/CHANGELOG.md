# User AAA Common Jar - Changelog

This document summarizes changes made to the Axone User AAA Common Test jar, maintained by the MDH Axone Platform team.

## Version: 0.1.0

* This is the first version of this project.

## Version: 0.2.0

List of changes introduced in this release to adopt OpenIAM 412 Instance for IDM test cases

   * Added a new method in Authentication policy to Update authentication policy with Invalid values
   * Added a new method in UserPage to check whether the user is in reset password window or not
   * Implementation of Logout confirmation button click
   * Removed UTCDate conversion in LogViewerPage
   * Refreshing the LoginPage so that UserSearch page gets displayed on login
   * UserSearch web element ID got Updated


## Version: 0.3.0

List of changes introduced in this release to adopt OpenIAM 412 Instance for IAM Design test cases

   * Added redirectToURLAfterPasswordChange(String URL) – After changing the password after the first login user will be re-directed to selfservice by default. Using this method user can be redirected to required URL.
   * Added changePasswordOnFirstTimeLogin(String oldPassword, String newPassword ) – When the user logs in for the first time user will be asked to change the password. This method is allowed to change the password by doing a check only if URL contains changePassword
   * Added verifyUserUnauthorizedForWebConsole() – Non Admin user is not allowed to access web console. This method is used to verify it.
   * Added verifyErrorMessageForInvalidRedirectURIOrClientId() – Used to verify the error messages for invalid ClientID/URL/Secret
   * Added verifyLoginButtonIsDisplayed() - verify login button is disabled after inactivity timeout
   * Added clickUserSetting() – Used to click the user setting in selfservice
   * Modularized login(..) methods to verifyUserIsInLoginPage(); enterLoginCredentials(login, password); clickOnSubmit();
   * Added verifyUserLoggedIntoWebConsole() – to verify user is logged in to webconsole
   * Added getTestAdminUser()/getTestAdminPassword() – to get the service admin user credentials for soap service invocation
   * Added getDefaultNewPassword () – to get the password that has to be set on change password

## Version: 0.4.0

List of changes introduced in this release to adopt OpenIAM 412 Instance for IAM SubSystem test cases
   * Added changes to support test cases related to inactivity timeout
   * Added README.md