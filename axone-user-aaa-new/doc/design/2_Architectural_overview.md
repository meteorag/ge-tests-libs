
Architectural Overview
======================
This section provides an overview of the Identity and Access Management architecture by identifying the service's architectural components and main Interfaces internally and with the external systems (including Software and Hardware).

In terms of ISO 62304, this architectural description is considered the "software system" and the architectural components are the top level "software items" which is decomposed in to "software units" (the 
lowest level of decomposition as defined by ISO 62304). This document contains the design details of a software items (Identity Management and Access Management). Documents that contain requirements on SOUP that are software items are referenced as well.

## System Model Definition 
Identity and Access Management provides user management, authentication and authorization functionalities for Axone applications.

Identity Management(IDM) provides following capabilities,

* Provisioning and De-provisioning users
* Provisioning and De-provisioning of Resources, Roles , Groups and Organizations
* Managing password and authentication policies
* Registering and De-registering a client with OpenIAM
* Managing the access control for the users
* Logging the details of the above-mentioned IDM workflows

Access Management provides following capabilities,

* Authentication and Authorization for the users
* Auditing the authentication and authorization events

Refer to the Technical Design Reviews to support the design decisions - TBD.

## System Deployment Definition
Identity and Access management is deployed as a docker image. Below are the information about the docker image,

Name - TBD

Tagging - TBD

Location - Docker hub

## Subsequent sections
The following sections detail the breakdown of the components that make up the Identity and Access Management and support software for building and testing it.

Each section identifies each software item with a brief description, identifies the ones that are classfied as Software of Unknown Pedigree (SOUP) included in the product, and specifies the software safety classification of each software item per the SDLC Work Instruction.

Detailed descriptions of the class B and C software units can be found in their referenced design documents.