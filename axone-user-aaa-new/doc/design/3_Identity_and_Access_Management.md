
Identity and Access Management
===========
The following diagram shows the Component diagram for the Identity and Access management in the Axone Enterprise server,
<img src="img\AAA_Component_Diagram.jpg"/>

## Access Management

### Design Constraints of Access Management

The following are the design constraints of the Access Management,

- To enable and provide authentication and authorization functionalities for Axone applications (Web/Native/Mobile applications).
- To configure authentication and authorization policies in a single place for all applications.
- To enable Single Sign On (SSO) capability to login to the applications.
- Support User Authentication and Authorization by integrating with multiple Identity management systems

These design constraints calls out for Federated Authentication and Access Management. OAuth is the ideal way of achieving this and the same is used in Axone Platform.

Refer TDR for more details.

### OAuth Concepts

#### OAuth Brief

The OAuth 2.0 authorization framework enables an application (a.k.a client), typically a third-party application, to obtain limited access to resources of a user (a.k.a resource owner) or of the system, exposed by a HTTP/HTTPS service. For the requests made by the resource owner or on his behalf by the client, then this is achieved by orchestrating an approval interaction between the resource owner and the HTTP service.

There are multiple parties/roles involved to achieve the workflow defined by OAuth2.0 framework. These are defined below -

#### OAuth Roles

##### Resource Owner

The resource owner is the user who authorizes an application to access their resources. In Axone, the resource owner can be a **Clinician** who authorizes **Viewer** application to use his resources e.g. address, email id etc.

##### Client

The client is the application that wants to access the user's resources. Before it may do so, it must be authorized by the user. In Axone a **Viewer** application is one of the client used by **Clinician** to access resources. The client can be authorized by the user or can be pre-authorized during configuration.

##### Authorization Server

* Authorization server provides **access grant** to the client application after authenticating the resource owner and obtaining authorization from the resource owner, if required.
* All the Client's policies and configurations are defined and stored here.
  * Client applications like **Viewer** which needs access to user resources are defined here
* Resource owner's policies like role based policies and resources which they are entitled to are defined here
  * An **IT Admin** cannot have access to patient data
  * A **Clinician** cannot access system configuration data like password policies

##### Resource Server

The server hosting user-owned resources that are protected by OAuth. This is typically an API provider that holds and protects data such as photos, videos, calendars, or contacts. In Axone, these resources are user attributes like address, email etc. which the client applications might want to use.

In Axone **OpenIAM** represents both the Authorization server and Resource server.

Below is the oAuth workflow to access protected resource,
<img src="img\OAuthRoles.jpg"/>


#### OAuth usage in Axone

Below work flow how oAuth 2.0 framework is used in the Axone platform. It shows how a user gets authenticated to access the viewer application,

<img src="img\Axone_AccessManagement_Workflow.jpg"/>

### Access token - workflows

**Access tokens** are credentials used to access protected resources which is obtained after user and client  authentication. It represents the authorization of the client and resource owner against their respective policies. The authorization, also called grant, can be obtained using different workflows depending on the type of client.

The following are the list of grant types using which access token can be obtained from the Authorization server,

* Authorization Code
* Implicit
* Resource owner
* Client Credentials [Not supported in the current design]

#### Authorization Code

The authorization code is obtained by using an authorization server  as an intermediary between the client and resource owner.  Instead of  requesting authorization directly from the resource owner, the client  directs the resource owner to an authorization server, which in turn directs the  resource owner back to the client with the authorization code.
Before directing the resource owner back to the client with the  authorization code, the authorization server authenticates the  resource owner and obtains authorization.  Because the resource owner  only authenticates with the authorization server, the resource  owner's credentials are never shared with the client.
In the Authorization code workflow, the directing the resource owner by client to authorization server and back to client is done through a user-agent at the resource owner's end like Browser.

<img src="img\AuthorizationCode.png"/>
##### OAuth request details for Authorization

Below work flow shows the request information required in each request to get authorized and get the access token,
<img src="img\Client_Authentication_Authorization_Code_Grant.jpg"/>

In image above, the Authorization server requires user and client authentication and based on the access policies configured access token is provided.

#### Implicit

The implicit grant is a simplified authorization code flow optimized for clients implemented in a browser using a scripting language such as JavaScript.  In the implicit flow, instead of issuing the client an authorization code, the client is issued an access token directly.  The grant type is implicit, as no intermediate credentials (such as an authorization code) are issued
<img src="img\Implicit.png"/>
##### Client Authentication in Implicit grant

The implicit grant type does not include client authentication, and relies on the presence of the resource owner and the registration of the redirection URI. Because the access token is encoded into the redirection URI, it may be exposed to the resource owner and other applications residing on the same device. Below work flow shows how the client get the access token in implicit grant,

 <img src="img\Client_Authentication_Implicit_Grant.jpg"/>

In the image above, the Authorization server requires only user authentication and the access token is provided based on the configured policies.

#### Resource Owner

The resource owner password credentials (i.e., username and password) can be used directly as an authorization grant to obtain an access token.  The credentials should only be used when there is a high degree of trust between the resource owner and the client (e.g., the client is part of the device operating system or a highly privileged application), and when other authorization grant types are not available (such as an authorization code).
<img src="img\ResourceOwner.png"/>

##### Client Authentication in Resource Owner grant
Below work flow explains how the client is authenticated in resource owner grant to get the access token,
<img src="img\Client_Authentication_Resource_Owner_Grant.jpg"/>

Here the Access token is provided after the user authentication based on the configured policies.

### Choosing Authorization Grant

A grant is a method of acquiring an access token. Deciding which grants to implement depends on the type of client the end user will be using and the experience of the end user. Below flowchart will help in choosing the grant type based on the requirement,

<img src="img\choose_grant_type.jpg"/>

**First party client** is a client that you trust enough to handle the end user's authorization credentials.

**Third party** client is a client that you don't trust.

##### When to Use

###### Authorization code grant

1. Client authentication is required with client credentials.
2. Clients running on a dedicated server, where the access token and client secret is shared between the client and Authorization server.
3. Access token is not stored in the user agent (browser/ application). Not accessible with resource owner.

###### Implicit grant

1. When the client authentication is not required. Only user authentication is required to get the access token. It is used when storing of client id and client secret is not recommended.
2. Used in case of clients running on user agent like JavaScript application (or Native application).
3. Access token is stored in user agent.

###### Resource Owner grant

- High degree of trust between the resource owner and the client.
- When user trusts the application with his credentials (plain text).
- Used in case of Desktop/ mobile applications.

### Storing and Managing Access Tokens

TBD

### Refresh Token

Refresh tokens are credentials used to obtain access tokens. Refresh tokens are issued to the client by the authorization server and are   used to obtain a new access token when the current access token becomes invalid or expires, or to obtain additional access tokens   with identical or narrower scope.

Issuing a refresh token is optional at the discretion of the authorization server.  If the authorization server issues a refresh token, it is included when issuing an access token

A refresh token is a string representing the authorization granted to the client by the resource owner.  The string is usually opaque to the client.  The token denotes an identifier used to retrieve the authorization information.  Unlike access tokens, refresh tokens are intended for use only with authorization servers and are never sent to resource servers.
<img src="img\RefreshToken.jpg"/>
### Endpoints

Authorization process uses the below endpoints in the Identity and Access Management,

* Authorization Endpoint
* Token Endpoint
* UserInfo Endpoint
* Token Info Endpoint

#### Authorization Endpoint

The authorization endpoint is the endpoint on the authorization server where the resource owner logs in, and grants authorization to the client application.
Authorization endpoint in OpenIAM,
`http://<hostname>/idp/oauth2/authorize`

####Token Endpoint
The token endpoint is used by the client to obtain an access token by  presenting its authorization grant or refresh token. Token endpoint in OpenIAM,
`http://<hostname>/idp/oauth2/token`
Sample  response for token endpoint is shown below,

<pre><code>
{
"access_token":"2cn6gL2KIt8UbNQHOof0tC5jVD6pyyUhljodc.Xalg.NTTx8hcJWB57ovjCEAPfaJoaHItdD2S6o1R5xQP6tcn1ZbU",
   "token_type":"Bearer",
   "expires_in":18000
}
</code></pre>

####UserInfo Endpoint
The UserInfo endpoint returns claims about a user that is authenticated with OpenIAM server. These Claims are normally represented by a JSON object that contains a collection of name and value pairs for the Claims.
Userinfo endpoint in OpenIAM,
`http://<hostname>/idp/oauth2/userinfo`
Sample request and response for userinfo endpoint is shown below, (the request requries access token received from token endpoint)
<pre><code>
http://3.204.111.72:8080/idp/oauth2/userinfo?token=WHvc82YW20r0-lAZoXLJEpGFM5qkhnX6NsoW31QJEJmxDq8v5hcLTXdgG
</code></pre>

<pre><code>
{
  "sub"        : "83692",
  "name"       : "Bob",
  "email"      : "bob@security.com",
  "department" : "Engineering",
  "birthdate"  : "1975-12-31"
}
</code></pre>

####Token Info Endpoint
The Token info endpoint returns the scopes associated with the authenticated user. Token info endpoint in OpenIAM,
`http://<hostname>/idp/oauth2/token/info`
Sample request and response for token info endpoint is shown below, (the request requries access token received from token endpoint)

<pre><code>
http://3.204.111.72:8080/idp/oauth2/token/info?token=WHvc82YW20r0-lAZoXLJEpGFM5qkhnX6NsoW31QJEJmxDq8v5hcLTXdgG
</code></pre>

<pre><code>
{
   "expired":false,
   "client_id":"5B9AC3C5AD3F4F4EB45DFF6720DB1959",
   "user_id":"834cefc858433cfd01587634d6230291",
   "access_token":"WHvc82YW20r0-lAZoXLJEpGFM5qkhnX6NsoW31QJEJmxDq8v5hcLTXdgG",
   "expires_in":18000,
   "expires_on":1485342100619,
   "scopes":[
      {
         "scopeId":"4028150c58300904015830d9c7af000f",
         "name":"Default - /oauth-example"
      },
      {
         "scopeId":"834cefc858433cfd01587632abbb028f",
         "name":"Default - /geservicepage"
      },
      {
         "scopeId":"OAUTH_given_name",
         "name":"given_name"
      },
      {
         "scopeId":"OAUTH_name",
         "name":"name"
      },
      {
         "scopeId":"OAUTH_nickname",
         "name":"nickname"
      }
   ]
}
</code></pre>
### Client Registration

Before initiating the protocol, the client registers with the authorization server which ypically involve end-user interaction with an HTML registration form. that can be access using the below URL,

`http://<hostname>/webconsole/menu/AM_PROV_NEW`

The client registration consists of,

* client id - The unique identifier  issued to the client application.
* client secret - The client application is also typically issued a secret (password) to authenticate itself at the token endpoint.
* redirect url - The server requires a redirection URI to send the browser back to the client application once the end-user has given their consent.

Below screen shows the list of authentication provider types available in OpenIAM,

<img src="img\Client_Registration_1.jpg"/>

#### Client Information
The following snap shot shows the information required for client registration,

<img src="img\Client_Registration_2.jpg"/>

#### Client Identity

After Client information is provided Client ID and Secret for the client will be generated.

<img src="img\Client_Registration_3.jpg"/>

### Access management using OAuth

To summarize, the diagram below can be used as the high level steps to achieve oAuth workflow,
<img src="img\Access_Management_Using_OAuth.jpg"/>

### OAuth Client code for Access management

Below steps shows how to perform Authentication and Authorization for a user with OpenIAM access management using OAuth protocol. For this example Spring based web application is used as a Client using the implicit grant type.

1) Filter the URLs (patterns) that needs to be access protected - Here, the URLs that need to be access protected should be identified and registered for e.g.

<pre><code>
   public FilterRegistrationBean myFilter() {
        final FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(oAuthFilter());
        registration.addUrlPatterns("/secured-resource");
        return registration;
   }
</code></pre>

2) While accessing the protected resource (/secured-resource). The request will be filtered and it will redirected to login page using the **authorization endpoint** for authentication. The code below is for implicit grant type and uses the **authorization endpoint**, represented by authorizeURI of OpenIAM for the user's authentication and getting the access token

<pre><code>
   final StringBuilder sb = new StringBuilder(authorizeURI)
                                        .append("?response_type=id_token")
                                        .append("&amp;client_id=").append(clientId).append("&amp;redirect_uri=").append(encodedHandlerURI)
                                        .append("&amp;state=").append(encodedURI)
                                        .append("&amp;scope=openid");
   /* Example request for authorization endpoint
   http://3.204.111.72:8080/idp/oauth2/authorize
                ?response_type=id_token
                &amp;client_id=E2CB541789C74FC0B7719D6FC719159F
                &amp;redirect_uri=http://localhost:8082/oauthhandler
                &amp;state=http://localhost:8082/oauth-example
                &amp;scope=openid
   */
   response.sendRedirect(sb.toString());
</code></pre>

3) After successful Authentication of the user, Client receives access token directly from the access token from OpenIAM server.

Note:  
- In case of Authorization code grant flow, client first receives authorization code and uses that to get the access token from the **token endpoint**.  
- In case of Resource owner grant flow, client uses the user credentials to get the access token from **token endpoint**  
- In case of Implicit grant flow, client does not interact with the **token endpoint** for access token.

4) After receiving access token, the token information is read to determined if the user is authorized to access the resource, as shown in the code below. The token's information is read using OpenIAM's **tokeninfo endpoint**, represented as tokenInfoEndpoint, in the code below (Apache Oltu library is used here to decompose the response information)

<pre><code>
   final String url = new StringBuilder(tokenInfoEndpoint).append("?token=").append(token.getAccessToken()).toString();
   final OAuthTokenInfoResponse tokenInfo = restTemplate.getForEntity(url, OAuthTokenInfoResponse.class).getBody();
   if(CollectionUtils.isEmpty(tokenInfo.getAuthorizedScopes()) ||
        tokenInfo.getAuthorizedScopes().stream().filter(e -> e.getName().contains(request.getRequestURI())).count() == 0) {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        return;
   }
   chain.doFilter(arg0, arg1);
/*

        Example request for token info endpoint
        http://3.204.111.72:8080/idp/oauth2/token/info
                        ?token=WHvc82YW20r0-lAZoXLJEpGFM5qkhnX6NsoW31QJEJmxDq8v5hcLTXdgG
*/
</code></pre>

5) In some cases if the client application wants to get the logged-in user's information (Eg., name, email etc) it can do that using the code below. Here, OpenIAM's **userinfo endpoint**, represented as userInfoEndpoint, is used to get the information about the logged-in user.

<pre><code>
   public String getUserInfo(String userInfoEndpoint, String accessToken) throws Exception {
        URL url = new URL(userInfoEndpoint+"?token="+accessToken);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
        }
        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
        StringBuilder message = new StringBuilder();
        String output;
        while ((output = br.readLine()) != null) {
                message.append(output);
        }
        conn.disconnect();
        return message.toString();
   }
/*
        Example request for userinfo endpoint
        http://3.204.111.72:8080/idp/oauth2/userinfo
                ?token=WHvc82YW20r0-lAZoXLJEpGFM5qkhnX6NsoW31QJEJmxDq8v5hcLTXdgG
*/
</code></pre>


Identity Management
-------------------
Identity Management part allows to perform the following,
* Create user
* Create Resource
* Create Role
* Associate Resource to Role
* Associate Role to user

### Create User
Creates a new user in the system.
URL `http://<hostname>/webconsole/menu/NEW_USER`

<img src="img\CreateUser.jpg"/>

### Create Resource
Creates a new resource in the system.
URL `http://<hostname>/webconsole/menu/CREATE_RESOURCE_MENU`

<img src="img\CreateResource.jpg"/>

### Create Role
Create a role in the system.
URL `http://<hostname>/webconsole/menu/NEW_ROLE`

<img src="img\CreateRole.jpg"/>

### Assign Resource to Role
After Creating Role entitle role to a resource.
<img src="img\AssignResourcetoRole.jpg"/>

### Assign Role to user
After creating user entitle user to a role.
<img src="img\AssignRoletoUser.jpg"/>


SOUP Classified Items
---------------------

Detailed descriptions of the SOUP Items in the referenced SOUP document.


Software Safety Classification
------------------------------
TBD
[root@openam SDD]#