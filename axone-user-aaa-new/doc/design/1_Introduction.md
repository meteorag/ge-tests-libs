
Introduction
============

## Purpose

This document provides the architecture definition for Identity and Access Management provided by the Axone platform and a description of the architectural design employed to satisfy the application requirements.

The objectives of this document are to:

* define the architecture of the software component,
* specify the software safety classification of those software items,
* identify the interfaces exposed and their functionalities,
* identify software items that are SOUP, ensure functional and
  performance requirements necessary for their intended use are
  captured, and ensure hardware and software necessary to support
  their operation are specified,
* explain the design of the segregation of class C software items and
  the strategy to ensure the segregation is effective,
* and (if employed) capture the rationale for lowering the software
  safety classification of software items by use of segregation.

## Scope
The scope of this document is to,

* describe the architecture of Identity and Access Management (OAuth implementation), its features and functionalities. 
* how applications using Axone can use Identity and Access Management to enable Authentication and Authorization capabilities. 

This software component enables the creation of medical products that provide clinical features, but does not itself include any software that takes measurements, analyze data, or provide the end-user the clinical data.

## References

TODO

## Definitions, Acronyms, Abbreviations

The following are the definitions, acronyms and abbreviations used in
this document:

SOUP - Software of unknown pedigree  
SDLC - Software Development Life Cycle 