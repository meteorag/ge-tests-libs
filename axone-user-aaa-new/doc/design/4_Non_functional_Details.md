# Platform Constraints #
* Identity and Access Management software component is made available as a docker image.
* It requires following platform,

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:bold;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg .tg-yw4l{vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-yw4l">Platform</th>
    <th class="tg-yw4l">Guest OS</th>
    <th class="tg-yw4l">Version</th>
  </tr>
  <tr>
    <td class="tg-yw4l">Intel x64</td>
    <td class="tg-yw4l">Cent OS</td>
    <td class="tg-yw4l"> 7 </td>
  </tr>
</table>

## Accounting
All the events in OpenIAM are logged and the same can be viewed using the log viewer in the link `http://<hostname>/webconsole/menu/SYNCLOG`. Below screen shows the log viewer where you can filter the logs based on event, date and status.

<img src="img\Auditing.jpg"/>

Also individual log record can be viewed to get the detailed information of that event,
<img src="img\Detailed_Log_Event.jpg"/>

## Performance CTQs[TBD]
Out of scope for AMS V1 release. No performance CTQs measured for AMS V1 release.
