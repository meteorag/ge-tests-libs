Feature: Docker Operational Behavior tests
  As a Product solution owner
  I need to run docker lifecycle commands to test my containerized service
  So that I can rely on using docker to manage my containerized service

Background:
  Given docker data file is available
  |features/support/docker_images_data.yaml|

@integration
Scenario Outline: Docker pull
  Given the DTR is accessible
  |https://registry.gear.ge.com|
  And the docker image "<img_name>" is available in DTR
  And the docker image "<img_name>" is not available on my system
  When the docker image "<img_name>" is pulled
  Then the docker image "<img_name>" is pulled successfully from DTR
  Examples:
  |img_name|
  |registry.gear.ge.com/heal-lcs/openiam-mariadb|
  |registry.gear.ge.com/heal-lcs/openiam-elasticsearch|
  |registry.gear.ge.com/heal-lcs/openiam-rabbitmq|
  |registry.gear.ge.com/heal-lcs/openiam-idm|
  |registry.gear.ge.com/heal-lcs/openiam-esb|
  |registry.gear.ge.com/heal-lcs/openiam-redis|
  |registry.gear.ge.com/heal-lcs/openiam-ui|

@build
Scenario Outline: Docker run
  Given the docker image "<img_name>" is available on my system
  When the  container of docker image "<img_name>"  is run
  Then  the container successfully runs
  Examples:
  |img_name|
  |registry.gear.ge.com/heal-lcs/openiam-mariadb|
  |registry.gear.ge.com/heal-lcs/openiam-elasticsearch|
  |registry.gear.ge.com/heal-lcs/openiam-rabbitmq|
  |registry.gear.ge.com/heal-lcs/openiam-idm|
  |registry.gear.ge.com/heal-lcs/openiam-esb|
  |registry.gear.ge.com/heal-lcs/openiam-redis|
  |registry.gear.ge.com/heal-lcs/openiam-ui|

@build
Scenario Outline: Docker Stop
  Given the docker image "<img_name>" has container running on my system
  When the container is stopped
  Then the container stops all its processes
  And exits with a successful exit code "<exit_code_value>"
  Examples:
  |img_name|exit_code_value|
  |registry.gear.ge.com/heal-lcs/openiam-mariadb|143|
  |registry.gear.ge.com/heal-lcs/openiam-elasticsearch|143|
  |registry.gear.ge.com/heal-lcs/openiam-rabbitmq|143|
  |registry.gear.ge.com/heal-lcs/openiam-idm|143|
  |registry.gear.ge.com/heal-lcs/openiam-esb|143|
  |registry.gear.ge.com/heal-lcs/openiam-redis|143|
  |registry.gear.ge.com/heal-lcs/openiam-ui|143|

@build
Scenario Outline: Docker Kill
  Given the docker image "<img_name>" has container running on my system
  When the container is killed
  Then the container is stopped forcefully
  Examples:
  |img_name|
  |registry.gear.ge.com/heal-lcs/openiam-mariadb|
  |registry.gear.ge.com/heal-lcs/openiam-elasticsearch|
  |registry.gear.ge.com/heal-lcs/openiam-rabbitmq|
  |registry.gear.ge.com/heal-lcs/openiam-idm|
  |registry.gear.ge.com/heal-lcs/openiam-esb|
  |registry.gear.ge.com/heal-lcs/openiam-redis|
  |registry.gear.ge.com/heal-lcs/openiam-ui|

@build
Scenario Outline: Docker start after killing its container
  Given the container of docker image "<img_name>" is shutdown with kill command
  When the container is started again
  Then the container is restarted successfully
  Examples:
  |img_name|
  |registry.gear.ge.com/heal-lcs/openiam-mariadb|
  |registry.gear.ge.com/heal-lcs/openiam-elasticsearch|
  |registry.gear.ge.com/heal-lcs/openiam-rabbitmq|
  |registry.gear.ge.com/heal-lcs/openiam-idm|
  |registry.gear.ge.com/heal-lcs/openiam-esb|
  |registry.gear.ge.com/heal-lcs/openiam-redis|
  |registry.gear.ge.com/heal-lcs/openiam-ui|

@build
Scenario Outline: Docker start after stopping its container
  Given the container of docker image "<img_name>" is shutdown with stop command
  When the container is started again
  Then the container is restarted successfully
  Examples:
  |img_name|
  |registry.gear.ge.com/heal-lcs/openiam-mariadb|
  |registry.gear.ge.com/heal-lcs/openiam-elasticsearch|
  |registry.gear.ge.com/heal-lcs/openiam-rabbitmq|
  |registry.gear.ge.com/heal-lcs/openiam-idm|
  |registry.gear.ge.com/heal-lcs/openiam-esb|
  |registry.gear.ge.com/heal-lcs/openiam-redis|
  |registry.gear.ge.com/heal-lcs/openiam-ui|


@build
Scenario Outline: Docker restart
  Given the docker image "<img_name>" has container running on my system
  When the command to restart the container is executed
  Then the container shuts down with a successful exit code "<exit_code_value>"
  And  the container successfully runs
  Examples:
  |img_name|exit_code_value|
  |registry.gear.ge.com/heal-lcs/openiam-mariadb|0|
  |registry.gear.ge.com/heal-lcs/openiam-elasticsearch|0|
  |registry.gear.ge.com/heal-lcs/openiam-rabbitmq|0|
  |registry.gear.ge.com/heal-lcs/openiam-idm|0|
  |registry.gear.ge.com/heal-lcs/openiam-esb|0|
  |registry.gear.ge.com/heal-lcs/openiam-redis|0|
  |registry.gear.ge.com/heal-lcs/openiam-ui|0|

@build
Scenario Outline: Docker remove container after stopping its container
  Given the container of docker image "<img_name>" is shutdown with stop command
  When the command to remove the container is executed
  Then the container is successfully removed
  Examples:
  |img_name|
  |registry.gear.ge.com/heal-lcs/openiam-mariadb|
  |registry.gear.ge.com/heal-lcs/openiam-elasticsearch|
  |registry.gear.ge.com/heal-lcs/openiam-rabbitmq|
  |registry.gear.ge.com/heal-lcs/openiam-idm|
  |registry.gear.ge.com/heal-lcs/openiam-esb|
  |registry.gear.ge.com/heal-lcs/openiam-redis|
  |registry.gear.ge.com/heal-lcs/openiam-ui|

@build
Scenario Outline: Docker remove container after killing its container
  Given the container of docker image "<img_name>" is shutdown with kill command
  When the command to remove the container is executed
  Then the container is successfully removed
  Examples:
  |img_name|
  |registry.gear.ge.com/heal-lcs/openiam-mariadb|
  |registry.gear.ge.com/heal-lcs/openiam-elasticsearch|
  |registry.gear.ge.com/heal-lcs/openiam-rabbitmq|
  |registry.gear.ge.com/heal-lcs/openiam-idm|
  |registry.gear.ge.com/heal-lcs/openiam-esb|
  |registry.gear.ge.com/heal-lcs/openiam-redis|
  |registry.gear.ge.com/heal-lcs/openiam-ui|

@integration
Scenario Outline: Docker remove image
  Given the docker image "<img_name>" has no running containers
  When the command to remove  docker image "<img_name>" is executed
  Then  the docker image "<img_name>" is successfully removed
  Examples:
  |img_name|
  |registry.gear.ge.com/heal-lcs/openiam-mariadb|
  |registry.gear.ge.com/heal-lcs/openiam-elasticsearch|
  |registry.gear.ge.com/heal-lcs/openiam-rabbitmq|
  |registry.gear.ge.com/heal-lcs/openiam-idm|
  |registry.gear.ge.com/heal-lcs/openiam-esb|
  |registry.gear.ge.com/heal-lcs/openiam-redis|
  |registry.gear.ge.com/heal-lcs/openiam-ui|
