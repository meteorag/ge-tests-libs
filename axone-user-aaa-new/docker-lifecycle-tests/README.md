This file provides the instructions to setup and run the docker operational behavior tests in both developer and team city environments

## Setup

- Below software must be installed

  - Ruby
  - Bundler
  - Gradle

- Below properties must be set in gradle.properties to access artifactory.

  - artifactory_username

  - artifactory_password

- Below environment variable(s) are set.
  - no_proxy = devcloud.swcoe.ge.com

## Developer Environment:
1.Update the epel release and install ruby-devel and its dependent rpms
2.Download axone_ruby_config file from devscripts folder
3.Run the command ./axone_ruby_config --help and follow the help instructions to run the script.


## Team City Environment:
1.Please ensure the Setup for Ruby Bundler with Artifactory is configured.

## Run Tests:
 To execute all BDDs

```
gradle docker_lifecycle_tests_all
```

 To execute tests to be run during build

```
gradle docker_lifecycle_test_during_build
```

 To execute tests to be run during integration

```
gradle docker_lifecycle_test_during_integration
```
