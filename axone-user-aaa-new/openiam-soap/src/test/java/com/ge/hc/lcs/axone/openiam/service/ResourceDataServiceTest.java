package com.ge.hc.lcs.axone.openiam.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openiam.idm.srvc.res.service.Resource;
import org.openiam.idm.srvc.res.service.ResourceDataWebService;
import org.openiam.idm.srvc.res.service.ResourceSearchBean;

import com.ge.hc.lcs.axone.openiam.constants.Constants;
import com.ge.hc.lcs.axone.openiam.factory.ServiceFactory;

public class ResourceDataServiceTest {

	private static ResourceDataService resourceDataService;
	private static RoleService roleService;
	private String resource = "http://axone.soaptest.com";
	// OpenIAM SOAP service
	private static ResourceDataWebService resourceDataWebService = null;

	@BeforeClass
	public static void setUp() {
		try {
			ServiceFactory.init(System.getProperty("hostname"), System.getProperty("login"),
					System.getProperty("password"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			resourceDataService = ResourceDataService.getInstance();
			roleService = RoleService.getInstance();
			resourceDataWebService = ServiceFactory.getResourceDataWebService();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("SOAP Service creation failed");
		}
	}

	@Test
	public void getAllResourcesTest() {
		// Getting values from wrapper API
		List<Resource> resources = resourceDataService.getAllResources();
		Assert.assertFalse(resources.isEmpty());
		// Invoking the OpenIAM SOAP API directly to fetch all resources
		List<Resource> verifyResources = getResources();
		// Asserting the value fetched by invoking the SOAP API with the value fetched
		// using wrapper function
		List<String> resourceNames = new ArrayList<String>();
		resources.forEach(resource -> resourceNames.add(resource.getId()));
		List<String> verifyResourceNames = new ArrayList<String>();
		verifyResources.forEach(resource -> verifyResourceNames.add(resource.getId()));
		Assert.assertEquals(resourceNames, verifyResourceNames);
	}

	@Test
	public void isResourceExistsTest() {

		// Precondition
		Assert.assertTrue(resourceDataService.createURLResource(resource));
		// Invoking wrapper API
		Assert.assertTrue(resourceDataService.isResourceExists(resource));
		// Invoking the OpenIAM SOAP API directly to fetch all resources
		Assert.assertTrue(resourceDataService.getAllResources().stream().anyMatch(r -> r.getName().equals(resource)));
		// Clean up
		Assert.assertTrue(resourceDataService.deleteResource(resource));
		// Negative test
		Assert.assertFalse(resourceDataService.isResourceExists("invalid_Resource"));
	}

	@Test
	public void getResourceIdTest() {
		// Precondition
		Assert.assertTrue(resourceDataService.createURLResource(resource));
		// Invoking wrapper API
		String resourceId = resourceDataService.getResourceId(resource);
		Assert.assertNotNull(resourceId);
		// Asserting the value fetched by invoking the SOAP API with the value fetched
		// using wrapper function
		Assert.assertEquals(resourceId, getResourceId(resource));
		// Clean up
		Assert.assertTrue(resourceDataService.deleteResource(resource));
		// Negative test
		Assert.assertNull(resourceDataService.getResourceId("invalid_Resource"));
	}

	@Test
	public void deleteResourceTest() {
		// Precondition
		Assert.assertTrue(resourceDataService.createURLResource(resource));
		// Invoking wrapper API
		Assert.assertTrue(resourceDataService.deleteResource(resource));
		// Invoking the OpenIAM SOAP API directly to verify if its deleted
		Assert.assertNull(getResourceId(resource));
		// Negative test
		Assert.assertFalse(resourceDataService.deleteResource("invalid_Resource"));
	}

	@Test
	public void createURLResourceTest() {
		Assert.assertTrue(resourceDataService.createURLResource(resource));
		// Invoking the OpenIAM SOAP API directly to verify if its deleted
		Assert.assertNotNull(getResourceId(resource));
		// Negative test
		Assert.assertFalse(resourceDataService.createURLResource(resource));
		// Clean up
		Assert.assertTrue(resourceDataService.deleteResource(resource));
	}

	@Test
	public void addRoleToResouceTest() {
		// Precondition
		String role = "testrole";
		Assert.assertTrue(roleService.addRole(role));
		Assert.assertTrue(resourceDataService.createURLResource(resource));
		// Invoking wrapper API
		boolean result = false;
		try {
			result = resourceDataService.addRoleToResouce(role, resource);
			Assert.assertTrue(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(result);
		}
		// Clean up
		Assert.assertTrue(roleService.deleteRole(role));
		Assert.assertTrue(resourceDataService.deleteResource(resource));
		// Negative test
		result = true;
		try {
			result = resourceDataService.addRoleToResouce("invalid_role", "invalid_resource");
			Assert.assertFalse(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertFalse(result);
		}
	}

	private List<org.openiam.idm.srvc.res.service.Resource> getResources() {
		ResourceSearchBean resourceSearchBean = new ResourceSearchBean();
		resourceSearchBean.setDeepCopy(true);
		return resourceDataWebService.findBeans(resourceSearchBean, null, Constants.RESOURCE_FROM,
				Constants.RESOURCE_TO);
	}

	private String getResourceId(final String resourceName) {
		// Invoking the OpenIAM SOAP API directly to fetch the value
		Resource resource = getResources().stream().filter(r -> r.getName().equals(resourceName)).findFirst()
				.orElse(null);
		if (resource == null) {
			return null;
		} else {
			return resource.getId();
		}
	}
}
