package com.ge.hc.lcs.axone.openiam.service;

import java.util.Collections;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openiam.idm.srvc.user.service.UserDataService;

import com.ge.hc.lcs.axone.openiam.factory.ServiceFactory;

public class UserServiceTest {

	private static RoleService roleService;
	private static UserService userService;
	// OpenIAM SOAP service
	private static UserDataService userDataService = null;

	@BeforeClass
	public static void setUp() {
		try {
			ServiceFactory.init(System.getProperty("hostname"), System.getProperty("login"),
					System.getProperty("password"));
			ServiceFactory.init("","","");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			userService = UserService.getInstance();
			roleService = RoleService.getInstance();
			userDataService = ServiceFactory.getUserDataService();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("SOAP Service creation failed");
		}

	}

	@Test
	public void addUserTest() {
		String user = "test.user";
		// Invoking the Wrapper API
		Assert.assertTrue(userService.addUser("test", "user", user));
		// Asserting the userID fetched by invoking the SOAP API with actual userID
		Assert.assertEquals(user,
				userDataService
						.getUserByPrincipal(user, "0",
								Collections.singletonList(org.openiam.idm.srvc.user.dto.UserCollection.ALL))
						.getPrincipalList().get(0).getLogin());
		// Negative test
		Assert.assertFalse(userService.addUser("test", "user", user));
		// Clean up
		Assert.assertTrue(userService.deleteUserByPrincipal(user));
	}

	@Test
	public void addUserTestWithEmail() {
		String user = "test.user";
		String email = "test.user@ge.com";
		// Invoking the Wrapper API
		Assert.assertTrue(userService.addUser("test", "user", user, email));
		// Asserting the userID fetched by invoking the SOAP API with actual userID
		Assert.assertEquals(user,
				userDataService
						.getUserByPrincipal(user, "0",
								Collections.singletonList(org.openiam.idm.srvc.user.dto.UserCollection.ALL))
						.getPrincipalList().get(0).getLogin());
		// Asserting the emailID fetched by invoking the SOAP API with actual emailID
		Assert.assertEquals(email,
				userDataService
						.getUserByPrincipal(user, "0",
								Collections.singletonList(org.openiam.idm.srvc.user.dto.UserCollection.ALL))
						.getEmailAddresses().get(0).getEmailAddress());

		// Negative test
		Assert.assertFalse(userService.addUser("test", "user", user, email));
		// Clean up
		Assert.assertTrue(userService.deleteUserByPrincipal(user));
	}

	@Test
	public void addUserWithRoleTest() {
		String user = "test.user";
		String role = "testrole";
		// Precondition
		Assert.assertTrue(roleService.addRole(role));
		// Invoking the Wrapper API
		boolean result = false;
		try {
			result = userService.addUserWithRole("test", "user", user, role);
			Assert.assertTrue(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(result);
		}
		// Asserting the loginID fetched by invoking the SOAP API with actual userID
		Assert.assertEquals(user,
				userDataService
						.getUserByPrincipal(user, "0",
								Collections.singletonList(org.openiam.idm.srvc.user.dto.UserCollection.ALL))
						.getPrincipalList().get(0).getLogin());
		result = false;
		try {
			result = roleService.getRolesForUser(user).contains(role);
			Assert.assertTrue(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(result);
		}
		// Negative test
		result = true;
		try {
			result = userService.addUserWithRole("test", "user", user, role);
			Assert.assertFalse(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertFalse(result);
		}
		// Clean up
		Assert.assertTrue(userService.deleteUserByPrincipal(user));
		Assert.assertTrue(roleService.deleteRole(role));
	}

	@Test
	public void getUserIdTest() {
		String user = "test.user";
		// Precondition
		Assert.assertTrue(userService.addUser("test", "user", user));
		// Invoking the Wrapper API to fetch the value
		String userID = userService.getUserId(user);
		Assert.assertNotNull(userID);
		// Invoking the OpenIAM SOAP API directly to fetch the value
		String actualUserID = userDataService.getUserByPrincipal(user, "0",
				Collections.singletonList(org.openiam.idm.srvc.user.dto.UserCollection.ALL)).getId();
		// Asserting the userID fetched by invoking the SOAP API with actual userID
		Assert.assertEquals(userID, actualUserID);
		// Clean up
		Assert.assertTrue(userService.deleteUserByPrincipal(user));
	}

	@Test
	public void deleteUserTest() {
		String user = "test.user";
		// Precondition
		Assert.assertTrue(userService.addUser("test", "user", user));
		// Invoking the Wrapper API
		Assert.assertTrue(userService.deleteUserByPrincipal(user));
		// Invoking the SOAP API directly to verify user is deleted
		Assert.assertNull(userDataService.getUserByPrincipal(user, "0",
				Collections.singletonList(org.openiam.idm.srvc.user.dto.UserCollection.ALL)));
	}

	@Test
	public void resetPasswordTest() {
		String user = "test.user";
		// Precondition
		Assert.assertTrue(userService.addUser("test", "user", user));
		// Invoking the Wrapper API
		Assert.assertTrue(userService.resetPassword(user, "Password$52"));
		// Clean up
		Assert.assertTrue(userService.deleteUserByPrincipal(user));
	}
}
