package com.ge.hc.lcs.axone.openiam.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openiam.idm.srvc.role.dto.Role;
import org.openiam.idm.srvc.role.service.RoleDataService;
import org.openiam.idm.srvc.role.service.RoleSearchBean;

import com.ge.hc.lcs.axone.openiam.constants.Constants;
import com.ge.hc.lcs.axone.openiam.factory.ServiceFactory;

public class RoleServiceTest {

	private static RoleService roleService;
	private static UserService userService;
	// OpenIAM SOAP service
	private static RoleDataService roleDataService;

	@BeforeClass
	public static void setUp() {
		try {
			ServiceFactory.init(System.getProperty("hostname"), System.getProperty("login"),
					System.getProperty("password"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			roleService = RoleService.getInstance();
			userService = UserService.getInstance();
			roleDataService = ServiceFactory.getRoleDataService();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("SOAP Service creation failed");
		}
	}

	@Test
	public void getAllRolesTest() {
		// Getting values from wrapper API
		List<String> roles = roleService.getAllRoles();
		// By default OpenIAM will have default roles
		Assert.assertFalse(roles.isEmpty());
		// Invoking the OpenIAM SOAP API directly to fetch all roles
		List<String> verifyRoles = new ArrayList<String>();
		getRoles().forEach(role -> verifyRoles.add(role.getName()));
		// Asserting the value fetched by invoking the SOAP API with the value fetched
		// using wrapper function
		Assert.assertEquals(roles, verifyRoles);
	}

	@Test
	public void addRoleTest() {
		String role = "testrole";
		Assert.assertTrue(roleService.addRole(role));
		// Invoking the OpenIAM SOAP API directly to verify if role is added
		Assert.assertNotNull(getRoleId(role));
		// Negative test
		Assert.assertFalse(roleService.addRole(role));
		// Clean up
		Assert.assertTrue(roleService.deleteRole(role));
	}

	@Test
	public void getRoleIdTest() {
		String role = "testrole";
		// Precondition
		Assert.assertTrue(roleService.addRole(role));
		// Asserting the value fetched by invoking the SOAP API with the value fetched
		// using wrapper function
		Assert.assertEquals(roleService.getRoleId(role), getRoleId(role));
		// Clean up
		Assert.assertTrue(roleService.deleteRole(role));
		// Negative test
		Assert.assertNull(roleService.getRoleId("invalid_role"));
	}

	@Test
	public void deleteRoleTest() {
		String role = "testrole";
		// Precondition
		Assert.assertTrue(roleService.addRole(role));
		// Invoking wrapper API
		Assert.assertTrue(roleService.deleteRole(role));
		// Invoking the OpenIAM SOAP API directly to verify if its deleted
		Assert.assertNull(getRoleId(role));
		// Negative test
		Assert.assertFalse(roleService.deleteRole("invalid_role"));
	}

	@Test
	public void addRoleToUserTest() {
		String role = "testrole";
		String user = "test.user";
		// Precondition
		Assert.assertTrue(userService.addUser("test", "user", user));
		Assert.assertTrue(roleService.addRole(role));
		// Invoking wrapper API
		boolean result = false;
		try {
			result = roleService.addRoleToUser(user, role);
			Assert.assertTrue(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(result);
		}
		// Verify the entitlement
		result = false;
		try {
			result = roleService.getRolesForUser(user).contains(role);
			Assert.assertTrue(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(result);
		}
		// Clean up
		Assert.assertTrue(userService.deleteUserByPrincipal(user));
		Assert.assertTrue(roleService.deleteRole(role));
		// Negative test
		result = true;
		try {
			result = roleService.addRoleToUser("invalid.user", "invalid_role");
			Assert.assertFalse(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertFalse(result);
		}
	}

	@Test
	public void getRolesForUserTest() {
		String role = "testrole";
		String user = "test.user";
		// Precondition
		Assert.assertTrue(userService.addUser("new_user", "for_role", user));
		Assert.assertTrue(roleService.addRole(role));
		boolean result = false;
		try {
			result = roleService.addRoleToUser(user, role);
			Assert.assertTrue(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(result);
		}
		// Invoking the Wrapper API to fetch the value
		List<String> roles = null;
		result = false;
		try {
			roles = roleService.getRolesForUser(user);
			result = roles.contains(role);
			Assert.assertTrue(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(result);
		}
		// Invoking the OpenIAM SOAP API directly to fetch the value
		String userID = userService.getUserId(user);
		RoleSearchBean roleSearchBean = new RoleSearchBean();
		roleSearchBean.setDeepCopy(true);
		if (userID != null) {
			roleSearchBean.getUserIdSet().add(0, userID);
		}
		List<String> verifyRoles = new ArrayList<String>();
		roleDataService.findBeans(roleSearchBean, null, Constants.ROLE_FROM, Constants.ROLE_TO)
				.forEach(r -> verifyRoles.add(r.getName()));
		// Asserting the value fetched by invoking the SOAP API with the value fetched
		// using wrapper function
		Assert.assertEquals(roles, verifyRoles);
		// Clean up
		Assert.assertTrue(userService.deleteUserByPrincipal(user));
		Assert.assertTrue(roleService.deleteRole(role));
		// Negative test
		roles = null;
		try {
			roles = roleService.getRolesForUser("invalid.user");
			Assert.assertNull(roles);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertNull(roles);
		}
	}

	private String getRoleId(final String roleName) {
		// Invoking the OpenIAM SOAP API directly to fetch the value
		Role role = getRoles().stream().filter(r -> r.getName().equals(roleName)).findFirst().orElse(null);
		if (role == null) {
			return null;
		}
		return role.getId();
	}

	private List<Role> getRoles() {
		// Invoking the OpenIAM SOAP API directly to fetch the value
		RoleSearchBean roleSearchBean = new RoleSearchBean();
		roleSearchBean.setDeepCopy(true);
		return roleDataService.findBeans(roleSearchBean, null, 0, 100);
	}

}