package com.ge.hc.lcs.axone.openiam.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openiam.idm.srvc.am.service.AuthProvider;
import org.openiam.idm.srvc.am.service.AuthProviderAttribute;
import org.openiam.idm.srvc.am.service.AuthProviderSearchBean;
import org.openiam.idm.srvc.am.service.AuthProviderWebService;

import com.ge.hc.lcs.axone.openiam.enums.AuthProviderAttributeEnum;
import com.ge.hc.lcs.axone.openiam.factory.ServiceFactory;

public class AuthProviderServiceTest {

	private static AuthProviderService authProviderService;
	private static ResourceDataService resourceDataService;
	private static List<String> redirectURLs = new ArrayList<String>();
	private static List<String> scopes = new ArrayList<String>();
	private static List<String> invalidScopes = new ArrayList<String>();
	// OpenIAM SOAP service
	private static AuthProviderWebService authProviderWebService;

	@BeforeClass
	public static void setUp() {
		try {
			ServiceFactory.init(System.getProperty("hostname"), System.getProperty("login"),
					System.getProperty("password"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			authProviderService = AuthProviderService.getInstance();
			resourceDataService = ResourceDataService.getInstance();
			authProviderWebService = ServiceFactory.getAuthProviderWebService();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("SOAP Service creation failed");
		}
		resourceDataService.createURLResource("authservicetest");
		redirectURLs.add("http://testredirect.com");
		scopes.add("authservicetest"); // user provided scope
		scopes.add("name"); // oauth default scope
		invalidScopes.add("invalid_scope");
	}

	@Test
	public void createImplicitAuthProviderTest() {
		String name = "implcit_test";
		String clientId = "implicit_test_id";
		String clientSecret = "implicit_test_secret";
		// Invoking wrapper API
		boolean result = false;
		try {
			result = authProviderService.createImplicitAuthProvider(name, clientId, clientSecret, redirectURLs, scopes,
					true, "1");
			Assert.assertTrue(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(result);
		}
		// Invoking the OpenIAM SOAP API
		AuthProvider verifyAuthProvider = getAuthProvider(name);
		Assert.assertNotNull(verifyAuthProvider);
		// Asserting the name fetched by invoking the SOAP API with actual name
		Assert.assertEquals(name, verifyAuthProvider.getName());
		// Clean up
		Assert.assertTrue(authProviderService.deleteAuthProvider(name));
		// Negative test
		result = true;
		try {
			result = authProviderService.createImplicitAuthProvider(name, "implicit_test_id", "implicit_test_secret",
					redirectURLs, invalidScopes, true, "1");
			Assert.assertFalse(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertFalse(result);
		}
		// Asserting the Client credentials fetched by invoking the SOAP API with actual
		// Client credentials
		AuthProviderAttribute clientIDAttribute = verifyAuthProvider.getAttributes().stream()
				.filter(attribute -> attribute.getAttributeId().equals("OAuthClientID")).findFirst().orElse(null);
		Assert.assertEquals(clientId, clientIDAttribute.getValue());
		AuthProviderAttribute clientSecretAttribute = verifyAuthProvider.getAttributes().stream()
				.filter(attribute -> attribute.getAttributeId().equals("OAuthClientSecret")).findFirst().orElse(null);
		Assert.assertEquals(clientSecret, clientSecretAttribute.getValue());
	}

	@Test
	public void createAuthroizationCodeAuthProviderTest() {
		String name = "authcode_test";
		String clientId = "authcode_test_id";
		String clientSecret = "authcode_test_secret";
		// Invoking wrapper API
		boolean result = false;
		try {
			result = authProviderService.createAuthroizationCodeAuthProvider(name, clientId, clientSecret, redirectURLs,
					scopes, true, "1");
			Assert.assertTrue(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(result);
		}
		// Invoking the OpenIAM SOAP API
		AuthProvider verifyAuthProvider = getAuthProvider(name);
		Assert.assertNotNull(verifyAuthProvider);
		// Asserting the name fetched by invoking the SOAP API with actual name
		Assert.assertEquals(name, verifyAuthProvider.getName());
		// Clean up
		Assert.assertTrue(authProviderService.deleteAuthProvider(name));
		// Negative test
		result = true;
		try {
			result = authProviderService.createAuthroizationCodeAuthProvider(name, "authcode_test_id",
					"authcode_test_secret", redirectURLs, invalidScopes, true, "1");
			Assert.assertFalse(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertFalse(result);
		}
		// Asserting the Client credentials fetched by invoking the SOAP API with actual
		// Client credentials
		AuthProviderAttribute clientIDAttribute = verifyAuthProvider.getAttributes().stream()
				.filter(attribute -> attribute.getAttributeId().equals("OAuthClientID")).findFirst().orElse(null);
		Assert.assertEquals(clientId, clientIDAttribute.getValue());
		AuthProviderAttribute clientSecretAttribute = verifyAuthProvider.getAttributes().stream()
				.filter(attribute -> attribute.getAttributeId().equals("OAuthClientSecret")).findFirst().orElse(null);
		Assert.assertEquals(clientSecret, clientSecretAttribute.getValue());
	}

	@Test
	public void createResourceOwnerAuthProviderTest() {
		String name = "resource_owner_test";
		String clientId = "resource_owner_test_id";
		String clientSecret = "resource_owner_test_secret";
		// Invoking wrapper API
		boolean result = false;
		try {
			result = authProviderService.createResourceOwnerAuthProvider(name, clientId, clientSecret, redirectURLs,
					scopes, true, "1");
			Assert.assertTrue(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(result);
		}
		// Invoking the OpenIAM SOAP API
		AuthProvider verifyAuthProvider = getAuthProvider(name);
		Assert.assertNotNull(verifyAuthProvider);
		// Asserting the name fetched by invoking the SOAP API with actual name
		Assert.assertEquals(name, verifyAuthProvider.getName());
		// Clean up
		Assert.assertTrue(authProviderService.deleteAuthProvider(name));
		// Negative test
		result = true;
		try {
			result = authProviderService.createResourceOwnerAuthProvider(name, "resource_owner_test_id",
					"resource_owner_test_secret", redirectURLs, invalidScopes, true, "1");
			Assert.assertFalse(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertFalse(result);
		}
		// Asserting the Client credentials fetched by invoking the SOAP API with actual
		// Client credentials
		AuthProviderAttribute clientIDAttribute = verifyAuthProvider.getAttributes().stream()
				.filter(attribute -> attribute.getAttributeId().equals("OAuthClientID")).findFirst().orElse(null);
		Assert.assertEquals(clientId, clientIDAttribute.getValue());
		AuthProviderAttribute clientSecretAttribute = verifyAuthProvider.getAttributes().stream()
				.filter(attribute -> attribute.getAttributeId().equals("OAuthClientSecret")).findFirst().orElse(null);
		Assert.assertEquals(clientSecret, clientSecretAttribute.getValue());
	}

	@Test
	public void getAllAuthProvidersTest() {
		// Invoking wrapper API
		List<AuthProvider> authProviders = authProviderService.getAllAuthProviders();
		// By default OpenIAM will have default authentication providers
		Assert.assertFalse(authProviders.isEmpty());
		// Invoking the OpenIAM SOAP API
		List<AuthProvider> verifyAuthProviders = getAllAuthProviders();
		// Asserting the auth providers fetched by invoking the SOAP API with auth
		// providers fetched from wrapper API
		List<String> authProviderIds = new ArrayList<String>();
		authProviders.forEach(authProvider -> authProviderIds.add(authProvider.getId()));
		List<String> verifyAuthProviderIds = new ArrayList<String>();
		verifyAuthProviders.forEach(authProvider -> verifyAuthProviderIds.add(authProvider.getId()));
		Assert.assertEquals(authProviderIds, verifyAuthProviderIds);
	}

	@Test
	public void isAuthProviderExistsTest() {
		String name = "test_provider";
		// Precondition
		boolean result = false;
		try {
			result = authProviderService.createImplicitAuthProvider(name, "test_id", "test_secret", redirectURLs,
					scopes, true, "1");
			Assert.assertTrue(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(result);
		}
		// Invoking wrapper API
		Assert.assertTrue(authProviderService.isAuthProviderExists(name));
		// Invoking the OpenIAM SOAP API
		Assert.assertNotNull(getAuthProvider(name));
		// Clean up
		Assert.assertTrue(authProviderService.deleteAuthProvider(name));
		// Negative test
		Assert.assertFalse(authProviderService.isAuthProviderExists(name));
	}

	@Test
	public void getAuthenticationProviderIdTest() {
		String name = "test_provider";
		// Precondition
		boolean result = false;
		try {
			result = authProviderService.createImplicitAuthProvider(name, "test_id", "test_secret", redirectURLs,
					scopes, true, "1");
			Assert.assertTrue(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(result);
		}
		// Invoking wrapper API
		String providerId = authProviderService.getAuthenticationProviderId(name);
		Assert.assertNotNull(providerId);
		// Invoking the OpenIAM SOAP API
		String verifyProviderId = getAuthProvider(name).getId();
		// Asserting the Id fetched by invoking the SOAP API with Id fetched from
		// wrapper API
		Assert.assertEquals(providerId, verifyProviderId);
		// Clean up
		Assert.assertTrue(authProviderService.deleteAuthProvider(name));
		// Negative test
		Assert.assertNull(authProviderService.getAuthenticationProviderId(name));
	}

	@Test
	public void deleteAuthProviderTest() {
		String name = "test_provider";
		// Precondition
		boolean result = false;
		try {
			result = authProviderService.createImplicitAuthProvider(name, "test_id", "test_secret", redirectURLs,
					scopes, true, "1");
			Assert.assertTrue(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(result);
		}
		// Invoking wrapper API
		Assert.assertTrue(authProviderService.deleteAuthProvider(name));
		// Invoking the OpenIAM SOAP API
		Assert.assertNull(getAuthProvider(name));
		// Negative test
		Assert.assertFalse(authProviderService.deleteAuthProvider(name));
	}

	private AuthProvider getAuthProvider(String provider) {
		AuthProvider authProvider = getAllAuthProviders().stream().filter(p -> p.getName().equals(provider)).findFirst()
				.orElse(null);
		if (authProvider == null) {
			return null;
		}
		return authProvider;
	}

	private List<AuthProvider> getAllAuthProviders() {
		AuthProviderSearchBean bean = new AuthProviderSearchBean();
		bean.setDeepCopy(true);
		return authProviderWebService.findAuthProviderBeans(bean, 0, 100);
	}

	@Test
	public void getAuthProviderAttributeTest() {
		String name = "authcode_test";
		String clientId = "authcode_test_id";
		String clientSecret = "authcode_test_secret";
		// Invoking wrapper API
		boolean result = false;
		try {
			result = authProviderService.createAuthroizationCodeAuthProvider(name, clientId, clientSecret, redirectURLs,
					scopes, true, "1");
			Assert.assertTrue(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(result);
		}
		Assert.assertEquals("AUTHORIZATION_CODE", authProviderService
				.getAuthProviderAttribute(name, AuthProviderAttributeEnum.OAUTH_AUTHORIZATION_GRANT_FLOW).getValue());
		// Clean up
		Assert.assertTrue(authProviderService.deleteAuthProvider(name));
	}

	@AfterClass
	public static void tearDown() {
		Assert.assertTrue(resourceDataService.deleteResource("authservicetest"));
	}
}
