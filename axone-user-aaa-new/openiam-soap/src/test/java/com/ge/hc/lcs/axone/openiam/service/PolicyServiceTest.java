package com.ge.hc.lcs.axone.openiam.service;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ge.hc.lcs.axone.openiam.constants.Constants;
import com.ge.hc.lcs.axone.openiam.enums.PolicyAttributeEnum;
import com.ge.hc.lcs.axone.openiam.factory.ServiceFactory;

public class PolicyServiceTest {

	private static PolicyService policyService;

	@BeforeClass
	public static void setUp() {
		try {
			ServiceFactory.init(System.getProperty("hostname"), System.getProperty("login"),
					System.getProperty("password"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			policyService = PolicyService.getInstance();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("SOAP Service creation failed");
		}
	}

	@Test
	public void getPolicyAttributeForDefaultPasswdPolicyTest() {
		Assert.assertNotNull(policyService.getPolicyAttributeById(Constants.DEFAULT_PASSWD_POLICY_ID,
				PolicyAttributeEnum.INITIAL_PASSWORD));
		// Negative Test
		Assert.assertNull(policyService.getPolicyAttributeById("invalid_password_policy_id",
				PolicyAttributeEnum.INITIAL_PASSWORD));
	}

	@Test
	public void getPolicyAttributeForDefaultAuthenticationPolicyTest() {
		Assert.assertNotNull(policyService.getPolicyAttributeById(Constants.DEFAULT_AUTHN_POLICY_ID,
				PolicyAttributeEnum.FAILED_AUTH_COUNT));
		// Negative Test
		Assert.assertNull(
				policyService.getPolicyAttributeById("invalid_auth_policy_id", PolicyAttributeEnum.FAILED_AUTH_COUNT));
	}

	@Test
	public void getPolicyAttributeTest() {
		// Positive Test
		Assert.assertNotNull(
				policyService.getPolicyAttributeByName("Default Pswd Policy", PolicyAttributeEnum.INITIAL_PASSWORD));
		// Negative Test
		Assert.assertNull(policyService.getPolicyAttributeByName("Invalid", PolicyAttributeEnum.INITIAL_PASSWORD));
	}

	@Test
	public void updatePolicyAttributeByPolicyNameTest() {
		updatePolicyAttributeByPolicyName("Default Pswd Policy", PolicyAttributeEnum.NUMERIC_CHARS, "3", "4");
		updatePolicyAttributeByPolicyName("Default Pswd Policy", PolicyAttributeEnum.PWD_LEN, "10", "14");
		updatePolicyAttributeByPolicyName("Default Pswd Policy", PolicyAttributeEnum.PWD_EXP_WARN, "2", "4");
		updatePolicyAttributeByPolicyName("Default Pswd Policy", PolicyAttributeEnum.REJECT_CHARS_IN_PSWD, "##", null);
		updatePolicyAttributeByPolicyName("Default Pswd Policy", PolicyAttributeEnum.PWD_HIST_VER, "3", null);
		updatePolicyAttributeByPolicyName("Default Pswd Policy", PolicyAttributeEnum.PWD_EXPIRATION, "60", null);
		updatePolicyAttributeByPolicyName("Default Pswd Policy", PolicyAttributeEnum.CHNG_PSWD_ON_RESET, "false", null);
		updatePolicyAttributeByPolicyName("Default Pswd Policy", PolicyAttributeEnum.QUEST_ANSWER_CORRECT, "2", null);
	}

	private void updatePolicyAttributeByPolicyName(String policyName, PolicyAttributeEnum attributeEnum,
			final String value1, final String value2) {
		String defaultValue1 = policyService.getPolicyAttributeByName(policyName, attributeEnum).getValue1();
		String defaultValue2 = null;
		if (value2 != null)
			defaultValue2 = policyService.getPolicyAttributeByName(policyName, attributeEnum).getValue2();
		Assert.assertTrue(policyService.updatePolicyAttributeByPolicyName(policyName, attributeEnum, value1, value2));
		Assert.assertEquals(value1, policyService.getPolicyAttributeByName(policyName, attributeEnum).getValue1());
		if (value2 != null)
			Assert.assertEquals(value2, policyService.getPolicyAttributeByName(policyName, attributeEnum).getValue2());
		Assert.assertTrue(policyService.updatePolicyAttributeByPolicyName(policyName, attributeEnum, defaultValue1,
				defaultValue2));
	}

	@Test
	public void updatePolicyAttributeByPolicyIdTest() {
		updatePolicyAttributeByPolicyId("4000", PolicyAttributeEnum.NUMERIC_CHARS, "3", "4");
		updatePolicyAttributeByPolicyId("4000", PolicyAttributeEnum.PWD_LEN, "10", "14");
		updatePolicyAttributeByPolicyId("4000", PolicyAttributeEnum.PWD_EXP_WARN, "2", "4");
		updatePolicyAttributeByPolicyId("4000", PolicyAttributeEnum.REJECT_CHARS_IN_PSWD, "##", null);
		updatePolicyAttributeByPolicyId("4000", PolicyAttributeEnum.PWD_HIST_VER, "3", null);
		updatePolicyAttributeByPolicyId("4000", PolicyAttributeEnum.PWD_EXPIRATION, "60", null);
		updatePolicyAttributeByPolicyId("4000", PolicyAttributeEnum.CHNG_PSWD_ON_RESET, "false", null);
		updatePolicyAttributeByPolicyId("4000", PolicyAttributeEnum.QUEST_ANSWER_CORRECT, "2", null);
	}

	private void updatePolicyAttributeByPolicyId(String policyID, PolicyAttributeEnum attributeEnum,
			final String value1, final String value2) {
		String defaultValue1 = policyService.getPolicyAttributeById(policyID, attributeEnum).getValue1();
		String defaultValue2 = null;
		if (value2 != null)
			defaultValue2 = policyService.getPolicyAttributeById(policyID, attributeEnum).getValue2();
		Assert.assertTrue(policyService.updatePolicyAttributeByPolicyId(policyID, attributeEnum, value1, value2));
		Assert.assertEquals(value1, policyService.getPolicyAttributeById(policyID, attributeEnum).getValue1());
		if (value2 != null)
			Assert.assertEquals(value2, policyService.getPolicyAttributeById(policyID, attributeEnum).getValue2());
		Assert.assertTrue(
				policyService.updatePolicyAttributeByPolicyId(policyID, attributeEnum, defaultValue1, defaultValue2));
	}

}