package com.ge.hc.lcs.axone.openiam.service;

import org.apache.log4j.Logger;
import org.openiam.idm.srvc.policy.service.Policy;
import org.openiam.idm.srvc.policy.service.PolicyAttribute;
import org.openiam.idm.srvc.policy.service.PolicySearchBean;
import org.openiam.idm.srvc.policy.service.PolicyWebService;

import com.ge.hc.lcs.axone.openiam.constants.Constants;
import com.ge.hc.lcs.axone.openiam.enums.PolicyAttributeEnum;
import com.ge.hc.lcs.axone.openiam.factory.ServiceFactory;

/**
 * Contains the functions which invokes PolicyWebService OpenIAM API's
 *
 */
public class PolicyService {

	final static Logger LOGGER = Logger.getLogger(PolicyService.class);
	private PolicyWebService policyWebService;
	private static PolicyService policyService;

	private PolicyService() throws Exception {
		policyWebService = ServiceFactory.getPolicyWebService();
	}

	/**
	 * Returns the instance of PolicyService
	 * 
	 * @return
	 * @throws Exception
	 */
	public static PolicyService getInstance() throws Exception {
		if (policyService == null) {
			synchronized (PolicyService.class) {
				if (policyService == null) {
					policyService = new PolicyService();
				}
			}
		}
		return policyService;
	}

	/**
	 * Returns Policy Attribute information for the default policy
	 * 
	 * @param policyId
	 *            Id of the policy for which the attribute value to be fetched
	 * @param attributeName
	 *            Policy attribute to be retrieved
	 * @return PolicyAttribute
	 */
	public PolicyAttribute getPolicyAttributeById(final String policyId, final PolicyAttributeEnum attributeName) {
		Policy policy = policyWebService.getPolicy(policyId);
		if (policy == null) {
			return null;
		}
		return policy.getPolicyAttributes().stream()
				.filter(attribute -> attribute.getName().equals(attributeName.toString())).findFirst().orElse(null);
	}

	/**
	 * Returns Policy Attribute information for the policyName
	 * 
	 * @param policyName
	 *            Policy for which the attribute value to be fetched
	 * @param attributeName
	 *            Policy attribute
	 * @return
	 */
	public PolicyAttribute getPolicyAttributeByName(final String policyName, final PolicyAttributeEnum attributeName) {
		String policyID = getPolicyId(policyName);
		LOGGER.debug("Policy ID for " + policyName + ((policyID != null) ? " is " + policyID : " does not exist"));
		if (policyID != null) {
			return getPolicyAttributeById(policyID, attributeName);
		}
		return null;
	}

	private String getPolicyId(String policyName) {
		// from and number of beans to be fetched.
		return policyWebService.findBeans(new PolicySearchBean(), Constants.POLICY_FROM, Constants.POLICY_TO).stream()
				.filter(p -> p.getName().equals(policyName)).map(Policy::getId).findFirst().orElse(null);
	}

	/**
	 * Updates password policy using name. If the attribute does not have value2
	 * pass as null.
	 * 
	 * @param policyName
	 * @param attributeName
	 * @param value1
	 * @param value2
	 * @return
	 */
	public boolean updatePolicyAttributeByPolicyName(final String policyName, final PolicyAttributeEnum attributeName,
			final String value1, final String value2) {
		String policyID = getPolicyId(policyName);
		LOGGER.debug("Policy ID for " + policyName + ((policyID != null) ? " is " + policyID : " does not exist"));
		return updatePolicyAttributeByPolicyId(policyID, attributeName, value1, value2);
	}

	/**
	 * Updates password policy using policy ID. If the attribute does not have
	 * value2 pass as null.
	 * 
	 * @param policyID
	 * @param attributeName
	 * @param value1
	 * @param value2
	 * @return
	 */
	public boolean updatePolicyAttributeByPolicyId(final String policyID, final PolicyAttributeEnum attributeName,
			final String value1, final String value2) {
		if (policyID == null) {
			return false;
		}
		Policy policy = policyWebService.getPolicy(policyID);
		if (policy == null) {
			return false;
		}
		PolicyAttribute policyAttribute = policy.getPolicyAttributes().stream()
				.filter(attribute -> attribute.getName().equals(attributeName.toString())).findFirst().orElse(null);
		policyAttribute.setValue1(value1);
		if (value2 != null) {
			policyAttribute.setValue2(value2);
		}
		org.openiam.idm.srvc.policy.service.Response response = policyWebService.savePolicy(policy);
		if (response != null) {
			if (response.getStatus().toString().equalsIgnoreCase(Constants.SUCCESS)) {
				return true;
			}
			LOGGER.error("Failed to update policy " + policy.getName() + " with ID " + policyID);
			LOGGER.error(response.getErrorCode() + " : " + response.getErrorText());
		}
		return false;
	}

}