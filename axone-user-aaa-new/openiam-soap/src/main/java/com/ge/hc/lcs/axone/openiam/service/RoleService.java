package com.ge.hc.lcs.axone.openiam.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openiam.idm.srvc.role.dto.Role;
import org.openiam.idm.srvc.role.service.Response;
import org.openiam.idm.srvc.role.service.RoleDataService;
import org.openiam.idm.srvc.role.service.RoleSearchBean;
import org.openiam.idm.srvc.user.dto.User;
import org.openiam.idm.srvc.user.dto.UserToRoleMembershipXref;
import org.openiam.idm.srvc.user.service.ProvisionUser;
import org.openiam.service.provision.AttributeOperationEnum;

import com.ge.hc.lcs.axone.openiam.constants.Constants;
import com.ge.hc.lcs.axone.openiam.factory.ServiceFactory;

/**
 * Contains the functions which invokes RoleDataWebService OpenIAM API's
 *
 */
public class RoleService {

	private RoleDataService roleDataService;
	private static RoleService roleService;
	final static Logger LOGGER = Logger.getLogger(RoleService.class);

	private RoleService() throws Exception {
		roleDataService = ServiceFactory.getRoleDataService();
	}

	/**
	 * Returns the instance of RoleService
	 * 
	 * @return
	 * @throws Exception
	 */
	public static RoleService getInstance() throws Exception {
		if (null == roleService) {
			synchronized (RoleService.class) {
				if (null == roleService) {
					roleService = new RoleService();
				}
			}
		}
		return roleService;
	}

	/**
	 * Creates the role with the roleName and sets the status as ACTIVE
	 * 
	 * @param roleName
	 * @return true if successful else failure
	 */
	public boolean addRole(String roleName) {
		Role role = new Role();
		role.setName(roleName);
		role.setStatus("ACTIVE");
		role.setDescription(roleName);
		Response response = roleDataService.saveRole(role);
		if (response != null) {
			if (response.getStatus().toString().equalsIgnoreCase(Constants.SUCCESS)) {
				return true;
			}
			LOGGER.error("Role creation failed : " + roleName);
			LOGGER.error(response.getErrorCode() + " : " + response.getErrorText());
		}
		return false;
	}

	/**
	 * Get roleId for the role
	 * 
	 * @param roleName
	 * @return roleId
	 */
	public String getRoleId(String roleName) {
		RoleSearchBean roleSearchBean = getBaseRoleSearchBean();
		// from and number of beans to be fetched.
		List<Role> roles = roleDataService.findBeans(roleSearchBean, null, Constants.ROLE_FROM, Constants.ROLE_TO);
		for (Role role : roles) {
			if (role.getName().equals(roleName)) {
				return role.getId();
			}
		}
		return null;
	}

	/**
	 * Get all the roles
	 * 
	 * @return role list
	 */
	public List<String> getAllRoles() {
		return getRoles(getBaseRoleSearchBean());
	}

	/**
	 * Get the roles for user
	 * 
	 * @return roles entitled for user
	 * @throws Exception
	 */
	public List<String> getRolesForUser(final String user) throws Exception {
		String userID = UserService.getInstance().getUserId(user);
		RoleSearchBean roleSearchBean = getBaseRoleSearchBean();
		if (userID != null) {
			roleSearchBean.getUserIdSet().add(0, userID);
		}
		LOGGER.debug("User ID for " + user + ((userID != null) ? " is " + userID : " does not exist"));
		if (userID != null) {
			return getRoles(roleSearchBean);
		} else {
			return null;
		}
	}

	private List<String> getRoles(final RoleSearchBean roleSearchBean) {
		List<String> roleNames = new ArrayList<String>();
		// from and number of beans to be fetched.
		List<Role> roles = roleDataService.findBeans(roleSearchBean, null, Constants.ROLE_FROM, Constants.ROLE_TO);
		for (Role role : roles) {
			roleNames.add(role.getName());
		}
		return roleNames;
	}

	private RoleSearchBean getBaseRoleSearchBean() {
		RoleSearchBean roleSearchBean = new RoleSearchBean();
		// Set as per OpenIAM documentation
		roleSearchBean.setDeepCopy(true);
		return roleSearchBean;
	}

	/**
	 * Entitles role to the user
	 * 
	 * @param user
	 * @param role
	 * @return true if successful else false
	 * @throws Exception
	 */
	public boolean addRoleToUser(final String userName, final String role) throws Exception {
		UserService userService = UserService.getInstance();
		User user = userService.getUserByPrincipal(userName);
		String roleID = getRoleId(role);
		LOGGER.debug("Role ID for " + role + ((roleID != null) ? " is " + roleID : " does not exist"));
		if (user != null && roleID != null) {
			// saveProvisionUser API is used for both create user and adding role to user
			// Get the User type object and set the ProvisionUser object
			ProvisionUser provisionUser = getProvisionUserFromUser(user);
			UserToRoleMembershipXref membershipXref = new UserToRoleMembershipXref();
			membershipXref.setEntityId(roleID);
			membershipXref.setMemberEntityId(user.getId());
			membershipXref.setOperation(AttributeOperationEnum.ADD);
			provisionUser.getRoles().add(membershipXref);
			boolean response = userService.saveProviosionUser(provisionUser);
			if (!response) {
				LOGGER.error("User " + user + " entitlement to role " + role + " failed");
			}
			return response;
		}
		return false;
	}

	/**
	 * Deletes the role from the system
	 * 
	 * @param roleName
	 * @return true if successful else false
	 */
	public boolean deleteRole(final String roleName) {
		String roleID = getRoleId(roleName);
		LOGGER.debug("Role ID for " + roleName + ((roleID != null) ? " is " + roleID : " does not exist"));
		if (roleID != null) {
			Response response = roleDataService.removeRole(roleID);
			if (response != null) {
				if (response.getStatus().toString().equalsIgnoreCase(Constants.SUCCESS)) {
					return true;
				}
				LOGGER.error("Deletion of role " + roleName + " failed");
				LOGGER.error(response.getErrorCode() + " : " + response.getErrorText());
			}
		}
		return false;
	}

	/**
	 * Create ProvisionUser from User DTO. This method needs to updated in case if
	 * there is change in schema of ProvisionUser or User
	 * 
	 * @param user
	 * @return ProvisionUser
	 */
	private ProvisionUser getProvisionUserFromUser(final org.openiam.idm.srvc.user.dto.User user) {
		ProvisionUser provisionUser = new ProvisionUser();
		provisionUser.setBirthdate(user.getBirthdate());
		provisionUser.setCompanyOwnerId(user.getCompanyOwnerId());
		provisionUser.setCreateDate(user.getCreateDate());
		provisionUser.setCreatedBy(user.getCreatedBy());
		provisionUser.setEmployeeId(user.getEmployeeId());
		provisionUser.setEmployeeTypeId(user.getEmployeeTypeId());
		provisionUser.setFirstName(user.getFirstName());
		provisionUser.setJobCodeId(user.getJobCodeId());
		provisionUser.setLastName(user.getLastName());
		provisionUser.setLastUpdate(user.getLastUpdate());
		provisionUser.setLastUpdatedBy(user.getLastUpdatedBy());
		provisionUser.setLocationCd(user.getLocationCd());
		provisionUser.setLocationName(user.getLocationName());
		provisionUser.setMdTypeId(user.getMdTypeId());
		provisionUser.setClassification(user.getClassification());
		provisionUser.setMiddleInit(user.getMiddleInit());
		provisionUser.setPrefix(user.getPrefix());
		provisionUser.setSex(user.getSex());
		provisionUser.setStatus(user.getStatus());
		provisionUser.setSecondaryStatus(user.getSecondaryStatus());
		provisionUser.setSuffix(user.getSuffix());
		provisionUser.setTitle(user.getTitle());
		provisionUser.setId(user.getId());
		provisionUser.setUserTypeInd(user.getUserTypeInd());
		provisionUser.setMailCode(user.getMailCode());
		provisionUser.setCostCenter(user.getCostCenter());
		provisionUser.setStartDate(user.getStartDate());
		provisionUser.setLastDate(user.getLastDate());
		provisionUser.setClaimDate(user.getClaimDate());
		provisionUser.setNickname(user.getNickname());
		provisionUser.setMaidenName(user.getMaidenName());
		provisionUser.setPasswordTheme(user.getPasswordTheme());
		provisionUser.setAlternateContactId(user.getAlternateContactId());
		provisionUser.setAlternativeEndDate(user.getAlternativeEndDate());
		provisionUser.setAlternativeStartDate(user.getAlternativeStartDate());
		provisionUser.setCreatedBy(user.getCreatedBy());
		provisionUser.setUserOwnerId(user.getUserOwnerId());
		provisionUser.setDateChallengeRespChanged(user.getDateChallengeRespChanged());
		provisionUser.setDatePasswordChanged(user.getDatePasswordChanged());
		provisionUser.setDateITPolicyApproved(user.getDateITPolicyApproved());
		provisionUser.getUserNotes().addAll(user.getUserNotes());
		provisionUser.getAffiliations().addAll(user.getAffiliations());
		provisionUser.setUserAttributes(user.getUserAttributes());
		provisionUser.getPhones().addAll(user.getPhones());
		provisionUser.getAddresses().addAll(user.getAddresses());
		provisionUser.getEmailAddresses().addAll(user.getEmailAddresses());
		provisionUser.getPrincipalList().addAll(user.getPrincipalList());
		provisionUser.getRoles().addAll(user.getRoles());
		provisionUser.getGroups().addAll(user.getGroups());
		provisionUser.getAffiliations().addAll(user.getAffiliations());
		provisionUser.getResources().addAll(user.getResources());
		provisionUser.setIsFromActivitiCreation(user.isIsFromActivitiCreation());
		if (user instanceof ProvisionUser) {
			provisionUser.setRequestId(((ProvisionUser) user).getRequestId());
		}
		provisionUser.setRequestorUserId(user.getRequestorUserId());
		provisionUser.setRequestClientIP(user.getRequestClientIP());
		provisionUser.setRequestorLogin(user.getRequestorLogin());
		provisionUser.setRequestorSessionID(user.getRequestorSessionID());
		provisionUser.setPrefixLastName(user.getPrefixLastName());
		provisionUser.setPrefixPartnerName(user.getPrefixPartnerName());
		provisionUser.setPartnerName(user.getPartnerName());
		provisionUser.setUserSubTypeId(user.getUserSubTypeId());
		provisionUser.getSubordinates().addAll(user.getSubordinates());
		provisionUser.getSupervisors().addAll(user.getSupervisors());
		return provisionUser;
	}

}
