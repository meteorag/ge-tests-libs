package com.ge.hc.lcs.axone.openiam.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openiam.idm.srvc.am.service.AuthAttributeDataType;
import org.openiam.idm.srvc.am.service.AuthProvider;
import org.openiam.idm.srvc.am.service.AuthProviderAttribute;
import org.openiam.idm.srvc.am.service.AuthProviderSearchBean;
import org.openiam.idm.srvc.am.service.AuthProviderWebService;
import org.openiam.idm.srvc.am.service.Response;
import org.openiam.idm.srvc.res.service.Resource;

import com.ge.hc.lcs.axone.openiam.constants.Constants;
import com.ge.hc.lcs.axone.openiam.enums.AuthProviderAttributeEnum;
import com.ge.hc.lcs.axone.openiam.factory.ServiceFactory;

/**
 * Contains the functions which invokes AuthProviderWebService OpenIAM API's
 *
 */
public class AuthProviderService {

	final static Logger LOGGER = Logger.getLogger(AuthProviderService.class);
	private AuthProviderWebService authProviderWebService;
	private static AuthProviderService authProviderService;

	private AuthProviderService() throws Exception {
		authProviderWebService = ServiceFactory.getAuthProviderWebService();
	}

	/**
	 * Returns the instance of AuthProviderService
	 * 
	 * @return
	 * @throws Exception
	 */
	public static AuthProviderService getInstance() throws Exception {
		if (authProviderService == null) {
			synchronized (AuthProviderService.class) {
				if (authProviderService == null) {
					authProviderService = new AuthProviderService();
				}
			}
		}
		return authProviderService;
	}

	/**
	 * Create Authentication provider for oAuth Implicit grant flow
	 * 
	 * @param name
	 * @param clientID
	 * @param clientSecret
	 * @param redirectURLs
	 * @param scopes
	 * @return true if successful else false
	 * @throws Exception
	 */
	public boolean createImplicitAuthProvider(final String name, final String clientID, final String clientSecret,
			final List<String> redirectURLs, final List<String> scopes, final Boolean useRefreshToken,
			final String tokenExpiration) throws Exception {
		AuthProvider provider = buildAuthProvider(name, clientID, clientSecret, redirectURLs, scopes, useRefreshToken,
				tokenExpiration);
		if (provider == null) {
			return false;
		}
		provider.getAttributes().add(createAuthProviderAttribute("OAuthAuthorizationGrantFlow",
				"Authorization Grant Flow", Constants.IMPLICIT));
		provider.getAttributes()
				.add(createAuthProviderAttribute("OAuthClientAuthType", "Client Authentication Type", "REQUEST_BODY"));
		// Create Authentication Provider and update client credentials
		return createAuthProvider(provider) && updateClientCredentials(name, clientID, clientSecret);
	}

	/**
	 * Create Authentication provider for oAuth Authorization code grant flow
	 * 
	 * @param name
	 * @param clientID
	 * @param clientSecret
	 * @param redirectURLs
	 * @param scopes
	 * @return true if successful else false
	 * @throws Exception
	 */
	public boolean createAuthroizationCodeAuthProvider(final String name, final String clientID,
			final String clientSecret, final List<String> redirectURLs, final List<String> scopes,
			final Boolean useRefreshToken, final String tokenExpiration) throws Exception {
		AuthProvider provider = buildAuthProvider(name, clientID, clientSecret, redirectURLs, scopes, useRefreshToken,
				tokenExpiration);
		if (provider == null) {
			return false;
		}
		provider.getAttributes().add(createAuthProviderAttribute("OAuthAuthorizationGrantFlow",
				"Authorization Grant Flow", Constants.AUTHORIZATION_CODE));
		provider.getAttributes()
				.add(createAuthProviderAttribute("OAuthClientAuthType", "Client Authentication Type", "BASIC_HTTP"));
		// Create Authentication Provider and update client credentials
		return createAuthProvider(provider) && updateClientCredentials(name, clientID, clientSecret);
	}

	/**
	 * Create Authentication provider for oAuth Resource owner grant flow
	 * 
	 * @param name
	 * @param clientID
	 * @param clientSecret
	 * @param redirectURLs
	 * @param scopes
	 * @return true if successful else false
	 * @throws Exception
	 */
	public boolean createResourceOwnerAuthProvider(final String name, final String clientID, final String clientSecret,
			final List<String> redirectURLs, final List<String> scopes, final Boolean useRefreshToken,
			final String tokenExpiration) throws Exception {
		AuthProvider provider = buildAuthProvider(name, clientID, clientSecret, redirectURLs, scopes, useRefreshToken,
				tokenExpiration);
		if (provider == null) {
			return false;
		}
		provider.getAttributes().add(createAuthProviderAttribute("OAuthAuthorizationGrantFlow",
				"Authorization Grant Flow", Constants.RESOURCE_OWNER));
		provider.getAttributes()
				.add(createAuthProviderAttribute("OAuthClientAuthType", "Client Authentication Type", "BASIC_HTTP"));
		// Create Authentication Provider and update client credentials
		return createAuthProvider(provider) && updateClientCredentials(name, clientID, clientSecret);
	}

	private boolean createAuthProvider(final AuthProvider provider) {
		Response response = authProviderWebService.saveAuthProvider(provider);
		if (response != null) {
			if (response.getStatus().toString().equalsIgnoreCase(Constants.SUCCESS)) {
				return true;
			}
			LOGGER.error("Authentication provider creation for " + provider.getName() + " failed");
			LOGGER.error(response.getErrorCode() + " : " + response.getErrorText());
		}
		return false;
	}

	private boolean updateClientCredentials(final String providerName, final String clientID,
			final String clientSecret) {
		AuthProvider provider = getAuthProvider(providerName);
		provider.getAttributes().stream()
				.filter(attribute -> attribute.getAttributeId()
						.equals(AuthProviderAttributeEnum.OAUTH_CLIENT_ID.getValue()))
				.findFirst().orElse(null).setValue(clientID);
		provider.getAttributes().stream()
				.filter(attribute -> attribute.getAttributeId()
						.equals(AuthProviderAttributeEnum.OAUTH_CLIENT_SECRECT.getValue()))
				.findFirst().orElse(null).setValue(clientSecret);
		return createAuthProvider(provider);
	}

	private List<String> getResourceIDs(List<String> scopes) throws Exception {
		ResourceDataService resourceDataService = ResourceDataService.getInstance();
		Map<String, String> resourceMap = new HashMap<String, String>();
		List<Resource> resources = resourceDataService.getAllResources();
		resources.forEach(resource -> resourceMap.put(resource.getCoorelatedName(), resource.getId()));
		List<String> scopeIDs = new ArrayList<String>();
		scopes.forEach(scope -> {
			String scopeID = resourceMap.get(scope);
			if (scopeID != null) {
				scopeIDs.add(scopeID);
			} else {
				scopeIDs.add(null);
				LOGGER.debug("Resource ID for " + scope + ((scopeID != null) ? " is " + scopeID : " does not exist"));
			}
		});
		return scopeIDs;
	}

	private AuthProvider buildAuthProvider(final String name, final String clientID, final String clientSecret,
			final List<String> redirectURLs, final List<String> scopes, final Boolean useRefreshToken,
			final String tokenExpiration) throws Exception {
		AuthProvider provider = null;
		List<String> scopesIDs = getResourceIDs(scopes);
		if (!scopesIDs.contains(null)) {
			provider = new AuthProvider();
			provider.setProviderType("OAUTH_CLIENT");
			provider.setName(name);
			provider.setManagedSysId(Constants.ZERO);
			List<AuthProviderAttribute> attributes = provider.getAttributes();
			attributes.add(createAuthProviderAttribute("JwtAlgorithm", "Signing Algorithm", "HS512"));
			attributes.add(createAuthProviderAttribute("JwtIssue", "JWT Issue", "OpenIAM"));
			attributes.add(createAuthProviderAttribute("OAuthRedirectUrl", "Redirect URL",
					String.join(Constants.COMMA, redirectURLs)));
			attributes.add(createAuthProviderAttribute("OAuthClientScopes", "Default scopes",
					String.join(Constants.COMMA, scopesIDs)));
			attributes.add(createAuthProviderAttribute("OAuthUseRefreshToken", "Use Refresh token",
					useRefreshToken.toString()));
			attributes.add(createAuthProviderAttribute("OAuthTokenExpiration", "Token Expiration", tokenExpiration));
		}
		return provider;
	}

	private AuthProviderAttribute createAuthProviderAttribute(final String id, final String name, final String value) {
		AuthProviderAttribute authProviderAttribute = new AuthProviderAttribute();
		authProviderAttribute.setAttributeId(id);
		authProviderAttribute.setAttributeName(name);
		authProviderAttribute.setValue(value);
		if (id.equals("OAuthClientScopes") || id.equals("OAuthRedirectUrl")) {
			authProviderAttribute.setDataType(AuthAttributeDataType.LIST_VALUE);
		} else {
			authProviderAttribute.setDataType(AuthAttributeDataType.SINGLE_VALUE);
		}
		return authProviderAttribute;
	}

	/**
	 * Returns the list of Authentication Providers
	 * 
	 * @return
	 */
	public List<AuthProvider> getAllAuthProviders() {
		AuthProviderSearchBean bean = new AuthProviderSearchBean();
		// Set as per OpenIAM documentation
		bean.setDeepCopy(true);
		// from and number of beans to be fetched. here from starting from first item
		// 250 items are requested
		return authProviderWebService.findAuthProviderBeans(bean, Constants.AUTH_PROVIDER_FROM,
				Constants.AUTH_PROVIDER_TO);
	}

	/**
	 * Verify whether the Authentication Provider exists
	 * 
	 * @param authProviderName
	 * @return true if exists else false
	 */
	public boolean isAuthProviderExists(String authProviderName) {
		AuthProvider authProvider = getAuthProvider(authProviderName);
		if (authProvider != null) {
			return true;
		}
		return false;
	}

	/**
	 * Returns the Id of the Authentication provider
	 * 
	 * @param authProviderName
	 * @return
	 */
	public String getAuthenticationProviderId(String authProviderName) {
		AuthProvider authProvider = getAuthProvider(authProviderName);
		if (authProvider != null) {
			return authProvider.getId();
		}
		return null;
	}

	private AuthProvider getAuthProvider(String authProviderName) {
		AuthProvider provider = null;
		List<AuthProvider> authProviders = getAllAuthProviders();
		for (AuthProvider authProvider : authProviders) {
			if (authProvider.getName().equals(authProviderName)) {
				provider = authProvider;
			}
		}
		return provider;
	}

	/**
	 * Deletes the authentication provider
	 * 
	 * @param authProvider
	 * @return true if successful else false
	 */
	public boolean deleteAuthProvider(String authProvider) {
		String authProviderID = getAuthenticationProviderId(authProvider);
		LOGGER.debug((authProviderID != null) ? "Auth provider ID for " + authProvider + " is " + authProviderID
				: "Auth provider ID " + authProviderID + " not exist");
		if (authProviderID != null) {
			Response response = authProviderWebService.deleteAuthProvider(authProviderID);
			if (response != null) {
				if (response.getStatus().value().equalsIgnoreCase(Constants.SUCCESS)) {
					return true;
				}
				LOGGER.error("Deletion of auth provider " + authProvider + " failed");
				LOGGER.error(response.getErrorCode() + " : " + response.getErrorText());
			}
		}
		return false;
	}

	/**
	 * Returns AuthProviderAttribute information for the auth provider
	 * 
	 * @param authProviderName
	 *            Name of the authentication provider
	 * @param attributeName
	 *            Auth provider attribute to be retrieved
	 * @return AuthProviderAttribute
	 */
	public AuthProviderAttribute getAuthProviderAttribute(final String authProviderName,
			final AuthProviderAttributeEnum attributeName) {
		AuthProvider authProvider = getAuthProvider(authProviderName);
		return authProvider.getAttributes().stream()
				.filter(attribute -> attribute.getAttributeId().equals(attributeName.getValue())).findFirst()
				.orElse(null);
	}

}
