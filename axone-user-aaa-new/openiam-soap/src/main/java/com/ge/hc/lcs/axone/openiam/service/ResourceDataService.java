package com.ge.hc.lcs.axone.openiam.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openiam.idm.srvc.res.service.Resource;
import org.openiam.idm.srvc.res.service.ResourceDataWebService;
import org.openiam.idm.srvc.res.service.ResourceSearchBean;
import org.openiam.idm.srvc.res.service.ResourceType;
import org.openiam.idm.srvc.res.service.Response;

import com.ge.hc.lcs.axone.openiam.constants.Constants;
import com.ge.hc.lcs.axone.openiam.factory.ServiceFactory;

/**
 * Contains the functions which invokes ResourceDataWebService OpenIAM API's
 *
 */
public class ResourceDataService {

	private ResourceDataWebService resourceDataWebService = null;
	private static ResourceDataService resourceDataService;
	final static Logger LOGGER = Logger.getLogger(ResourceDataService.class);

	private ResourceDataService() throws Exception {
		resourceDataWebService = ServiceFactory.getResourceDataWebService();
	}

	/**
	 * Returns the instance of ResourceDataService
	 * 
	 * @return
	 * @throws Exception
	 */
	public static ResourceDataService getInstance() throws Exception {
		if (null == resourceDataService) {
			synchronized (ResourceDataService.class) {
				if (null == resourceDataService) {
					resourceDataService = new ResourceDataService();
				}
			}
		}
		return resourceDataService;
	}

	/**
	 * Gets all the resources available in the system
	 * 
	 * @return List of resources
	 */
	public List<Resource> getAllResources() {
		ResourceSearchBean resourceSearchBean = new ResourceSearchBean();
		// Set as per OpenIAM documentation
		resourceSearchBean.setDeepCopy(true);
		// Set as per OpenIAM documentation
		resourceSearchBean.setName(Constants.EMPTY);
		// from and number of beans to be fetched. here from starting from first item
		// 1000 items are requested
		return resourceDataWebService.findBeans(resourceSearchBean, null, Constants.RESOURCE_FROM,
				Constants.RESOURCE_TO);
	}

	/**
	 * Verifies whether the resource exists in the system
	 * 
	 * @param resourceName
	 * @return true if exists else false
	 */
	public boolean isResourceExists(final String resourceName) {
		Resource resource = findResource(resourceName);
		if (resource != null) {
			return true;
		}
		return false;
	}

	/**
	 * Get the Id of the resource
	 * 
	 * @param resourceName
	 * @return Id of the resource
	 */
	public String getResourceId(final String resourceName) {
		Resource resource = findResource(resourceName);
		if (resource != null) {
			return resource.getId();
		}
		return null;
	}

	/**
	 * Finds the resource based on the name
	 * 
	 * @param resourceName
	 * @return resource
	 */
	private Resource findResource(final String resourceName) {
		List<Resource> resources = getAllResources();
		for (Resource resource : resources) {
			if (resource.getCoorelatedName() != null && resource.getCoorelatedName().equals(resourceName)) {
				return resource;
			}
		}
		return null;
	}

	/**
	 * Deletes the resource
	 * 
	 * @param resource
	 * @return true if successful else failure
	 */
	public boolean deleteResource(final String resource) {
		String resourceID = getResourceId(resource);
		LOGGER.debug("Resource ID for " + resource + ((resourceID != null) ? " is " + resourceID : " does not exist"));
		if (resourceID != null) {
			Response response = resourceDataWebService.deleteResource(resourceID);
			if (response != null) {
				if (response.getStatus().value().equalsIgnoreCase(Constants.SUCCESS)) {
					return true;
				}
				LOGGER.error("Resource deletion failed for " + resource);
				LOGGER.error(response.getErrorCode() + " : " + response.getErrorText());
			}
		}

		return false;
	}

	/**
	 * Create an resource of type URL pattern
	 * 
	 * @param url
	 * @return true if successful else failure
	 */
	public boolean createURLResource(final String url) {
		Resource resource = new Resource();
		resource.setName(url);
		resource.setDisplayName(url);
		resource.setCoorelatedName(url);
		ResourceType resourceType = new ResourceType();
		resourceType.setDescription("URL Resource " + url);
		resourceType.setId(Constants.URL_PATTERN);
		resource.setResourceType(resourceType);
		resource.setURL(url);
		Response response = resourceDataWebService.saveResource(resource);
		if (response != null) {
			if (response.getStatus().value().equalsIgnoreCase(Constants.SUCCESS)) {
				return true;
			}
			LOGGER.error("Resource creation failed : " + url);
			LOGGER.error(response.getErrorCode() + " : " + response.getErrorText());
		}
		return false;
	}

	/**
	 * Entitles the resource to the role
	 * 
	 * @param role
	 * @param resource
	 * @return true if successful else failure
	 * @throws Exception
	 */
	public boolean addRoleToResouce(final String role, final String resource) throws Exception {
		String resourceID = getResourceId(resource);
		LOGGER.debug("Resource ID for " + resource + ((resourceID != null) ? " is " + resourceID : " does not exist"));
		String roleID = RoleService.getInstance().getRoleId(role);
		LOGGER.debug("Role ID for " + role + ((roleID != null) ? " is " + roleID : " does not exist"));
		if (resourceID != null && roleID != null) {
			Response response = resourceDataWebService.addRoleToResource(resourceID, roleID, new ArrayList<String>(),
					null, null);
			if (response != null) {
				if (response.getStatus().value().equalsIgnoreCase(Constants.SUCCESS)) {
					return true;
				}
				LOGGER.error("Assigning resource " + resource + " to role " + role + " failed");
				LOGGER.error(response.getErrorCode() + " : " + response.getErrorText());
			}
		}
		return false;
	}
}
