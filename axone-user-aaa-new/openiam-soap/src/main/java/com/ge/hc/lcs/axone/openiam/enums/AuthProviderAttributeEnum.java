package com.ge.hc.lcs.axone.openiam.enums;

public enum AuthProviderAttributeEnum {
	JWT_ISSUE("JwtIssue"),
	OAUTH_CLIENT_ID("OAuthClientID"),
	OAUTH_TOKEN_EXPIRATION("OAuthTokenExpiration"),
	OAUTH_REDIRECT_URL("OAuthRedirectUrl"),
	OAUTH_AUTHORIZATION_GRANT_FLOW("OAuthAuthorizationGrantFlow"),
	OAUTH_CLIENT_SECRECT("OAuthClientSecret"),
	OAUTH_CLIENT_SCOPES("OAuthClientScopes"),
	JWT_ALGORITHM("JwtAlgorithm"),
	OAUTH_REFRESH_TOKEN("OAuthUseRefreshToken"),
	OAUTH_CLIENT_AUTH_TYPE("OAuthClientAuthType");
	
	private String attribute;
	
	private AuthProviderAttributeEnum(String attribute) {
		this.attribute = attribute;
	}
	
	public String getValue() {
		return this.attribute;
	}
}
