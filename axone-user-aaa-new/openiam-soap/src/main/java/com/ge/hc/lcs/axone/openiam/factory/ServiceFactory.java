package com.ge.hc.lcs.axone.openiam.factory;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.message.Message;
import org.json.JSONObject;
import org.openiam.idm.srvc.am.service.AuthProviderWebService;
import org.openiam.idm.srvc.policy.service.PolicyWebService;
import org.openiam.idm.srvc.pswd.service.ChallengeResponseWebService;
import org.openiam.idm.srvc.res.service.ResourceDataWebService;
import org.openiam.idm.srvc.role.service.RoleDataService;
import org.openiam.idm.srvc.user.service.UserDataService;
import org.openiam.service.provision.ProvisionControllerService;

import com.ge.hc.lcs.axone.openiam.constants.Constants;

public class ServiceFactory {

	/**
	 * Contains the list of services created using this factory. If the service is
	 * already created it will fetched from this.
	 */
	private static Map<String, Object> services = new HashMap<String, Object>();
	private static String oAuthToken = null;
	private static String hostname;
	private static String login;
	private static String password;
	private static boolean isInitialized;

	/**
	 * Initiliazes the hostname, login and password for the SOAP service instance
	 * for API invocations
	 * 
	 * @param hostname
	 *            hostname of the OpenIAM instance Eg: http://172.0.0.1 (or)
	 *            http://axone.openiam.com
	 * @param login
	 *            loginID of the OpenIAM user
	 * @param password
	 *            password for the OpenIAM login
	 * @throws Exception 
	 */
	public static void init(final String hostname, final String login, final String password) throws Exception {
		if(isInitialized) {
			throw new Exception("ServiceFactory is already initialized.");
		}
		ServiceFactory.hostname = hostname;
		ServiceFactory.login = login;
		ServiceFactory.password = password;
		isInitialized = true;
	}

	@SuppressWarnings("unchecked")
	private static <E> E getService(final String serviceURL, final Class<E> clazz) throws Exception {
		if (services.containsKey(clazz.getName())) {
			return (E) services.get(clazz.getName());
		}
		Objects.requireNonNull(hostname, "hostname for OpenIAM instance is required");
		Objects.requireNonNull(login, "loginID for OpenIAM instance is required");
		Objects.requireNonNull(password, "password for loginID of OpenIAM instance is required");
		JaxWsProxyFactoryBean factoryBean = new JaxWsProxyFactoryBean();
		factoryBean.setServiceClass(clazz);
		factoryBean.setAddress((hostname + serviceURL).replaceAll(Constants.URL_REGEX, Constants.FWD_SLASH));
		E e = (E) factoryBean.create();
		Client proxy = ClientProxy.getClient(e);
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		if (null == oAuthToken) {
			oAuthToken = getOAuthToken(hostname, login, password);
		}
		headers.put("Cookie", Arrays.asList("OPENIAM_AUTH_TOKEN=" + oAuthToken));
		proxy.getRequestContext().put(Message.PROTOCOL_HEADERS, headers);
		services.put(clazz.getName(), e);
		return e;
	}

	public static UserDataService getUserDataService() throws Exception {
		return getService(Constants.USER_DATA_SERVICE, UserDataService.class);
	}

	public static AuthProviderWebService getAuthProviderWebService() throws Exception {
		return getService(Constants.AUTH_PROVIDER_SERVICE, AuthProviderWebService.class);
	}

	public static RoleDataService getRoleDataService() throws Exception {
		return getService(Constants.ROLE_DATA_SERVICE, RoleDataService.class);
	}

	public static ChallengeResponseWebService getChallengeResponseWebService() throws Exception {
		return getService(Constants.CHALLENGE_RESPONSE_SERVICE, ChallengeResponseWebService.class);
	}

	public static ResourceDataWebService getResourceDataWebService() throws Exception {
		return getService(Constants.RESOURCE_DATA_SERVICE, ResourceDataWebService.class);
	}

	public static ProvisionControllerService getProvisionControllerService() throws Exception {
		return getService(Constants.PROVISIONING_SERVICE, ProvisionControllerService.class);
	}

	public static PolicyWebService getPolicyWebService() throws Exception {
		return getService(Constants.POLICY_WEB_SERVICE, PolicyWebService.class);
	}

	private static String getOAuthToken(final String hostname, final String login, final String password)
			throws Exception {
		URL url = new URL((hostname + Constants.LOGIN_LINK).replaceAll(Constants.URL_REGEX, Constants.FWD_SLASH));
		String urlParameters = "login=" + login + "&password=" + password;
		byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setInstanceFollowRedirects(false);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		conn.setRequestProperty("charset", "utf-8");
		conn.setRequestProperty("Content-Length", Integer.toString(postData.length));
		conn.setUseCaches(false);
		try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
			wr.write(postData);
		}
		if (conn.getResponseCode() != 200) {
			conn.disconnect();
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode() + ", Message : "
					+ conn.getResponseMessage());
		}
		BufferedReader br1 = new BufferedReader(new InputStreamReader((conn.getInputStream())));
		String output;
		StringBuilder loginResponse = new StringBuilder();
		while ((output = br1.readLine()) != null) {
			loginResponse.append(output);
		}
		JSONObject loginJSON = new JSONObject(loginResponse.toString());
		JSONObject tokenInfoJSON = loginJSON.getJSONObject("tokenInfo");
		return tokenInfoJSON.get("authToken").toString();
	}

}
