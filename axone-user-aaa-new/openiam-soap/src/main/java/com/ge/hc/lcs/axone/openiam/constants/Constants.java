package com.ge.hc.lcs.axone.openiam.constants;

public interface Constants {
	String ESB_SERVICE = "/openiam-esb/idmsrvc";
	String AUTH_PROVIDER_SERVICE = ESB_SERVICE + "/AuthProviderWebService";
	String CHALLENGE_RESPONSE_SERVICE = ESB_SERVICE + "/ChallengeResponseWebService";
	String RESOURCE_DATA_SERVICE = ESB_SERVICE + "/ResourceDataWebService";
	String ROLE_DATA_SERVICE = ESB_SERVICE + "/RoleDataWebService";
	String USER_DATA_SERVICE = ESB_SERVICE + "/UserDataService";
	String PROVISIONING_SERVICE = ESB_SERVICE + "/ProvisioningService";
	String POLICY_WEB_SERVICE = ESB_SERVICE + "/PolicyWebService";
	String OAUTH_WEB_SERVICE = ESB_SERVICE + "/OAuthWebService";
	String LOG_WSDL_URL = "WSDL URL : ";
	String SUCCESS = "SUCCESS";
	String EMPTY = "";
	String URL_PATTERN = "URL_PATTERN";
	String URL_REGEX = "(?<!(http:|https:))//";
	Integer ROLE_FROM = 0; // Start index from where roles has to fetched
	Integer ROLE_TO = 100; // End index up to which roles has to fetched
	Integer RESOURCE_FROM = 1; // Start index from where resource has to fetched
	Integer AUTH_PROVIDER_FROM = 0; // Start index from where authentication provider has to fetched
	Integer RESOURCE_TO = 1000; // End index up to which resource has to fetched
	Integer AUTH_PROVIDER_TO = 100; // End index up to which authentication provider has to fetched
	Integer POLICY_FROM = 0; // Start index from where policies has to fetched
	Integer POLICY_TO = 1000; // End index up to which policies has to fetched
	String FWD_SLASH = "/";
	String COMMA = ",";
	String ZERO = "0";
	String IMPLICIT = "IMPLICIT";
	String AUTHORIZATION_CODE = "AUTHORIZATION_CODE";
	String RESOURCE_OWNER = "RESOURCE_OWNER";
	String DEFAULT_PASSWD_POLICY_ID = "4000";
	String DEFAULT_AUTHN_POLICY_ID = "4001";
	String LOGIN_LINK = "/idp/rest/api/auth/public/login";
}
