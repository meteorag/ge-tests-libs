package com.ge.hc.lcs.axone.openiam.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openiam.idm.srvc.continfo.dto.EmailAddress;
import org.openiam.idm.srvc.pswd.service.ChallengeResponseWebService;
import org.openiam.idm.srvc.pswd.service.UserIdentityAnswer;
import org.openiam.idm.srvc.user.dto.User;
import org.openiam.idm.srvc.user.dto.UserStatus;
import org.openiam.idm.srvc.user.dto.UserToRoleMembershipXref;
import org.openiam.idm.srvc.user.service.ProvisionUserResponse;
import org.openiam.idm.srvc.user.service.ProvisionUser;
import org.openiam.idm.srvc.user.service.UserDataService;
import org.openiam.service.provision.AttributeOperationEnum;
import org.openiam.service.provision.Login;
import org.openiam.service.provision.PasswordSync;
import org.openiam.service.provision.ProvisionControllerService;

import com.ge.hc.lcs.axone.openiam.constants.Constants;
import com.ge.hc.lcs.axone.openiam.factory.ServiceFactory;

/**
 * Contains the functions which invokes UserDataService OpenIAM API's
 *
 */
public class UserService {

	private UserDataService userDataService = null;
	private static UserService userService = null;
	private static ChallengeResponseWebService challengeResponseWebService = null;
	private static ProvisionControllerService provisionControllerService = null;
	final static Logger LOGGER = Logger.getLogger(UserService.class);

	private UserService() throws Exception {
		challengeResponseWebService = ServiceFactory.getChallengeResponseWebService();
		userDataService = ServiceFactory.getUserDataService();
		provisionControllerService = ServiceFactory.getProvisionControllerService();
	}

	/**
	 * Returns the instance of UserService
	 * 
	 * @return
	 * @throws Exception
	 */
	public static UserService getInstance() throws Exception {
		if (null == userService) {
			synchronized (UserService.class) {
				if (null == userService) {
					userService = new UserService();
				}
			}
		}
		return userService;
	}

	/**
	 * Creates user with default password as 'Password$51' and status as
	 * PENDING_INITIAL_LOGIN
	 * 
	 * @param firstname
	 * @param lastname
	 * @param loginId
	 * @return true if successful else false
	 * @throws Exception
	 */
	public boolean addUser(final String firstname, final String lastname, final String loginId) {
		ProvisionUser provisionUser = buildUser(firstname, lastname, loginId);
		if (createUserAndSetChallengeResponse(provisionUser, loginId)) {
			return true;
		}
		LOGGER.error("User creation failed " + loginId);
		return false;
	}

	/**
	 * Creates user with email default password as 'Password$51' and status as
	 * PENDING_INITIAL_LOGIN
	 * 
	 * @param firstname
	 * @param lastname
	 * @param loginId
	 * @param email
	 * @return true if successful else false
	 * @throws Exception
	 */
	public boolean addUser(final String firstname, final String lastname, final String loginId, final String email) {
		ProvisionUser provisionUser = buildUser(firstname, lastname, loginId);
		EmailAddress emailAddress = new EmailAddress();
		emailAddress.setEmailAddress(email);
		emailAddress.setActive(true);
		provisionUser.getEmailAddresses().add(emailAddress);
		if (createUserAndSetChallengeResponse(provisionUser, loginId)) {
			return true;
		}
		LOGGER.error("User creation failed " + loginId);
		return false;
	}

	/**
	 * Creates user and entitle with role with default password as 'Password$51' and
	 * status as PENDING_INITIAL_LOGIN
	 * 
	 * @param firstname
	 * @param lastname
	 * @param loginId
	 * @param role
	 * @return true if successful else false
	 * @throws Exception
	 */
	public boolean addUserWithRole(final String firstname, final String lastname, final String loginId,
			final String role) throws Exception {
		ProvisionUser provisionUser = buildUser(firstname, lastname, loginId);
		UserToRoleMembershipXref userToRoleMembershipXref = new UserToRoleMembershipXref();
		String roleID = RoleService.getInstance().getRoleId(role);
		LOGGER.debug("Role ID for " + role + ((roleID != null) ? " is " + roleID : " does not exist"));
		userToRoleMembershipXref.setEntityId(roleID);
		userToRoleMembershipXref.setOperation(AttributeOperationEnum.ADD);
		provisionUser.getRoles().add(userToRoleMembershipXref);
		if (createUserAndSetChallengeResponse(provisionUser, loginId)) {
			return true;
		}
		LOGGER.error("User creation failed " + loginId);
		return false;
	}

	private boolean createUserAndSetChallengeResponse(final ProvisionUser provisionUser, final String loginID) {
		if (!saveProviosionUser(provisionUser)) {
			return false;
		}
		if (!setChallengeResponse(loginID)) {
			deleteUserByPrincipal(loginID);
			return false;
		}
		return true;
	}

	boolean saveProviosionUser(final ProvisionUser provisionUser) {
		ProvisionUserResponse response = userDataService.saveProvisionUser(provisionUser, null);
		if (response != null) {
			if (response.getStatus().toString().equalsIgnoreCase(Constants.SUCCESS)) {
				return true;
			}
			LOGGER.error(response.getErrorCode() + " : " + response.getErrorText());
		}
		return false;
	}

	private ProvisionUser buildUser(final String firstname, final String lastname, final String loginId) {
		ProvisionUser provisionUser = new ProvisionUser();
		provisionUser.setTestRequest(true);
		provisionUser.setFirstName(firstname);
		provisionUser.setLastName(lastname);
		provisionUser.setStatus(UserStatus.PENDING_INITIAL_LOGIN);
		Login login = new Login();
		login.setLogin(loginId);
		// '0' is the default managed system ID used by OpenIAM
		login.setManagedSysId(Constants.ZERO);
		login.setPassword("Password$51");
		login.setActive(true);
		provisionUser.getPrincipalList().add(login);
		return provisionUser;
	}

	/**
	 * Deletes the user if the user exists
	 * 
	 * @param user
	 * @return true if successful else false
	 * @throws Exception
	 */
	public boolean deleteUserByPrincipal(final String user) {
		org.openiam.service.provision.DeleteUserProvisioningRequest request = new org.openiam.service.provision.DeleteUserProvisioningRequest();
		request.setManagedSysId(Constants.ZERO);
		request.setPrincipal(user);
		request.setStatus(UserStatus.REMOVE);
		org.openiam.service.provision.Response response = provisionControllerService.deleteUser(request);
		if (response != null) {
			if (response.getStatus().toString().equalsIgnoreCase(Constants.SUCCESS)) {
				return true;
			}
			LOGGER.error("User deletion failed : " + user);
			LOGGER.error(response.getErrorCode() + " : " + response.getErrorText());
		}
		return false;
	}

	/**
	 * Get the userId for the user
	 * 
	 * @param loginId
	 *            of the user
	 * @return true if successful else false
	 */
	public String getUserId(final String loginId) {
		// '0' is the default managed system ID used by OpenIAM
		User user = getUserByPrincipal(loginId);
		if (user != null) {
			return user.getId();
		}
		return null;
	}

	/**
	 * Get User object by principal name
	 * 
	 * @param loginId
	 * @return
	 */
	User getUserByPrincipal(final String loginId) {
		return userDataService.getUserByPrincipal(loginId, Constants.ZERO, null);
	}

	private boolean setChallengeResponse(final String loginId) {
		String userId = this.getUserId(loginId);
		List<UserIdentityAnswer> userIdentityAnswers = new ArrayList<UserIdentityAnswer>();
		userIdentityAnswers.add(buildUserIdentityAnswer(userId, "201", "What is your mothers maiden name?", "a_name"));
		userIdentityAnswers.add(buildUserIdentityAnswer(userId, "202", "Where did you go to school?", "a_school"));
		userIdentityAnswers.add(buildUserIdentityAnswer(userId, "203", "What is your pets name?", "a_pet"));
		org.openiam.idm.srvc.pswd.service.Response response = challengeResponseWebService
				.saveAnswers(userIdentityAnswers);
		if (response != null) {
			if (response.getStatus().toString().equalsIgnoreCase(Constants.SUCCESS)) {
				return true;
			}
			LOGGER.error("Setting challenge response for " + loginId + " failed");
			LOGGER.error(response.getErrorCode() + " : " + response.getErrorText());
		}

		return false;
	}

	private UserIdentityAnswer buildUserIdentityAnswer(final String userId, final String questionId,
			final String question, final String answer) {
		UserIdentityAnswer userIdentityAnswer = new UserIdentityAnswer();
		userIdentityAnswer.setUserId(userId);
		userIdentityAnswer.setQuestionId(questionId);
		userIdentityAnswer.setQuestionText(question);
		userIdentityAnswer.setQuestionAnswer(answer);
		return userIdentityAnswer;
	}

	/**
	 * Resets the password for the user
	 * 
	 * @param user
	 * @param password
	 * @return true if successful else false
	 */
	public boolean resetPassword(final String user, final String password) {
		String userID = getUserId(user);
		LOGGER.debug("User ID for " + user + ((userID != null) ? " is " + userID : " does not exist"));
		if (userID != null) {
			PasswordSync passwordSync = new PasswordSync();
			passwordSync.setPrincipal(user);
			passwordSync.setUserId(userID);
			passwordSync.setPassword(password);
			passwordSync.setSendPasswordToUser(false);
			org.openiam.service.provision.Response response = provisionControllerService.resetPassword(passwordSync);
			if (response != null) {
				if (response.getStatus().toString().equalsIgnoreCase(Constants.SUCCESS)) {
					return true;
				}
				LOGGER.error("Password reset for User " + user + " failed");
				LOGGER.error(response.getErrorCode() + " : " + response.getErrorText());
			}
		}
		return false;
	}

}