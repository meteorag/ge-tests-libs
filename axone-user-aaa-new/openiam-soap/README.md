
OpenIAM SOAP library
======================================
* Provides a JAVA wrapper methods to invoke the SOAP API's exposed by OpenIAM Server. 
* To invoke the API's exposed by this library, Each service should be instantiated with the host name where the actual services are running


Build Scripts
---------------------------

**Gradle**

`gradle clean build publish -Phostname=http://172.0.0.1 -Pwsdl_services=AuthProviderWebService,ResourceDataWebService,RoleDataWebService,ChallengeResponseWebService,UserDataService,AuthorizationManagerWebService,ProvisioningService,PolicyWebService -Plogin=sysadmin -Ppassword=passwd00`

*hostname* - name of the host where the OpenIAM esb services are running from where the wsdl schema's are fetched

*wsdl_services* - List of OpenIAM web services which are used in this library for invoking their API's

**publish**

Publish artifacts generated in this build to the configured remote repository

Usage
-------------

<code><pre>
	//Initialize ServiceFactory with hostname, login and password. Initiliazing again will throw exception.
	ServiceFactory.init("http://172.0.0.1", "admin", "pwd");
	UserService userService = UserService.getInstance();
	userService.getUserId("sysadmin");
</pre></code>

