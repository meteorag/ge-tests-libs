# Axone User AAA

This project provides design level tests to create, manage user accounts and their access controls using the User Administrative portal which is implemented using OpenIAM solution.

## Project Structure

```
├── access_management       <-- contains feature files and tests for user access controls.
├── doc                     <-- contains all User AAA project related document like SDD FMEA etc.
├── docker-lifecycle-tests  <-- contains docker operational behavior tests.
├── identity_management     <-- contains feature files and tests for create and manage users and configure authentication policies.
├── openiam-soap            <-- contains wrapper methods to invoke SOAP API's which are used by identity and access management tests.
├── build.gradle            <-- contains tasks needed to start and delete the OpenIAM docker containers,run the IDM and IAM BDD Tests
└── README.md               <-- overview of the project
```

## Start OpenIAM containers

To start OpenIAM containers

```
gradle initDockerSwarm startOpenIAMContainers
```

## Run Tests:
Prerequisite(s):
1. Ensure that OpenIAM containers are up and running and are in healthy state

To execute IDM tests

```
gradle bddIDMTest -Phostname=<OpenIAM_Host_IP> -Ptimeout_level=<Timeout Level in Integer> -Pwebdriver.driver=<chrome/firefox/edge/ie/safari>
```

To execute IAM tests

```
gradle bddIAMTest -Phostname=<OpenIAM_Host_IP> -Ptimeout_level=<Timeout Level in Integer> -Pwebdriver.driver=<chrome/firefox/edge/ie/safari>
```

## Delete OpenIAM containers

To delete OpenIAM containers

```
gradle tearDownOpenIAMContainers leaveDockerSwarm
```

Note: Ensure that OpenIAM containers are deleted after invoking either of IDM or IAM tests
