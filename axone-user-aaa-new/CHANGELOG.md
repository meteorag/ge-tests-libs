# UserAAA - Changelog

This document summarizes changes made to the Axone User AAA identity management project, maintained by the MDH Axone Platform team.

## Version: 0.1.0

### Components Used

OpenIAM Identity Manager 4.1.0-prod

### Changes
* Updated to support Multi-project builds and tests
* Added new task to download chromedriver for testing in windows,linux and mac environments
* Added new task to convert markDownToHTML
* Added tests related to Authentication Policy
* Added tests related to Password Policy
* Added tests for user management
* Added tests for auditing action events

### Known Issues

https://devcloud.swcoe.ge.com/devspace/pages/viewpage.action?spaceKey=LCSAXON&title=OpenIAM+open+issues 


