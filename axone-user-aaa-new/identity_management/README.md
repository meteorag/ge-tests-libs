Identity Management
========
This project provides design level tests implementation for the following capabilities using OpenIAM admin console.

- Creating and Managing users
- Configuring RBAC(Role Based Access Controls)
- Configuring authentication policies.

## Project Structure

```
├── build.gradle        <-- contains tasks and configurations specific to Identify Management.
├── CHANGELOG.md        <-- contains the information about the changes made in each release
├── README.md           <-- overview of the project
└── src                 <-- contains JAVA files for design level test implementation.
```

## Module

* src 
   * test (automated tests)
     * java(It defines design level tests)
     * resources has features folder(It defines requirements)

## Configurations

These Configurations has to be done in OpenIAM after deployment before execution of Identity Management tests,

### Password policy configuration

Update the password policy attribute as per GE configuration defined in SDD

### Configure the principal name to be displayed in the search results

In webconsole, go to `Administration -> System Configuration tab -> System` Add 'Principal name' in 'The columns shown in the User search results' section

### Configure challenge response QA used for password reset

In webconsole, go to `Administration -> System Configuration tab -> Password` Add 'Challenge Response Questions' in 'Password Reset Methods'  

### Create sysadmin clone user for SOAP API

In webconsole,create the clone user of sysadmin for initializing the SOAP API with the following credentials sysadmin_clone/passwd00

