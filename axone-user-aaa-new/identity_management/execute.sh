#!/usr/bin/env bash
# Script to execute Adhoc tests for Identity Management

if [[ -z "${1// }" ]]; then
	echo "Provide OpenIAM host as input. for example : http://127.0.0.1:8000"
	exit 1
fi

echo -e '\n##################################################################'
echo -e '# Assumption - Git client, maven and Chrome (v63-65) is installed'
echo -e '# Constraint - It requires OpenIAM host as input'
echo -e '##################################################################\n'

if [ "$1" == "-h" ]; then
    echo "Usage: ./`basename $0` [openiam-host]"
    exit 0
fi

exit_status=0

PS3='Please enter your choice: '
options=("Create and delete same user stress test - SOAP" "Create and delete users stress test - SOAP" "Admin Reset Password For User Test"
"Create Delete Same User Loop" "Create Users Batch Test" "Delete User Batch Test" "Login Load Test Different User" "Login Load Test Same User" 
"User Password Reset Test" "Create User Race Condition" "Delete User Race Condition" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Create and delete same user stress test - SOAP")
		echo -e "\nOption choosen - Create and delete same user stress test - SOAP"
		echo -e "Default times - 3. Configure this in com.ge.hc.lcs.axone.security.mua.adhoc.soap.Create_Delete_User_Stress_Test 'no_of_times_same_user_create_delete' \n"
            	mvn -Dtest=*Create_Delete_User_Stress_Test#createDelete_Same_User test -Dhostname=$1
		exit_status=$?
	    	break
	    	;; 
	 "Create and delete users stress test - SOAP")
		echo -e "\nOption choosen - Create and delete users stress test - SOAP"
		echo -e "Default times - 3. Configure this in com.ge.hc.lcs.axone.security.mua.adhoc.soap.Create_Delete_User_Stress_Test 'no_of_users_create_delete' \n"
            	mvn -Dtest=*Create_Delete_User_Stress_Test#create_Delete_User test -Dhostname=$1
            	exit_status=$?
		break
            	;;
        "Admin Reset Password For User Test")
		echo -e "\nOption choosen - Admin Reset Password For User Test"
		echo -e "Default times - 3. Configure this in com.ge.hc.lcs.axone.security.mua.adhoc.ui.Admin_Reset_Password_For_User_Test 'no_of_times' \n"
            	mvn -Dtest=*Admin_Reset_Password_For_User_Test#admin_Reset_Password_Test test -Dhostname=$1
            	exit_status=$?
		break
		;;
	"Create Delete Same User Loop")
		echo -e "\nOption choosen - Create Delete Same User Loop"
		echo -e "Default times - 3. Configure this in com.ge.hc.lcs.axone.security.mua.adhoc.ui.Create_Delete_Same_User_Loop 'no_of_times' \n"
            	mvn -Dtest=*Create_Delete_Same_User_Loop#create_delete_UserTest test -Dhostname=$1
            	exit_status=$?
		break
		;;
	"Create Users Batch Test")
		echo -e "\nOption choosen - Create Users Batch Test"
		echo -e "Default times - 3. Configure this in com.ge.hc.lcs.axone.security.mua.adhoc.ui.Create_Users_Batch_Test 'no_of_users' \n"
		mvn -Dtest=*Create_Users_Batch_Test#createUserBatchTest test -Dhostname=$1
		exit_status=$?
        	break
            	;;
	"Delete User Batch Test")
		echo -e "\nOption choosen - Delete User Batch Test"
		echo -e "Default times - 3. Configure this in com.ge.hc.lcs.axone.security.mua.adhoc.ui.Delete_User_Batch_Test 'no_of_users' \n"
		mvn -Dtest=*Delete_User_Batch_Test#deleteUserTest test -Dhostname=$1
		exit_status=$?
            	break
            	;;
	"Login Load Test Different User")
		echo -e "\nOption choosen - Login Load Test Different User"
		echo -e "Default times - 3. Configure this in com.ge.hc.lcs.axone.security.mua.adhoc.ui.LoginLoadTest_DifferentUser 'no_of_users' \n"
		mvn -Dtest=*LoginLoadTest_DifferentUser#loginTest test -Dhostname=$1
		exit_status=$?
            	break
            	;;	
	"Login Load Test Same User")
		echo -e "\nOption choosen - Login Load Test Same User"
		echo -e "Default times - 3. Configure this in com.ge.hc.lcs.axone.security.mua.adhoc.ui.LoginLoadTest_SameUser 'no_of_times' \n"
		mvn -Dtest=*LoginLoadTest_SameUser#loginTest test -Dhostname=$1
		exit_status=$?
            	break
            	;;
	"User Password Reset Test")
		echo -e "\nOption choosen - User Password Reset Test"
		echo -e "Default times - 3. Configure this in com.ge.hc.lcs.axone.security.mua.adhoc.ui.User_Password_Reset_Test 'no_of_times' \n"
		mvn -Dtest=*User_Password_Reset_Test#user_Reset_Password_Test test -Dhostname=$1
		exit_status=$?
            	break
            	;;
	"Create User Race Condition")
		echo -e "\nOption choosen - Create User Race Condition"
		echo -e "Default times - 3. Configure this in com.ge.hc.lcs.axone.security.mua.adhoc.ui.Create_User_Race_Condition 'no_of_users'"
		echo -e "Minimum value for 'no_of_users' should be 2 to simulate race condition  \n"
		mvn -Dtest=*Create_User_Race_Condition#createUserRaceTest test -Dhostname=$1
		exit_status=$?
            	break
            	;;
	"Delete User Race Condition")
		echo -e "\nOption choosen - Delete User Race Condition"
		echo -e "Default times - 3. Configure this in com.ge.hc.lcs.axone.security.mua.adhoc.ui.Delete_User_Race_Condition 'no_of_users'"
		echo -e "Minimum value for 'no_of_users' should be 2 to simulate race condition  \n"
		mvn -Dtest=*Delete_User_Race_Condition#deleteUserRaceTest test -Dhostname=$1
		exit_status=$?
            	break
            	;;
        "Quit")
            	break
            	;;
        *) 
		echo invalid option
		exit_status=1
		break
		;;
    esac
done

exit $exit_status
