package com.ge.hc.lcs.axone.security.iam.config;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.springframework.core.io.ClassPathResource;

import com.google.gson.Gson;

public class ConfigUtil {

	private static ConfigUtil configUtil;
	
	private ConfigUtil() {
	}
	
	public static ConfigUtil getInstance() {
		if (configUtil == null) {
			configUtil = new ConfigUtil();
		}
		return configUtil;
	}
	
	public Data getConfig() throws IOException {

		ClassPathResource resource = new ClassPathResource("testData.json");

		Reader fileReader = new FileReader(resource.getFile());
		Gson gson = new Gson();
		Data data = gson.fromJson(fileReader, Data.class);

		return data;

	}

}
