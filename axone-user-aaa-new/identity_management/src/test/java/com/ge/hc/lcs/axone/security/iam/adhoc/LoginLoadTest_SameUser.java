package com.ge.hc.lcs.axone.security.iam.adhoc;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ge.hc.lcs.axone.security.iam.pageobjects.LoginPage;

/**
 * To stress test concurrent same user login to self-service.
 */
public class LoginLoadTest_SameUser {

	// Number of users to be login concurrently in batch
	private final static int no_of_times = 3;

	/**
	 * User login to self-service portal
	 * @throws Exception 
	 * 
	 * @invocationCount - The number of times this method should be invoked which is
	 *                  number of times same user to login to selfservice
	 * @threadPoolSize - The size of the thread pool for this method. It can be any
	 *                 numeric value based on the number of concurrent executions to
	 *                 be tested
	 */
	@Test(invocationCount = no_of_times, threadPoolSize = no_of_times)
	public void loginTest() throws Exception {
		ChromeOptions options = new ChromeOptions();
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		} else {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver");
			options.addArguments("--no-sandbox");
		}
		WebDriver driver = new ChromeDriver(options);
		LoginPage loginPage = new LoginPage(driver);
		loginPage.loginAsUser("sysadmin", "passwd00");
		Assert.assertTrue(loginPage.verifyUserIsInHomePage());
		driver.quit();
	}

}