package com.ge.hc.lcs.axone.security.iam.adhoc;

import java.util.concurrent.atomic.AtomicInteger;

import org.testng.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ge.hc.lcs.axone.openiam.service.UserService;
import com.ge.hc.lcs.axone.security.iam.pageobjects.LoginPage;
import com.ge.hc.lcs.axone.security.iam.pageobjects.UserPage;

/**
 * To stress test the work flow where the user resets the password for
 * configured number of times and every time the user should be able to login
 * with updated password.
 */
public class User_Password_Reset_Test {
	// Host where OpenIAM instance is running to be tested
	private static String host = System.getProperty("hostname");
	private static UserService userService = UserService.getInstance(host + "/openiam-esb/idmsrvc");
	AtomicInteger sequence = new AtomicInteger(0);
	private String password = "Password$51";
	private static String user = "password_resetuser";
	// Number of times user resets the password
	private final static int no_of_times = 3;

	// Pre-condition - Create the user for whom the password to be reset
	@BeforeClass
	public static void createUsers() {
		Assert.assertTrue(userService.addUser("user_password_reset", "testuser", user));
	}

	/**
	 * This method resets the password for a user by user itself and verifies the same by
	 * user login to self service portal
	 * @throws Exception 
	 * 
	 * @invocationCount - The number of times this method should be invoked which is
	 *                  number of times user's password has to be reset
	 * @threadPoolSize - The size of the thread pool for this method. Since it is
	 *                 for single user thread pool size is 1.
	 */
	@Test(invocationCount = no_of_times, threadPoolSize = 1)
	public void user_Reset_Password_Test() throws Exception {
		int count = sequence.addAndGet(1);
		ChromeOptions options = new ChromeOptions();
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		} else {
			options.addArguments("--no-sandbox");
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver");
		}
		WebDriver driver = new ChromeDriver(options);
		LoginPage loginPage = new LoginPage(driver);
		loginPage.loginAsUser(user, password);
		UserPage userPage = new UserPage(driver);
		userPage.changePassword(password, "Password$6" + count);
		userPage.verifyChangePasswordByUser();
		loginPage.logOut();
		loginPage = new LoginPage(driver);
		loginPage.verifyUserLoginAfterPasswordReset(user, "Password$6" + count);
		driver.quit();
		password = "Password$6" + count;
		driver.quit();
	}

	/**
	 * Deletes the user created for test
	 */
	@AfterClass
	public static void deleteUsers() {
		Assert.assertTrue(userService.deleteUserByPrincipal(user));
	}


}
