package com.ge.hc.lcs.axone.security.iam.adhoc;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import org.testng.Assert;
import org.openiam.idm.srvc.user.dto.User;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ge.hc.lcs.axone.openiam.factory.ServiceFactory;
import com.ge.hc.lcs.axone.openiam.service.UserService;
import com.ge.hc.lcs.axone.security.iam.pageobjects.LoginPage;
import com.ge.hc.lcs.axone.security.iam.pageobjects.UserPage;

public class Create_User_Race_Condition {

	// Host where OpenIAM instance is running to be tested
	private static String host = System.getProperty("hostname");
	// Number of users to be created concurrently in batch
	private final static int no_of_users = 3;
	private static String webservicehost = host + "/openiam-esb/idmsrvc";
	static UserService userService = UserService.getInstance(webservicehost);
	private static String user = "create_user_race_condition";
	AtomicInteger sequence = new AtomicInteger(0);
	static int count = 0;
	
	@BeforeClass
	public static void setUp() {
		// Verify the user to be created does not exist
		Assert.assertFalse(isUserExists(user));
	}

	/**
	 * Creates the user. Here the invocation count and thread pool size is same to simulate race condition.
	 * no_of_users should be set to minimum of 2 to simulate race condition
	 * @throws Exception
	 */
	@Test(invocationCount = no_of_users, threadPoolSize = no_of_users)
	public void createUserRaceTest() throws Exception {
		ChromeOptions options = new ChromeOptions();
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		} else {
			options.addArguments("--no-sandbox");
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver");
		}
		WebDriver driver = new ChromeDriver(options);
		LoginPage loginPage = new LoginPage(driver);
		loginPage.login("sysadmin", "passwd00", host + "/webconsole/newUser.html");
		UserPage userPage = new UserPage(driver);
		try {
			userPage.setUserInfo("Internal User", "createuser", "race_condition", user, "");
		} catch (Exception e) {
			// Calculate the number of times the execution failed
			count = sequence.addAndGet(1);
		}
		driver.quit();
	}

	/**
	 * Clean up - deletes the users created as part of test
	 */
	@AfterClass
	public static void deleteUsers() {
		// Verify only user is created only once and remaining occurrence is failure
		org.testng.Assert.assertEquals(count, no_of_users - 1);
		Assert.assertTrue(userService.deleteUserByPrincipal(user));
		// Verify User is created only once
		Assert.assertFalse(isUserExists(user));
	}

	private static boolean isUserExists(String userID) {
		org.openiam.idm.srvc.user.service.UserDataService userDataService = ServiceFactory
				.getUserDataService(webservicehost);
		User user = userDataService.getUserWithDependent(userID, new ArrayList<>());
		if (user != null) {
			return true;
		}
		return false;
	}

}
