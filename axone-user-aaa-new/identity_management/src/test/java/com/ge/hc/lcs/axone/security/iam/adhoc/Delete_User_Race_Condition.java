package com.ge.hc.lcs.axone.security.iam.adhoc;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import org.openiam.idm.srvc.user.dto.User;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ge.hc.lcs.axone.openiam.factory.ServiceFactory;
import com.ge.hc.lcs.axone.openiam.service.UserService;
import com.ge.hc.lcs.axone.security.iam.pageobjects.LoginPage;
import com.ge.hc.lcs.axone.security.iam.pageobjects.UserPage;

public class Delete_User_Race_Condition {

	// Host where OpenIAM instance is running to be tested
	private static String host = System.getProperty("hostname");
	// Number of users to be deleted in parallel for race condition
	private final static int no_of_users = 3;
	private static String webservicehost = host + "/openiam-esb/idmsrvc";
	static UserService userService = UserService.getInstance(webservicehost);
	private static String user = "deleteuser_race_condition";
	private static String userID = "";
	AtomicInteger sequence = new AtomicInteger(0);
	static int count = 0;

	/**
	 * Pre-condition - Creates the user to be used for test
	 */
	@BeforeClass
	public static void createUsers() {
		Assert.assertTrue(userService.addUser("delete", "user", user));
		userID = userService.getUserId(user);
	}

	/**
	 * Deletes the user. Here the invocation count and thread pool size is same to simulate race condition.
	 * no_of_users should be set to minimum of 2 to simulate race condition
	 * @throws Exception
	 */
	@Test(invocationCount = no_of_users, threadPoolSize = no_of_users)
	public void deleteUserRaceTest() throws Exception {
		ChromeOptions options = new ChromeOptions();
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		} else {
			options.addArguments("--no-sandbox");
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver");
		}
		WebDriver driver = new ChromeDriver(options);
		LoginPage loginPage = new LoginPage(driver);
		loginPage.login("sysadmin", "passwd00", host + String.format("/webconsole/editUser.html?id=%s", userID));
		UserPage userPage = new UserPage(driver);
		try {
			userPage.executeDelete();
		} catch (Exception e) {
			// Calculate the number of times the execution failed
			count = sequence.addAndGet(1);
		}
		driver.quit();
	}

	/**
	 * Clean up - verify the user is deleted
	 */
	@AfterClass
	public static void deleteUsers() {
		// Verify only user is deleted only once and remaining occurrence is failure
		Assert.assertEquals(count, no_of_users - 1);
		// Verify User is created only once
		Assert.assertFalse(isUserExists(user));
	}

	private static boolean isUserExists(String userID) {
		org.openiam.idm.srvc.user.service.UserDataService userDataService = ServiceFactory
				.getUserDataService(webservicehost);
		User user = userDataService.getUserWithDependent(userID, new ArrayList<>());
		if (user != null) {
			return true;
		}
		return false;
	}

}
