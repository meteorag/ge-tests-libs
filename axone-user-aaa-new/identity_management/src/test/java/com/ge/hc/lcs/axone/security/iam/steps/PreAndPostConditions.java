package com.ge.hc.lcs.axone.security.iam.steps;

import java.util.List;

import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.BeforeStories;
import org.junit.Assert;

import com.ge.hc.lcs.axone.openiam.factory.ServiceFactory;
import com.ge.hc.lcs.axone.openiam.service.RoleService;
import com.ge.hc.lcs.axone.openiam.service.UserService;
import com.ge.hc.lcs.axone.security.iam.config.ConfigDTO;
import com.ge.hc.lcs.axone.security.iam.config.ConfigUtil;
import com.ge.hc.lcs.axone.security.iam.config.Data;
import com.ge.hc.lcs.axone.security.iam.config.UserDTO;

import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class PreAndPostConditions {

	private RoleService roleService;
	private UserService userService;

	public PreAndPostConditions() throws Exception {
		EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();		
		// cloneAdminUserName has to be created manually by copying the access of sysadmin.
		ServiceFactory.init(System.getProperty("hostname"), variables.getProperty("cloneAdminUserName"), variables.getProperty("cloneAdminPassword"));		
		userService = UserService.getInstance();
		roleService=RoleService.getInstance();
	}

	@BeforeStories
	public void beforeStories() throws Exception {
		setUpforRoleScenarios();
		setUpforUserScenarios();
		setUpforLoginLogoutAuditScenarios();
		setUpforPasswordPolicyScenarios();
	}

	private void setUpforRoleScenarios() throws Exception {
		List<String> roles = ConfigUtil.getInstance().getConfig().configDTO.getRoles();
		roles.forEach(role -> Assert.assertTrue(roleService.addRole(role)));
		Data config = ConfigUtil.getInstance().getConfig();
		ConfigDTO configDTO = config.configDTO;
		UserDTO roleUser1 = configDTO.getRoleUser1();
		UserDTO roleUser2 = configDTO.getRoleUser2();
		UserDTO roleUser3 = configDTO.getRoleUser3();
		UserDTO roleUser4 = configDTO.getRoleUser4();		
		Assert.assertTrue(userService.addUser(roleUser1.firstName, roleUser1.lastName, roleUser1.loginID));
		Assert.assertTrue(userService.addUser(roleUser2.firstName, roleUser2.lastName, roleUser2.loginID));
		Assert.assertTrue(userService.addUserWithRole(roleUser3.firstName, roleUser3.lastName, roleUser3.loginID, "GE Administrator"));
		Assert.assertTrue(userService.addUserWithRole(roleUser4.firstName, roleUser4.lastName, roleUser4.loginID, "IT Biomed Administrator"));
	}
	
	private void setUpforUserScenarios() throws Exception {
		Data config = ConfigUtil.getInstance().getConfig();
		ConfigDTO configDTO = config.configDTO;
		UserDTO userToDelete = configDTO.getDeteleUser();
		userService.addUser(userToDelete.firstName, userToDelete.lastName, userToDelete.loginID);
		UserDTO userToModify = configDTO.getModifyUser();
		userService.addUser(userToModify.firstName, userToModify.lastName, userToModify.loginID);
		UserDTO userToPasswordResetAdmin = configDTO.getUserPasswordResetAdmin();
		userService.addUser(userToPasswordResetAdmin.firstName, userToPasswordResetAdmin.lastName, userToPasswordResetAdmin.loginID);
		UserDTO userToPasswordResetSelf = configDTO.getUserPasswordResetSelf();
		userService.addUser(userToPasswordResetSelf.firstName, userToPasswordResetSelf.lastName, userToPasswordResetSelf.loginID);		
		UserDTO userToSearch = configDTO.getSearchUser();
		userService.addUser(userToSearch.firstName, userToSearch.lastName, userToSearch.loginID,userToSearch.email);
		UserDTO userToPasswordResetLog = configDTO.getUserResetPasswordLog();
		userService.addUser(userToPasswordResetLog.firstName, userToPasswordResetLog.lastName, userToPasswordResetLog.loginID);
		UserDTO userToPasswordResetSelfLog = configDTO.getUserChangePasswordLog();
		userService.addUser(userToPasswordResetSelfLog.firstName, userToPasswordResetSelfLog.lastName, userToPasswordResetSelfLog.loginID);
		UserDTO userForgotPwd = configDTO.getForgotPasswordUser();
		userService.addUser(userForgotPwd.firstName, userForgotPwd.lastName, userForgotPwd.loginID);
		UserDTO userForgotPwdLog = configDTO.getLogForgotPasswordUser();
		userService.addUser(userForgotPwdLog.firstName, userForgotPwdLog.lastName, userForgotPwdLog.loginID);
		UserDTO deletedUserToSearch = configDTO.getDeletedUserForSearch();
		userService.addUser(deletedUserToSearch.firstName, deletedUserToSearch.lastName, deletedUserToSearch.loginID);			
		UserDTO userToNonCompliancePasswordReset = configDTO.getUserNonCompliancePassword();
		userService.addUser(userToNonCompliancePasswordReset.firstName, userToNonCompliancePasswordReset.lastName, userToNonCompliancePasswordReset.loginID);
		UserDTO userInvalidModify = configDTO.getInvalidModifyUser();
		userService.addUser(userInvalidModify.firstName, userInvalidModify.lastName, userInvalidModify.loginID);
		UserDTO authenticationPolicyUser=configDTO.getAuthenticationPolicyUser();
		userService.addUser(authenticationPolicyUser.firstName, authenticationPolicyUser.lastName, authenticationPolicyUser.loginID);
		UserDTO userForgotPwdInvalid1 = configDTO.getInvalidForgotPwdUser1();
		userService.addUser(userForgotPwdInvalid1.firstName, userForgotPwdInvalid1.lastName, userForgotPwdInvalid1.loginID);
		UserDTO userForgotPwdInvalid2 = configDTO.getInvalidForgotPwdUser2();
		userService.addUser(userForgotPwdInvalid2.firstName, userForgotPwdInvalid2.lastName, userForgotPwdInvalid2.loginID);
	}

	private void setUpforLoginLogoutAuditScenarios() throws Exception {
		Data config = ConfigUtil.getInstance().getConfig();
		ConfigDTO configDTO = config.configDTO;
		UserDTO userLoginLogoutLog = configDTO.getUserLoginLogoutLog();
		Assert.assertTrue(userService.addUser(userLoginLogoutLog.firstName, userLoginLogoutLog.lastName,
				userLoginLogoutLog.loginID));
		UserDTO logOutUser = configDTO.getLogoutUser();
		Assert.assertTrue(userService.addUser(logOutUser.firstName, logOutUser.lastName,
				logOutUser.loginID));
	}
	
	private void setUpforPasswordPolicyScenarios() throws Exception {
		Data config = ConfigUtil.getInstance().getConfig();
		ConfigDTO configDTO = config.configDTO;
		UserDTO userPwdPolicy = configDTO.getUserPasswordPolicy();
		Assert.assertTrue(userService.addUser(userPwdPolicy.firstName, userPwdPolicy.lastName,userPwdPolicy.loginID));
	}
	
	@AfterStories
	public void afterStories() throws Exception {
		cleanUpForRoleScenarios();
		cleanUpForUserScenarios();
		cleanUpForLoginLogoutAuditScenarios();
		cleanUpForPasswordPolicyScenarios();
	}

	private void cleanUpForRoleScenarios() throws Exception {
		Data config = ConfigUtil.getInstance().getConfig();
		ConfigDTO configDTO = config.configDTO;
		UserDTO roleUser1 = configDTO.getRoleUser1();
		UserDTO roleUser2 = configDTO.getRoleUser2();
		UserDTO roleUser3 = configDTO.getRoleUser3();
		UserDTO roleUser4 = configDTO.getRoleUser4();
		Assert.assertTrue(userService.deleteUserByPrincipal(roleUser1.loginID));
	    Assert.assertTrue(userService.deleteUserByPrincipal(roleUser2.loginID));
		Assert.assertTrue(userService.deleteUserByPrincipal(roleUser3.loginID));
		Assert.assertTrue(userService.deleteUserByPrincipal(roleUser4.loginID));
		List<String> roles = ConfigUtil.getInstance().getConfig().configDTO.getRoles();
		roles.forEach(role -> Assert.assertTrue(roleService.deleteRole(role)));
	}
	
	private void cleanUpForUserScenarios() throws Exception {
		ConfigDTO configDTO = ConfigUtil.getInstance().getConfig().configDTO;
		userService.deleteUserByPrincipal(configDTO.getCreateUser().loginID);
		userService.deleteUserByPrincipal(configDTO.getCreateUserWithDot().loginID);
		userService.deleteUserByPrincipal(configDTO.getDeteleUser().loginID);
		userService.deleteUserByPrincipal(configDTO.getModifyUser().loginID);
		userService.deleteUserByPrincipal(configDTO.getLogUser().loginID);
		userService.deleteUserByPrincipal(configDTO.getUserPasswordResetAdmin().loginID);
		userService.deleteUserByPrincipal(configDTO.getUserPasswordResetSelf().loginID);
		userService.deleteUserByPrincipal(configDTO.getUserNonCompliancePassword().loginID);
		userService.deleteUserByPrincipal(configDTO.getSearchUser().loginID);
		userService.deleteUserByPrincipal(configDTO.getCreateUserWithRole().loginID);
		userService.deleteUserByPrincipal(configDTO.getUserResetPasswordLog().loginID);
		userService.deleteUserByPrincipal(configDTO.getUserChangePasswordLog().loginID);
		userService.deleteUserByPrincipal(configDTO.getForgotPasswordUser().loginID);
		userService.deleteUserByPrincipal(configDTO.getChallengeResponseUser().loginID);
		userService.deleteUserByPrincipal(configDTO.getLogForgotPasswordUser().loginID);
		userService.deleteUserByPrincipal(configDTO.getDeletedUserForSearch().loginID);
		userService.deleteUserByPrincipal(configDTO.getInvalidModifyUser().loginID);
        userService.deleteUserByPrincipal(configDTO.getAuthenticationPolicyUser().loginID);
        userService.deleteUserByPrincipal(configDTO.getInvalidForgotPwdUser1().loginID);
        userService.deleteUserByPrincipal(configDTO.getInvalidForgotPwdUser2().loginID);
	}
	
	private void cleanUpForLoginLogoutAuditScenarios() throws Exception {
		ConfigDTO configDTO = ConfigUtil.getInstance().getConfig().configDTO;
		userService.deleteUserByPrincipal(configDTO.getUserLoginLogoutLog().loginID);
		userService.deleteUserByPrincipal(configDTO.getLogoutUser().loginID);
	}
	
	private void cleanUpForPasswordPolicyScenarios() throws Exception {
		ConfigDTO configDTO = ConfigUtil.getInstance().getConfig().configDTO;
		userService.deleteUserByPrincipal(configDTO.getUserPasswordPolicy().loginID);
	}
}
