
package com.ge.hc.lcs.axone.security.iam.config;

import java.util.List;

public class ConfigDTO {

	public List<UserDTO> userDTOs;
	private List<String> roles;

	public List<String> getRoles() {
		return roles;
	}

	public List<UserDTO> getUserDTOs() {
		return userDTOs;
	}

	public UserDTO getCreateUser() {
		return userDTOs.get(0);
	}

	public UserDTO getDeteleUser() {
		return userDTOs.get(1);
	}

	public UserDTO getModifyUser() {
		return userDTOs.get(2);
	}

	public UserDTO getRoleUser() {
		return userDTOs.get(3);
	}

	public UserDTO getLogUser() {
		return userDTOs.get(4);
	}

	public UserDTO getRoleUser1() {
		return userDTOs.get(5);
	}

	public UserDTO getRoleUser2() {
		return userDTOs.get(6);
	}

	public UserDTO getRoleUser3() {
		return userDTOs.get(7);
	}

	public UserDTO getRoleUser4() {
		return userDTOs.get(12);
	}

	public UserDTO getUserPasswordResetAdmin() {
		return userDTOs.get(8);
	}

	public UserDTO getUserPasswordResetSelf() {
		return userDTOs.get(9);
	}

	public UserDTO getUserNonCompliancePassword() {
		return userDTOs.get(10);
	}

	public UserDTO getSearchUser() {
		return userDTOs.get(11);
	}

	public UserDTO getCreateUserWithRole() {
		return userDTOs.get(13);
	}

	public UserDTO getUserResetPasswordLog() {
		return userDTOs.get(14);
	}

	public UserDTO getUserChangePasswordLog() {
		return userDTOs.get(15);
	}

	public UserDTO getForgotPasswordUser() {
		return userDTOs.get(16);
	}

	public UserDTO getChallengeResponseUser() {
		return userDTOs.get(17);
	}

	public UserDTO getLogForgotPasswordUser() {
		return userDTOs.get(18);
	}
	public UserDTO getDeletedUserForSearch() {
		return userDTOs.get(19);
	}

	public UserDTO getInvalidModifyUser() {
		return userDTOs.get(20);
	}

	public UserDTO getUserLoginLogoutLog() {
 		return userDTOs.get(21);
 	}

	public UserDTO getUserPasswordPolicy() {
 		return userDTOs.get(22);
 	}

	public UserDTO getCreateUserWithDot() {
		return userDTOs.get(23);
	}

	public UserDTO getAuthenticationPolicyUser() {
		return userDTOs.get(24);
	}	

	public UserDTO getLogoutUser() {
		return userDTOs.get(25);
	}
	
	public UserDTO getInvalidForgotPwdUser1() {
		return userDTOs.get(26);
	}
	
	public UserDTO getInvalidForgotPwdUser2() {
		return userDTOs.get(27);
	}
}

