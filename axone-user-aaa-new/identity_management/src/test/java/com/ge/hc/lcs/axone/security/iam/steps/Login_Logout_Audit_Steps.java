package com.ge.hc.lcs.axone.security.iam.steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import com.ge.hc.lcs.axone.security.iam.mua.constants.UIComponentConstants;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.LogViewerPage;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.LoginPage;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.MenuPage;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.UserPage;
import com.ge.hc.lcs.axone.security.iam.mua.utilities.Utility;

import net.thucydides.core.steps.ScenarioSteps;

public class Login_Logout_Audit_Steps extends ScenarioSteps {

	private static final long serialVersionUID = 1L;
	private LoginPage loginPage;
	private MenuPage menuPage;
	private UserPage userPage;
	private String startTime;
	private String endTime;
	private LogViewerPage logViewerPage;
	private String targetUser;
	private String action;
	private String actionStatus;

	@Given("an administrator user <openiam_admin_user> with password <password> is logged into user management portal")
	public void givenAnAdministratorUseropeniam_admin_userWithPasswordpasswordIsLoggedIntoUserManagementPortal(
			@Named("openiam_admin_user") String login, @Named("password") String password) throws Exception {
		targetUser = login;
		action = "LOGIN";
		actionStatus = "SUCCESS";
		startTime = Utility.getCurrentTimeStamp();
		loginPage.loginAsAdmin(login, password);
		Assert.assertTrue(userPage.verifyUserIsInHomePage());
		endTime = Utility.getCurrentTimeStamp();
	}

	@Given("an administrator user <openiam_admin_user> with password <admin_password> is logged into user management portal to view logs")
	public void givenAnAdministratorUserIsLoggedIntoUserManagementPortal(
			@Named("openiam_admin_user") String login, @Named("admin_password") String password) throws Exception {
		String Url = System.getProperty(UIComponentConstants.HOSTNAME) + UIComponentConstants.WEBCONSOLE;
		userPage.navigateToLoginPage(Url);
		loginPage.login(login, password);
		Assert.assertTrue(userPage.verifyUserIsInHomePage());
	}

	@Given("succeeds to login to user management portal in the subsequent attempt to view logs")
	public void succeedsToLoginToUserManagementPortalnTheSubsequentAttempt(
			@Named("openiam_admin_user") String login, @Named("admin_password") String password) throws Exception {
		loginPage.loginAsAdmin(login, password);
		Assert.assertTrue(userPage.verifyUserIsInHomePage());
	}

	@Given("logs again to user management portal to view logs")
	public void logsAgainToUserManagementPortalToViewLogs(
			@Named("openiam_admin_user") String login, @Named("admin_password") String password) throws Exception {
		loginPage.loginAsAdmin(login, password);
		Assert.assertTrue(userPage.verifyUserIsInHomePage());
	}

	@When("the administrator user retrieves the audit logs in user management portal")
	public void whenTheAdministratorUserRetrievesTheAuditLogsInUserManagementPortal() throws Exception {
		menuPage.selectLogViewerMenu();
		logViewerPage.searchLogs(startTime, endTime, action, actionStatus);
	}

	@Then("a log exists for successful login of administrator user into user management portal")
	public void thenALogExistsForSuccessfulLoginOfAdministratorUserIntoUserManagementPortal() throws Exception {
		verifyLogGenerated();
	}

	@Given("a user with login ID <login_id> and password <password> is logged into self service portal")
	public void givenAUserWithloginlogin_idAndPasswordpasswordIsLoggedIntoSelfServicePortal(
			@Named("login_id") String login, @Named("password") String password) throws Exception {
		action = "LOGIN";
		actionStatus = "SUCCESS";
		targetUser = login;
		startTime = Utility.getCurrentTimeStamp();
		loginPage.loginAsUser(login, password);		
		Assert.assertTrue(userPage.verifyUserIsInHomePage());
		endTime = Utility.getCurrentTimeStamp();
		loginPage.logOut();
		loginPage.verifyUserLogout();
	}

	@Then("a log exists for successful login of user into self service portal")
	public void thenALogExistsForSuccessfulLoginOfUserIntoSelfServicePortal() throws Exception {
		verifyLogGenerated();
	}

	@Given("an administrator user <openiam_admin_user> is logged out from user management portal")
	public void givenAnAdministratorUseropeniam_admin_userIsLoggedOutFromUserManagementPortal(
			@Named("openiam_admin_user") String login, @Named("admin_password") String password) {
		targetUser = login;
		action = "LOGOUT";
		actionStatus = "SUCCESS";
		loginPage.loginAsAdmin(login, password);
		Assert.assertTrue(userPage.verifyUserIsInHomePage());
		startTime = Utility.getCurrentTimeStamp();
		loginPage.logOut();
		loginPage.verifyUserLogout();
		endTime = Utility.getCurrentTimeStamp();
	}

	@Then("a log exists for successful logout of administrator user from user management portal")
	public void thenALogExistsForSuccessfulLogoutOfAdministratorUserFromUserManagementPortal() throws Exception {
		verifyLogGenerated();
	}

	@Given("a user with login ID <login_id> is logged out from self service portal")
	public void givenAUserWithloginlogin_idIsLoggedOutFromSelfServicePortal(@Named("login_id") String login,
			@Named("password") String password) throws Exception {
		targetUser = login;
		action = "LOGOUT";
		actionStatus = "SUCCESS";
		loginPage.loginAsUser(login, password);		
		Assert.assertTrue(userPage.verifyUserIsInHomePage());
		startTime = Utility.getCurrentTimeStamp();
		loginPage.logOut();
		loginPage.verifyUserLogout();
		endTime = Utility.getCurrentTimeStamp();
	}

	@Then("a log exists for successful logout of user from self service portal")
	public void thenALogExistsForSuccessfulLogoutOfUserFromSelfServicePortal() throws Exception {
		verifyLogGenerated();
	}

	@Given("an adminstrator user <openiam_admin_user> has failed to login to user management portal at the first attempt")
	public void givenAnAdministratorUseropeniam_admin_userHasFailedToLoginToUserManagementPortal(@Named("openiam_admin_user") String login) {
		action = "LOGIN";
		targetUser = "";
		actionStatus = "FAILURE";
		startTime = Utility.getCurrentTimeStamp();
		loginPage.loginAsAdmin(login, "invalid");
		loginPage.verifyErrorMessage("Invalid Login");
		endTime = Utility.getCurrentTimeStamp();
	}

	@Then("a log exists for failed login attempt of user into user management portal")
	public void thenALogExistsForFailedLoginAttemptOfUserIntoUserManagementPortal() throws Exception {
		verifyLogGenerated();
	}

	@Given("a user with login ID <login_id> has failed to login to self service portal")
	public void givenAUserWithloginlogin_idHasFailedToLoginToSelfServicePortal(@Named("login_id") String login) {
		action = "LOGIN";
		actionStatus = "FAILURE";
		targetUser = "";
		startTime = Utility.getCurrentTimeStamp();
		loginPage.loginAsUser(login, "invalid");
		loginPage.verifyErrorMessage("Invalid Login");
		endTime = Utility.getCurrentTimeStamp();
	}

	@Then("a log exists for failed login attempt of user into self service portal")
	public void thenALogExistsForFailedLoginAttemptOfUserIntoSelfServicePortal() throws Exception {
		verifyLogGenerated();
	}

	private void verifyLogGenerated() throws Exception {
		Assert.assertTrue(logViewerPage.verifyLogs(targetUser, action, actionStatus, startTime, endTime));
		startTime = "";
		endTime = "";
		targetUser = "";
		action = "";
		actionStatus = "";
	}

}
