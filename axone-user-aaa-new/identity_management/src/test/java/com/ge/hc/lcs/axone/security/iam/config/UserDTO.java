package com.ge.hc.lcs.axone.security.iam.config;

public class UserDTO {
	
	public String firstName;
	public String middleName;
	public String lastName;
	public String userType;
	public String password;
	public String email;
	public String phoneNumber;
	public String loginID;
	public String challengeRespQA;

}
