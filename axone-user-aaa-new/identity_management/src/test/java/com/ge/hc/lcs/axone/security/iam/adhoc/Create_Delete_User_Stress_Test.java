package com.ge.hc.lcs.axone.security.iam.adhoc;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ge.hc.lcs.axone.openiam.service.UserService;

/**
 * Stress tests to create and delete users for configured number of times.
 *
 */
public class Create_Delete_User_Stress_Test {

	private String webservicehost = System.getProperty("hostname") + "/openiam-esb/idmsrvc/";
	UserService userService = UserService.getInstance(webservicehost);
	private final static int no_of_times_same_user_create_delete = 3;
	private final static int no_of_users_create_delete = 3;

	/**
	 * Create and delete same user for the configured number of times
	 */
	@Test
	public void createDelete_Same_User() {
		for (int i = 0; i < no_of_times_same_user_create_delete; i++) {
			Assert.assertTrue(userService.addUser("create", "delete_user", "create_delete_user"));
			Assert.assertTrue(userService.deleteUserByPrincipal("create_delete_user"));
		}
	}

	/**
	 * Create and delete users for the configured number of times
	 */
	@Test
	public void create_Delete_User() {
		// Stress test for creating user
		for (int i = 0; i < no_of_users_create_delete; i++) {
			Assert.assertTrue(userService.addUser("create", "delete_user", "create_delete_user" + i));
		}
		// Stress test for deleting user
		for (int i = 0; i < no_of_users_create_delete; i++) {
			Assert.assertTrue(userService.deleteUserByPrincipal("create_delete_user" + i));
		}
	}

}
