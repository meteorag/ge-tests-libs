package com.ge.hc.lcs.axone.security.iam.steps;

import java.util.Map;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.junit.Assert;

import com.ge.hc.lcs.axone.openiam.enums.PolicyAttributeEnum;
import com.ge.hc.lcs.axone.security.iam.config.ConfigUtil;
import com.ge.hc.lcs.axone.security.iam.mua.constants.UIComponentConstants;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.AuthenticationPolicyPage;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.LoginPage;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.MenuPage;
import com.ge.hc.lcs.axone.security.iam.mua.utilities.Utility;
import com.ge.hc.lcs.axone.security.iam.utilities.SOAPUtility;


public class AuthenticationPolicySteps {

	private MenuPage menuPage;
	private LoginPage loginPage;
	private AuthenticationPolicyPage authenticationPolicyPage;
	private boolean updatePolicyResult=false;
	private String updatePolicyErrorMessage="Authentication Policy Attribute name Passed by examples table may not be matching!!!";
	private final String DEFAULT_AUTHENTICATION_POLICY_NAME="Default Authn Policy";
	private String defaultFailedAuthCount;
	private String defaultAutoUnlockTime;

	@Given("the authentication policy exists with the below attributes $attributes")
	public void givenTheAuthenticationPolicyExistsWithAttributes(ExamplesTable attributes)throws Exception{
		for (Map<String, String> row : attributes.getRows()) {
			this.setDefaultFailedAuthCount(row.get("FAILED_AUTH_COUNT"));
			this.setDefaultAutoUnlockTime(row.get("AUTO_UNLOCK_TIME"));
			Assert.assertEquals(this.getDefaultFailedAuthCount(),SOAPUtility.getAuthenticationPolicyAttributeValue(PolicyAttributeEnum.FAILED_AUTH_COUNT).getValue1());
			Assert.assertEquals(this.getDefaultAutoUnlockTime(),SOAPUtility.getAuthenticationPolicyAttributeValue(PolicyAttributeEnum.AUTO_UNLOCK_TIME).getValue1());
		}

	}
	@Given("the administrator user <openiam_admin_user> updates failed authentication count attribute with <failed_auth_count> in the authentication policy")
	public void givenTheAuthorizedUserUpdatesfailedAuthenticationCountFieldValue(@Named("openiam_admin_user") String userName,@Named("failed_auth_count") String value) throws Exception {
		loginPage.loginAsAdmin(userName,
				 ConfigUtil.getInstance().getConfig().adminPwd);
		menuPage.selectAuthenticationPolicyMenu();
		authenticationPolicyPage.searchAuthenticationPolicy(DEFAULT_AUTHENTICATION_POLICY_NAME);
		updatePolicyResult=authenticationPolicyPage.updateAuthenticationPolicy(UIComponentConstants.AuthenticationPolicyAttribute.FAILED_AUTH_COUNT, value);
		loginPage.logOut();
	}

	@When("the user updates failed authentication count attribute with <failed_auth_count> in the authentication policy")
	public void whenTheAuthorizedUserUpdatesfailedAuthenticationCountFieldValue(@Named("failed_auth_count") String value) throws Exception {
		setStartDateTimeInLogViewer();
		menuPage.selectAuthenticationPolicyMenu();
		authenticationPolicyPage.searchAuthenticationPolicy(DEFAULT_AUTHENTICATION_POLICY_NAME);
		updatePolicyResult=authenticationPolicyPage.updateAuthenticationPolicy(UIComponentConstants.AuthenticationPolicyAttribute.FAILED_AUTH_COUNT, value);
	}

	@When("the user updates auto unlock time attribute with <auto_unlock_time> in the authentication policy")
	public void whenTheAuthorizedUserUpdatesAutoUnlockTimeFieldValue(@Named("auto_unlock_time") String value) throws Exception {
		setStartDateTimeInLogViewer();
		menuPage.selectAuthenticationPolicyMenu();
		authenticationPolicyPage.searchAuthenticationPolicy(DEFAULT_AUTHENTICATION_POLICY_NAME);
		updatePolicyResult=authenticationPolicyPage.updateAuthenticationPolicy(UIComponentConstants.AuthenticationPolicyAttribute.AUTO_UNLOCK_TIME, value);
	}

	@When("the user updates failed authentication count attribute with invalid value as <invalid_failed_auth_count> in the authentication policy")
	public void whenTheAuthorizedUserUpdatesfailedAuthenticationCountFieldWithInvalidValue(@Named("invalid_failed_auth_count") String value) throws Exception {
		menuPage.selectAuthenticationPolicyMenu();
		authenticationPolicyPage.searchAuthenticationPolicy(DEFAULT_AUTHENTICATION_POLICY_NAME);
		updatePolicyResult=authenticationPolicyPage.updateAuthenticationPolicyWithInvalidValues(UIComponentConstants.AuthenticationPolicyAttribute.FAILED_AUTH_COUNT, value);
	}

	@When("the user updates auto unlock time attribute with invalid value as <invalid_auto_unlock_time> in the authentication policy")
	public void whenTheAuthorizedUserUpdatesAutoUnlockTimeFieldWithInvalidValue(@Named("invalid_auto_unlock_time") String value) throws Exception {
		menuPage.selectAuthenticationPolicyMenu();
		authenticationPolicyPage.searchAuthenticationPolicy(DEFAULT_AUTHENTICATION_POLICY_NAME);
		updatePolicyResult=authenticationPolicyPage.updateAuthenticationPolicyWithInvalidValues(UIComponentConstants.AuthenticationPolicyAttribute.AUTO_UNLOCK_TIME, value);
	}

	@Then("the failed authentication count attribute is configured successfully with <failed_auth_count>")
	public void thenFailedAuthCountValueIsConfiguredSuccessfullyWithvalue(@Named("failed_auth_count") String failedAuthCount) throws Exception {
		menuPage.selectAuthenticationPolicyMenu();
		authenticationPolicyPage.searchAuthenticationPolicy(DEFAULT_AUTHENTICATION_POLICY_NAME);
		Assert.assertTrue(updatePolicyErrorMessage, updatePolicyResult);
		Assert.assertTrue(authenticationPolicyPage.verifyAttributeValue(UIComponentConstants.AuthenticationPolicyAttribute.FAILED_AUTH_COUNT,failedAuthCount,DEFAULT_AUTHENTICATION_POLICY_NAME));
		//Reverting to Default Values
		authenticationPolicyPage.updateAuthenticationPolicy(UIComponentConstants.AuthenticationPolicyAttribute.FAILED_AUTH_COUNT,this.getDefaultFailedAuthCount());
	}

	@Then("the auto unlock time attribute is configured successfully with <auto_unlock_time>")
	public void thenAuthCountValueIsConfiguredSuccessfullyWithvalue(@Named("auto_unlock_time") String autoUnlockTime) throws Exception {
		menuPage.selectAuthenticationPolicyMenu();
		authenticationPolicyPage.searchAuthenticationPolicy(DEFAULT_AUTHENTICATION_POLICY_NAME);
		Assert.assertTrue(updatePolicyErrorMessage, updatePolicyResult);
		Assert.assertTrue(authenticationPolicyPage.verifyAttributeValue(UIComponentConstants.AuthenticationPolicyAttribute.AUTO_UNLOCK_TIME,autoUnlockTime,DEFAULT_AUTHENTICATION_POLICY_NAME));
		//Reverting to Default Values
		authenticationPolicyPage.updateAuthenticationPolicy(UIComponentConstants.AuthenticationPolicyAttribute.AUTO_UNLOCK_TIME,this.getDefaultAutoUnlockTime());
	}

	@Then("the system does not allow modification of failed authentication count attribute")
	public void thenSystemDoesNotAllowModificationOfFailedAuthCountAttribute()throws Exception{
		menuPage.selectAuthenticationPolicyMenu();
		authenticationPolicyPage.searchAuthenticationPolicy(DEFAULT_AUTHENTICATION_POLICY_NAME);
		Assert.assertTrue(updatePolicyErrorMessage, updatePolicyResult);
		Assert.assertTrue(authenticationPolicyPage.verifyAttributeValue(UIComponentConstants.AuthenticationPolicyAttribute.FAILED_AUTH_COUNT,this.getDefaultFailedAuthCount(),DEFAULT_AUTHENTICATION_POLICY_NAME));		
	}

	@Then("the system does not allow modification of auto unlock time attribute")
	public void thenSystemDoesNotAllowModificationOfAutoUnlockTimeAttribute()throws Exception{
		menuPage.selectAuthenticationPolicyMenu();
		authenticationPolicyPage.searchAuthenticationPolicy(DEFAULT_AUTHENTICATION_POLICY_NAME);
		Assert.assertTrue(updatePolicyErrorMessage, updatePolicyResult);
		Assert.assertTrue(authenticationPolicyPage.verifyAttributeValue(UIComponentConstants.AuthenticationPolicyAttribute.AUTO_UNLOCK_TIME,this.getDefaultAutoUnlockTime(),DEFAULT_AUTHENTICATION_POLICY_NAME));		
	}

	@Then("the system does not allow the user to login to self service portal")
	public void thenSystemDoesnotAllowUserToLogin() throws Exception
	{
	   loginPage.verifyErrorMessage("Login failed");
	}
	@Then("the account of the user is locked")
	public void thenUserAcccountisLocked()throws Exception{
	   loginPage.verifyErrorMessage("account has been locked");
	   loginPage.loginAsAdmin(ConfigUtil.getInstance().getConfig().adminUser,
				 ConfigUtil.getInstance().getConfig().adminPwd);
		menuPage.selectAuthenticationPolicyMenu();
		authenticationPolicyPage.searchAuthenticationPolicy(DEFAULT_AUTHENTICATION_POLICY_NAME);
		updatePolicyResult=authenticationPolicyPage.updateAuthenticationPolicy(UIComponentConstants.AuthenticationPolicyAttribute.FAILED_AUTH_COUNT,this.getDefaultFailedAuthCount());
		loginPage.logOut();
	}


	public String getDefaultFailedAuthCount() {
		return defaultFailedAuthCount;
	}
	public void setDefaultFailedAuthCount(String defaultFailedAuthCount) {
		this.defaultFailedAuthCount = defaultFailedAuthCount;
	}
	public String getDefaultAutoUnlockTime() {
		return defaultAutoUnlockTime;
	}
	public void setDefaultAutoUnlockTime(String defaultAutoUnlockTime) {
		this.defaultAutoUnlockTime = defaultAutoUnlockTime;
	}

	private void setStartDateTimeInLogViewer() {
		LogViewerSteps.setStartDateTime(Utility.getCurrentTimeStamp());
	}

}
