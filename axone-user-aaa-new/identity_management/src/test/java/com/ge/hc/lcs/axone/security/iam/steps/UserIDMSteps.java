package com.ge.hc.lcs.axone.security.iam.steps;

import java.util.Arrays;

import org.jbehave.core.annotations.Alias;
import org.jbehave.core.annotations.Aliases;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.json.JSONObject;
import org.junit.Assert;


import com.ge.hc.lcs.axone.security.iam.config.ConfigDTO;
import com.ge.hc.lcs.axone.security.iam.config.ConfigUtil;
import com.ge.hc.lcs.axone.security.iam.config.Data;
import com.ge.hc.lcs.axone.security.iam.config.UserDTO;
import com.ge.hc.lcs.axone.security.iam.mua.constants.UIComponentConstants;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.LoginPage;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.MenuPage;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.RolesPage;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.UserPage;
import com.ge.hc.lcs.axone.security.iam.mua.utilities.Utility;
import com.ge.hc.lcs.axone.security.iam.utilities.SOAPUtility;

import net.thucydides.core.steps.ScenarioSteps;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class UserIDMSteps extends ScenarioSteps {

	private static final long serialVersionUID = 1L;
	private LoginPage loginPage;
	private UserPage userPage;
	private MenuPage menuPage;
	private RolesPage rolePage;
	private boolean searchResult = false;
	private String searchErrorMessage = "Search Text Passed by examples may not be matching!!!";
	private String soapPassword;
	private String tempPassword;

	@Given("an administrator user <openiam_admin_user> is logged into user management portal")
	public void checkAdminUser(@Named("openiam_admin_user") String authUser) throws Exception {
		 loginPage.loginAsAdmin(authUser,
		 ConfigUtil.getInstance().getConfig().adminPwd);
		 Assert.assertTrue(loginPage.verifyUserIsInHomePage());
	}

	@When("the administrator user creates a user with login ID <login_id>,first name <firstname> and last name <lastname>")
	public void whenAdminCreatesUser(@Named("firstname") String firstName, @Named("lastname") String lastName,
			@Named("login_id") String userLoginName) throws Exception {
		// Below variable will be used as the start date time filter while viewing audit
		// logs
		setStartDateTimeInLogViewer();
		menuPage.selectCreateUserMenu();
		userPage.createUser(firstName, lastName, userLoginName);
	}

	@Then("the user is created successfully with login ID <login_id>,first name <firstname> and last name <lastname>")
	public void verifyCreatedUser(@Named("login_id") String loginName, @Named("firstname") String firstName,
			@Named("lastname") String lastName) throws Exception {
		menuPage.selectUserAdminMenu();
		userPage.searchUserByPrincipal(loginName);
		Assert.assertTrue(userPage.verifySearchValueExistsInSearchResults(loginName));
		userPage.clickEditUser();
		Assert.assertEquals(firstName, userPage.getFirstName());
		Assert.assertEquals(lastName, userPage.getLastName());
	}

	@Given("a user with login ID <login_id> exists")
	public void checkUserExist(@Named("login_id") String user) throws Exception {
		 Assert.assertTrue(SOAPUtility.checkUserExistsBySOAP(user));
	}

	@When("the administrator user deletes the user")
	public void whenAdminDeletesUser(@Named("login_id") String user) throws Exception {
		// Below variable will be used as the start date time filter while viewing audit
		// logs
		setStartDateTimeInLogViewer();
		menuPage.selectUserAdminMenu();
		userPage.searchUserByPrincipal(user);
		userPage.clickEditUser();
		userPage.deleteUser();
	}

	@Then("the user is deleted successfully")
	public void deletesUser(@Named("login_id") String user) throws Exception {
		Assert.assertFalse(userPage.verifyUserExists(user));
	}

	@When("the administrator user modifies the first name of the user with <new_name>")
	public void whenAdminModifiesAttributeValueUser(@Named("login_id") String user, @Named("new_name") String newName)
			throws Exception {
		// Below variable will be used as the start date time filter while viewing audit
		// logs
		setStartDateTimeInLogViewer();
		menuPage.selectUserAdminMenu();
		userPage.searchUserByPrincipal(user);
		userPage.clickEditUser();
		userPage.updateFirstName(newName);
		userPage.saveUser();
	}

	@Then("the first name of the user is modified with <new_name> successfully")
	public void updateAttributeValueForUser(@Named("login_id") String loginName,@Named("new_name") String newName)
			throws Exception {
		menuPage.selectUserAdminMenu();
		userPage.searchUserByPrincipal(loginName);
		userPage.clickEditUser();
		String attrValue = userPage.getFirstName();
		Assert.assertEquals(newName, attrValue);
	}

	@When("the administrator user modifies the last name of the user with <new_name>")
	public void whenTheAdministratorUserModifiesTheLastNameOfTheUserWithnew_name(@Named("login_id") String user,
			@Named("new_name") String newName) throws Exception {
		// Below variable will be used as the start date time filter while viewing audit
		// logs
		setStartDateTimeInLogViewer();
		menuPage.selectUserAdminMenu();
		userPage.searchUserByPrincipal(user);
		userPage.clickEditUser();
		userPage.updateLastName(newName);
		userPage.saveUser();
	}

	@Then("the last name of the user is modified with <new_name> successfully")
	public void thenTheLastNameOfTheUserIsModifiedWithnew_nameSuccessfully(@Named("login_id") String loginName,@Named("new_name") String newName) throws Exception {
		menuPage.selectUserAdminMenu();
		userPage.searchUserByPrincipal(loginName);
		userPage.clickEditUser();
		String attrValue = userPage.getLastName();
		Assert.assertEquals(newName, attrValue);
	}

	@When("the administrator user creates a user with with login ID <login_id>,invalid first name <invalid_firstname> and last name <lastname>")
	public void createsUserWithInvalidFirstNameField(@Named("invalid_firstname") String firstName,
			@Named("lastname") String lastName, @Named("login_id") String userLoginName) throws Exception {
		menuPage.selectCreateUserMenu();
		userPage.createInvalidUser(firstName, lastName, userLoginName);
	}

	@When("the administrator user creates a user with login ID <login_id>,first name <firstname> and invalid last name <invalid_lastname>")
	public void createsUserWithInvalidLastNameField(@Named("firstname") String firstName,
			@Named("invalid_lastname") String lastName, @Named("login_id") String userLoginName) throws Exception {
		menuPage.selectCreateUserMenu();
		userPage.createInvalidUser(firstName, lastName, userLoginName);
	}

	@When("the administrator user updates user with invalid first name <invalid_firstname>")
	public void whenTheAdministratorUserUpdatesUserWithInvalidFirstNameinvalid_firstname(
			@Named("login_id") String userLoginName, @Named("invalid_firstname") String firstName) throws Exception {
		// Below variable will be used as the start date time filter while viewing audit
		// logs
		setStartDateTimeInLogViewer();
		menuPage.selectUserAdminMenu();
		userPage.searchUserByPrincipal(userLoginName);
		userPage.clickEditUser();
		userPage.updateFirstName(firstName);
		userPage.saveUser();
		userPage.validateFailureMsg();
	}

	@When("the administrator user updates user with invalid last name <invalid_lastname>")
	public void whenTheAdministratorUserUpdatesUserWithInvalidLastNameinvalid_firstname(
			@Named("login_id") String userLoginName, @Named("invalid_lastname") String lastName) throws Exception {
		// Below variable will be used as the start date time filter while viewing audit
		// logs
		setStartDateTimeInLogViewer();
		menuPage.selectUserAdminMenu();
		userPage.searchUserByPrincipal(userLoginName);
		userPage.clickEditUser();
		userPage.updateLastName(lastName);
		userPage.saveUser();
		userPage.validateFailureMsg();
	}
	@Then("the system does not allow modification of first name of the user")
	public void thenTheSystemDoesNotAllowModificationOfFirstname(@Named("login_id") String userLoginName) throws Exception {
		Data config = ConfigUtil.getInstance().getConfig();
		ConfigDTO configDTO = config.configDTO;
		UserDTO modifyUser = configDTO.getInvalidModifyUser();
		menuPage.selectUserAdminMenu();
		userPage.searchUserByPrincipal(userLoginName);
		userPage.clickEditUser();
		String attrValue = userPage.getFirstName();
		Assert.assertEquals(modifyUser.firstName, attrValue);
	}

	@Then("the system does not allow modification of last name of the user")
	public void thenTheSystemDoesNotAllowModificationOfLastname(@Named("login_id") String userLoginName) throws Exception {
		Data config = ConfigUtil.getInstance().getConfig();
		ConfigDTO configDTO = config.configDTO;
		UserDTO modifyUser = configDTO.getInvalidModifyUser();
		menuPage.selectUserAdminMenu();
		userPage.searchUserByPrincipal(userLoginName);
		userPage.clickEditUser();
		String attrValue = userPage.getLastName();
		Assert.assertEquals(modifyUser.lastName, attrValue);
	}

	@Then("the system does not allow creation of the user")
	public void thenTheSystemDoesNotAllowCreationOfTheUser(@Named("login_id") String user) throws Exception {
		menuPage.selectUserAdminMenu();
		Assert.assertFalse(userPage.verifyUserExists(user));
	}


	@When("the administrator user reset password to a temp password <temp_password>")
	@Alias("an administrator user <openiam_admin_user> resets password to a temp password <temp_password>")
	public void whenAuthUserResetsPassword(@Named("openiam_admin_user") String auth_user,
			@Named("login_id") String user, @Named("temp_password") String newPassword) throws Exception {
		menuPage.selectUserAdminMenu();
		userPage.searchUserByPrincipal(user);
		userPage.clickEditUser();
		menuPage.clickResetPasswordMenu();
		userPage.enterPasswordValuesByAdmin(newPassword);
	}

	@When("the administrator user resets the password of the user to <non_compliant_password> which is non-compliant to the password policy")
	@Alias("an administrator user <openiam_admin_user> resets password to a <non_compliant_password> which is non compliant to the updated password policy")
	public void whenAuthUserResetsPasswordToNonCompliantPassword(@Named("openiam_admin_user") String auth_user,
			@Named("login_id") String user, @Named("non_compliant_password") String newPassword) throws Exception {
		menuPage.selectUserAdminMenu();
		userPage.searchUserByPrincipal(user);
		userPage.clickEditUser();
		menuPage.clickResetPasswordMenu();
		userPage.enterPasswordValuesByAdmin(newPassword);
	}

	@Then("the reset password by the administrator user is successful")
	public void checkResetPassword() throws Exception {
		userPage.verifyResetPassword();
		loginPage.logOut();

	}

	@Then("the user is allowed to login with new password <temp_password>")
	public void checkLoginWithTempPassword(@Named("temp_password") String tempPassword, @Named("login_id") String user)
			throws Exception {
		String URL = System.getProperty(UIComponentConstants.HOSTNAME) + UIComponentConstants.SELFSERVICE;
		userPage.navigateToLoginPage(URL);
		loginPage.loginAsUser(user, tempPassword);
		userPage.verifyUserIsInHomePage();
	}


	@Then("the user is allowed to login with new password <new_password>")
	public void checkLoginWithNewPassword(@Named("new_password") String newPassword, @Named("login_id") String user)
			throws Exception {
		String URL = System.getProperty(UIComponentConstants.HOSTNAME) + UIComponentConstants.SELFSERVICE;
		userPage.navigateToLoginPage(URL);
		loginPage.login(user, newPassword);
		userPage.verifyUserIsInHomePage();
	}

	@Given("a user with login ID <login_id> and password <password> is logged into self service portal to change the password")
	public void checkUserLoggedIn(@Named("login_id") String user, @Named("password") String password) throws Exception {
		loginPage.loginAsUser(user, password);
	}

	@When("the user changes the password to <new_password>")
	public void whenUserResetsPassword(@Named("new_password") String newPassword)
			throws Exception {
		this.setTempPassword();
		userPage.changePassword(getTempPassword(), newPassword);
	}

	@Then("the password is changed for the user")
	public void checkResetPasswordForUser() throws Exception {
		userPage.verifyChangePasswordByUser();
		loginPage.logOut();
	}

	@When("the user changes the password to <non_compliant_password> which is non-compliant to the password policy")
	public void whenUserResetWithNonCompliantPassword(@Named("non_compliant_password") String newPassword) throws Exception {
		this.setTempPassword(); 
		userPage.changePassword(getTempPassword(), newPassword);
	}

	@Then("the password is not changed for the user")
	public void checkPasswordNotChanged() throws Exception {
		 Assert.assertTrue(userPage.verifyPasswordNotChanged());
	}

	@When("the adminstrator user searches for the user in the field <field> with the value <value>")
	public void whenSearchUser(@Named("field") UIComponentConstants.SearchBy fieldName,
			@Named("value") String searchValue) throws Exception {
		if(!(fieldName==UIComponentConstants.SearchBy.FIRSTNAME))
		menuPage.selectUserAdminMenu();
		searchResult = userPage.searchUserByValue(fieldName, searchValue);
	}

	@Then("the users with the lastname <value> is displayed in the result")
	@Aliases(values = { "the users with the firstname <value> is displayed in the result",
			"the users with the email <value> is displayed in the result" })
	public void checkUser(@Named("value") String searchValue) throws Exception {
		Assert.assertTrue(searchErrorMessage, searchResult);
		Assert.assertTrue(userPage.verifySearchValueExistsInSearchResults(searchValue));
		searchResult = false;
	}

	@Given("the role <role> exists")
	public void givenTheroleExists(@Named("role") String role) throws Exception {
		SOAPUtility.checkRolesExists(Arrays.asList(role));
	}

	@When("the administrator user creates user with login ID <login_id>,first name <firstname>,last name <lastname> and role <role>")
	public void whenCreatesUserWithRole(@Named("firstname") String firstName, @Named("lastname") String lastName,
			@Named("login_id") String userLoginName, @Named("role") String role) throws Exception {
		// Below variable will be used as the start date time filter while viewing audit
		// logs
		setStartDateTimeInLogViewer();
		menuPage.selectCreateUserMenu();
		userPage.createUserWithRole(firstName, lastName, userLoginName, role);
	}

	@Then("the role <role> is assigned to the created user")
	public void thenTherolesAreAssignedToTheCreateduser(@Named("login_id") String user,
			@Named("role") String roleName) {
		menuPage.selectUserEntitlementMenu();
		Assert.assertTrue(rolePage.isRoleEntitledForUser(user, roleName));

	}

	@Given("an administrator user <openiam_admin_user> reset the password for a user <user> with <new_password>")
	public void givenTheopeniam_admin_userResetsThePasswordForuserWithnew_password(
			@Named("openiam_admin_user") String authUser, @Named("user") String user,
			@Named("new_password") String newPassword) throws Exception {
		loginPage.loginAsAdmin(authUser, ConfigUtil.getInstance().getConfig().adminPwd);
		// Below variable will be used as the start date time filter while viewing audit
		// logs
		setStartDateTimeInLogViewer();
		menuPage.selectUserAdminMenu();
		userPage.searchUserByPrincipal(user);
		userPage.clickEditUser();
		menuPage.clickResetPasswordMenu();
		userPage.enterPasswordValuesByAdmin(newPassword);
		userPage.verifyResetPassword();
		loginPage.logOut();
		String URL = System.getProperty(UIComponentConstants.HOSTNAME) + UIComponentConstants.SELFSERVICE;
		userPage.navigateToLoginPage(URL);
		loginPage.loginAsUser(user, newPassword);
		userPage.verifyUserIsInHomePage();
	}

	@Given("a user with login ID <login_id> changed the password from <password> to <new_password> in self service portal")
	public void givenTheuserChangedThePasswordFrompasswordTonew_password(@Named("login_id") String user,
			@Named("password") String password, @Named("new_password") String newPassword) throws Exception {
		loginPage.loginAsUser(user, password);
		// Below variable will be used as the start date time filter while viewing audit
		// logs
		setStartDateTimeInLogViewer();
		userPage.verifyUserIsInHomePage();
		this.setTempPassword();
		userPage.changePassword(getTempPassword(), newPassword);
		userPage.verifyChangePasswordByUser();
		loginPage.logOut();
	}

	@Given("a user with login ID <login_id> wants to retrieve the forgotten password")
	public void givenTheuserForgotThePassword(@Named("login_id") String user) throws Exception {
		Assert.assertTrue(SOAPUtility.checkUserExistsBySOAP(user));
		this.setSoapPassword();		
		loginPage.loginAsUser(user, getSoapPassword());	
		userPage.verifyUserIsInHomePage();
		loginPage.logOut();
		String Url = System.getProperty(UIComponentConstants.HOSTNAME) + UIComponentConstants.SELFSERVICE;
		userPage.navigateToLoginPage(Url);		
		userPage.clickForgotPasswordLink();
	}

	@When("the user resets the password to <new_password> by providing the challenge response answers")
	public void whenTheuserResetsThePasswordTonew_passwordByChallengeResponseAnswers(@Named("login_id") String user,
			@Named("new_password") String newPassword) throws Exception {
		Data config = ConfigUtil.getInstance().getConfig();
		ConfigDTO configDTO = config.configDTO;
		UserDTO userForgotPwd = configDTO.getForgotPasswordUser();
		JSONObject jsonchallengeResponseQA = new JSONObject(userForgotPwd.challengeRespQA);
		userPage.resetPasswordByChallengeResponse(jsonchallengeResponseQA, user, newPassword);
	}

	@Then("the reset password by the user is successful")
	public void checkResetPasswordAfterChallengeRespAnswers() throws Exception {
		userPage.verifyResetPasswordAfterChallengeRespAnswers();

	}

	@Given("a user with login ID <login_id> is logged into selfservice portal for the first time")
	public void givenAuserWithpasswordIsLoggedInForTheFirstTime(@Named("login_id") String user,
			@Named("password") String password, @Named("challenge_response") String challengeResponseQA)
			throws Exception {
		loginPage.loginAsAdmin(ConfigUtil.getInstance().getConfig().adminUser,
				ConfigUtil.getInstance().getConfig().adminPwd);
		Data config = ConfigUtil.getInstance().getConfig();
		ConfigDTO configDTO = config.configDTO;
		UserDTO userChallengeResp = configDTO.getChallengeResponseUser();
		menuPage.selectCreateUserMenu();
		userPage.createUser(userChallengeResp.firstName, userChallengeResp.lastName, userChallengeResp.loginID);
		Assert.assertTrue(userPage.verifyUserNameInEditPage(userChallengeResp.firstName, userChallengeResp.lastName));
		menuPage.clickResetPasswordMenu();
		userPage.enterPasswordValuesByAdmin(userChallengeResp.password);
		userPage.verifyResetPassword();
		loginPage.logOut();
		String URL = System.getProperty(UIComponentConstants.HOSTNAME) + UIComponentConstants.SELFSERVICE;
		userPage.navigateToLoginPage(URL);
		loginPage.loginAsUser(user, userChallengeResp.password);
	}

	@Given("the user is prompted for challenge response questions and answers")
	public void givenTheuserIsPromptedForChallengeResponseQuestionsAndAnswers() throws Exception {
		userPage.verifyUserPromptedForChallengeRespQA();
	}

	@When("the user sets the challenge response questions and answers <challenge_response> and custom questions and answers <custom_question>")
	public void whenTheuserSetsTheChallengeResponseQuestionsAndAnswers(
			@Named("challenge_response") String challengeResponseQA, @Named("custom_question") String customQA)
			throws Exception {
		JSONObject jsonChallengeResponseQA = new JSONObject(challengeResponseQA);
		JSONObject jsonCustomQA = new JSONObject(customQA);
		userPage.setChallengeResponseQA(jsonChallengeResponseQA, jsonCustomQA);

	}

	@Then("the challenge response questions and answers are successfully set for the user")
	public void thenTheChallengeResponseQuestionsAndAnswersAreSuccessfullySetForTheuser() throws Exception {
		userPage.verifyChallengeResponseQAAreSet();
	}

	@Then("the user is allowed to access the self service portal")
	public void thenTheuserAllowedToAccessTheApplication() throws Exception {
		Assert.assertTrue(userPage.verifyUserIsInHomePage());
		loginPage.logOut();
	}

	@Given("a user with login ID <login_id> reset the password to <new_password> by providing the challenge response answers")
	public void givenTheuserResetThePasswordTonew_passwordByChallengeResponseAnswers(@Named("login_id") String user,
			@Named("new_password") String newPassword) throws Exception {
		// Below variable will be used as the start date time filter while viewing audit
		// logs
		setStartDateTimeInLogViewer();
		Assert.assertTrue(SOAPUtility.checkUserExistsBySOAP(user));
		this.setSoapPassword();
		loginPage.loginAsUser(user, getSoapPassword());
		loginPage.logOut();
		String Url = System.getProperty(UIComponentConstants.HOSTNAME) + UIComponentConstants.SELFSERVICE;
		userPage.navigateToLoginPage(Url);		
		userPage.clickForgotPasswordLink();		
		Data config = ConfigUtil.getInstance().getConfig();
		ConfigDTO configDTO = config.configDTO;
		UserDTO userForgotPwdLog = configDTO.getLogForgotPasswordUser();
		JSONObject jsonchallengeResponseQA = new JSONObject(userForgotPwdLog.challengeRespQA);
		userPage.resetPasswordByChallengeResponse(jsonchallengeResponseQA, user, newPassword);
		userPage.verifyResetPasswordAfterChallengeRespAnswers();
		loginPage.loginAsUser(user, newPassword);		
		Assert.assertTrue(userPage.verifyUserIsInHomePage());	
		loginPage.logOut();
	}

	@Given("a user with login ID <login_id> is deleted")
	public void givenAUserWithLoginIDlogin_idDoesNotExists(@Named("login_id") String user) throws Exception {
		menuPage.selectUserAdminMenu();
		userPage.searchUserByPrincipal(user);
		userPage.clickEditUser();
		userPage.deleteUser();
	}

	@Then("the search result does not display any user")
	public void thenTheSearchResultDoesNotDisplayAnyUser(@Named("value") String lastName) throws Exception {
		Assert.assertFalse(userPage.verifySearchValueExistsInSearchResults(lastName));
	}

	@Given("the administrator user adds the first name field in search criteria")
	public void givenTheAdministratorUserAddsTheFirstNameFieldInSearchCriteria() throws Exception {
		//Below text "First Name " is passed as the search text for autocomplete text box to add the first name field as the search field
		menuPage.selectUserAdminMenu();
		userPage.addSearchField("First Name");
	}

	@Then("the users with the login id <value> is displayed in the result")
	public void verifyLoginIDInSearchResults(@Named("value") String loginName) throws Exception {
		Assert.assertTrue(userPage.verifySearchValueExistsInSearchResults(loginName));
	}

	@When("the user initiates the reset password process And the user provides an incorrect challenge response answer(s) <incorrect_QA>")
	public void whenTheuserResetsThePasswordTonew_passwordByInvalidChallengeResponseAnswers(@Named("login_id") String user,
			@Named("incorrect_QA") String incorrectChallengeResponseQA) throws Exception {
		JSONObject jsonchallengeResponseQA = new JSONObject(incorrectChallengeResponseQA);
		userPage.submitIncorrectChallengeResponseQA(jsonchallengeResponseQA, user);
	}

	@Then("the user is not allowed to reset the password")
	public void thenTheuserNotAllowedToResetPassword() throws Exception {
		Assert.assertTrue(userPage.getErrorMsgForIncorrectChallengeResponseQA().contains("You answers to these questions were incorrect"));
		Assert.assertFalse(userPage.verifyUserInUnlockUserAccountPage());
	}

	@Given("the user is already made <failed_auth_count> failed login attempts")
	public void givenTheUserMadeLoginAttemptsAsPerTheInput(@Named("failed_auth_count") String value,@Named("login_id") String loginID)throws Exception{
		int numberOfAttempts=Integer.parseInt(value);
		String url = System.getProperty(UIComponentConstants.HOSTNAME) + UIComponentConstants.SELFSERVICE;
		loginPage.openURL(url);
		for (int i=1;i<numberOfAttempts;i++)
		{
			loginPage.login(loginID, "invalid");
			loginPage.verifyErrorMessage("Invalid Login");
		}

	}
	@When ("the user tries to login with invalid password <invalid_password> to self service portal")
	public void whenUserTriesLoginWithInvalidPassword(@Named("invalid_password") String invalidPassword,@Named("login_id") String user) throws Exception
    {
		loginPage.login(user, invalidPassword);
    }

	private void setStartDateTimeInLogViewer() {
		LogViewerSteps.setStartDateTime(Utility.getCurrentTimeStamp());
	}
	
	@Then("the user is allowed to access the self service portal with the new password")
	public void thenTheuserAllowedToAccessTheApplicationWithNewPassword(@Named("login_id") String user,
			@Named("new_password") String newPassword) throws Exception {
		loginPage.loginAsUser(user, newPassword);		
		Assert.assertTrue(userPage.verifyUserIsInHomePage());
	}
	
	public void setTempPassword() {		
		EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();		
		this.tempPassword= variables.getProperty("defaultNewPassword");		
	}
	
	public void setSoapPassword() {		
		EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();		
		this.soapPassword= variables.getProperty("soapDefaultPwd");		
	}
	
	public String getSoapPassword() {
		return soapPassword;
	}
	
	public String getTempPassword() {
		return tempPassword;
	}
	
}
