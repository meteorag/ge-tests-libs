package com.ge.hc.lcs.axone.security.iam.adhoc;

import java.util.concurrent.atomic.AtomicInteger;

import org.testng.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ge.hc.lcs.axone.openiam.service.UserService;
import com.ge.hc.lcs.axone.security.iam.pageobjects.LoginPage;
/**
 * To stress test concurrent user login to selfservice.
 */
public class LoginLoadTest_DifferentUser {

	// Host where OpenIAM instance is running to be tested
	private static String host = System.getProperty("hostname");
	//	Number of users to be login concurrently in batch
	private final static int no_of_users = 3;
	private static String webservicehost = host + "/openiam-esb/idmsrvc";
	static UserService userService = UserService.getInstance(webservicehost);
	AtomicInteger sequence = new AtomicInteger(0);

	/**
	 * Pre-condition - Creates the configured number of users to be used for test
	 */
	@BeforeClass
	public static void createUsers() {
		for (int i = 1; i <= no_of_users; i++) {
			Assert.assertTrue(userService.addUser("login", "user", "login_usr" + i));
		}
	}

	/**
	 * User login to selservice portal
	 * @throws Exception 
	 * @invocationCount - The number of times this method should be invoked which is
	 *                  number of users to login to selfservice
	 * @threadPoolSize - The size of the thread pool for this method. It can be any
	 *                 numeric value based on the number of concurrent executions to
	 *                 be tested
	 */
	@Test(invocationCount = no_of_users, threadPoolSize = no_of_users)
	public void loginTest() throws Exception {
		ChromeOptions options = new ChromeOptions();
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		} else {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver");
			options.addArguments("--no-sandbox");
		}
		WebDriver driver = new ChromeDriver(options);
		
		LoginPage loginPage = new LoginPage(driver);
		int count = sequence.addAndGet(1);
		loginPage.loginAsUser("login_usr" + count, "Password$51");
		Assert.assertTrue(loginPage.verifyUserIsInHomePage());
		driver.quit();
	}

	/**
	 * Clean up - deletes the users created as part of test setup
	 */
	@AfterClass
	public static void deleteUsers() {
		for (int i = 1; i <= no_of_users; i++) {
			Assert.assertTrue(userService.deleteUserByPrincipal("login_usr" + i));
		}
	}

}