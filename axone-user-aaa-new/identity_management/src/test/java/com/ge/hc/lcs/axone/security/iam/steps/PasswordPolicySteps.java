package com.ge.hc.lcs.axone.security.iam.steps;

import java.util.Map;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.junit.Assert;

import com.ge.hc.lcs.axone.openiam.enums.PolicyAttributeEnum;
import com.ge.hc.lcs.axone.security.iam.config.ConfigUtil;
import com.ge.hc.lcs.axone.security.iam.mua.constants.UIComponentConstants;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.LoginPage;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.MenuPage;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.PasswordPolicyPage;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.UserPage;
import com.ge.hc.lcs.axone.security.iam.utilities.SOAPUtility;
import com.ge.hc.lcs.axone.security.iam.mua.utilities.Utility;

public class PasswordPolicySteps {

	private MenuPage menuPage;
	private PasswordPolicyPage passwordPolicyPage;
	private UserPage userPage;
	private boolean updatePolicyResult=false;
	private String updatePolicyErrorMessage="Password Policy Attribute name is incorrect!!!";
	private String defaultMinPwdLength="";
	private String defaultMaxPwdLength="";
	private String defaultMinNumChars="";
	private String defaultMaxNumChars="";
	private String defaultHistVersion="";
	private String defaultPwdRejectChars="";
	private String defaultQACorrect="";
	private String defaultQuestCnt="";
	private String defaultPwdChangeOnReset="";
	private String defaultMinPwdExpWar="";
	private String defaultMaxPwdExpWar="";
	private String defaultPwdExpDays="";
	//As priority is not configured in the default password policy, so it is initialized here.
	private String defaultPriority="10";
	private LoginPage loginPage;

	/**
	 * Updates password policy attribute.
	 *
	 * @param attributeName the attribute name
	 * @param value1 the from value of the attribute
	 * @param value2 the to value of the attribute
	 * @param valueType the value type whether it is valid or invalid
	 * @throws Exception the exception
	 */
	private void updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute attributeName,String value1, String value2,String valueType) throws Exception {
		menuPage.selectPasswordPolicyMenu();
		passwordPolicyPage.searchAndEditPasswordPolicy(UIComponentConstants.PASSWORDPOLICY_NAME);
		if(valueType.equals("valid"))
		updatePolicyResult=passwordPolicyPage.updatePasswordPolicy(attributeName, value1,value2);
		else
		updatePolicyResult=passwordPolicyPage.updatePasswordPolicyWithInvalidValues(attributeName, value1,value2);
	}

	private void verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute attributeName, String value1,String value2) throws Exception {
		menuPage.selectPasswordPolicyMenu();
		passwordPolicyPage.searchAndEditPasswordPolicy(UIComponentConstants.PASSWORDPOLICY_NAME);
		Assert.assertTrue(updatePolicyErrorMessage, updatePolicyResult);
		Assert.assertTrue(passwordPolicyPage.verifyPasswordPolicyAttribute(attributeName, value1,value2));
	}

	@When("the user updates password length with minimum <min_password_length> and maximum <max_password_length> in the password policy")
	public void whenTheUserUpdatesPasswordLengthWithMinimummin_password_lengthAndMaximummax_password_lengthInThePasswordPolicy(@Named("min_password_length") String minLength, @Named("max_password_length") String maxLength) throws Exception {
		setStartDateTimeInLogViewer();
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.PWD_LEN, minLength,maxLength,"valid");
	}

	@Then("the password length is configured successfully with minimum <min_password_length> and maximum <max_password_length>")
	public void thenThePasswordLengthIsConfiguredSuccessfullyWithMinimummin_password_lengthAndMaximummax_password_length(@Named("min_password_length") String minLength, @Named("max_password_length") String maxLength) throws Exception {
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.PWD_LEN, minLength,maxLength);
		passwordPolicyPage.updatePasswordPolicy(UIComponentConstants.PasswordPolicyAttribute.PWD_LEN, defaultMinPwdLength,defaultMaxPwdLength);
		updatePolicyResult=false;
	}

	@When("the user updates number of numeric characters with minimum <min_numeric_chars> and maximum <max_numeric_chars> in the password policy")
	public void whenTheUserUpdatesNumberOfNumericCharactersWithMinimummin_numeric_charsAndMaximummax_numeric_charsInThePasswordPolicy(@Named("min_numeric_chars") String minChars, @Named("max_numeric_chars") String maxChars) throws Exception {
		setStartDateTimeInLogViewer();
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.NUMERIC_CHARS, minChars,maxChars,"valid");
	}

	@Then("the number of numeric characters is configured successfully with minimum <min_numeric_chars> and maximum <max_numeric_chars>")
	public void thenTheNumberOfNumericCharactersIsConfiguredSuccessfullyWithMinimummin_numeric_charsAndMaximummax_numeric_chars(@Named("min_numeric_chars") String minChars, @Named("max_numeric_chars") String maxChars) throws Exception {
		//To range of the numeric character is not configured in SDD. So it is passed as empty.
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.NUMERIC_CHARS, minChars,maxChars);
		passwordPolicyPage.updatePasswordPolicy(UIComponentConstants.PasswordPolicyAttribute.NUMERIC_CHARS, defaultMinNumChars,defaultMaxNumChars);
		updatePolicyResult=false;
	}
	@When("the user updates password history version with <password_history_version> in the password policy")
	public void whenTheUserUpdatesPasswordHistoryVersionWithpassword_history_versionInThePasswordPolicy(@Named("password_history_version") String histVersion) throws Exception {
		setStartDateTimeInLogViewer();
		//As there is no range values (minimum and maximum) need to be passed for password history, one argument is passed as empty.
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.PWD_HIST_VER, histVersion,"","valid");
	}

	@Then("the password history version is configured successfully with <password_history_version>")
	public void thenThePasswordHistoryVersionIsConfiguredSuccessfullyWithpassword_history_version(@Named("password_history_version") String histVersion) throws Exception {
		//As there is no range values (minimum and maximum) need to be passed for password history attribute, one argument is passed as empty.
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.PWD_HIST_VER, histVersion,"");
		passwordPolicyPage.updatePasswordPolicy(UIComponentConstants.PasswordPolicyAttribute.PWD_HIST_VER, defaultHistVersion,"");
		updatePolicyResult=false;
	}

	@When("the user updates character which is not allowed in the password with <reject_char> in the password policy")
	public void whenTheUserUpdatesCharacterWhichIsNotAllowedInThePasswordWithreject_charInThePasswordPolicy(@Named("reject_char") String rejectChar) throws Exception {
		setStartDateTimeInLogViewer();
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.REJECT_CHARS_IN_PSWD, rejectChar,"","valid");
	}

	@Then("the character which is not allowed in the password is updated successfully with <reject_char>")
	public void thenTheCharacterWhichIsNotAllowedInThePasswordIsUpdatedSuccessfullyWithreject_char(@Named("reject_char") String rejectChar) throws Exception {
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.REJECT_CHARS_IN_PSWD, rejectChar,"");
		passwordPolicyPage.updatePasswordPolicy(UIComponentConstants.PasswordPolicyAttribute.REJECT_CHARS_IN_PSWD, defaultPwdRejectChars,"");
		updatePolicyResult=false;
	}

	@When("the user updates number of correct answers with <number_of_correct_answers> in the password policy")
	public void whenTheUserUpdatesNumberOfCorrectAnswersWithnumber_of_correct_answersInThePasswordPolicy(@Named("number_of_correct_answers") String correctAns) throws Exception {
		setStartDateTimeInLogViewer();
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.QUEST_ANSWER_CORRECT, correctAns,"","valid");
	}

	@Then("the number of correct answers is configured successfully with <number_of_correct_answers>")
	public void thenTheNumberOfCorrectAnswersIsConfiguredSuccessfullyWithnumber_of_correct_answers(@Named("number_of_correct_answers") String correctAns) throws Exception {
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.QUEST_ANSWER_CORRECT, correctAns,"");
		passwordPolicyPage.updatePasswordPolicy(UIComponentConstants.PasswordPolicyAttribute.QUEST_ANSWER_CORRECT, defaultQACorrect,"");
		updatePolicyResult=false;
	}

	@When("the user updates number of questions to be displayed with <questions_to_display> in the password policy")
	public void whenTheUserUpdatesNumberOfQuestionsToBeDisplayedWithquestions_to_displayInThePasswordPolicy(@Named("questions_to_display") String questionCount) throws Exception {
		setStartDateTimeInLogViewer();
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.QUEST_COUNT, questionCount,"","valid");
	}

	@Then("the number of questions to be displayed is configured successfully with <questions_to_display>")
	public void thenTheNumberOfQuestionsToBeDisplayedIsConfiguredSuccessfullyWithquestions_to_display(@Named("questions_to_display") String questionCount) throws Exception {
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.QUEST_COUNT, questionCount,"");
		passwordPolicyPage.updatePasswordPolicy(UIComponentConstants.PasswordPolicyAttribute.QUEST_COUNT, defaultQuestCnt,"");
		updatePolicyResult=false;
	}

	@When("the user updates change password after reset attribute with <change_password_after_reset> in the password policy")
	public void whenTheUserUpdatesChangePasswordAfterResetAttributeWithchange_password_after_resetInThePasswordPolicy(@Named("change_password_after_reset") String chgPwdOnReset) throws Exception {
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		setStartDateTimeInLogViewer();
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.CHNG_PSWD_ON_RESET, chgPwdOnReset,"","valid");
	}

	@Then("the change password after reset attribute is configured successfully with <change_password_after_reset>")
	public void thenTheChangePasswordAfterResetAttributeIsConfiguredSuccessfullyWithchange_password_after_reset(@Named("change_password_after_reset") String chgPwdOnReset) throws Exception {
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.CHNG_PSWD_ON_RESET, chgPwdOnReset,"");
		passwordPolicyPage.updatePasswordPolicy(UIComponentConstants.PasswordPolicyAttribute.CHNG_PSWD_ON_RESET, defaultPwdChangeOnReset,"");
		updatePolicyResult=false;
	}

	@When("the user updates password expiration warning attribute with minimum <min_days> and maximum <max_days> days in the password policy")
	public void whenTheUserUpdatesPasswordExpirationWarningAttributeWithMinimummin_daysAndMaximummax_daysDaysInThePasswordPolicy(@Named("min_days") String minDays,@Named("max_days") String maxDays) throws Exception {
		setStartDateTimeInLogViewer();
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.PWD_EXP_WARN, minDays,maxDays,"valid");
	}

	@Then("the change password expiration warning attribute is configured successfully with with minimum <min_days> and maximum <max_days> days")
	public void thenTheChangePasswordExpirationWarningAttributeIsConfiguredSuccessfullyWithWithMinimummin_daysAndMaximummax_daysDays(@Named("min_days") String minDays,@Named("max_days") String maxDays) throws Exception {
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.PWD_EXP_WARN, minDays,maxDays);
		passwordPolicyPage.updatePasswordPolicy(UIComponentConstants.PasswordPolicyAttribute.PWD_EXP_WARN, defaultMinPwdExpWar, defaultMaxPwdExpWar);
		updatePolicyResult=false;
	}

	@When("the user updates password expiration days attribute with <password_expiration_days> in the password policy")
	public void whenTheUserUpdatesPasswordExpirationDaysAttributeWithpassword_expiration_daysInThePasswordPolicy(@Named("password_expiration_days") String pwdExpDays) throws Exception {
		setStartDateTimeInLogViewer();
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.PWD_EXPIRATION, pwdExpDays,"","valid");
	}

	@Then("the password expiration days attribute is configured successfully with <password_expiration_days>")
	public void thenThePasswordExpirationDaysAttributeIsConfiguredSuccessfullyWithpassword_expiration_days(@Named("password_expiration_days") String pwdExpDays) throws Exception {
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.PWD_EXPIRATION, pwdExpDays,"");
		passwordPolicyPage.updatePasswordPolicy(UIComponentConstants.PasswordPolicyAttribute.PWD_EXPIRATION, defaultPwdExpDays, "");
		updatePolicyResult=false;
	}

	@When("the user updates priority attribute with <priority> in the password policy")
	public void whenTheUserUpdatesPriorityAttributeWithpriorityInThePasswordPolicy(@Named("priority") String priority) throws Exception {
		setStartDateTimeInLogViewer();
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.PRIORITY, priority,"","valid");
	}

	@Then("the priority attribute is configured successfully with <priority>")
	public void thenThePriorityAttributeIsConfiguredSuccessfullyWithpriority(@Named("priority") String priority) throws Exception {
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.PRIORITY, priority,"");
		passwordPolicyPage.updatePasswordPolicy(UIComponentConstants.PasswordPolicyAttribute.PRIORITY, defaultPriority, "");
		updatePolicyResult=false;
	}

	@When("the user modifies the name of the password policy with <name>")
	public void whenTheUserModifiesTheNameOfThePasswordPolicyWithname(@Named("name") String policyName) throws Exception {
		setStartDateTimeInLogViewer();
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.POLICY_NAME, policyName,"","valid");
	}



	@Then("the name of the password policy is updated successfully with <name>")
	public void thenTheNameOfThePasswordPolicyIsUpdatedSuccessfullyWithname(@Named("name") String policyName) throws Exception {
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		menuPage.selectPasswordPolicyMenu();
		passwordPolicyPage.searchAndEditPasswordPolicy(policyName);
		Assert.assertTrue(updatePolicyErrorMessage, updatePolicyResult);
		Assert.assertTrue(passwordPolicyPage.verifyPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.POLICY_NAME, policyName,""));
		passwordPolicyPage.updatePasswordPolicy(UIComponentConstants.PasswordPolicyAttribute.POLICY_NAME, UIComponentConstants.PASSWORDPOLICY_NAME,"");
		updatePolicyResult=false;
	}

	@When("the user updates priority attribute with invalid value <invalid_priority> in the password policy")
	public void whenTheUserUpdatesPriorityAttributeWithInvalidValueinvalid_priorityInThePasswordPolicy(@Named("invalid_priority") String invalidPriority) throws Exception {
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.PRIORITY, invalidPriority,"","invalid");
	}

	@Then("the system does not allow modification of priority in the password policy")
	public void thenTheSystemDoesNotAllowModificationOfPriorityInThePasswordPolicy() throws Exception {
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.PRIORITY, defaultPriority,"");
		updatePolicyResult=false;
	}

	@When("the user modifies the name of the password policy with invalid value <invalid_name>")
	public void whenTheUserModifiesTheNameOfThePasswordPolicyWithInvalidValueinvalid_name(@Named("invalid_name") String invalid_name) throws Exception {
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.POLICY_NAME, invalid_name,"","invalid");
	}

	@Then("the system does not allow modification of the name of the password policy")
	public void thenTheSystemDoesNotAllowModificationOfTheNameOfThePasswordPolicy() throws Exception {
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.POLICY_NAME, UIComponentConstants.PASSWORDPOLICY_NAME,"");
		updatePolicyResult=false;
	}

	@When("the user updates password length with invalid minimum value as <invalid_min> and invalid maximum value as <invalid_max> in the password policy")
	public void whenTheUserUpdatesPasswordLengthWithInvalidMinimumValueAsinvalid_minAndInvalidMaximumValueAsinvalid_maxInThePasswordPolicy(@Named("invalid_min") String invalidMin,@Named("invalid_max") String invalidMax) throws Exception {
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.PWD_LEN, invalidMin,invalidMax,"invalid");
	}

	@Then("the system does not allow modification of the password length in the password policy")
	public void thenTheSystemDoesNotAllowModificationOfThePasswordLengthInThePasswordPolicy() throws Exception {
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.PWD_LEN,defaultMinPwdLength,defaultMaxPwdLength);
		updatePolicyResult=false;
	}

	@When("the user updates number of numeric characters with invalid minimum <invalid_min_numeric_chars> and maximum <invalid_max_numeric_chars> in the password policy")
	public void whenTheUserUpdatesNumberOfNumericCharactersWithInvalidMinimuminvalid_min_numeric_charsAndMaximuminvalid_max_numeric_charsInThePasswordPolicy(@Named("invalid_min_numeric_chars") String invalidMin,@Named("invalid_max_numeric_chars") String invalidMax) throws Exception {
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.NUMERIC_CHARS, invalidMin,invalidMax,"invalid");
	}

	@Then("the system does not allow modification of the number of numeric characters in the password policy")
	public void thenTheSystemDoesNotAllowModificationOfTheNumberOfNumericCharactersInThePasswordPolicy() throws Exception {
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.NUMERIC_CHARS,defaultMinNumChars,defaultMaxNumChars);
		updatePolicyResult=false;
	}

	@When("the user updates password history version with invalid value as <invalid_password_history_version> in the password policy")
	public void whenTheUserUpdatesPasswordHistoryVersionWithInvalidValueAsinvalid_password_history_versionInThePasswordPolicy(@Named("invalid_password_history_version") String invalidMin) throws Exception {
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.PWD_HIST_VER, invalidMin,"","invalid");
	}

	@Then("the system does not allow modification of the password history version")
	public void thenTheSystemDoesNotAllowModificationOfThePasswordHistoryVersion() throws Exception {
		//As there is no range values (minimum and maximum) need to be passed for password history attribute, one argument is passed as empty.
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.PWD_HIST_VER, defaultHistVersion,"");
		updatePolicyResult=false;
	}

	@When("the user updates number of correct answers with <invalid_number_of_correct_answers> in the password policy")
	public void whenTheUserUpdatesNumberOfCorrectAnswersWithinvalid_number_of_correct_answersInThePasswordPolicy(@Named("invalid_number_of_correct_answers") String invalidMin) throws Exception {
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.QUEST_ANSWER_CORRECT, invalidMin,"","invalid");
	}

	@Then("the system does not allow modification of the number of correct answers in the password policy")
	public void thenTheSystemDoesNotAllowModificationOfTheNumberOfCorrectAnswersInThePasswordPolicy() throws Exception {
		//As there is no range values (minimum and maximum) need to be passed for password history attribute, one argument is passed as empty.
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.QUEST_ANSWER_CORRECT, defaultQACorrect,"");
		updatePolicyResult=false;
	}

	@When("the user updates password expiration warning attribute with invalid minimum and maximum value as <invalid_min_days> and <invalid_max_days> days in the password policy")
	public void whenTheUserUpdatesPasswordExpirationWarningAttributeWithInvalidMinimumAndMaximumValueAsinvalid_min_daysAndinvalid_max_daysDaysInThePasswordPolicy(@Named("invalid_min_days") String invalidMin,@Named("invalid_max_days") String invalidMax) throws Exception {
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.PWD_EXP_WARN, invalidMin,invalidMax,"invalid");
	}

	@Then("the system does not allow modification of the change password expiration warning attribute in the password policy")
	public void thenTheSystemDoesNotAllowModificationOfTheChangePasswordExpirationWarningAttributeInThePasswordPolicy() throws Exception {
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.PWD_EXP_WARN, defaultMinPwdExpWar, defaultMaxPwdExpWar);
		updatePolicyResult=false;
	}

	@When("the user updates password expiration days attribute with invalid value as <invalid_password_expiration_days> in the password policy")
	public void whenTheUserUpdatesPasswordExpirationDaysAttributeWithInvalidValueAsinvalid_password_expiration_daysInThePasswordPolicy(@Named("invalid_password_expiration_days") String invalidMin) throws Exception {
		//As there is no range values (minimum and maximum) need to be passed, one argument is passed as empty.
		updatesPasswordPolicyAttribute(UIComponentConstants.PasswordPolicyAttribute.PWD_EXPIRATION, invalidMin,"","invalid");
	}

	@Then("the system does not allow modification of the password expiration days attribute")
	public void thenTheSystemDoesNotAllowModificationOfThePasswordExpirationDaysAttribute() throws Exception {
		//As there is no range values (minimum and maximum) need to be passed for password history attribute, one argument is passed as empty.
		verifyAttributeValue(UIComponentConstants.PasswordPolicyAttribute.PWD_EXPIRATION, defaultPwdExpDays,"");
		updatePolicyResult=false;
	}

	@Given("the password policy is updated with the below attributes $attributes")
	public void givenThePasswordPolicyIsUpdatedWithTheBelowAttributesPWD_LENNUMERIC_CHARSREJECT_CHARS_IN_PSWD5102(ExamplesTable attributes,@Named("openiam_admin_user") String authUser) throws Exception {
		loginPage.loginAsAdmin(authUser,ConfigUtil.getInstance().getConfig().adminPwd);
		String minLen="";
		String maxLen="";
		String minNumChar="";
		String rejChar="";
		for (Map<String, String> row : attributes.getRows()) {
			minLen=row.get("PWD_LEN").split(",")[0];
			maxLen=row.get("PWD_LEN").split(",")[1];
			minNumChar=row.get("NUMERIC_CHARS");
			rejChar=row.get("REJECT_CHARS_IN_PSWD");
		}
		menuPage.selectPasswordPolicyMenu();
		passwordPolicyPage.searchAndEditPasswordPolicy(UIComponentConstants.PASSWORDPOLICY_NAME);
		passwordPolicyPage.enterAttributeValues(UIComponentConstants.PasswordPolicyAttribute.PWD_LEN, minLen,maxLen);
		passwordPolicyPage.enterAttributeValues(UIComponentConstants.PasswordPolicyAttribute.NUMERIC_CHARS, minNumChar,"");
		passwordPolicyPage.enterAttributeValues(UIComponentConstants.PasswordPolicyAttribute.REJECT_CHARS_IN_PSWD, rejChar,"");
		passwordPolicyPage.savePolicy();
		menuPage.selectUserAdminMenu();
	}

	@Then("the password is not changed for the user as per the updated password policy rules")
	public void checkPasswordNotChanged() throws Exception {
		 Assert.assertTrue(userPage.verifyPasswordNotChanged());
		 menuPage.selectPasswordPolicyMenu();
		 passwordPolicyPage.searchAndEditPasswordPolicy(UIComponentConstants.PASSWORDPOLICY_NAME);
		 passwordPolicyPage.enterAttributeValues(UIComponentConstants.PasswordPolicyAttribute.PWD_LEN, defaultMinPwdLength,defaultMaxPwdLength);
		 passwordPolicyPage.enterAttributeValues(UIComponentConstants.PasswordPolicyAttribute.NUMERIC_CHARS, defaultMinNumChars,defaultMaxNumChars);
		 passwordPolicyPage.enterAttributeValues(UIComponentConstants.PasswordPolicyAttribute.REJECT_CHARS_IN_PSWD, defaultPwdRejectChars,"");
		 passwordPolicyPage.savePolicy();
	}

	@Given("the password policy exists with the below attributes $attributes")
	public void givenThePasswordPolicyExists(ExamplesTable attributes) throws Exception {
		for (Map<String, String> row : attributes.getRows()) {
			defaultHistVersion=row.get("PWD_HIST_VER");
			defaultMinPwdLength=row.get("PWD_LEN").split(",")[0];
			defaultMaxPwdLength=row.get("PWD_LEN").split(",")[1];
			defaultMinNumChars=row.get("NUMERIC_CHARS").split(",")[0];;
			defaultMaxNumChars=row.get("NUMERIC_CHARS").split(",")[1];;
			defaultPwdRejectChars=row.get("REJECT_CHARS_IN_PSWD");
			defaultQACorrect=row.get("QUEST_ANSWER_CORRECT");
			defaultQuestCnt=row.get("QUEST_COUNT");
			defaultPwdChangeOnReset=row.get("CHNG_PSWD_ON_RESET");
			defaultMinPwdExpWar=row.get("PWD_EXP_WARN").split(",")[0];
			defaultMaxPwdExpWar=row.get("PWD_EXP_WARN").split(",")[1];
			defaultPwdExpDays=row.get("PWD_EXPIRATION");
			Assert.assertEquals(row.get("PWD_HIST_VER"),
					SOAPUtility.getPasswordPolicyAttributeValue(PolicyAttributeEnum.PWD_HIST_VER).getValue1());
			Assert.assertEquals(row.get("PWD_LEN").split(",")[0],
					SOAPUtility.getPasswordPolicyAttributeValue(PolicyAttributeEnum.PWD_LEN).getValue1());
			Assert.assertEquals(row.get("PWD_LEN").split(",")[1],
					SOAPUtility.getPasswordPolicyAttributeValue(PolicyAttributeEnum.PWD_LEN).getValue2());
			Assert.assertEquals(row.get("NUMERIC_CHARS").split(",")[0],
					SOAPUtility.getPasswordPolicyAttributeValue(PolicyAttributeEnum.NUMERIC_CHARS).getValue1());
			Assert.assertEquals(row.get("NUMERIC_CHARS").split(",")[1],
					SOAPUtility.getPasswordPolicyAttributeValue(PolicyAttributeEnum.NUMERIC_CHARS).getValue2());
			Assert.assertEquals(row.get("REJECT_CHARS_IN_PSWD"),
					SOAPUtility.getPasswordPolicyAttributeValue(PolicyAttributeEnum.REJECT_CHARS_IN_PSWD).getValue1());
			Assert.assertEquals(row.get("QUEST_ANSWER_CORRECT"),
					SOAPUtility.getPasswordPolicyAttributeValue(PolicyAttributeEnum.QUEST_ANSWER_CORRECT).getValue1());
			Assert.assertEquals(row.get("QUEST_COUNT"),
					SOAPUtility.getPasswordPolicyAttributeValue(PolicyAttributeEnum.QUEST_COUNT).getValue1());
			Assert.assertEquals(row.get("CHNG_PSWD_ON_RESET"),
					SOAPUtility.getPasswordPolicyAttributeValue(PolicyAttributeEnum.CHNG_PSWD_ON_RESET).getValue1());
			Assert.assertEquals(row.get("PWD_EXP_WARN").split(",")[0],
					SOAPUtility.getPasswordPolicyAttributeValue(PolicyAttributeEnum.PWD_EXP_WARN).getValue1());
			Assert.assertEquals(row.get("PWD_EXP_WARN").split(",")[1],
					SOAPUtility.getPasswordPolicyAttributeValue(PolicyAttributeEnum.PWD_EXP_WARN).getValue2());
			Assert.assertEquals(row.get("PWD_EXPIRATION"),
					SOAPUtility.getPasswordPolicyAttributeValue(PolicyAttributeEnum.PWD_EXPIRATION).getValue1());
		}

	}
	private void setStartDateTimeInLogViewer() {
		LogViewerSteps.setStartDateTime(Utility.getCurrentTimeStamp());
	}
}
