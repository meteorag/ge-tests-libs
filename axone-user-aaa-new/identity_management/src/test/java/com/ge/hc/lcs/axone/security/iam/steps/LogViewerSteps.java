package com.ge.hc.lcs.axone.security.iam.steps;

import java.util.Arrays;
import java.util.List;

import org.jbehave.core.annotations.Alias;
import org.jbehave.core.annotations.Aliases;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import com.ge.hc.lcs.axone.security.iam.config.ConfigUtil;
import com.ge.hc.lcs.axone.security.iam.mua.constants.UIComponentConstants;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.LogViewerPage;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.LoginPage;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.MenuPage;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.UserPage;
import com.ge.hc.lcs.axone.security.iam.mua.utilities.Utility;

public class LogViewerSteps {
	private LoginPage loginPage;
	private MenuPage menuPage;
	private UserPage userPage;
	private LogViewerPage logViewerPage;
	private static String startDateTime="";
	public static int logCount = 1;

	public void verifyTheLogIsGenerated(String targetUser,String action) throws Exception {
		menuPage.selectLogViewerMenu();
		String endDateTime = Utility.getCurrentTimeStamp();
		Assert.assertTrue(logViewerPage.verifyLogForTheAction(targetUser, startDateTime, endDateTime, action, logCount));
		// Clearing the start date time class variable
		startDateTime = "";
	}

	@Then("a log is generated for the aforementioned action(s) <action>")
	@Aliases(values = { "a log is present for the aforementioned action(s) <action>" })
	public void thenTheLogIsGeneratedForThisaction(@Named("openiam_admin_user") String targetUser,
			@Named("action") String action) throws Exception {
		verifyTheLogIsGenerated(targetUser,action);
	}

	@Then("a log is present for password change by the user with the log action <action>")
	@Alias("a log is generated for the password change through challenge response answers with the log action <action>")
	public void thenTheLogIsGeneratedForPasswordChange(@Named("login_id") String targetUser,
			@Named("action") String action) throws Exception {
		verifyTheLogIsGenerated(targetUser,action);
	}

	// The below step will be used in scenarios only when the user action performed
	// in self service portal and the admin wants to verify the log in webconsole.
	@When("the administrator logs in to user management portal to retrieve the audit logs")
	public void whenTheauthorized_userVerfiesTheAuditLogsInUserManagementPortal(
			@Named("openiam_admin_user") String authUser) throws Exception {
		String Url = System.getProperty(UIComponentConstants.HOSTNAME) + UIComponentConstants.WEBCONSOLE;
		userPage.navigateToLoginPage(Url);
		loginPage.login(authUser, ConfigUtil.getInstance().getConfig().adminPwd);
	}

	public void verifyLogsAreGeneratedForThisActions(String targetUser, String actions) throws Exception {
		// Selecting the Log Viewer Menu is common for all the log related scenarios.
		menuPage.selectLogViewerMenu();
		String endDateTime = Utility.getCurrentTimeStamp();
		List<String> userActions = Arrays.asList(actions.split(","));
		for (String action : userActions) {
			Assert.assertTrue(
					logViewerPage.verifyLogForTheAction(targetUser, startDateTime, endDateTime, action, logCount));
		}
		// Clearing the start date time class variable
		startDateTime = "";
	}

	@Then("a log is generated for the aforementioned action(s) <actions>")
	public void thenTheLogsAreGeneratedForThisActions(@Named("openiam_admin_user") String targetUser,
			@Named("actions") String actions) throws Exception {
		verifyLogsAreGeneratedForThisActions(targetUser,actions);
	}

	@Then("a log is generated for the password change through challenge response answers with the log actions <actions>")
	public void thenTheLogsAreGeneratedForActions(@Named("login_id") String targetUser,
			@Named("actions") String actions) throws Exception {
		verifyLogsAreGeneratedForThisActions(targetUser,actions);
	}

	public static String getStartDateTime() {
		return startDateTime;
	}

	public static void setStartDateTime(String dateTime) {
		startDateTime = dateTime;
	}

}
