package com.ge.hc.lcs.axone.security.iam.adhoc;

import java.util.ArrayList;

import org.testng.Assert;
import org.openiam.idm.srvc.user.dto.User;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import com.ge.hc.lcs.axone.openiam.factory.ServiceFactory;
import com.ge.hc.lcs.axone.openiam.service.UserService;
import com.ge.hc.lcs.axone.security.iam.pageobjects.LoginPage;
import com.ge.hc.lcs.axone.security.iam.pageobjects.UserPage;

/**
 * To stress test the work flow where the Admin user creates a user (same
 * principal/loginID) and deletes the same user for configured number of times
 */
public class Create_Delete_Same_User_Loop {
	// Host where OpenIAM instance is running to be tested
	private static String host = System.getProperty("hostname");
	// Number of times the same user has to be created and deleted
	private final static int no_of_times = 3;
	private static String webservicehost = host + "/openiam-esb/idmsrvc";
	static UserService userService = UserService.getInstance(webservicehost);

	/**
	 * Admin creates and deletes the same user by logging in to webconsole
	 * 
	 * @throws Exception
	 * 
	 * @invocationCount - The number of times this method should be invoked which is
	 *                  number of times admin creates and delete the same user
	 * @threadPoolSize - The size of the thread pool for this method. Since it is
	 *                 for single user thread pool size is 1.
	 */
	@Test(invocationCount = no_of_times, threadPoolSize = 1)
	public void create_delete_UserTest() throws Exception {
		ChromeOptions options = new ChromeOptions();
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		} else {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver");
			options.addArguments("--no-sandbox");
		}
		WebDriver driver = new ChromeDriver(options);
		LoginPage loginPage = new LoginPage(driver);
		loginPage.login("sysadmin", "passwd00", host + "/webconsole/newUser.html");
		UserPage userPage = new UserPage(driver);
		userPage.setUserInfo("Internal User","createdelete","usertest", "create_deleteusertest","");
		userPage.executeDelete();
		driver.quit();
		// Verify users are deleted successfully
		Assert.assertFalse(isUserExists("create_deleteusertest"));
	}

	private boolean isUserExists(String userID) {
		org.openiam.idm.srvc.user.service.UserDataService userDataService = ServiceFactory
				.getUserDataService(webservicehost);
		User user = userDataService.getUserWithDependent(userID, new ArrayList<>());
		if (user != null) {
			return true;
		}
		return false;
	}
}
