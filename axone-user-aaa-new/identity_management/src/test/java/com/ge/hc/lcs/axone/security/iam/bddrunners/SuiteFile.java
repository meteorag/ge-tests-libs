package com.ge.hc.lcs.axone.security.iam.bddrunners;

import net.serenitybdd.jbehave.SerenityStories;

public class SuiteFile extends SerenityStories {

	public SuiteFile() {
		findStoriesCalled(
				"com/ge/hc/lcs/axone/security/iam/feature/PasswordPolicy.story;"
				+"com/ge/hc/lcs/axone/security/iam/feature/AuthenticationPolicy.story;"
				+ "com/ge/hc/lcs/axone/security/iam/feature/User.story;"
				+ "com/ge/hc/lcs/axone/security/iam/feature/Login_out.story;");
	}
}
