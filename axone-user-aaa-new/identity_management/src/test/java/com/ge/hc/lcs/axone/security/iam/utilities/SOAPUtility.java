package com.ge.hc.lcs.axone.security.iam.utilities;

import java.util.List;

import org.junit.Assert;
import org.openiam.idm.srvc.policy.service.PolicyAttribute;

import com.ge.hc.lcs.axone.openiam.constants.Constants;
import com.ge.hc.lcs.axone.openiam.enums.PolicyAttributeEnum;
import com.ge.hc.lcs.axone.openiam.service.PolicyService;
import com.ge.hc.lcs.axone.openiam.service.RoleService;
import com.ge.hc.lcs.axone.openiam.service.UserService;

public class SOAPUtility {

	/**
	 * Check roles exists using Role Service.This method should be used only for
	 * precondition.
	 * @throws Exception
	  */
	public static void checkRolesExists(List<String> roleNames) throws Exception {
		RoleService roleService = RoleService.getInstance();
		Assert.assertTrue(roleService.getAllRoles().containsAll(roleNames));
	}

	/**
	 * Below method is to check whether user exists using User service. This method
	 * should be used only for precondition.
	 */
	public static boolean checkUserExistsBySOAP(String principal) throws Exception {
		UserService userService = UserService.getInstance();
		boolean exist = false;
		if (userService.getUserId(principal) != null)
			exist = true;
		return exist;
	}

	/**
	 * Retrieve the value of attribute from default password policy
	 *
	 * @param attributeName
	 * 			Attribute name of the value to be retrieved
	 * @return
	 * @throws Exception
	 */
	public static PolicyAttribute getPasswordPolicyAttributeValue(PolicyAttributeEnum attributeName) throws Exception {
		return getPolicyAttributeValue(Constants.DEFAULT_PASSWD_POLICY_ID, attributeName);
	}

	/**
	 * Retrieve the value of attribute from default Authentication policy
	 *
	 * @param attributeName
	 * 			Attribute name of the value to be retrieved
	 * @return
	 * @throws Exception
	 */
	public static PolicyAttribute getAuthenticationPolicyAttributeValue(PolicyAttributeEnum attributeName) throws Exception {
		return getPolicyAttributeValue(Constants.DEFAULT_AUTHN_POLICY_ID, attributeName);
	}

	/**
	 * Get value of given attribute from the given policy id.
	 * @param policyId
	 * 			Id of the policy to retrieve the attribute
	 * @param attributeName
	 * 			Attribute name of the value to be retrieved
	 * @return
	 * @throws Exception
	 */
	private static PolicyAttribute getPolicyAttributeValue(String policyId, PolicyAttributeEnum attributeName) throws Exception {
		PolicyService service = PolicyService.getInstance();
		PolicyAttribute policy_attribute=new PolicyAttribute();
		policy_attribute= service.getPolicyAttributeById(policyId, attributeName);
		return policy_attribute;
	}

}
