package com.ge.hc.lcs.axone.security.iam.adhoc;

import java.util.concurrent.atomic.AtomicInteger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ge.hc.lcs.axone.openiam.service.UserService;
import com.ge.hc.lcs.axone.security.iam.pageobjects.LoginPage;
import com.ge.hc.lcs.axone.security.iam.pageobjects.MenuPage;
import com.ge.hc.lcs.axone.security.iam.pageobjects.UserPage;

/**
 * To stress test the work flow where the Admin user resets the password for
 * user for the required number of times and every time the user should be able to login
 * with updated password.
 */
public class Admin_Reset_Password_For_User_Test {

	// Host where OpenIAM instance is running to be tested
	private static String host = System.getProperty("hostname");
	// Number of times the password has to be reset for the user
	private final static int no_of_times = 3;
	// User to whom the password has to be reset
	private static String user = "admin_reset_password_testuser";
	private static String webservicehost = host + "/openiam-esb/idmsrvc";
	static UserService userService = UserService.getInstance(webservicehost);
	AtomicInteger sequence = new AtomicInteger(0);

	// Pre-condition - Create the user for whom the password to be reset
	@BeforeClass
	public static void createUsers() {
		Assert.assertTrue(userService.addUser("test", "user", user));
	}

	/**
	 * This method resets the password for a user by admin and verifies the same by
	 * user login to self service portal
	 * 
	 * @throws Exception
	 * 
	 * @invocationCount - The number of times this method should be invoked which is
	 *                  number of times user's password has to be reset
	 * @threadPoolSize - The size of the thread pool for this method. Since it is
	 *                 for single user thread pool size is 1.
	 */
	@Test(invocationCount = no_of_times, threadPoolSize = 1)
	public void admin_Reset_Password_Test() throws Exception {
		int count = sequence.addAndGet(1);
		ChromeOptions options = new ChromeOptions();
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		} else {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver");
			options.addArguments("--no-sandbox");
		}
		WebDriver driver = new ChromeDriver(options);

		LoginPage loginPage = new LoginPage(driver);
		loginPage.loginAsAdmin("sysadmin", "passwd00");
		UserPage userPage = new UserPage(driver);
		userPage.searchUserByPrincipal(user);
		userPage.clickEditUser();
		MenuPage menuPage = new MenuPage(driver);
		menuPage.clickResetPasswordMenu();
		String newPassword = "Password$" + count;
		userPage.enterPasswordValuesByAdmin(newPassword);
		userPage.verifyResetPassword();
		loginPage.logOut();
		loginPage = new LoginPage(driver);
		Assert.assertTrue(loginPage.verifyUserLoginAfterPasswordReset(user, newPassword));
		driver.quit();
	}

	// Clean up - delete the user created in per-condition
	@AfterClass
	public static void deleteUsers() {
		Assert.assertTrue(userService.deleteUserByPrincipal(user));
	}

}
