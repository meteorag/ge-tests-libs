# Ad-hoc tests - Identity Management

This folder contains the source code for ad-hoc test for Identity Management tests.

## Pre-Conditions

Executing this tests requires following installations,

* Git client
* Chrome (v63-65)
* Maven

## Test Scenarios

It contains the following test scenarios,

### Admin reset password for user

To stress test the work flow where the Admin user resets the password for user for the required number of times and every time the user should be able to login with updated password.

Configure number of times in `Admin_Reset_Password_For_User_Test.java`  `no_of_times`

### Create delete same user loop

To stress test the work flow where the Admin user creates a user (same principal/loginID) and deletes the same user for configured number of times

Configure number of times in `Create_Delete_Same_User_Loop.java` `no_of_times`

### Create delete users stress test (SOAP API)

#### Create delete same user

Create and delete same user for the configured number of times

Configure number of times in `Delete_User_Stress_Test.java` `no_of_times_same_user_create_delete`

#### Create delete user

Create and delete users for the configured number of times

Configure number of times in `Create_Delete_User_Stress_Test.java` `no_of_users_create_delete`

### Create users in batch

To stress test concurrent user creation by admin for the configured number of times

Configure this in `Create_Users_Batch_Test.java` `no_of_users`

### Delete users in batch

To stress test concurrent user deletion by admin for the configured number of times

Configure this in `Delete_User_Batch_Test.java` `no_of_users`

### Login load test different users

To stress test concurrent user login to selfservice

Configure this in `LoginLoadTest_DifferentUser.java` `no_of_users`

### Login load test same user

To stress test concurrent same user login to selfservice

Configure this in `LoginLoadTest_SameUser.java` `no_of_times`

### User password reset

To stress test the work flow where the user resets the password for configured number of times and every time the user should be able to login with updated password.

Configure this in `User_Password_Reset_Test.java` `no_of_times`

### Create user race condition

Create the same user simultaneously to simulate race condition

Configure this in `Create_User_Race_Condition.java` `no_of_users`

### Delete user race condition

Delete the same user simultaneously to simulate race condition

Configure this in `Delete_User_Race_Condition.java` `no_of_users`