package com.ge.hc.lcs.axone.security.iam.adhoc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ge.hc.lcs.axone.openiam.service.UserService;
import com.ge.hc.lcs.axone.security.iam.pageobjects.LoginPage;
import com.ge.hc.lcs.axone.security.iam.pageobjects.UserPage;

/**
 * To stress test concurrent user deletion by admin for the configured number of
 * users.
 */
public class Delete_User_Batch_Test {

	// Host where OpenIAM instance is running to be tested
	private static String host = System.getProperty("hostname");
	// Number of users to be deleted concurrently in batch
	private final static int no_of_users = 3;
	static UserService userService = UserService.getInstance(host + "/openiam-esb/idmsrvc");
	private static List<String> userIDs = new ArrayList<String>();
	AtomicInteger sequence = new AtomicInteger(0);

	/**
	 * Pre-condition - Creates the configured number of users to be used for test
	 */
	@BeforeClass
	public static void createUsers() {
		for (int i = 1; i <= no_of_users; i++) {
			Assert.assertTrue(userService.addUser("delete", "user", "delete_user_batch" + i));
			// UserID is required to navigate to the navigate to the user info page directly
			// eg: http://axone.openiam.com/webconsole/editUser.html?id='userID'
			userIDs.add(userService.getUserId("delete_user_batch" + i));
		}
	}

	/**
	 * Admin deletes users by logging in to webconsole
	 * 
	 * @throws Exception
	 * @invocationCount - The number of times this method should be invoked which is
	 *                  number of users to be deleted
	 * @threadPoolSize - The size of the thread pool for this method. It can be any
	 *                 numeric value based on the number of concurrent executions to
	 *                 be tested
	 */
	@Test(invocationCount = no_of_users, threadPoolSize = no_of_users)
	public void deleteUserTest() throws Exception {
		int count = sequence.addAndGet(1);
		String userID = userIDs.get(count - 1);
		ChromeOptions options = new ChromeOptions();
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		} else {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver");
			options.addArguments("--no-sandbox");
		}
		WebDriver driver = new ChromeDriver(options);

		LoginPage loginPage = new LoginPage(driver);
		loginPage.login("sysadmin", "passwd00",
				host + String.format("/webconsole/editUser.html?id=%s", userID));
		UserPage userPage = new UserPage(driver);
		userPage.executeDelete();
		driver.quit();
	}

	@AfterClass
	public static void deleteUsers() {
		for (int i = 1; i <= no_of_users; i++) {
			userService.deleteUserByPrincipal("delete_user_batch" + i);
		}
	}
}
