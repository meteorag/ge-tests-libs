package com.ge.hc.lcs.axone.security.iam.adhoc;

import java.util.concurrent.atomic.AtomicInteger;

import org.testng.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.ge.hc.lcs.axone.openiam.service.UserService;
import com.ge.hc.lcs.axone.security.iam.pageobjects.LoginPage;
import com.ge.hc.lcs.axone.security.iam.pageobjects.UserPage;

/**
 * To stress test concurrent user creation by admin for the configured number of
 * users.
 */
public class Create_Users_Batch_Test {
	// Host where OpenIAM instance is running to be tested
	private static String host = System.getProperty("hostname");
	AtomicInteger sequence = new AtomicInteger(0);
	// Number of users to be created concurrently in batch
	private final static int no_of_users = 3;
	private static String webservicehost = host + "/openiam-esb/idmsrvc";
	static UserService userService = UserService.getInstance(webservicehost);

	/**
	 * Admin creates users by logging in to webconsole
	 * 
	 * @invocationCount - The number of times this method should be invoked which is
	 *                  number of users to be created
	 * @threadPoolSize - The size of the thread pool for this method. It can be any
	 *                 numeric value based on the number of concurrent executions to
	 *                 be tested
	 * @throws InterruptedException
	 */
	@Test(invocationCount = no_of_users, threadPoolSize = no_of_users)
	public void createUserBatchTest() throws Exception {
		ChromeOptions options = new ChromeOptions();
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		} else {
			options.addArguments("--no-sandbox");
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver");
		}
		WebDriver driver = new ChromeDriver(options);
		LoginPage loginPage = new LoginPage(driver);
		loginPage.login("sysadmin", "passwd00", host + "/webconsole/newUser.html");
		UserPage userPage = new UserPage(driver);
		int count = sequence.addAndGet(1);
		userPage.setUserInfo("Internal User", "createuser", "batchtest", "createuser_batchtest" + count,"");
		driver.quit();
	}

	/**
	 * Clean up - deletes the users created as part of test
	 */
	@AfterClass
	public static void deleteUsers() {
		for (int i = 1; i <= no_of_users; i++) {
			Assert.assertTrue(userService.deleteUserByPrincipal("createuser_batchtest" + i));
		}
	}
}
