package com.ge.hc.lcs.axone.security.iam.steps;

import java.util.Arrays;
import java.util.List;

import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.AfterScenario.Outcome;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.ScenarioType;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import com.ge.hc.lcs.axone.openiam.service.RoleService;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.LoginPage;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.MenuPage;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.RolesPage;
import com.ge.hc.lcs.axone.security.iam.mua.pageobjects.UserPage;
import com.ge.hc.lcs.axone.security.iam.mua.utilities.Utility;
import com.ge.hc.lcs.axone.security.iam.utilities.SOAPUtility;

public class RoleIDMSteps {

	private LoginPage login;
	private MenuPage menuPage;
	private RolesPage rolePage;
	private UserPage userPage;

	@When("the authorized user lists the roles")
	public void whenTheAuthorizedUserListsTheRoles() {
		menuPage.selectRolesMenu();
	}

	@Then("<roles> are listed")
	public void thenrolesAreListed(@Named("roles") String roles) {
		List<String> roleNames = Arrays.asList(roles.split(","));
		rolePage.verifyRoles(roleNames);
	}

	@Given("the <roles> exists")
	public void givenTherolesExists(@Named("roles") String roles) {
		try {
			SOAPUtility.checkRolesExists(Arrays.asList(roles.split(",")));
		} catch (Exception e) {			
			e.printStackTrace();
			Assert.fail("Exception in SOAP API");
		}
	}

	@When("the authorized user assigns the <roles> to <user>")
	public void whenTheAuthorizedUserAssignsTherolesTouserUsingmenu(@Named("roles") String roles,
			@Named("user") String user) throws Exception {
		// Below variable will be used as the start date time filter while viewing audit
		// logs
		setStartDateTimeInLogViewer();
		userPage.searchUserByPrincipal(user);
		userPage.clickEditUser();
		menuPage.selectUserEntitlementMenu();
		List<String> roleNames = Arrays.asList(roles.split(","));
		roleNames.forEach(roleName -> rolePage.addRoletoUser(roleName));
	}

	@Then("the <roles> are assigned to the <user>")
	public void thenTherolesAreAssignedToTheuser(@Named("roles") String roles, @Named("user") String user)
			throws InterruptedException {
		List<String> roleNames = Arrays.asList(roles.split(","));
		rolePage.verifyRolesForUser(user, roleNames);
	}

	@Given("the <role> is assigned to the <user>")
	public void givenTheroleIsAssignedToTheuser(@Named("role") String role, @Named("user") String user){
		RoleService roleService;
		try {
			roleService = RoleService.getInstance();
			List<String> roles = roleService.getRolesForUser(user);
			roles.forEach(item -> {
				roles.set(roles.indexOf(item), item.trim());
			});
			Assert.assertTrue(roles.contains(role.trim()));
		} catch (Exception e) {			
			e.printStackTrace();
			Assert.fail("Exception in SOAP API");
		}	

	}

	@When("the authorized user removes the <role> from <user>")
	public void whenTheAuthorizedUserRemovesTheroleFromuser(@Named("role") String role, @Named("user") String user)
			throws Exception {
		// Below variable will be used as the start date time filter while viewing audit
		// logs
		setStartDateTimeInLogViewer();
		userPage.searchUserByPrincipal(user);
		userPage.clickEditUser();
		menuPage.selectUserEntitlementMenu();
		rolePage.removeRoleFromUser(role);
	}

	@Then("the <role> is removed for the <user>")
	public void thenTheroleIsRemovedForTheuser(@Named("role") String role, @Named("user") String user) {
		Assert.assertFalse(rolePage.isRoleEntitledForUser(user, role));
	}

	@When("the authorized user updates the <user> role from <role> to <new_role>")
	public void whenTheAuthorizedUserUpdatesTheUserRoleFromroleTonew_role(@Named("user") String user,
			@Named("role") String role, @Named("new_role") String new_role) throws Exception {
		// Below variable will be used as the start date time filter while viewing audit
		// logs
		setStartDateTimeInLogViewer();
		userPage.searchUserByPrincipal(user);
		userPage.clickEditUser();
		menuPage.selectUserEntitlementMenu();
		rolePage.removeRoleFromUser(role);
		rolePage.addRoletoUser(new_role);
	}

	@Then("the <new_role> is assigned to the <user>")
	public void thenThenew_roleIsAssignedToTheuser(@Named("user") String user, @Named("new_role") String new_role) {
		Assert.assertTrue(rolePage.isRoleEntitledForUser(user, new_role));
	}

	@AfterScenario(uponType = ScenarioType.ANY, uponOutcome = Outcome.ANY)
	public void cleanup() throws Exception {
		login.logOut();
	}

	private void setStartDateTimeInLogViewer() {
		LogViewerSteps.setStartDateTime(Utility.getCurrentTimeStamp());
	}


}
