Feature: Configuring password policy using user management portal
Narrative:
As an authorized user who manages user access controls
I need to configure password policy
So that the password policy will be enforced on reset password

Scenario: User management portal shall allow the administrator user to configure password length in password
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates password length with minimum <min_password_length> and maximum <max_password_length> in the password policy
Then the password length is configured successfully with minimum <min_password_length> and maximum <max_password_length>
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|min_password_length|max_password_length|action|
|sysadmin| 5 |10|MODIFY_PASSWORD_POLICY|
|sysadmin| 100 |100|MODIFY_PASSWORD_POLICY|


Scenario: User management portal shall allow the administrator user to configure number of numeric characters allowed in password
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates number of numeric characters with minimum <min_numeric_chars> and maximum <max_numeric_chars> in the password policy
Then the number of numeric characters is configured successfully with minimum <min_numeric_chars> and maximum <max_numeric_chars>
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|min_numeric_chars|max_numeric_chars|action|
|sysadmin|2|5|MODIFY_PASSWORD_POLICY|
|sysadmin| 100 |100|MODIFY_PASSWORD_POLICY|

Scenario: User management portal shall allow the administrator user to configure password history versions
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates password history version with <password_history_version> in the password policy
Then the password history version is configured successfully with <password_history_version>
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|password_history_version|action|
|sysadmin| 9|MODIFY_PASSWORD_POLICY|

Scenario: User management portal shall allow the administrator user to configure character which is not allowed in the password
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates character which is not allowed in the password with <reject_char> in the password policy
Then the character which is not allowed in the password is updated successfully with <reject_char>
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|reject_char|action|
|sysadmin| @|MODIFY_PASSWORD_POLICY|

Scenario: User management portal shall allow the administrator user to configure number of correct answers in password policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates number of correct answers with <number_of_correct_answers> in the password policy
Then the number of correct answers is configured successfully with <number_of_correct_answers>
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|number_of_correct_answers|action|
|sysadmin| 2|MODIFY_PASSWORD_POLICY|

Scenario: User management portal shall allow the administrator user to configure number of questions to be displayed in password policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates number of questions to be displayed with <questions_to_display> in the password policy
Then the number of questions to be displayed is configured successfully with <questions_to_display>
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|questions_to_display|action|
|sysadmin| 4|MODIFY_PASSWORD_POLICY|

Scenario: User management portal shall allow the administrator user to configure change password after reset in password policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates change password after reset attribute with <change_password_after_reset> in the password policy
Then the change password after reset attribute is configured successfully with <change_password_after_reset>
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|change_password_after_reset|action|
|sysadmin|true|MODIFY_PASSWORD_POLICY|
|sysadmin|false|MODIFY_PASSWORD_POLICY|

Scenario: User management portal shall allow the administrator user to configure password expiration warning in password policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates password expiration warning attribute with minimum <min_days> and maximum <max_days> days in the password policy
Then the change password expiration warning attribute is configured successfully with with minimum <min_days> and maximum <max_days> days
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|min_days|max_days|action|
|sysadmin| 4|9|MODIFY_PASSWORD_POLICY|
|sysadmin| 100 |100|MODIFY_PASSWORD_POLICY|

Scenario: User management portal shall allow the administrator user to configure password expiration days
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates password expiration days attribute with <password_expiration_days> in the password policy
Then the password expiration days attribute is configured successfully with <password_expiration_days>
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|password_expiration_days|action|
|sysadmin|130|MODIFY_PASSWORD_POLICY|

Scenario: User management portal shall allow the administrator user to configure priority in password policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates priority attribute with <priority> in the password policy
Then the priority attribute is configured successfully with <priority>
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|priority|action|
|sysadmin|20|MODIFY_PASSWORD_POLICY|

Scenario: User management portal shall allow the administrator user to modify the name in password policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user modifies the name of the password policy with <name>
Then the name of the password policy is updated successfully with <name>
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|name|action|
|sysadmin|Default Pswd Policy new|MODIFY_PASSWORD_POLICY|

Scenario: Updated password policy is enforced while the administrator user reset the password
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
And the password policy is updated with the below attributes
|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|
|5,10|2|@|
And a user with login ID <login_id> exists
When an administrator user <openiam_admin_user> resets password to a <non_compliant_password> which is non compliant to the updated password policy
Then the password is not changed for the user as per the updated password policy rules
Examples:
|openiam_admin_user|non_compliant_password|login_id|
|sysadmin|ab@1|GE_PasswordPolicyUserNew|


Scenario: User management portal shall not allow the administrator user to configure priority with invalid value in password policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates priority attribute with invalid value <invalid_priority> in the password policy
Then the system does not allow modification of priority in the password policy
Examples:
|openiam_admin_user|invalid_priority|
|sysadmin|abcd|
|sysadmin|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|
|sysadmin||

Scenario: User management portal shall not allow the administrator user to modify the name with invalid value in password policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user modifies the name of the password policy with invalid value <invalid_name>
Then the system does not allow modification of the name of the password policy
Examples:
|openiam_admin_user|invalid_name|
|sysadmin|longnamewwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww|
|sysadmin||

Scenario: User management portal shall not allow the administrator user to configure password length with invalid value in password policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates password length with invalid minimum value as <invalid_min> and invalid maximum value as <invalid_max> in the password policy
Then the system does not allow modification of the password length in the password policy
Examples:
|openiam_admin_user|invalid_min|invalid_max|
|sysadmin| abcd |abcdddd|
|sysadmin|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|5|
|sysadmin|100|5|
|sysadmin|-10|5|
|sysadmin|5|-10|
|sysadmin|||

Scenario: User management portal shall not allow the administrator user to configure number of numeric characters with invalid value in password policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates number of numeric characters with invalid minimum <invalid_min_numeric_chars> and maximum <invalid_max_numeric_chars> in the password policy
Then the system does not allow modification of the number of numeric characters in the password policy
Examples:
|openiam_admin_user|invalid_min_numeric_chars|invalid_max_numeric_chars|
|sysadmin| abcd |abcdddd|
|sysadmin|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|5|
|sysadmin|||
|sysadmin|100|5|
|sysadmin|-10|5|
|sysadmin|5|-10|

Scenario: User management portal shall not allow the administrator user to configure number of correct answers with invalid value in password policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates number of correct answers with <invalid_number_of_correct_answers> in the password policy
Then the system does not allow modification of the number of correct answers in the password policy
Examples:
|openiam_admin_user|invalid_number_of_correct_answers|
|sysadmin|abcdddd|

Scenario: User management portal shall not allow the administrator user to configure password expiration warning with invalid value in password policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates password expiration warning attribute with invalid minimum and maximum value as <invalid_min_days> and <invalid_max_days> days in the password policy
Then the system does not allow modification of the change password expiration warning attribute in the password policy
Examples:
|openiam_admin_user|invalid_min_days|invalid_max_days|
|sysadmin| abcd |abcdddd|
|sysadmin|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|5|
|sysadmin|||
|sysadmin|100|5|
|sysadmin|-10|5|
|sysadmin|5|-10|

Scenario: User management portal shall not allow the administrator user to configure password length with invalid value in password policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates password length with invalid minimum value as <invalid_min> and invalid maximum value as <invalid_max> in the password policy
Then the system does not allow modification of the password length in the password policy
Examples:
|openiam_admin_user|invalid_min|invalid_max|
|sysadmin| abcd |abcdddd|
|sysadmin|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|5|
|sysadmin|||
|sysadmin|1|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|
|sysadmin|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|
|sysadmin|100|5|
|sysadmin|-10|5|
|sysadmin|5|-10|

Scenario: User management portal shall not allow the administrator user to configure number of numeric characters with invalid value in password policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates number of numeric characters with invalid minimum <invalid_min_numeric_chars> and maximum <invalid_max_numeric_chars> in the password policy
Then the system does not allow modification of the number of numeric characters in the password policy
Examples:
|openiam_admin_user|invalid_min_numeric_chars|invalid_max_numeric_chars|
|sysadmin| abcd |abcdddd|
|sysadmin|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|5|
|sysadmin|||
|sysadmin|1|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|
|sysadmin|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|
|sysadmin|100|5|
|sysadmin|-10|5|
|sysadmin|5|-10|

Scenario: User management portal shall not allow the administrator user to configure number of correct answers with invalid value in password policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates number of correct answers with <invalid_number_of_correct_answers> in the password policy
Then the system does not allow modification of the number of correct answers in the password policy
Examples:
|openiam_admin_user|invalid_number_of_correct_answers|
|sysadmin|abcdddd|
|sysadmin| 100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|
|sysadmin||

Scenario: User management portal shall not allow the administrator user to configure password expiration warning with invalid value in password policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates password expiration warning attribute with invalid minimum and maximum value as <invalid_min_days> and <invalid_max_days> days in the password policy
Then the system does not allow modification of the change password expiration warning attribute in the password policy
Examples:
|openiam_admin_user|invalid_min_days|invalid_max_days|
|sysadmin| abcd |abcdddd|
|sysadmin|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|5|
|sysadmin|||
|sysadmin|1|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|
|sysadmin|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|
|sysadmin|100|5|
|sysadmin|-10|5|
|sysadmin|5|-10|

Scenario: User management portal shall not allow the administrator user to configure password expiration days with invalid value
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates password expiration days attribute with invalid value as <invalid_password_expiration_days> in the password policy
Then the system does not allow modification of the password expiration days attribute
Examples:
|openiam_admin_user|invalid_password_expiration_days|
|sysadmin|abcd|
|sysadmin|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|
|sysadmin||

Scenario: User management portal shall not allow the administrator user to configure password history versions with invalid value
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
Given an administrator user <openiam_admin_user> is logged into user management portal
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user updates password history version with invalid value as <invalid_password_history_version> in the password policy
Then the system does not allow modification of the password history version
Examples:
|openiam_admin_user|invalid_password_history_version|
|sysadmin| abcdddd|
|sysadmin|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|
|sysadmin||