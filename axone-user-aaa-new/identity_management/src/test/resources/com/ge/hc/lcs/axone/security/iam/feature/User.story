Feature: Managing users using user management portal
Narrative:
As an openIAM admin user who manages users
I need the user management portal to facilitate the workflows to create, modify, remove users and password management

Scenario: User management portal shall allow creation of a user by administrator user
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1410
@platssrs_1528
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
When the administrator user creates a user with login ID <login_id>,first name <firstname> and last name <lastname>
Then the user is created successfully with login ID <login_id>,first name <firstname> and last name <lastname>
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|login_id|firstname|lastname|action|
|sysadmin|GE.UserCreate|GE|UserCreateTest|CREATE_USER|
|sysadmin|GE_ServiceUserCreatenew|GE|ServiceUserCreate|CREATE_USER|

Scenario: User management portal shall allow deletion of a user by administrator user
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1414
@platssrs_1528
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
And a user with login ID <login_id> exists
When the administrator user deletes the user
Then the user is deleted successfully
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|login_id|action|
|sysadmin|GE_ServiceUserToDelete|USER_REMOVE|

Scenario: User management portal shall allow modification of first name of the user by administrator user
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1412
@platssrs_1528
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
And a user with login ID <login_id> exists
When the administrator user modifies the first name of the user with <new_name>
Then the first name of the user is modified with <new_name> successfully
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|login_id|new_name|action|
|sysadmin|GE_ServiceUserToModify|testuser|MODIFY_USER|
|sysadmin|GE_ServiceUserToModify|testUSER|MODIFY_USER|
|sysadmin|GE_ServiceUserToModify|user1|MODIFY_USER|

Scenario: User management portal shall allow modification of last name of the user by administrator user
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1412
@platssrs_1528
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
And a user with login ID <login_id> exists
When the administrator user modifies the last name of the user with <new_name>
Then the last name of the user is modified with <new_name> successfully
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|login_id|new_name|action|
|sysadmin|GE_ServiceUserToModify|testuser|MODIFY_USER|
|sysadmin|GE_ServiceUserToModify|testUSER|MODIFY_USER|
|sysadmin|GE_ServiceUserToModify|user1|MODIFY_USER|

Scenario: User management portal shall not allow creation of user with invalid first name
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1410
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
When the administrator user creates a user with with login ID <login_id>,invalid first name <invalid_firstname> and last name <lastname>
Then the system does not allow creation of the user
Examples:
|openiam_admin_user|invalid_firstname|lastname|login_id|
|sysadmin|invalidcharacterat@q12|user|test_invalid_firstname|
|sysadmin||user|test_invalid_firstname|
|sysadmin|longfirstnamewwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww|user|test_invalid_firstname|

Scenario: User management portal shall not allow creation of user with invalid last name
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1410
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
When the administrator user creates a user with login ID <login_id>,first name <firstname> and invalid last name <invalid_lastname>
Then the system does not allow creation of the user
Examples:
|openiam_admin_user|invalid_lastname|firstname|login_id|
|sysadmin|invalidcharacterat@q12|user|test_invalid_lastname|
|sysadmin||user|test_invalid_lastname|
|sysadmin|longlastnamewwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww|user|test_invalid_lastname|

Scenario: User management portal shall not allow modification of user by administrator user with invalid first name
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1412
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
And a user with login ID <login_id> exists
When the administrator user updates user with invalid first name <invalid_firstname>
Then the system does not allow modification of first name of the user
Examples:
|openiam_admin_user|login_id|invalid_firstname|
|sysadmin|GE_ServiceUserForInvalidModify|longfirstnamewwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww|
|sysadmin|GE_ServiceUserForInvalidModify||
|sysadmin|GE_ServiceUserForInvalidModify|test@1!$|

Scenario: User management portal shall not allow modification of user by administrator user with invalid last name
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1412
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
And a user with login ID <login_id> exists
When the administrator user updates user with invalid last name <invalid_lastname>
Then the system does not allow modification of last name of the user
Examples:
|openiam_admin_user|login_id|invalid_lastname|
|sysadmin|GE_ServiceUserForInvalidModify|longlastnamewwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww|
|sysadmin|GE_ServiceUserForInvalidModify||
|sysadmin|GE_ServiceUserForInvalidModify|test@1!$|

Scenario: User management portal shall allow resetting of user password by administrator user
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1431
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
And a user with login ID <login_id> exists
When the administrator user reset password to a temp password <temp_password>
Then the reset password by the administrator user is successful
And the user is allowed to login with new password <temp_password>
Examples:
|openiam_admin_user|login_id|temp_password|
|sysadmin|GE_UserForPasswordResetAdmin|AutomatioN$1|

Scenario: Self service portal shall allow users to change their own passwords
Meta:
@skip
@ignored true
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1431
@user-aaa
Given a user with login ID <login_id> and password <password> is logged into self service portal to change the password
When the user changes the password to <new_password>
Then the password is changed for the user
And the user is allowed to login with new password <new_password>
Examples:
|login_id|password|new_password|
|GE_UserForPasswordResetSelf|Password$51|Password$2|

Scenario: User management portal shall not allow the administrator user to reset the user password with a non-compliant password
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1431
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
And a user with login ID <login_id> exists
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the administrator user resets the password of the user to <non_compliant_password> which is non-compliant to the password policy
Then the password is not changed for the user
Examples:
|openiam_admin_user|login_id|password|non_compliant_password||
|sysadmin|GE_UserForNonCompPassword|Password$51|abc|


Scenario: Self service portal shall not allow user to change their password with a non-compliant password
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1431
@user-aaa
Given a user with login ID <login_id> and password <password> is logged into self service portal to change the password
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user changes the password to <non_compliant_password> which is non-compliant to the password policy
Then the password is not changed for the user
Examples:
|login_id|password|non_compliant_password|
|GE_UserForNonCompPassword|Password$51|abc|

Scenario: User management portal shall allow searching for the user by last name
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1412
@platssrs_1414
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
And a user with login ID <login_id> exists
When the adminstrator user searches for the user in the field <field> with the value <value>
Then the users with the lastname <value> is displayed in the result
Examples:
|openiam_admin_user|field|value|login_id|
|sysadmin|LASTNAME|UserForSearch|GE_ServiceUserForSearch|

Scenario: User management portal shall allow searching for the user by first name
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1412
@platssrs_1414
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
And a user with login ID <login_id> exists
And the administrator user adds the first name field in search criteria
When the adminstrator user searches for the user in the field <field> with the value <value>
Then the users with the firstname <value> is displayed in the result
Examples:
|openiam_admin_user|field|value|login_id|
|sysadmin|FIRSTNAME|GESearch|GE_ServiceUserForSearch|

Scenario: User management portal search does not give any results when the user doesn't exist
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1412
@platssrs_1414
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
And a user with login ID <login_id> is deleted
And the administrator user adds the first name field in search criteria
When the adminstrator user searches for the user in the field <field> with the value <value>
Then the search result does not display any user
Examples:
|openiam_admin_user|field|value|login_id|
|sysadmin|FIRSTNAME|GEDeleted|GE_DeletedUserForSearch|

Scenario: User management portal shall allow searching for the user by login ID
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1412
@platssrs_1414
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
And a user with login ID <login_id> exists
When the adminstrator user searches for the user in the field <field> with the value <value>
Then the users with the login id <value> is displayed in the result
Examples:
|openiam_admin_user|field|value|login_id|
|sysadmin|PRINCIPAL|GE_ServiceUserForSearch|GE_ServiceUserForSearch|

Scenario: User management portal shall allow searching for the user by email
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1412
@platssrs_1414
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
And a user with login ID <login_id> exists
When the adminstrator user searches for the user in the field <field> with the value <value>
Then the users with the email <value> is displayed in the result
Examples:
|openiam_admin_user|field|value|login_id|
|sysadmin|EMAIL|userForSearch@ge.com|GE_ServiceUserForSearch|

Scenario: User management portal shall allow creation of user with role by administrator user for valid inputs
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1410
@platssrs_1452
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
And the role <role> exists
When the administrator user creates user with login ID <login_id>,first name <firstname>,last name <lastname> and role <role>
Then the user is created successfully with login ID <login_id>,first name <firstname> and last name <lastname>
And the role <role> is assigned to the created user
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|login_id|firstname|lastname|role|action|
|sysadmin|GE_ServiceUserCreateWithRole|GE|ServiceUserCreateWithRole|GE Administrator|CREATE_USER|

Scenario:User management portal shall log the action of password reset by administrator user
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1528
@user-aaa
Given an administrator user <openiam_admin_user> reset the password for a user <user> with <new_password>
When the administrator logs in to user management portal to retrieve the audit logs
Then a log is present for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|user|new_password|action|
|sysadmin|GE_UserForLogPasswordResetAdmin|automation@1|USER_RESET_PASSWORD|


Scenario: User management portal shall log the action of password change by the user
Meta:
@skip
@ignored true
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1528
@user-aaa
Given a user with login ID <login_id> changed the password from <password> to <new_password> in self service portal
When the administrator logs in to user management portal to retrieve the audit logs
Then a log is present for password change by the user with the log action <action>
Examples:
|openiam_admin_user|login_id|password|new_password|action|
|sysadmin|GE_UserForLogPasswordResetSelf|Password$51|Password$2|CHANGE_PASSWORD|

Scenario: Self service portal shall allow the user to reset the password by challenge response answers
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1431
@user-aaa
Given a user with login ID <login_id> wants to retrieve the forgotten password
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user resets the password to <new_password> by providing the challenge response answers
Then the reset password by the user is successful
And the user is allowed to access the self service portal with the new password
Examples:
|login_id|new_password|
|GE_ServiceUserForgotPassword|passwd02|

Scenario: Self service portal shall not allow the user to reset the password by incorrect challenge response answers
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1431
@user-aaa
Given a user with login ID <login_id> wants to retrieve the forgotten password
And the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180|
When the user initiates the reset password process And the user provides an incorrect challenge response answer(s) <incorrect_QA>
Then the user is not allowed to reset the password
Examples:
|login_id|incorrect_QA|
|GE_InvalidUserForgotPassword|{What is your mothers maiden name?:invalid_name,Where did you go to school?:invalid_school,What is your pets name?:invalid_pet}|
|GE_InvalidUserForgotPassword2|{What is your mothers maiden name?:"",Where did you go to school?:"",What is your pets name?:""}|

Scenario:Self service portal shall allow the user to set the challenge response questions and answers
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1431
@user-aaa
Given a user with login ID <login_id> is logged into selfservice portal for the first time
And the user is prompted for challenge response questions and answers
When the user sets the challenge response questions and answers <challenge_response> and custom questions and answers <custom_question>
Then the challenge response questions and answers are successfully set for the user
And the user is allowed to access the self service portal
Examples:
|login_id|password|challenge_response|custom_question|
|GE_UserSetChallengeResponseQA|Password$51|{Where did you go to school?:school,What are the last four digits of your social security number?:1234,What is your mothers maiden name?:name}|{What is your favourite colour ?:pink,What is your first car?:swift,What is your hobby?:painting}|

Scenario:User management portal shall log the password reset action through challenge response answers
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1431
@user-aaa
Given the password policy exists with the below attributes
|PWD_HIST_VER|PWD_LEN|NUMERIC_CHARS|REJECT_CHARS_IN_PSWD|QUEST_ANSWER_CORRECT|QUEST_COUNT|CHNG_PSWD_ON_RESET|PWD_EXP_WARN|PWD_EXPIRATION|
|6|8,12|1,2|<>|3|3|true|1,5|180| 
And a user with login ID <login_id> reset the password to <new_password> by providing the challenge response answers
When the administrator logs in to user management portal to retrieve the audit logs
Then a log is generated for the password change through challenge response answers with the log actions <actions>
Examples:
|login_id|new_password|actions|openiam_admin_user|
|GE_ServiceUserLogForgotPassword|passwd02|RESET_PASSWORD_FORM_SUBMIT,ANSWER_UNLOCK_PASSWORD_QUESTIONS|sysadmin|

Scenario: Self service portal shall allow the user to retrieve the user name by providing email id (skipping this scenario for now)
Meta:
@skip
@ignored true
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1412
@platssrs_1414
@user-aaa
Given a user with login ID <login_id> wants to retrieve the forgotten user name
When the user initiates the retrieval process by providing the registered email id <email>
Then the user name <user_name> is sent to the provided email id
Examples:
|login_id|email|user_name|
|GE_ServiceUserForgotPassword|xyz@ge.com|

