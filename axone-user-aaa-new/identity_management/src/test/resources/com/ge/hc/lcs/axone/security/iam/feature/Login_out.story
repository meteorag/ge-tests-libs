Feature: Audit logs for user actions
Narrative:
As an authorized user
I need system to log user actions
So that logs are available for audit

Scenario: System shall log successful login of a user into user management portal
Meta:
@gwtasreq
@scenasreq
@platssrs_1528
@platssrs_1801
@user-aaa
@scenplatdds_ID
Given an administrator user <openiam_admin_user> with password <password> is logged into user management portal
When the administrator user retrieves the audit logs in user management portal
Then a log exists for successful login of administrator user into user management portal
Examples:
|openiam_admin_user|password|
|sysadmin|passwd00|

Scenario: System shall log successful login of a user into self service portal
Meta:
@gwtasreq
@scenasreq
@platssrs_1528
@platssrs_1801
@user-aaa
@scenplatdds_ID
Given a user with login ID <login_id> and password <password> is logged into self service portal
And an administrator user <openiam_admin_user> with password <admin_password> is logged into user management portal to view logs
When the administrator user retrieves the audit logs in user management portal
Then a log exists for successful login of user into self service portal
Examples:
|login_id|password|openiam_admin_user|admin_password|
|ge_loguser|Password$51|sysadmin|passwd00|

Scenario: System shall log successful logout of user from user management portal
Meta:
@gwtasreq
@scenasreq
@platssrs_1528
@user-aaa
@scenplatdds_ID
Given an administrator user <openiam_admin_user> is logged out from user management portal
And logs again to user management portal to view logs
When the administrator user retrieves the audit logs in user management portal
Then a log exists for successful logout of administrator user from user management portal
Examples:
|openiam_admin_user|admin_password|
|sysadmin|passwd00|

Scenario: System shall log successful logout of user from self service portal
Meta:
@gwtasreq
@scenasreq
@platssrs_1528
@user-aaa
@scenplatdds_ID
Given a user with login ID <login_id> is logged out from self service portal
And an administrator user <openiam_admin_user> with password <admin_password> is logged into user management portal to view logs
When the administrator user retrieves the audit logs in user management portal
Then a log exists for successful logout of user from self service portal
Examples:
|login_id|password|openiam_admin_user|admin_password|
|ge_logoutuser|Password$51|sysadmin|passwd00|

Scenario: System shall log failed login attempt of user into user management portal
Meta:
@gwtasreq
@scenasreq
@platssrs_1528
@platssrs_1802
@user-aaa
@scenplatdds_ID
Given an adminstrator user <openiam_admin_user> has failed to login to user management portal at the first attempt
And succeeds to login to user management portal in the subsequent attempt to view logs
When the administrator user retrieves the audit logs in user management portal
Then a log exists for failed login attempt of user into user management portal
Examples:
|openiam_admin_user|admin_password|
|sysadmin|passwd00|

Scenario: System shall log failed login attempt of user into self service portal
Meta:
@gwtasreq
@scenasreq
@platssrs_1528
@platssrs_1802
@user-aaa
@scenplatdds_ID
Given a user with login ID <login_id> has failed to login to self service portal
And an administrator user <openiam_admin_user> with password <admin_password> is logged into user management portal to view logs
When the administrator user retrieves the audit logs in user management portal
Then a log exists for failed login attempt of user into self service portal
Examples:
|openiam_admin_user|admin_password|login_id|
|sysadmin|passwd00|ge_loguser|
