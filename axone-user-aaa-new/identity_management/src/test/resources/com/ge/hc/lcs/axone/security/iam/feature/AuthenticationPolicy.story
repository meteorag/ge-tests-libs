Feature: Configuring authentication policy using user management portal
Narrative:
As an authorized user who manages user access controls
I need to configure authentication policy
So that the authentication policy will be enforced on user authentication

Scenario:User management portal shall allow the administrator user to configure the failed authentication count attribute in authentication policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
And the authentication policy exists with the below attributes
|FAILED_AUTH_COUNT|AUTO_UNLOCK_TIME|
|3|30|
When the user updates failed authentication count attribute with <failed_auth_count> in the authentication policy
Then the failed authentication count attribute is configured successfully with <failed_auth_count>
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|failed_auth_count|action|
|sysadmin|4|MODIFY_AUTHENTICATION_POLICY|

Scenario: Update auto unlock time attribute in authentication policy by administrator user
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
And the authentication policy exists with the below attributes
|FAILED_AUTH_COUNT|AUTO_UNLOCK_TIME|
|3|30|
When the user updates auto unlock time attribute with <auto_unlock_time> in the authentication policy
Then the auto unlock time attribute is configured successfully with <auto_unlock_time>
And a log is generated for the aforementioned action(s) <action>
Examples:
|openiam_admin_user|auto_unlock_time|action|
|sysadmin| 50 |MODIFY_AUTHENTICATION_POLICY|

Scenario: Updated authentication policy is enforced while the user attempts failed login
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
@user-aaa
Given the authentication policy exists with the below attributes
|FAILED_AUTH_COUNT|AUTO_UNLOCK_TIME|
|3|30|
And the administrator user <openiam_admin_user> updates failed authentication count attribute with <failed_auth_count> in the authentication policy
And a user with login ID <login_id> exists
And the user is already made <failed_auth_count> failed login attempts
When the user tries to login with invalid password <invalid_password> to self service portal
Then the system does not allow the user to login to self service portal
And the account of the user is locked
Examples:
|openiam_admin_user|login_id|failed_auth_count|invalid_password|
|sysadmin|GE_UserForAuthenticationPolicy|4|invalid|

Scenario: User management portal shall not allow the administrator user to configure the failed authentication count attribute with invalid value in authentication policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
And the authentication policy exists with the below attributes
|FAILED_AUTH_COUNT|AUTO_UNLOCK_TIME|
|3|30|
When the user updates failed authentication count attribute with invalid value as <invalid_failed_auth_count> in the authentication policy
Then the system does not allow modification of failed authentication count attribute
Examples:
|openiam_admin_user|invalid_failed_auth_count|
|sysadmin|sdsdvxv|
|sysadmin|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|
|sysadmin|*#$%&**|


Scenario: User management portal shall not allow the administrator user to configure the auto unlock time attribute with invalid value in authentication policy
Meta:
@scenplatdds_ID
@gwtasreq
@scenasreq
@platssrs_1430
@user-aaa
Given an administrator user <openiam_admin_user> is logged into user management portal
And the authentication policy exists with the below attributes
|FAILED_AUTH_COUNT|AUTO_UNLOCK_TIME|
|3|30|
When the user updates auto unlock time attribute with invalid value as <invalid_auto_unlock_time> in the authentication policy
Then the system does not allow modification of auto unlock time attribute
Examples:
|openiam_admin_user|invalid_auto_unlock_time|
|sysadmin|sdsdvxv|
|sysadmin|100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000|
|sysadmin|*#$%&**|
