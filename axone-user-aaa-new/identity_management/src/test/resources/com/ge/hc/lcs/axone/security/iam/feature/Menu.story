Feature: Managing user menus
Narrative:
As a authorized user for user management portal
I need to ensure only the required menus are available
So that the appropriate menus are available

@scenplatssrs_1452
@scenplatssrs_1462
@scenplatssrs_1471
@scenplatssrs_1448
@scenplatssrs_1528

Scenario: Verify Menus are availble
Given the <authorized_user> is logged into user management portal
When the <authorized_user> verifies the <menu> is available
Then the <menu> is available
Examples:
|authorized_user|menu|
|sysadmin|User Admin|
|sysadmin|User Admin -> User|
|sysadmin|User Admin -> User -> Create User|

Scenario: Verify Menus are not availble
Given the <authorized_user> is logged into user management portal
When the <authorized_user> verifies the <menu> is available
Then the <menu> is not available
Examples:
|authorized_user|menu|
|sysadmin|User Admin -> Organization|
|sysadmin|User Admin -> Organization Types|
|sysadmin|Access Control -> Group|
|sysadmin|Access Control -> Content Providers|
|sysadmin|Access Control -> Resource Type|