Feature: Managing user access control using user management portal
Narrative:
As an authorized user who manages user access controls
I need the system to allow management of roles of users
So that users have access control mapped to roles

@gwtasreq
@scenasreq
@platssrs_1471
@user-aaa
Scenario: User management portal shall allow authorized user to list the roles in the system
Meta:
@scenplatdds_ID
Given an administrator user <openiam_admin_user> is logged into user management portal
And role(s) <roles> exists in the system
When the administrator user lists the roles
Then the roles <roles> in the system are displayed
Examples:
|openiam_admin_user|roles| 
|sysadmin|Super Security Admin,GE Administrator,IT Biomed Administrator,Clinician Administrator|

@gwtasreq
@scenasreq
@platssrs_1452
@platssrs_1528
@user-aaa
Scenario: User management portal shall allow authorized user to assign role(s) to a user
Meta:
@scenplatdds_ID
Given an administrator user <openiam_admin_user> is logged into user management portal
And the role(s) <roles> exists in the system
And a user with login ID <login_id> exists in the system
When the administrator user assigns the role(s) <roles> to the user
Then the role(s) <roles> are assigned to the user
Then a log is generated for the aforementioned action(s) <action>
Examples:  
|openiam_admin_user|login_id|roles|action|
|sysadmin|ge.roleuser01|GE Administrator|ADD_USER_TO_ROLE|
|sysadmin|ge.roleuser01|Clinician Administrator|ADD_USER_TO_ROLE|
|sysadmin|ge.roleuser01|IT Biomed Administrator|ADD_USER_TO_ROLE|
|sysadmin|ge.roleuser02|IT Biomed Administrator,Clinician Administrator|ADD_USER_TO_ROLE|
|sysadmin|ge.roleuser02|GE Administrator,Clinician Administrator|ADD_USER_TO_ROLE|
|sysadmin|ge.roleuser02|IT Biomed Administrator,GE Administrator|ADD_USER_TO_ROLE|
|sysadmin|ge.roleuser02|IT Biomed Administrator,GE Administrator,Clinician Administrator|ADD_USER_TO_ROLE|

@gwtasreq
@scenasreq
@platssrs_1462
@platssrs_1528
@user-aaa
Scenario: User management portal shall allow authorized user to remove role(s) assigned to a user
Meta:
@scenplatdds_ID
Given an administrator user <openiam_admin_user> is logged into user management portal
And a user with login ID <login_id> exists in the system
And the role(s) <role> are assigned to the user
When the administrator user removes the role(s) <role> from the user
Then the role(s) <role> are removed for the user
Then a log is generated for the aforementioned action(s) <action>
Examples:  
|openiam_admin_user|login_id|role|action|
|sysadmin|ge.roleuser03|GE Administrator|REMOVE_USER_FROM_ROLE|
|sysadmin|ge.roleuser03|Clinician Administrator|REMOVE_USER_FROM_ROLE|
|sysadmin|ge.roleuser03|IT Biomed Administrator|REMOVE_USER_FROM_ROLE|

@gwtasreq
@scenasreq
@platssrs_1462
@platssrs_1528
@user-aaa
Scenario: User management portal shall allow authorized user to update role of a user
Meta:
@scenplatdds_ID
Given an administrator user <openiam_admin_user> is logged into user management portal
And a user with login ID <login_id> exists in the system
And the role <role> is assigned to the user
When the administrator user updates the user role from <role> to <new_role>
Then the role <role> is removed for the user
Then the new role <new_role> is assigned to the user
Then a log is generated for the aforementioned action(s) <actions>
Examples:  
|openiam_admin_user|login_id|role|new_role|actions|
|sysadmin|ge.roleuser04|IT Biomed Administrator|Clinician Administrator|REMOVE_USER_FROM_ROLE,ADD_USER_TO_ROLE|
|sysadmin|ge.roleuser04|Clinician Administrator|IT Biomed Administrator|REMOVE_USER_FROM_ROLE,ADD_USER_TO_ROLE|
|sysadmin|ge.roleuser04|GE Administrator|IT Biomed Administrator|REMOVE_USER_FROM_ROLE,ADD_USER_TO_ROLE|
|sysadmin|ge.roleuser04|GE Administrator|Clinician Administrator|REMOVE_USER_FROM_ROLE,ADD_USER_TO_ROLE|
|sysadmin|ge.roleuser04|Clinician Administrator|GE Administrator|REMOVE_USER_FROM_ROLE,ADD_USER_TO_ROLE|
|sysadmin|ge.roleuser04|IT Biomed Administrator|GE Administrator|REMOVE_USER_FROM_ROLE,ADD_USER_TO_ROLE|
