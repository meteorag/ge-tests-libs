Feature: User Authentication Oauth

Narrative:
The application allows user to login

Meta:
@TestType Functional
@Paramater Username


Scenario: User Authentication for a web application client
Given the application is a third-party web <application> running on a server
When the user accesses the <resource> of the <application>
And the user provides the <username> and <password> via Authentication provider login page
Then the system authenticates the user through the authorization endpoint
And the system authenticates the application through the token endpoint
And the system grants access to the <resource>
Examples: 
| username | password | application      | resource| 
| admin.usr7 | Password$51 | http://3.204.109.101:8083 | oauth-info | 

Scenario: User Authentication for a third-party native application client
Given the application is a third-party native <application> running on user workstation
When the user accesses the <resource> of the <application>
And the user provides the <username> and <password> via Authentication provider login page
Then the system authenticates the user through the token endpoint
And the system grants access to the <resource>
Examples: 
| username | password | application | resource | 
| admin.usr7 | Password$51 | http://3.204.109.101:8082 | oauth-info |

Scenario: User Authentication for a first-party native application client
Given product is a first-party <application> running on user workstation as a client
When the user accesses the <resource> of the <application>
And a user provides the <username> and <password> via first-party <application> login page
Then the system authenticates the user through the token endpoint
And the system grants access to the <resource>
Examples: 
| username | password | application | resource | 
| admin.usr7 | Password$51 | http://3.204.109.101:8084 | oauth-info | 

Scenario: Authentication of a web Client when invalid re-directURI is provided
Given the application is a third-party web <application> running on a server
When the user accesses the <resource> of the <application>
And the user provides the <username> and <password> via Authentication provider login page
Then an unregistered redirect-URI <exceptionMessage> message received by client
Examples: 
| username | password | application      | resource| exceptionMessage | 
| admin.usr7 | Password$51 | http://3.204.109.101:8088 | oauth-info | invalid_request - Redirect URI for Client ID invalidClientID is not registered | 

Scenario: Authentication of a user when invalid username/ invalid password is provided
Given the application is a third-party web <application> running on a server
When the user accesses the <resource> of the <application>
And the user provides the <username> and <password> via Authentication provider login page
Then an invalid user <exceptionMessage> message received from the authorization endpoint
Examples: 
| username    | password| exceptionMessage        | application        | resource |
| GE.ServiceUse | Password$51 | Invalid Login and/or Password | http://3.204.109.101:8083 | oauth-info |  
| GE.ServiceUse | Password$51 | Invalid Login and/or Password | http://3.204.109.101:8083 | oauth-info |  
| admin.usr7 | Password$5 | Invalid Login and/or Password | http://3.204.109.101:8083 | oauth-info |  
| admin.usr7 | Password$5 | Invalid Login and/or Password | http://3.204.109.101:8083 | oauth-info |   


Scenario: Authentication of a client when invalid clientID is provided
Given the <application> is started with invalid client_id
When the user accesses the <resource> of the <application>
And the user provides the <username> and <password> via Authentication provider login page
Then an unregistered Client <exceptionMessage> message received from the authorization endpoint
Examples: 
| username | password |  application      | resource | exceptionMessage                                               | 
| admin.usr7 | Password$51 | http://3.204.109.101:8087 | oauth-info |invalid_request - OAuth Client ID invalid_cient_id is not registered | 
  
Scenario: Authentication of a client when valid clientID but invalid clientSecret is provided
Given the <application> is started with valid client_id but invalid client_password
When the user accesses the <resource> of the <application>
And the user provides the <username> and <password> via Authentication provider login page
Then the system authenticates the user through the authorization endpoint
And an unauthorized Client <exceptionMessage> message received from the token endpoint
Examples: 
| application      | username | password | resource | exceptionMessage                                        | 
| http://3.204.109.101:8086 | admin.usr7 | Password$51  | oauth-info | This is error page due to unauthorized access or System error unauthorized_client, Client ID / Client secret don't match | 


Scenario: User Authentication for a web application client
Given the application is a third-party web <application> running on a server
And the user accesses the <resource> of the <application>
And the user provides the <username> and <password> via Authentication provider login page
When the admin verifies the log for the <event> with status <status> for <username>
Then the log is generated for the <event> for <username> with <status> and hostname

Examples: 
|username|password|application|resource|event|status|
|admin.usr7|Password$51|http://3.204.109.101:8083|oauth-info|LOGIN|SUCCESS|

Scenario: Authentication of a user when invalid username/ invalid password is provided
Given the application is a third-party web <application> running on a server
And the user accesses the <resource> of the <application>
And the user provides the <username> and <password> via Authentication provider login page
When the admin verifies the log for the <event> with status <status> for <username>
Then the log is generated for the <event> for <username> with <status> and hostname
Examples: 
|username|password|application|resource|event|status|
|GE.ServiceUse|Password$51|http://3.204.109.101:8083|oauth-info|LOGIN|FAILURE|
|GE.ServiceUser1|Password$5|http://3.204.109.101:8083|oauth-info|LOGIN|FAILURE|