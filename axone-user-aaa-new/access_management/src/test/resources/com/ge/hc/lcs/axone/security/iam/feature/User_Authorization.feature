Feature: User Authorization
As a product solutions owner
I want the system to have the ability to authorize the users of the product to various resources on the product
So that the system is protected from malicious intruders and system functionality is not threatened by unintended usage that could potentially endanger patient safety.

Scenario: Getting access to application resources
Given the user is trying to use a <resource> of <application>
When the <user> is authenticated
Then based on the role of the user and application's permission the request for <resource> is <granted>
Examples:
|user|application|resource|granted|

|admin.usr7|http://3.204.109.101:8083|geservicepage|accessible|

|admin.usr7|http://3.204.109.101:8082|geservicepage|not accessible|

|Biomed.adminUser7|http://3.204.109.101:8083|geservicepage|not accessible|

|admin.usr7|http://3.204.109.101:8083|hospitalpage|not accessible|

Scenario: Getting access to User information by the client
Given <user> is trying to use a scope of <application>
When the application accesses the <user information> from userInfoEndpoint
Then based on the role of the user and application's permission the <user information> is <granted>
Examples:
|user|application|user information|granted|

|admin.usr7|http://3.204.109.101:8083|nickname|accessible|

|admin.usr7|http://3.204.109.101:8082|nickname|not accessible|

|Biomed.adminUser7|http://3.204.109.101:8083|nickname|not accessible|

|admin.usr7|http://3.204.109.101:8083|middle_name|not accessible|

Scenario: Getting information about the token by the client
Given the user is trying to use a <resource> of <application>
When the <user> is authenticated to get the scope information using the tokenInfoEndpoint
Then the scope of the token <matches> with the <resource>
Examples:
|user|application|resource|matches|

|admin.usr7|http://3.204.109.101:8083|geservicepage|accessible|

|admin.usr7|http://3.204.109.101:8083|hospitalpage|not accessible| 
