package com.ge.hc.lcs.axone.security.iam.utilities;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public class HttpURLParameter {
	private URI baseUrl;
	List<NameValuePair> parsedUrlList;
	private String encoding = new String("UTF-8");
	
	public HttpURLParameter(String url)
	{
		try 
		{
			baseUrl = new URI(url);
		}
		catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
	
	public void parse()
	{
		parsedUrlList = URLEncodedUtils.parse(baseUrl,encoding);
	}
	
	public String searchParameter(String parameterName)
	{
		String parameterValue = null;
		for (NameValuePair param : parsedUrlList) 
		{
		  if (param.getName().equals(parameterName))
		  {
	        parameterValue = param.getValue();
		  }
		}
		
		return parameterValue;
	}
}
