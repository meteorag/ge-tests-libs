package com.ge.hc.lcs.axone.security.iam.pageobjects;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ge.hc.lcs.axone.security.iam.aaa_enum.AuthorizationServerEndpointEnum;
import com.ge.hc.lcs.axone.security.iam.utilities.HttpURLParameter;


import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;


/**
 * 
 * @author 502526482
 *
 */
public class ClientApplicationPage extends PageObject {
	
	private final int driverSleep=60;
	private final int threadSleep=1500;

	@FindBy(xpath = "//div/a[contains(text(),'Click here')]")
	private WebElementFacade loginButton;
	private WebDriver driver;
	private String openClientLink = "//div/a[@href='oauth-info' and contains(text(),\"Click here\")]";
	private String userInfoTable = "//div[@id=\"userinformation\"]/table[@id=\"userinformation_table\"]//";
	private String userScopeTable = "//div[@id='userscopes']/table[@id='userscopes_table']//";
	private String tokenInfoTable = "//div[@id='tokeninformation']/table[@id='tokeninformation_table']//";
	private String resourceNotAllowedError = "//div[@id='resourceNotAllowed']/p[@id='errorMessage']";
	private static WebDriverWait wait;
	private static String clientRedirectUrl;
	

	public ClientApplicationPage(WebDriver driver){
		super(driver);
		this.driver = driver;
		wait = new WebDriverWait(driver, driverSleep);
	}

	/**
	 * This method is for opening the client application at the specified url.
	 * @param startingUrl - Clinet application URL
	 * @throws Exception
	 */
	public void openClient(String startingUrl) throws Exception
	{
		this.openAt(startingUrl);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(openClientLink)));
	}
	public void closeDriver() throws Exception
	{
		
		this.driver.close();;
		Thread.sleep(threadSleep);
	}
	/**
	 * This method is for clicking the login button on the client application
	 * @throws Exception
	 */
	public void login() throws Exception
	{
		loginButton.click();
	}

	/**
	 * This method is for getting the authorization code
	 * @return string
	 * @throws InterruptedException 
	 * @throws URISyntaxException 
	 */
	public String getAuthorizationCode() throws InterruptedException, URISyntaxException
	{
		while(true){
		String reDirecturl = driver.getCurrentUrl();
			if(reDirecturl.contains("code")){
				ClientApplicationPage.clientRedirectUrl = reDirecturl;
				break;
			}
		}
		HttpURLParameter utility=new HttpURLParameter(ClientApplicationPage.clientRedirectUrl);
		utility.parse();
		
		String authorizationCode = utility.searchParameter("code");
		return authorizationCode;
	}
	
	/**
	 * This method is for selecting and navigating to a service specified in the passed parameter
	 * @param serviceName
	 * @throws Exception
	 */
	public void selectServiceOnClient(AuthorizationServerEndpointEnum serviceName) throws Exception
	{
		String endpointLabel = serviceName.getValue();
		clickLinkOnClient(endpointLabel);
	}

	public void selectResourceOnClient(String resourceName) throws Exception
	{
		clickLinkOnClient(resourceName);
	}
	
	
	/**
	 * This method is for processing the user request by returning the required data
	 * @param service
	 * @throws Exception
	 */
	public HashMap<String,String> returnRequestedData(AuthorizationServerEndpointEnum service) throws Exception
	{
		HashMap<String,String> requestedDataMap = null;
		
		switch(service){
			case TOKEN:
				requestedDataMap = this.returnTokenInformation();
				break;
			case USER_INFO:
				requestedDataMap = this.returnUserInformation();
				break;
			case TOKEN_INFO:
				requestedDataMap = this.returnScopeInformation();
				break;
		}

		return requestedDataMap;
	}

	/**
	 * This method is for getting the user information
	 * @return
	 * @throws Exception
	 */
	public HashMap<String,String> returnUserInformation() throws Exception
	{
		int rowNumber = 2;
		String currentValue="";
		String currentKey="";
		List<WebElement> userInfoHeaderElements;
		HashMap<String, String> userInformationMap = new HashMap<String, String>();

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(userInfoTable+"tr/td")));
		userInfoHeaderElements = driver.findElements(By.xpath(userInfoTable+"tr/td[1]"));

		for(WebElement currentHeaderElement: userInfoHeaderElements)
		{

			currentKey = currentHeaderElement.getText();
			currentValue = driver.findElement(By.xpath(userInfoTable+"tr["+rowNumber+"]/td[2]")).getText();
			userInformationMap.put(currentKey, currentValue);
			rowNumber++;
		}
		
		return userInformationMap;
	}
	
	/**
	 * This method is for getting the scope information
	 * @return
	 * @throws Exception
	 */
	public HashMap<String,String> returnScopeInformation() throws Exception
	{
		int rowNumber = 2;
		String currentValue;
		String currentKey;
		List<WebElement> scopeInfoElements ;
		HashMap<String, String> scopeInformationMap = new HashMap<String, String>();

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(userScopeTable+"tr/td")));
		scopeInfoElements = driver.findElements(By.xpath(userScopeTable+"tr/td[1]"));

		for(WebElement currenScopetElement: scopeInfoElements)
		{
			currentKey = currenScopetElement.getText();
			currentValue = driver.findElement(By.xpath(userScopeTable+"tr["+rowNumber+"]/td[2]")).getText();
			scopeInformationMap.put(currentKey, currentValue);
			rowNumber++;
		}
		
		return scopeInformationMap;
	}
	
	/**
	 * This method is for getting the token information
	 * @return
	 * @throws Exception
	 */
	public HashMap<String,String> returnTokenInformation() throws Exception
	{
		int rowNumber = 2;
		String currentValue;
		String currentKey;
		List<WebElement> tokenKeyElements ;
		HashMap<String, String> tokenInformationMap = new HashMap<String, String>();

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(tokenInfoTable+"tr/td")));
		tokenKeyElements = driver.findElements(By.xpath(tokenInfoTable+"tr/td[1]"));

		for(WebElement currentHeaderElement: tokenKeyElements)
		{
			currentKey = currentHeaderElement.getText();
			currentValue = driver.findElement(By.xpath(tokenInfoTable+"tr["+rowNumber+"]/td[2]")).getText();
			tokenInformationMap.put(currentKey, currentValue);
			rowNumber++;
		}
		
		return tokenInformationMap;
	}

	/**
	 * This method is for navigating to a resource url, specified in the parameter
	 * @return
	 * @throws Exception
	 */
	public void accessResource(String resourceUrl) throws Exception
	{
		this.driver.get(resourceUrl);
		Thread.sleep(threadSleep);
	}

	/**
	 * This method is for checking if a resource is accessible for the current user or not. 
	 * Boolean true is returned if the resource is accessible else Boolean false is returned
	 * @param resourceAccessible
	 * @return
	 * @throws Exception
	 */
	public Boolean isCurrentResourceAccessible(String resourceAccessible) throws Exception
	{	
		String message;

		if("not accessible".equals(resourceAccessible))
		{
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(resourceNotAllowedError)));
			message = driver.findElement(By.xpath(resourceNotAllowedError)).getText();
			if(message.contains("This is error page due to unauthorized access or System error"))
				return false;
		}
		return true;
	}
	
	private void clickLinkOnClient(String Link) {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/a[@href='"+Link+"']")));
		driver.findElement(By.xpath("//div/a[@href='"+Link+"']")).click();
	}

}