package com.ge.hc.lcs.axone.security.iam.aaa_enum;

/**
 * 
 * The Enum is used for representing the end-Points for getting different informations 
 * like UserInfo,TokenInfoand userScopes.
 *
 */

public enum AuthorizationServerEndpointEnum {
	USER_INFO("userinformation"),
	TOKEN_INFO("userscopes"),
	TOKEN("tokeninformation");
	
	private final String info;
	
	private AuthorizationServerEndpointEnum(String info){
		this.info = info;
	}
	
	public String getValue(){
		return info;
	}
}
