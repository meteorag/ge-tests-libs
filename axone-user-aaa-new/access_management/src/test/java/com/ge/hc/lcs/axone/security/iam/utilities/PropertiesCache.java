/*
 * Copyright (C) 2016 General Electric Company. All Rights Reserved.
 */
package com.ge.hc.lcs.axone.security.iam.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;


/**
 * 
 * @author 212480093
 *
 */
public class PropertiesCache
{
	private final Properties configProp = new Properties();
	private static Logger logger = Logger.getLogger(PropertiesCache.class.getName());
	private PropertiesCache()
	{
		InputStream config;
		try {			
			config = new FileInputStream(".\\src\\config\\config.properties");
			configProp.load(config);
			
		} catch (IOException e) {
			logger.error("Exception: " + e.getMessage());
		}
	}

	private static class PropertyHolder 
	{
		private static final PropertiesCache INSTANCE = new PropertiesCache();
	}

	public static PropertiesCache getInstance() {
		return PropertyHolder.INSTANCE;
	}

	public String getProperty(String key) {
		return configProp.getProperty(key);
	}

	public Set<String> getAllPropertyNames() {
		return configProp.stringPropertyNames();
	}

	public boolean containsKey(String key) {
		return configProp.containsKey(key);
	}
}
