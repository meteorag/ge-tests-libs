package com.ge.hc.lcs.axone.security.iam.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.pages.PageObject;

public class ResourcePage extends PageObject {
	private final int threadSleep=5000;
	public ResourcePage(WebDriver driver){
		super(driver);
	}
	private String frameId= "contentFrame";
	private String selectResourceType = ".//*[@id='resourceType']";
	private String textResourceName = ".//*[@id='resourceName']";
	private String buttonSave = ".//*[@id='save']";

	public void createResource(String selectResType, String resName) throws InterruptedException{
		WebElement resType;
		WebElement resourceName;
		getDriver().switchTo().frame(frameId);
		(new WebDriverWait(getDriver(), 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(selectResourceType)));
		resType = getDriver().findElement(By.xpath(selectResourceType));
		selectFromDropdown(resType, selectResType);
		resourceName = find(By.xpath(textResourceName));
		resourceName.clear();
		resourceName.sendKeys(resName);
		find(By.xpath(buttonSave)).click();
		Thread.sleep(threadSleep);
		getDriver().switchTo().defaultContent();
	}
}
