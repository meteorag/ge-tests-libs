package com.ge.hc.lcs.axone.security.iam.config;

import java.util.List;

public class ConfigDTO {
	
	public List<UserDTO> userDTOs;
	public List<ApplicationDTO> applicationDTOs;
	
	public List<UserDTO> getUserDTOs() {
		return userDTOs;
	}
	public List<ApplicationDTO> getApplicationDTOs() {
		return applicationDTOs;
	}
	
	
}