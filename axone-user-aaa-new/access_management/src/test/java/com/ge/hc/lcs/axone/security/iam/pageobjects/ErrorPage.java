package com.ge.hc.lcs.axone.security.iam.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.pages.PageObject;

public class ErrorPage extends PageObject{

	private WebDriver driver;
	private static WebDriverWait wait;

	public ErrorPage(WebDriver driver){
		super(driver);
		this.driver = driver;
		wait = new WebDriverWait(driver, 60);
	}
	
	public String getErrormessageForAuthentication()
	{
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class=\"authError\"]/p[@class=\"error\"]")));
		System.err.println("error message : " + driver.findElement(By.xpath("//div[@class='authError']/p[@class='error']")).getText().trim());
		return driver.findElement(By.xpath("//div[@class='authError']/p[@class='error']")).getText().trim();
	}
	
	public String getUnauthorizedClientErrorMessage(){
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id=\"resourceNotAllowed\"]")));
		String firstErrorMessage = driver.findElement(By.xpath("//div[@id='resourceNotAllowed']//p[@id='errorMessage']")).getText().trim();
		String secondErrorMessage = driver.findElement(By.xpath("//div[@id='resourceNotAllowed']//span")).getText().trim();
        return (firstErrorMessage + " " + secondErrorMessage);
    }


}
