package com.ge.hc.lcs.axone.security.iam.pageobjects;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ge.hc.lcs.axone.security.iam.config.ResourceDTO;
import com.ge.hc.lcs.axone.security.iam.config.ScopeDTO;
import com.ge.hc.lcs.axone.security.iam.constants.UIComponentConstants;
import com.ge.hc.lcs.axone.security.iam.utilities.TestUtility;

import net.serenitybdd.core.pages.PageObject;

public class AuthenticationProviderPage extends PageObject{
	
	public AuthenticationProviderPage(WebDriver driver){
		super(driver);
	}
	private String frameId= "contentFrame";
	private String dropDownProviderType = ".//*[@id='select2-authProviderType-container']";
	private String textProviderType = ".//*[@id='js']/body/span/span/span[1]/input";
	private String textProviderName = ".//*[@id='name']";
	private String textRedirectUrl = ".//*[@id='OAuthRedirectUrl']/div/span/input";
	private String dropDownGrantFLow = ".//*[@id='select2-OAuthAuthorizationGrantFlow-container']";
	private String textGrantFlow = ".//*[@id='js']/body/span/span/span[1]/input";
	private String dropDownClientAuthenticationType = ".//*[@id='select2-OAuthClientAuthType-container']";
	private String textClientAuthenticationType = ".//*[@id='js']/body/span/span/span[1]/input";
	private String linkOauthScope = ".//*[@id='scopeListLink']";
	private String textOauth2Scope = "//div[@class='b-item m-input ctrlElement']/input[@id='name']";
	private String buttonSearchScopePopup = "//div[@class='b-btn m-button m-small m-type1']/a[contains(text(),'Search')]";
	private String closeScopePopup = ".//*[@id='js']/body/div[6]/div[1]/button";
	private String textTokenExpiration = "//input[@id='OAuthTokenExpiration']";
	private String radioButtonUseRefreshToken = "//div[@class='b-item__in m-radio']//input[@value='true']";
	private String buttonSave = ".//*[@id='save']";
	private String textEditOauth = "//div[@class='b-subline']//h1[contains(text(),'Edit oAuth2 client')]";
	private String tableProviderDetails = "//div[@id='entitlementsContainer']";
	private String inputClientID = "//input[@id='OAuthClientID']";
	private String inputClientSecret = "//input[@id='OAuthClientSecret']";
	private String textAuthenticationProviders = "//div[@class='b-table-wrapper']//h2[contains(text(),'Authentication Providers')]";
	
	public void registerClientWebApplication(String providerType, String providerName, List<String> redirectURLList, String authorizationGrantFlow, String clientAuthenticationType, List<ScopeDTO> scopeDTOs,List<ResourceDTO> resourceDTOs, String refreshTokenTime,String ClientID,String ClientSecret) throws InterruptedException{
			WebElement providertype;
			WebElement providername;
			WebElement clientUrl;
			WebElement grantFlow;
			WebElement clientType;
			WebElement tokenExp;
			getDriver().switchTo().frame(frameId);
			find(By.xpath(dropDownProviderType)).click();
			providertype = find(By.xpath(textProviderType));
			providertype.clear();
			providertype.sendKeys(providerType);
			find(By.xpath("//ul[@id='select2-authProviderType-results']/li[contains(text(),'"+providerType+"')]")).click();
			Thread.sleep(5000);
			providername = find(By.xpath(textProviderName));
			providername.clear();
			providername.sendKeys(providerName);
			clientUrl = find(By.xpath(textRedirectUrl));
			clientUrl.clear();
			for (String redirectURL : redirectURLList) {
				clientUrl.sendKeys(redirectURL);
				clientUrl.sendKeys(Keys.ENTER);
			}
			find(By.xpath(dropDownGrantFLow)).click();
			grantFlow = find(By.xpath(textGrantFlow));
			grantFlow.clear();
			grantFlow.sendKeys(authorizationGrantFlow);
			find(By.xpath("//span[@class='select2-results']/ul/li[contains(text(),'"+authorizationGrantFlow+"')]")).click();

			find(By.xpath(dropDownClientAuthenticationType)).click();
			clientType = find(By.xpath(textClientAuthenticationType));
			clientType.clear();
			clientType.sendKeys(clientAuthenticationType);
			find(By.xpath("//ul[@id='select2-OAuthClientAuthType-results']/li[contains(text(),'"+clientAuthenticationType+"')]")).click();
			for (Iterator<ResourceDTO> iterator = resourceDTOs.iterator(); iterator
					.hasNext();) {
				ResourceDTO resourceDTO = (ResourceDTO) iterator.next();
				this.assignScopeToClient(resourceDTO.resourceName);
				
			}
			for (Iterator<ScopeDTO> iterator = scopeDTOs.iterator(); iterator
					.hasNext();) {
				ScopeDTO scopeDTO = (ScopeDTO) iterator.next();
				this.assignScopeToClient(scopeDTO.scopeName);
				
			}
			tokenExp = find(By.xpath(textTokenExpiration));
			tokenExp.clear();
			tokenExp.sendKeys(refreshTokenTime);
			find(By.xpath(radioButtonUseRefreshToken)).click();
			find(By.xpath(buttonSave)).click();
			(new WebDriverWait(getDriver(), 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(textAuthenticationProviders)));
			editClientDetails(providerName,ClientID,ClientSecret);
			getDriver().switchTo().defaultContent();
		}
	private void assignScopeToClient(String scope) throws InterruptedException{
			
		
		WebElement textScope;
	
			find(By.xpath(linkOauthScope)).click();
			Thread.sleep(5000);
			
			textScope = find(By.xpath(textOauth2Scope));
			textScope.clear();
			textScope.sendKeys(scope);
			
			find(By.xpath(buttonSearchScopePopup)).click();
			Thread.sleep(3000);
			
			find(By.xpath("//td[@class='b-cell']//span[contains(text(),'"+scope+"')]/../..//i[contains(text(),'add')]")).click();
			
			find(By.xpath(closeScopePopup)).click();
			Thread.sleep(5000);
			
	}
	
		public void editClientDetails(String providerName,String ClientID,String ClientSecret) throws InterruptedException{
			WebElement clientID;
			WebElement clientSecret;
			getDriver().findElement(By.xpath(tableProviderDetails));
			TestUtility.sleep(UIComponentConstants.FIVE);
			find(By.xpath("//span[contains(text(),'"+providerName+"')]/../..//i[contains(text(),'mode_edit')]")).click();  
			(new WebDriverWait(getDriver(), 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(textEditOauth)));
			clientID = find(By.xpath(inputClientID));
			clientID.clear();
			clientID.sendKeys(ClientID);
			clientSecret = find(By.xpath(inputClientSecret));
			clientSecret.clear();
			clientSecret.sendKeys(ClientSecret);
			find(By.xpath(buttonSave)).click();
			(new WebDriverWait(getDriver(), 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(textAuthenticationProviders)));
		}

}
