package com.ge.hc.lcs.axone.security.iam.config;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

import org.springframework.core.io.ClassPathResource;

import com.google.gson.Gson;

public class ConfigUtil {
	
	private static Data data;
	static{
		ClassPathResource resource = new ClassPathResource("testData.json");
		 Reader fileReader = null;
		try {
			fileReader = new FileReader(resource.getFile());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
			Gson gson = new Gson();
			data = gson.fromJson(fileReader, Data.class);
	}

	public static List<ConfigDTO> getConfig() throws IOException {
		return data.configDTOs;
	}
}
