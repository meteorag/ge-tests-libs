package com.ge.hc.lcs.axone.security.iam.steps;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.BeforeStories;
import org.springframework.core.io.ClassPathResource;

import com.ge.hc.lca.axone.openiam.service.AuthProviderService;
import com.ge.hc.lca.axone.openiam.service.ChallengeResponseService;
import com.ge.hc.lca.axone.openiam.service.ResourceDataService;
import com.ge.hc.lca.axone.openiam.service.RoleService;
import com.ge.hc.lca.axone.openiam.service.UserService;
import com.ge.hc.lcs.axone.security.iam.config.ApplicationDTO;
import com.ge.hc.lcs.axone.security.iam.config.ConfigDTO;
import com.ge.hc.lcs.axone.security.iam.config.ConfigUtil;
import com.ge.hc.lcs.axone.security.iam.config.ResourceDTO;
import com.ge.hc.lcs.axone.security.iam.config.ScopeDTO;
import com.ge.hc.lcs.axone.security.iam.config.UserDTO;
import com.ge.hc.lcs.axone.security.iam.pageobjects.AuthenticationProviderPage;
import com.ge.hc.lcs.axone.security.iam.pageobjects.LoginPage;
import com.ge.hc.lcs.axone.security.iam.pageobjects.MenuPage;

import net.serenitybdd.core.pages.PageObject;

public class PreAndPostConditions extends PageObject {

	private LoginPage login;
	private MenuPage menuPage;
	private ResourceDataService resourceDataService;
	private RoleService roleService;
	private UserService userService;
	private AuthProviderService authProviderService;
	private ChallengeResponseService challengeResponseService;
	private AuthenticationProviderPage authenticationProviderPage;
	List<ConfigDTO> configDTOs;
	Set<String> resourceSet;

	public PreAndPostConditions() throws IOException {
		configDTOs = ConfigUtil.getConfig();
		resourceDataService = ResourceDataService.getInstance();
		roleService = RoleService.getInstance();
		userService = UserService.getInstance();
		authProviderService = AuthProviderService.getInstance();
		challengeResponseService = ChallengeResponseService.getInstance();
	}

	@BeforeStories
	public void beforeStories() throws Exception {
		try {
			createNewResources();
			createNewRolesAndAssignResources();
			createNewUsers();
			login.openDriver();
			ClassPathResource resource = new ClassPathResource("serenity.properties");
			InputStream input = new FileInputStream(resource.getFile());
			Properties prop = new Properties();
			prop.load(input);
			login.userLogin(prop.getProperty("adminUserName"), prop.getProperty("adminPassword"));
			createNewAuthenticationProviders();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		login.closeDriver();
	}

	private void createNewUsers() throws Exception {
		for (Iterator<ConfigDTO> iterator = configDTOs.iterator(); iterator.hasNext();) {
			ConfigDTO configDTO = (ConfigDTO) iterator.next();
			for (Iterator<?> iterator2 = configDTO.userDTOs.iterator(); iterator2.hasNext();) {
				UserDTO userDTO = (UserDTO) iterator2.next();
				if (!userService.addUser(userDTO.userType, userDTO.firstName, userDTO.middleName, userDTO.lastName,
						userDTO.roleDTO.roleName, userDTO.nickName, userDTO.login)) {
					throw new Exception("User Creation Failed for user " + userDTO.login);
				}
				if (!userService.activateUser(userDTO.login)) {
					throw new Exception("User Creation Failed for user " + userDTO.login);
				}
				if (!challengeResponseService.setChallengeResponse(userDTO.login)) {
					throw new Exception("User Creation Failed for user " + userDTO.login);
				}
			}
		}
	}

	private void createNewResources() throws Exception {
		resourceSet = new HashSet<String>();
		for (Iterator<ConfigDTO> iterator = configDTOs.iterator(); iterator.hasNext();) {
			ConfigDTO configDTO = (ConfigDTO) iterator.next();
			for (Iterator<?> iterator2 = configDTO.userDTOs.iterator(); iterator2.hasNext();) {
				UserDTO userDTO = (UserDTO) iterator2.next();
				for (Iterator<?> iterator3 = userDTO.roleDTO.resourceDTOs.iterator(); iterator3.hasNext();) {
					ResourceDTO resourceDTO = (ResourceDTO) iterator3.next();
					if (!(resourceSet.contains(resourceDTO.resourceName))) {
						if (!resourceDataService.createURLResource(resourceDTO.resourceName)) {
							throw new Exception("Resource Creation Failed for " + resourceDTO.resourceName);
						}
						resourceSet.add(resourceDTO.resourceName);
					}
				}
			}
		}
	}

	private void createNewRolesAndAssignResources() throws Exception {
		for (Iterator<ConfigDTO> iterator = configDTOs.iterator(); iterator.hasNext();) {
			ConfigDTO configDTO = (ConfigDTO) iterator.next();
			for (Iterator<?> iterator2 = configDTO.userDTOs.iterator(); iterator2.hasNext();) {
				UserDTO userDTO = (UserDTO) iterator2.next();
				roleService.addRole(userDTO.roleDTO.roleName);
				for (Iterator<?> iterator3 = userDTO.roleDTO.resourceDTOs.iterator(); iterator3.hasNext();) {
					ResourceDTO resourceDTO = (ResourceDTO) iterator3.next();
					if (!resourceDataService.addRoleToResouce(userDTO.roleDTO.roleName, resourceDTO.resourceName)) {
						throw new Exception("Assign Role to Resource Failed for role " + userDTO.roleDTO.roleName + ", Resource " + resourceDTO.resourceName);
					}
				}
				for (Iterator<?> iterator3 = userDTO.roleDTO.scopeDTOs.iterator(); iterator3.hasNext();) {
					ScopeDTO scopeDTO = (ScopeDTO) iterator3.next();
					if (!resourceDataService.addRoleToResouce(userDTO.roleDTO.roleName, scopeDTO.scopeName)) {
						throw new Exception("Assign Role to Resource Failed for role " + userDTO.roleDTO.roleName + ", Resource " + scopeDTO.scopeName);
					}
				}
			}

		}
	}

	private void createNewAuthenticationProviders() throws Exception {
		for (Iterator<ConfigDTO> iterator = configDTOs.iterator(); iterator.hasNext();) {
			ConfigDTO configDTO = (ConfigDTO) iterator.next();
			for (Iterator<?> iterator2 = configDTO.applicationDTOs.iterator(); iterator2.hasNext();) {
				ApplicationDTO applicationDTO = (ApplicationDTO) iterator2.next();
				menuPage.selectAuthenticationProvider();
				authenticationProviderPage.registerClientWebApplication(applicationDTO.providerType,
						applicationDTO.providerName, applicationDTO.getReDirectURIList(), applicationDTO.AuthGrantFlow,
						applicationDTO.ClientAuthType, applicationDTO.scopeDTOs, applicationDTO.resourceDTOs,
						applicationDTO.RefreshTokenTime, applicationDTO.ClientID, applicationDTO.ClientSecret);

			}

		}
	}

	@AfterStories
	public void afterStories() throws Exception {
		deleteResources();
		deleteRoles();
		deleteUsers();
		deleteOauthProviders();
	}

	private void deleteResources() throws Exception {
		resourceSet = new HashSet<String>();
		for (Iterator<ConfigDTO> iterator = configDTOs.iterator(); iterator.hasNext();) {
			ConfigDTO configDTO = (ConfigDTO) iterator.next();
			for (Iterator<?> iterator2 = configDTO.userDTOs.iterator(); iterator2.hasNext();) {
				UserDTO userDTO = (UserDTO) iterator2.next();
				for (Iterator<?> iterator3 = userDTO.roleDTO.resourceDTOs.iterator(); iterator3.hasNext();) {
					ResourceDTO resourceDTO = (ResourceDTO) iterator3.next();
					if (!(resourceSet.contains(resourceDTO.resourceName))) {
						resourceSet.add(resourceDTO.resourceName);
						resourceDataService.deleteResource(resourceDTO.resourceName);
					}
				}
			}
		}
	}

	private void deleteRoles() throws Exception {
		for (Iterator<ConfigDTO> iterator = configDTOs.iterator(); iterator.hasNext();) {
			ConfigDTO configDTO = (ConfigDTO) iterator.next();
			for (Iterator<?> iterator2 = configDTO.userDTOs.iterator(); iterator2.hasNext();) {
				UserDTO userDTO = (UserDTO) iterator2.next();
				roleService.deleteRole(userDTO.roleDTO.roleName);
			}
		}
	}

	private void deleteUsers() throws Exception {
		for (Iterator<ConfigDTO> iterator = configDTOs.iterator(); iterator.hasNext();) {
			ConfigDTO configDTO = (ConfigDTO) iterator.next();
			for (Iterator<?> iterator2 = configDTO.userDTOs.iterator(); iterator2.hasNext();) {
				UserDTO userDTO = (UserDTO) iterator2.next();
				userService.deleteUserByPrincipal(userDTO.login);
			}
		}
	}

	private void deleteOauthProviders() throws Exception {
		
		for (Iterator<ConfigDTO> iterator = configDTOs.iterator(); iterator.hasNext();) {
			ConfigDTO configDTO = (ConfigDTO) iterator.next();
			for (Iterator<ApplicationDTO> iterator2 = configDTO.applicationDTOs.iterator(); iterator2.hasNext();) {
				ApplicationDTO applicationDTO = (ApplicationDTO) iterator2.next();
				authProviderService.deleteAuthProvider(applicationDTO.providerName);
			}
		}
	}
	
	public static void main(String[] args) throws IOException, Exception {
		new PreAndPostConditions().afterStories();
	}
}
