package com.ge.hc.lcs.axone.security.iam.pageobjects;

import java.io.IOException;
import java.net.InetAddress;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.ge.hc.lcs.axone.security.iam.constants.UIComponentConstants;
import com.ge.hc.lcs.axone.security.iam.utilities.PageObjectUtility;
import com.ge.hc.lcs.axone.security.iam.utilities.TestUtility;

import net.serenitybdd.core.pages.PageObject;

public class LogViewer extends PageObject {
	PageObjectUtility pageUtility;
	LoginPage loginPage;
	@FindBy(id = "action")
	@CacheLookup
	private WebElement action;

	@FindBy(id = "result")
	@CacheLookup
	private WebElement result;

	@FindBy(id = "searchLogs")
	@CacheLookup
	private WebElement searchLogs;
	@FindBy(xpath = "//*[@id='userSearchForm']/button")
	@CacheLookup
	private WebElement additionalSearchCriteria;

	@FindBy(xpath = "//input[@title='Requestor Login']")
	@CacheLookup
	private WebElement requestorLoginCriteria;

	@FindBy(id = "requestor")
	@CacheLookup
	private WebElement requestorLogin;
	@FindBy(xpath = "/html/body/div[1]/div[2]/table/tbody/tr[6]/td[2]/span")
	private WebElement hostNameValue;
	private int rowCount;
	private boolean logStatus = false;
	private String hostName;
	private String logInfoTable = "//*[@id='entitlementsContainer']/table/tbody/tr";
	public LogViewer() {
	}

	public LogViewer(WebDriver driver) {
		super(driver);
	}

	public LogViewer clickSearhLogsButton(String event, String status, String principal) {
		getDriver().switchTo().frame(UIComponentConstants.CONTENT_FRAME);
		Select actionDropdown = new Select(action);
		actionDropdown.selectByValue(event);
		Select resultDropdown = new Select(result);
		resultDropdown.selectByValue(status);
		additionalSearchCriteria.click();
		requestorLoginCriteria.click();
		requestorLogin.sendKeys(principal);
		searchLogs.click();
		TestUtility.sleep(UIComponentConstants.FIVE);
		getDriver().switchTo().defaultContent();
		return this;
	}

	public String verifyLogsGenerated(String principal) {
		String status = null;
		getDriver().switchTo().frame("contentFrame");
		rowCount = pageUtility.getCount(logInfoTable);
		for (int row = 1; row <= rowCount; row++) {

			String requestor = getDriver()
					.findElement(By.xpath(logInfoTable + "[" + row + "]/td[@openiam-row='principal']")).getText();

			if (requestor.equalsIgnoreCase(principal)) {

				status = getDriver().findElement(By.xpath(logInfoTable + "[" + row + "]/td[@openiam-row='result']"))
						.getText();

				break;

			}

		}

		return status;

	}

	public boolean verifyDetailedLog(String principal) throws IOException {
		rowCount = pageUtility.getCount(logInfoTable);
		for (int row = 1; row <= rowCount; row++) {

			String requestor = getDriver()
					.findElement(By.xpath(logInfoTable + "[" + row + "]/td[@openiam-row='principal']")).getText();

			if (requestor.equalsIgnoreCase(principal)) {

				getDriver().findElement(By.xpath(logInfoTable + "[" + row + "]/td[6]/a/i")).click();

				hostName = hostNameValue.getText();
				if (InetAddress.getLocalHost().getHostAddress().equals(hostName)) {

					logStatus = true;
				}
				break;
			}
			
		}
		return logStatus;
	}
}
