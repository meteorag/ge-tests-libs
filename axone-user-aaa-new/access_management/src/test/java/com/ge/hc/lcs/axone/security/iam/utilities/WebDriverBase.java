/*
 * Copyright (C) 2016 General Electric Company. All Rights Reserved.
 */
package com.ge.hc.lcs.axone.security.iam.utilities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * 
 * @author 212480093
 *
 */
public class WebDriverBase {
	private static WebDriver _driver = null;

	public static WebDriver StartBrowser() {
		try {
			String browserName = PropertiesCache.getInstance().getProperty("browser");
			switch (browserName) {
			case "electron":
				System.setProperty("webdriver.chrome.driver",
						PropertiesCache.getInstance().getProperty("chromedriverpath"));
				ChromeOptions options = new ChromeOptions();
				options.addArguments("disable-extensions");
				options.setBinary(PropertiesCache.getInstance().getProperty("electronbinarypath"));
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				
				
				getSynElectronDriver(capabilities);
				
				break;
			case "chrome":
				System.setProperty("webdriver.chrome.driver",
						PropertiesCache.getInstance().getProperty("chromedriverpath"));
				ChromeOptions chromeOptions = new ChromeOptions();
				chromeOptions.addArguments("disable-extensions");
				DesiredCapabilities chromeCapabilities = new DesiredCapabilities();
				chromeCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
				
				
				getSynElectronDriver(chromeCapabilities);
				break;
				
			case "firefox":
				ProfilesIni profile = new ProfilesIni();
				FirefoxProfile firefoxProfile = profile.getProfile(PropertiesCache
						.getInstance().getProperty("fireFoxProfileName"));
				_driver = new FirefoxDriver(firefoxProfile);
				break;
			}

		} catch (Exception e) {
			return null;
		}
		_driver.manage().timeouts().implicitlyWait(
				Integer.parseInt(PropertiesCache.getInstance().getProperty("timeout")), TimeUnit.SECONDS);
		_driver.manage().timeouts().setScriptTimeout(
				Integer.parseInt(PropertiesCache.getInstance().getProperty("timeout")), TimeUnit.SECONDS);
		_driver.manage().timeouts().pageLoadTimeout(
				Integer.parseInt(PropertiesCache.getInstance().getProperty("timeout")), TimeUnit.SECONDS);
		return _driver;
	}
	

	
	public static ChromeDriver getSynElectronDriver(Capabilities capability) {
		
		if (_driver == null) {
			synchronized (WebDriverBase.class) {
				
				if(_driver == null)
				   _driver = new ChromeDriver(capability);
				
			}
		}
		
		return (ChromeDriver) _driver;
		
	}
	
}


