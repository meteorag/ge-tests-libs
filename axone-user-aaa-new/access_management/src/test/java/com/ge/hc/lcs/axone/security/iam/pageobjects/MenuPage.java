package com.ge.hc.lcs.axone.security.iam.pageobjects;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.core.io.ClassPathResource;

import com.ge.hc.lcs.axone.security.iam.constants.UIComponentConstants;
import com.ge.hc.lcs.axone.security.iam.utilities.PageObjectUtility;
import com.ge.hc.lcs.axone.security.iam.utilities.TestUtility;

import net.serenitybdd.core.pages.PageObject;

public class MenuPage extends PageObject {

	private PageObjectUtility pageObjectUtility;
	private final int threadSleep = 5000;

	public MenuPage(WebDriver driver) {
		super(driver);
		pageObjectUtility = new PageObjectUtility();
	}

	private String linkAccessControl = ".//*[@id='outerMenuACC_CONTROL']";
	private String linkResource = "//a[@id='outerMenuSECURITY_RES']";
	private String linkRole = "//a[@id='outerMenuSECURITY_ROLE']";
	private String linkUserAdmin = ".//*[@id='outerMenuIDMAN']/span[2]";
	private String linkUser = "//a[@id='outerMenuUSER']";
	private String linkAuthenticationProvider = ".//*[@id='outerMenuAM_PROV_SEARCH_CHILD']";
	private String buttonCreateNewResource = "//a[@class='b-link']//span[contains(text(),'Create New Resource')]";
	private String buttonCreateNewRole = "//a[@class='b-link']//span[contains(text(),'Create New Role')]";
	private String buttonCreateNewUser = "//a[@class='b-link']//span[contains(text(),'Create New User')]";
	private String buttonCreateNewProvider = "//a[@class='b-link']//span[contains(text(),'Create New Provider')]";
	private String linkLogout = "//a[@id='logout']/../..//span[contains(text(),'Logout')]";
	private String textLogoutSuccess = "//div[@id='credentials']/div[contains(text(),'You have successfully logged out')]";
	@FindBy(id = "outerMenuADMIN")
	@CacheLookup
	private WebElement administration;

	@FindBy(id = "outerMenuSYNCLOG")
	@CacheLookup
	private WebElement logViewer;

	public void selectResourcesMenu() throws InterruptedException {
		selectMenu(linkAccessControl, linkResource);
		find(By.xpath(buttonCreateNewResource)).click();
		TestUtility.sleep(UIComponentConstants.FIVE);
	}

	private void selectMenu(String mainMenu, String subMenu) throws InterruptedException {
		getDriver().switchTo().defaultContent();
		TestUtility.sleep(UIComponentConstants.TWO);
		pageObjectUtility.click(mainMenu);
		TestUtility.sleep(UIComponentConstants.FIVE);
		pageObjectUtility.clickBuildExecute(subMenu);
		TestUtility.sleep(UIComponentConstants.FIVE);
	}

	public void selectRolesMenu() throws InterruptedException {
		selectMenu(linkAccessControl, linkRole);
		find(By.xpath(buttonCreateNewRole)).click();
		TestUtility.sleep(UIComponentConstants.FIVE);
	}

	public void selectUserAdminMenu() throws InterruptedException {
		selectMenu(linkUserAdmin, linkUser);
		find(By.xpath(buttonCreateNewUser)).click();
		TestUtility.sleep(UIComponentConstants.FIVE);
	}

	public void selectAuthenticationProvider() throws InterruptedException {
		selectMenu(linkAccessControl, linkAuthenticationProvider);
		find(By.xpath(buttonCreateNewProvider)).click();
		TestUtility.sleep(UIComponentConstants.FIVE);
	}

	public void logout() throws InterruptedException, IOException {
		ClassPathResource resource = new ClassPathResource("serenity.properties");
		InputStream input = new FileInputStream(resource.getFile());
		Properties prop = new Properties();
		prop.load(input);
		find(By.xpath(linkLogout)).click();
		(new WebDriverWait(getDriver(), 10))
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(textLogoutSuccess)));
		getDriver().navigate().to(prop.getProperty("webdriver.base.url"));
		Thread.sleep(threadSleep);
	}

	public void clickAdministartionMenu() {
		administration.click();
	}

	public void clickLogViewerLink() {
		logViewer.click();
	}
}
