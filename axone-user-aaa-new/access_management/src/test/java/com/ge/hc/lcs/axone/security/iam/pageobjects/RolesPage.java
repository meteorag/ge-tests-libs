package com.ge.hc.lcs.axone.security.iam.pageobjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ge.hc.lcs.axone.security.iam.constants.UIComponentConstants;
import com.ge.hc.lcs.axone.security.iam.utilities.TestUtility;

import net.serenitybdd.core.pages.PageObject;

public class RolesPage extends PageObject {
	
	public RolesPage(WebDriver driver){
		super(driver);
	}
	private String frameId= "contentFrame";
	private String textRoleName = "//input[@id='name']";
	private String buttonSave = "//a[@id='save']";
	private String buttonRoleEntitlements = "//a[@class='b-link']//span[contains(text(),'Role Entitlements')]";
	private String buttonEntitledResources = "//div/a[contains(text(),'Entitled to Resources')]";
	private String buttonAddResource = ".//*[@id='searchBtn']";
	private String textResourceName = "//input[@id='name']";
	private String fieldResourceType = ".//*[@id='editDialog']/div/div[1]/div[2]/span/span[1]/span/ul";
	private String textResourceType =  ".//*[@id='editDialog']/div/div[1]/div[2]/span/span[1]/span/ul/li/input";
	private String buttonSearchPopUp  = "//div[@class='b-btn m-button m-small m-type1']/a[contains(text(),'Search')]";
	private String textSearch = "//input[@id='searchInput']";
	private String buttonRoleSearch = "//div[@class='b-item']/div/input[@value='Search']";
	private String buttonDeleteRole = "//a[@id='deleteRole']";
	private String buttonConfirmDelete = "//div[@class='b-item']/div//a[contains(text(),'Delete this Role')]";
	public void createRole(String roleName) throws InterruptedException{
		
		WebElement elementRole;
		
		getDriver().switchTo().frame(frameId);
		(new WebDriverWait(getDriver(), UIComponentConstants.TEN)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(textRoleName)));
		
		elementRole = find(By.xpath(textRoleName));
		elementRole.clear();
		elementRole.sendKeys(roleName);
			
		find(By.xpath(buttonSave)).click();
		getDriver().switchTo().defaultContent();
		getDriver().manage().timeouts().implicitlyWait(UIComponentConstants.EIGHT, TimeUnit.SECONDS);
		find(By.xpath(buttonRoleEntitlements)).click();
		getDriver().manage().timeouts().implicitlyWait(UIComponentConstants.FIVE, TimeUnit.SECONDS);
		
		TestUtility.sleep(UIComponentConstants.FIVE);
	}
	
		//This method is used to assign resource while creating Role
		public void assignResourceToRole(String resName, String resType) throws InterruptedException{
			
			WebElement resourceName;
			WebElement resourcetype;
			getDriver().switchTo().frame(frameId);
			TestUtility.sleep(UIComponentConstants.FIVE);
			find(By.xpath(buttonEntitledResources)).click();
			TestUtility.sleep(UIComponentConstants.FIVE);
			find(By.xpath(buttonAddResource)).click();
			TestUtility.sleep(UIComponentConstants.FIVE);
			resourceName = find(By.xpath(textResourceName));
			resourceName.clear();
			resourceName.sendKeys(resName);
					
			find(By.xpath(fieldResourceType)).click();
			resourcetype = find(By.xpath(textResourceType));
			resourcetype.clear();
			resourcetype.sendKeys(resType);
			find(By.xpath("//span[@class='select2-results']/ul/li[contains(text(),'"+resType+"')] ")).click();
													
			find(By.xpath(buttonSearchPopUp)).click();
			getDriver().manage().timeouts().implicitlyWait(UIComponentConstants.FIVE, TimeUnit.SECONDS);
			TestUtility.sleep(UIComponentConstants.FIVE);
		        
			find(By.xpath("//td[@class='b-cell']//span[contains(text(),'"+resName+"')]/../..//i[contains(text(),'add')]")).click();
			getDriver().manage().timeouts().implicitlyWait(UIComponentConstants.TEN, TimeUnit.SECONDS);
			TestUtility.sleep(UIComponentConstants.FIVE);
			getDriver().switchTo().defaultContent();
			
				
		}
		public void searchRole(String roleName) throws InterruptedException{
			WebElement rolename;
			
			getDriver().switchTo().frame(frameId);
			rolename = find(By.xpath(textSearch));
			rolename.clear();
			rolename.sendKeys(roleName);
			getDriver().manage().timeouts().implicitlyWait(UIComponentConstants.FIVE, TimeUnit.SECONDS);
			find(By.xpath(buttonRoleSearch)).click();
			getDriver().manage().timeouts().implicitlyWait(UIComponentConstants.FIVE, TimeUnit.SECONDS);
			find(By.xpath("//td[@class='b-cell']/span[contains(text(),'"+roleName+"')]/../..//i[contains(text(),'mode_edit')]")).click();
			getDriver().manage().timeouts().implicitlyWait(UIComponentConstants.FIVE, TimeUnit.SECONDS);
		}
		public void deleteRole(String roleName) throws InterruptedException{
			searchRole(roleName);
			find(By.xpath(buttonDeleteRole)).click();
			(new WebDriverWait(getDriver(), UIComponentConstants.TEN)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(buttonConfirmDelete)));
			find(By.xpath(buttonConfirmDelete)).click();
			getDriver().manage().timeouts().implicitlyWait(UIComponentConstants.FIVE, TimeUnit.SECONDS);
			getDriver().switchTo().defaultContent();
			
		}
			
}
