package com.ge.hc.lcs.axone.security.iam.steps;

import java.util.HashMap;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import com.ge.hc.lcs.axone.security.iam.aaa_enum.AuthorizationServerEndpointEnum;
import com.ge.hc.lcs.axone.security.iam.pageobjects.ClientApplicationPage;
import com.ge.hc.lcs.axone.security.iam.pageobjects.LoginPage;

import net.thucydides.core.steps.ScenarioSteps;

public class UserAuthorizationSteps extends ScenarioSteps {

    private static final long serialVersionUID = -2039168153651137752L;
    String receivedToken;
    String receivedIdToken;
    ClientApplicationPage clientApplicationPage;
    LoginPage loginPage;
  
    HashMap<String, String> userInfoMap = new HashMap<String, String>();
    HashMap<String, String> tokenInfoMap = new HashMap<String, String>();
    String userInfoService="userinformation";
    String tokenInfoService="scopeinformation";
   String positiveStatus="accessible";
   String negativeStatus="not accessible";
    
    @Given("the user is trying to use a <resource> of <application>")
    public void buildURL(@Named("resource") String resource,@Named("application") String application) throws Exception {
        //OpenHomePageOf Application and append resource to URL
    	//Implementation:-Construct URl-->applicationURL+"/"+resource and open in browser
        //constructURLandOpenInBrowser(application,resource)
    	//clientApplicationPage.accessResource(application+"/"+resource);
    	clientApplicationPage.openClient(application);
    	clientApplicationPage.login();
    	
    }
    
    @When("the <user> is authenticated")
    public void verifyUserAuthentication(@Named("user") String uname) throws Exception {
    	loginPage.userLogin(uname,"Password$51");
    	loginPage.allowAccess();
    }
    
    
    @Then("based on the role of the user and application's permission the request for <resource> is <granted>")
    public void verifyResourceAccessStatusByUser(@Named("resource") String resource, @Named("granted") String status) throws Exception {
        //Check the Resource page opened or not without error
    	//The method implemetation(STAR Team):
    	//1. after login the home page is shown 
    	//2.click on any resource page("Click here to access GE page")
    	//3. Return the content of the page
        //String accessible = verifyResourcePageShown(resource);
        //Assert.asstertrue(returnType);
    	
    	clientApplicationPage.selectResourceOnClient(resource);
    	
    	String resourceAccessStatus=negativeStatus;
    	if(clientApplicationPage.isCurrentResourceAccessible(status)){
    		resourceAccessStatus=positiveStatus;
    	}
    	Assert.assertEquals(status, resourceAccessStatus);
    	clientApplicationPage.closeDriver();
    }
    
    @Given("<user> is trying to use a scope of <application>")
    public void accessResourceOfApplication(@Named("user") String uname,@Named("application") String application) throws Exception {
      
    	//Implementation:-Construct URl-->applicationURL+"/"+resource and open in browser
    	  // value of resource can be hard-coded since its not important for user information
        //constructURLandOpenInBrowser(application,resource)
    	//will be implemented by Faceoff
        //String upasswd = getPasswordForUserFromConfig(@Named("user") String uname);
    	//Will be implemented by STAR tesm
        //login(uname,upasswd);
    	clientApplicationPage.openClient(application);
    	clientApplicationPage.login();
    	//String upasswd = stepsUtility.getPasswordForUserFromConfig(uname);
    	loginPage.userLogin(uname, "Password$51");
    }

    String id_token;
    @When("the application accesses the <user information> from userInfoEndpoint")
    public void getInformationFromUserInfoEndpoint(@Named("user information") String user_information) throws Exception
    {
        //Perform the action to hit the UserInfo endpoint(STAR team)
        //requestTouserInfoEndpoint();
    	clientApplicationPage.selectServiceOnClient(AuthorizationServerEndpointEnum.USER_INFO);
    }
    
    @Then("based on the role of the user and application's permission the <user information> is <granted>")
    public void verifyResultReceivedFromUserInfoEndpoint(@Named("granted") String status,@Named("user information") String user_information) throws Exception
    {
        //Get the value of the user information displayed on the screen(STAR Team)
    	//pass the parameter user info what you want check(name/email/geven_name)
    	//Return:the value of the user Info
        //String userinfo = getUserInfoFromUserInfoLink(user_information);
        //Assert.equals(userinfo,granted);
    	userInfoMap=clientApplicationPage.returnRequestedData(AuthorizationServerEndpointEnum.USER_INFO);
    	String userInfoStatus=negativeStatus;
    	if(userInfoMap.containsKey(user_information)){
    		userInfoStatus=positiveStatus;
    	}
    	Assert.assertEquals(status, userInfoStatus);
    	clientApplicationPage.closeDriver();
    	
    }
    
    
    @When("the <user> is authenticated to get the scope information using the tokenInfoEndpoint")
    public void getInformationFromTokenInfoEndpoint(@Named("user") String uname) throws Exception
    {
        //Check the Redirection to the tokenInfoEndpoint from Login Page(STAR Team)
        //redirectTotokenInfoEndpoint();
    	//String upasswd = stepsUtility.getPasswordForUserFromConfig(uname);
    	//Will be implemented by STAR tesm
        //login(uname,upasswd);
    	loginPage.userLogin(uname,"Password$51");
    	clientApplicationPage.selectServiceOnClient(AuthorizationServerEndpointEnum.TOKEN_INFO);
    }
    
    @Then("the scope of the token <matches> with the <resource>")
    public void verifyResultReceivedFromTokenInfoEndpoint(@Named("resource") String userscope,@Named("matches") String status) throws Exception
    {
        //Get the value of the token information displayed on the screen(STAR Team)
    	//pass the parameter Resource name
    	//return Resource value
        //String tokeninfo = getTokenInfoFromTokenInfoLink();
        //Assert.matches(tokeninfo,resource)
    	tokenInfoMap=clientApplicationPage.returnRequestedData(AuthorizationServerEndpointEnum.TOKEN_INFO);
    	
    	String scopeInfoStatus=negativeStatus;
    	if(tokenInfoMap.containsValue("Default - /"+userscope)){
    		scopeInfoStatus=positiveStatus;
    	}
    	Assert.assertEquals(status, scopeInfoStatus);
    	clientApplicationPage.closeDriver();
    }
    
}
