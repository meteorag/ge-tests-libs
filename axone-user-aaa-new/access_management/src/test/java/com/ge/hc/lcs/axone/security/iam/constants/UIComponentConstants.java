package com.ge.hc.lcs.axone.security.iam.constants;

public final class UIComponentConstants {
	public static String CONTENT_FRAME = "contentFrame";
	
	public static Integer ONE = 1;
	public static Integer TWO = 2;
	public static Integer THREE = 3;
	public static Integer FIVE = 5;
	public static Integer SEVEN = 7;
	public static Integer EIGHT = 8;
	public static Integer TEN = 10;
}
