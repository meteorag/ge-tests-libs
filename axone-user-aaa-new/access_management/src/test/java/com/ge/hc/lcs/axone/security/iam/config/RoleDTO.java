package com.ge.hc.lcs.axone.security.iam.config;

import java.util.List;

public class RoleDTO {
	public String roleName;
	public List<ResourceDTO> resourceDTOs;
	public List<ScopeDTO> scopeDTOs;
	
}
