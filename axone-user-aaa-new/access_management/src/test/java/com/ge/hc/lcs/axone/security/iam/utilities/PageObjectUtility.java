package com.ge.hc.lcs.axone.security.iam.utilities;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.google.common.collect.Lists;

import net.serenitybdd.core.pages.PageObject;

public class PageObjectUtility extends PageObject {

	public void enterValue(String xpath, String value) {
		WebElement username = find(By.xpath(xpath));
		username.clear();
		username.sendKeys(value);
	}

	public void click(final String xpath) {
		find(By.xpath(xpath)).click();
	}

	public void clickBuildExecute(String xPath) {
		Actions actions = new Actions(getDriver());
		WebElement element = getDriver().findElement(By.xpath(xPath));
		actions.moveToElement(element);
		actions.click().build().perform();
	}

	public void moveToElement(String xPath) {
		Actions actions = new Actions(getDriver());
		WebElement element = getDriver().findElement(By.xpath(xPath));
		actions.moveToElement(element);
	}

	public <T extends net.serenitybdd.core.pages.WebElementFacade> T find(By... selectors) {
		return find(Lists.newArrayList(selectors));
	}

	public int getCount(String xPath) {
		return getDriver().findElements(By.xpath(xPath)).size();
	}

	// Xpath of the table Exampe - */table/tr
	public List<String> getColumnValues(String xPath, int columnNumber) {
		int row_count = getCount(xPath);
		int col_count = getCount(xPath + "[1]/td");
		List<String> values = null;
		if (row_count > 0 && col_count > 0) {
			values = new ArrayList<String>();
			for (int rowCounter = 1; rowCounter <= row_count; rowCounter++) {
				String final_xpath = xPath + "[" + rowCounter + "]/td[" + columnNumber + "]";
				values.add(getDriver().findElement(By.xpath(final_xpath)).getText());
			}
		}
		return values;
	}

	public String getElementValue(String xPath) {
		return find(By.xpath(xPath)).getText();
	}

	public boolean isElementAvailable(String xPath) {
		try {
			return find(By.xpath(xPath)).isPresent();
		} catch (Exception e) {
		}
		return false;
	}
}
