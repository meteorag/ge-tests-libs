package com.ge.hc.lcs.axone.security.iam.config;

public class UserDTO {
	public String firstName;
	public String middleName;
	public String lastName;
	public String userType;
	public String password;
	public String nickName;
	public RoleDTO roleDTO;
	public String login;

}
