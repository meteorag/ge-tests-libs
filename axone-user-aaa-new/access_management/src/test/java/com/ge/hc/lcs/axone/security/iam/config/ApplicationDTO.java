package com.ge.hc.lcs.axone.security.iam.config;

import java.util.Arrays;
import java.util.List;

public class ApplicationDTO {
	public String providerName;
	public String providerType;
	private String redirectURIList;
	public String AuthGrantFlow;
	public String ClientAuthType;
	public String RefreshTokenTime;
	public String ClientID;
	public String ClientSecret;
	public List<ResourceDTO> resourceDTOs;
	public List<ScopeDTO> scopeDTOs;
	

	
	public List<String> getReDirectURIList(){
		String redirectURIArray[] = redirectURIList.split(",");
		return Arrays.asList(redirectURIArray);
	}
	
}
