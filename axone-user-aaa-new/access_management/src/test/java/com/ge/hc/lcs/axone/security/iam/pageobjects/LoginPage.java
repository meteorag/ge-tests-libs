package com.ge.hc.lcs.axone.security.iam.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ge.hc.lcs.axone.security.iam.constants.UIComponentConstants;
import com.ge.hc.lcs.axone.security.iam.utilities.PageObjectUtility;
import com.ge.hc.lcs.axone.security.iam.utilities.TestUtility;

import net.serenitybdd.core.pages.PageObject;

public class LoginPage extends PageObject {
	
	private WebDriver driver;
	private PageObjectUtility pageObjectUtility;
	
	public LoginPage(WebDriver driver){
		super(driver);
		pageObjectUtility = new PageObjectUtility();
		this.driver = driver;
		new WebDriverWait(driver, 60);
	}
	private String userNamePath= "//input[@id='principal']";
	private String passwordPath= "//input[@id='input_password']";
	private String buttonLogin = "//input[@id='submit']";
	private String messageLoginError = "//div[@class='error']";
	@FindBy(id = "allowBtn")
	@CacheLookup
	private WebElement allowBtn;
	
	public void openDriver(){
		open();
		this.driver.manage().window().maximize();
	}
	
	public void userLogin(String uname,String password) throws Exception {
		WebElement username;
		WebElement passwordField;
					
		username = find(By.xpath(userNamePath));
		username.clear();
		username.sendKeys(uname);
		
		passwordField = find(By.xpath(passwordPath));
		passwordField.clear();
		passwordField.sendKeys(password);
		
		find(By.xpath(buttonLogin)).click();
		TestUtility.sleep(UIComponentConstants.FIVE);
		
	}
	
	public void allowAccess() {
		if (driver.findElements(By.id("allowBtn")).size() != 0){
			int rowCount = pageObjectUtility.getCount("//table/tbody/tr");
			for (int i = 2; i <= rowCount ; i++) {
				find(By.xpath("//table/tbody/tr["+i+"]/td[2]/input[1]")).click();
			}
			find(By.id("allowBtn")).click();
			TestUtility.sleep(UIComponentConstants.FIVE);
		}
	}
	
	public void loginViaFirstParty(String username, String password){
        WebElement userNameField;
        WebElement passwordField;
        
        userNameField = find(By.xpath("//input[@name='uname']"));
        userNameField.clear();
        userNameField.sendKeys(username);
        
        passwordField = find(By.xpath("//input[@name='psw']"));
        passwordField.clear();
        passwordField.sendKeys(password);
        
        find(By.xpath("//button[contains(text(),'Login')]")).click();
	        
	 }
	public void closeDriver() throws Exception {
		this.driver.close();
		
	}

	public String errorMessageWhenUserLogin(){
		
		String errorMessage = find(By.xpath(messageLoginError)).getText();
		return errorMessage;
	}
}
