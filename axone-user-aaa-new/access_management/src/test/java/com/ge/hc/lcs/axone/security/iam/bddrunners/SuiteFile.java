package com.ge.hc.lcs.axone.security.iam.bddrunners;

import net.serenitybdd.jbehave.SerenityStories;

public class SuiteFile extends SerenityStories{
	
	public SuiteFile()
	{
		findStoriesCalled("**/*user_authentication.feature,**/*User_Authorization.feature");
	}

}
