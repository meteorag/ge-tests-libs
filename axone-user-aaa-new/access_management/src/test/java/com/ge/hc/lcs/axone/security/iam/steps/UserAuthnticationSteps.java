package com.ge.hc.lcs.axone.security.iam.steps;

import java.io.IOException;
import java.net.URISyntaxException;

import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.ScenarioType;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import com.ge.hc.lcs.axone.security.iam.aaa_enum.AuthorizationServerEndpointEnum;
import com.ge.hc.lcs.axone.security.iam.pageobjects.ClientApplicationPage;
import com.ge.hc.lcs.axone.security.iam.pageobjects.ErrorPage;
import com.ge.hc.lcs.axone.security.iam.pageobjects.LogViewer;
import com.ge.hc.lcs.axone.security.iam.pageobjects.LoginPage;
import com.ge.hc.lcs.axone.security.iam.pageobjects.MenuPage;

import net.thucydides.core.steps.ScenarioSteps;

public class UserAuthnticationSteps extends ScenarioSteps {

	private static final long serialVersionUID = -6377603612463777853L;
	private ClientApplicationPage clientApplicationPage;
	private LoginPage loginPage;
	private ErrorPage errorPage;
	LogViewer logViewer;
	MenuPage menuPage;

	private final String ERROR_MSG_FOR_REDIRECT_URI_STARTSWITH = "invalid_request - Redirect URI for Client ID";
	private final String ERROR_MSG_FOR_REDIRECT_URI_ENDSWITH = "is not registered";

	@Given("the <application> is started with invalid client_id")
	public void givenThewebapplicationIsStartedWithInvalidclient_id(@Named("application") String webapplication) {
	}

	@Given("the <application> is started with valid client_id but invalid client_password")
	public void givenThewebapplicationIsStartedWithValidclient_idButInvalidclient_password(
			@Named("application") String application) {
	}

	@Then("an unregistered redirect-URI <exceptionMessage> message received by client")
	public void thenAnUnregisteredRedirectURIexceptionMessageMessageReceivedByClient(
			@Named("exceptionMessage") String exceptionMessage) {
		String errorMsg = errorPage.getErrormessageForAuthentication();
		boolean isErrorMsgPart1Exists = errorMsg.startsWith(ERROR_MSG_FOR_REDIRECT_URI_STARTSWITH);
		boolean isErrorMsgPart2Exists = errorMsg.endsWith(ERROR_MSG_FOR_REDIRECT_URI_ENDSWITH);
		Assert.assertTrue(isErrorMsgPart1Exists);
		Assert.assertTrue(isErrorMsgPart2Exists);
	}

	@Given("the application is a third-party web <application> running on a server")
	public void givenProductIsAThirdpartyWebapplicationRunningInAServer(@Named("application") String application) {
	}

	@Given("the user accesses the <resource> of the <application>")
	public void whenAUserAccessTheresourceFromTheapplicationGiven(@Named("resource") String resource,
			@Named("application") String application) throws Exception {
		clientApplicationPage.accessResource(application.concat("/" + resource));
	}

	@When("the user accesses the <resource> of the <application>")
	public void whenAUserAccessTheresourceFromTheapplication(@Named("resource") String resource,
			@Named("application") String application) throws Exception {
		clientApplicationPage.accessResource(application.concat("/" + resource));
	}

	@Given("the user provides the <username> and <password> via Authentication provider login page")
	public void whenAUserProvidesTheusernameAndpasswordViaAuthenticationProviderLoginPageLog(
			@Named("username") String username, @Named("password") String password) throws Exception {
		loginPage.userLogin(username, password);
		loginPage.closeDriver();
	}

	@When("the user provides the <username> and <password> via Authentication provider login page")
	public void whenAUserProvidesTheusernameAndpasswordViaAuthenticationProviderLoginPage(
			@Named("username") String username, @Named("password") String password) throws Exception {
		loginPage.userLogin(username, password);
		loginPage.allowAccess();
	}

	@Then("the system authenticates the user through the authorization endpoint")
	public void thenTheSystemAuthenticatesTheUserFromTheAuthorizationEndpoint()
			throws InterruptedException, URISyntaxException {
		String authCode = clientApplicationPage.getAuthorizationCode();
		Assert.assertNotNull(authCode);
	}

	@Then("the system authenticates the application through the token endpoint")
	public void thenTheSystemAuthenticatesTheApplicationFromTheTokenEndpoint() throws Exception {
		clientApplicationPage.selectServiceOnClient(AuthorizationServerEndpointEnum.TOKEN);
		Object token = clientApplicationPage.returnRequestedData(AuthorizationServerEndpointEnum.TOKEN);
		Assert.assertNotNull(token);
	}

	@Then("the system grants access to the <resource>")
	public void thenTheSystemGrantsAccessToTheresource(@Named("resource") String resource) throws Exception {
	}

	@Given("the application is a third-party native <application> running on user workstation")
	public void givenProductIsAThirdpartyNativeapplicationRunningOnUserWorkstationAsAClient(
			@Named("application") String application) {
	}

	@Then("the system authenticates the user through the token endpoint")
	public void thenTheSystemAuthenticatesTheUserFromTheTokenEndpoint() throws Exception {
		clientApplicationPage.selectServiceOnClient(AuthorizationServerEndpointEnum.TOKEN);
		Object token = clientApplicationPage.returnRequestedData(AuthorizationServerEndpointEnum.TOKEN);
		Assert.assertNotNull(token);
	}

	@Given("product is a first-party <application> running on user workstation as a client")
	public void givenProductIsAFirstpartyapplicationRunningOnUserWorkstationAsAClient(
			@Named("application") String application) {
	}

	@When("a user provides the <username> and <password> via first-party <application> login page")
	public void whenAUserProvidesTheusernameAndpasswordViaFirstpartyapplicationLoginPage(
			@Named("username") String username, @Named("password") String password) {
		loginPage.loginViaFirstParty(username, password);
	}

	@Then("an invalid user <exceptionMessage> message received from the authorization endpoint")
	public void thenAnInvalidUserexceptionMessageMessageReceivedFromTheAuthorizationEndpoint(
			@Named("exceptionMessage") String exceptionMessage) {
		String exceptionMsg = loginPage.errorMessageWhenUserLogin();
		Assert.assertEquals(exceptionMessage, exceptionMsg);
	}

	@Then("an unregistered Client <exceptionMessage> message received from the authorization endpoint")
	public void thenAnUnregisteredClientexceptionMessageMessageReceivedFromTheAuthorizationEndpoint(
			@Named("exceptionMessage") String exceptionMessage) {
		String errorMsg = errorPage.getErrormessageForAuthentication();
		Assert.assertEquals(errorMsg, exceptionMessage);
	}

	@Then("an unauthorized Client <exceptionMessage> message received from the token endpoint")
	public void thenAnUnauthorizedClientexceptionMessageMessageReceivedFromTheTokenEndpoint(
			@Named("exceptionMessage") String exceptionMessage) {
		String errorMsg = errorPage.getUnauthorizedClientErrorMessage();
		Assert.assertEquals(exceptionMessage, errorMsg);
	}

	@When("the admin verifies the log for the <event> with status <status> for <username>")
	public void whenTheAdminVerifiesTheLogForTheeventWithStatus(@Named("event") String event,
			@Named("status") String status, @Named("username") String principal) throws Exception {
		// A bug in OpenIAM causes the log viewer to generate logs in
		// consistently that causes the failure of the execution. This code can
		// be used once the bug is fixed

		// loginPage.openDriver();
		// ClassPathResource resource = new
		// ClassPathResource("serenity.properties");
		// InputStream input = new FileInputStream(resource.getFile());
		// Properties prop = new Properties();
		// prop.load(input);
		// loginPage.userLogin(prop.getProperty("adminUserName"),
		// prop.getProperty("adminPassword"));
		// TestUtility.sleep(UIComponentConstants.FIVE);
		// menuPage.clickAdministartionMenu();
		// TestUtility.sleep(UIComponentConstants.TWO);
		// menuPage.clickLogViewerLink();
		// TestUtility.sleep(UIComponentConstants.FIVE);
		// logViewer.clickSearhLogsButton(event, status, principal);
	}

	@Then("the log is generated for the <event> for <username> with <status> and hostname")
	public void thenTheLogIsGeneratedForTheeventWithstatus(@Named("event") String event, @Named("status") String status,
			@Named("username") String principal) throws IOException {
		// A bug in OpenIAM causes the log viewer to generate logs in
		// consistently that causes the failure of the execution. This code can
		// be used once the bug is fixed

		// String result=logViewer.verifyLogsGenerated(principal);
		// Assert.assertEquals(status, result);
		// logStatus=logViewer.verifyDetailedLog(principal);
		// Assert.assertTrue(logStatus);
	}

	@AfterScenario(uponType = ScenarioType.EXAMPLE)
	public void afterEachExampleScenario() throws Exception {
		clientApplicationPage.closeDriver();
	}
}
