package com.ge.hc.lcs.axone.security.iam.pageobjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ge.hc.lcs.axone.security.iam.constants.UIComponentConstants;
import com.ge.hc.lcs.axone.security.iam.utilities.TestUtility;

import net.serenitybdd.core.pages.PageObject;

public class UserPage extends PageObject {
	private final int threadSleep=5000;
	public UserPage(WebDriver driver){
		super(driver);
	}
	MenuPage menuPage;
	LoginPage loginPage;
	
	private String frameId= "contentFrame";
	private String dropDownMetaDataType = ".//*[@id='select2-metadataTypeId-container']";
	private String textMetaDataType  = ".//*[@id='js']/body/span/span/span[1]/input";
	private String textFirstName = "//input[@id='firstName']";
	private String textMiddleName = "//input[@id='middleInit']";
	private String textLastName = "//input[@id='lastName']";
	private String linkSearchRole =  "//a[@id='userRoleId']";
	private String textRoleName = "//label[contains(text(),'Role Name')]/../input[@id='name']";
	private String buttonSearch = "//div[@class='b-btn m-button m-small m-type1']//a[contains(text(),'Search')]";
	private String closePopup = ".//*[@id='js']/body/div[7]/div[1]/button";
	private String buttonSave = "//a[contains(text(),'Save')]";
	private String closePopupSucess  = ".//*[@id='js']/body/div[8]/div[1]/button";
	private String textSearchLastName= "//div[@class='b-item m-input']/label[contains(text(),'Last name')]/../input[@id='lastName']";
	private String buttonUserSearch = "//div[@class='b-btn m-button m-small m-type1']/a[contains(text(),'Search')]";
	private String buttonDeleteUser = "//div//input[@id='REMOVE_USER']";
	private String buttonSaveDeleteConfirm = "//div[@class='b-btn m-button m-small m-type1']//a[contains(text(),'Save')]";
	private String iconEditButton= "//td[@class='b-cell']/../..//i[contains(text(),'mode_edit')]";
	private String textResponseToUnlock = "//div[@class='b-subline']//h1[contains(text(),'Response questions to unlock')]";
	private String dropdownSelectQuestion = "//span[@class='select2-selection__arrow']//b[@role='presentation']";
	private String selectingQuestion  = "//span[@class='select2-results']/ul/li[2]";
	private String inputFirstAnswer = ".//*[@id='js']/body/div[1]/div/div/div[2]/div/div[1]/div/div[2]/input";
	private String inputSecondAnswer = ".//*[@id='js']/body/div/div/div/div[2]/div/div[1]/div/div[3]/input";
	private String buttonSaveResponeQuestion = "//div[@class='b-btn m-button m-small m-type1 ']//input[@id='save']";
	private String dropDownEmail = "//span[@id='select2-emailTypeId-container'][contains(text(),'Please Select...')]";
	private String textEmail = ".//*[@id='js']/body/span/span/span[1]/input";
	private String selectEmailType = "//span[@class='select2-results']/ul/li";
	private String inputEmail = "//label[contains(text(),'Email Address:')]/../input";
	private String dropDownPhoneType = ".//*[@id='select2-phoneTypeId-container']";
	private String textPhone = ".//*[@id='js']/body/span/span/span[1]/input";
	private String selectPhoneType ="//span[@class='select2-results']/ul/li";
	private String inputPhoneNumber = ".//*[@id='phoneNumber']";
	
	public void setUserInfo(String metadataType, String firstName, String middleName, String lastName, String roleName, String email,String phoneNumber) throws InterruptedException {
		WebElement metaType;
		WebElement firstname;
		WebElement middlename;
		WebElement lastname;
		WebElement emailFieldType;
		WebElement emailField;
		WebElement phoneFieldType;
		WebElement phoneNumberField;
		getDriver().switchTo().frame(frameId);
		(new WebDriverWait(getDriver(), UIComponentConstants.TEN)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(dropDownMetaDataType)));
		TestUtility.sleep(UIComponentConstants.FIVE);
		find(By.xpath(dropDownMetaDataType)).click();
		metaType = find(By.xpath(textMetaDataType));
		metaType.clear();
		metaType.sendKeys(metadataType);
		find(By.xpath("//ul[@id='select2-metadataTypeId-results']//li[contains(text(),'"+metadataType+"')]")).click();
		firstname = find(By.xpath(textFirstName));
		firstname.clear();
		firstname.sendKeys(firstName);
		middlename = find(By.xpath(textMiddleName));
		middlename.clear();
		middlename.sendKeys(middleName);
		lastname = find(By.xpath(textLastName));
		lastname.clear();
		lastname.sendKeys(lastName);
		this.assignRoleToUser(roleName);
		TestUtility.sleep(UIComponentConstants.THREE);
		find(By.xpath(dropDownEmail)).click();
		TestUtility.sleep(UIComponentConstants.THREE);
		emailFieldType = find(By.xpath(textEmail));
		emailFieldType.clear();
		emailFieldType.sendKeys("email");
		find(By.xpath(selectEmailType)).click();
		emailField = find(By.xpath(inputEmail));
		emailField.clear();
		emailField.sendKeys(email);
		find(By.xpath(dropDownPhoneType)).click();
		phoneFieldType = find(By.xpath(textPhone));
		phoneFieldType.clear();
		phoneFieldType.sendKeys("phone");
		find(By.xpath(selectPhoneType)).click();
		phoneNumberField = find(By.xpath(inputPhoneNumber));
		phoneNumberField.clear();
		phoneNumberField.sendKeys(phoneNumber);
		find(By.xpath(buttonSave)).click();
		(new WebDriverWait(getDriver(), UIComponentConstants.TEN)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(closePopupSucess)));
		find(By.xpath(closePopupSucess)).click();
		getDriver().manage().timeouts().implicitlyWait(UIComponentConstants.FIVE, TimeUnit.SECONDS);
		Thread.sleep(threadSleep);
		getDriver().switchTo().defaultContent();
	}
		
	//This method is used to assign role while creating user
	public void assignRoleToUser(String roleName) throws InterruptedException {
		
		WebElement getRoleName;
		find(By.xpath(linkSearchRole)).click();
		getDriver().manage().timeouts().implicitlyWait(UIComponentConstants.THREE, TimeUnit.SECONDS);
			
		getRoleName = find(By.xpath(textRoleName));
		getRoleName.clear();
		getRoleName.sendKeys(roleName);
		
		find(By.xpath(buttonSearch)).click();
		getDriver().manage().timeouts().implicitlyWait(UIComponentConstants.FIVE, TimeUnit.SECONDS);
		
		find(By.xpath("//td[@class='b-cell']//span[contains(text(),'"+roleName+"')]/../..//i[contains(text(),'add')]")).click();
		
		(new WebDriverWait(getDriver(), UIComponentConstants.TEN)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(closePopup)));
		find(By.xpath(closePopup)).click();
		getDriver().manage().timeouts().implicitlyWait(UIComponentConstants.FIVE, TimeUnit.SECONDS);
				
	}
	public void searchUser(String lastName) throws InterruptedException{
		
		WebElement lastname;
		getDriver().switchTo().frame(frameId);
		
		lastname = find(By.xpath(textSearchLastName));
		lastname.clear();
		lastname.sendKeys(lastName);
		getDriver().manage().timeouts().implicitlyWait(UIComponentConstants.FIVE, TimeUnit.SECONDS);
		find(By.xpath(buttonUserSearch)).click();
		getDriver().manage().timeouts().implicitlyWait(UIComponentConstants.FIVE, TimeUnit.SECONDS);
		find(By.xpath(iconEditButton)).click();
		getDriver().manage().timeouts().implicitlyWait(UIComponentConstants.FIVE, TimeUnit.SECONDS);
		
	}
	public void deleteUser(String lastName) throws InterruptedException{
		
		new WebDriverWait(getDriver(),UIComponentConstants.FIVE);
		searchUser(lastName);
		find(By.xpath(buttonDeleteUser)).click();
		(new WebDriverWait(getDriver(), UIComponentConstants.TEN)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(buttonDeleteUser)));
		find(By.xpath(buttonSaveDeleteConfirm)).click();
		getDriver().manage().timeouts().implicitlyWait(UIComponentConstants.FIVE, TimeUnit.SECONDS);
		getDriver().switchTo().defaultContent();
	}
public void activatingNewUser(String firstName,String lastName,String password) throws Exception{
		
		WebElement firstAnswer;
		WebElement secondAnswer;
		String principal=firstName+"."+lastName;
		
		menuPage.logout();
		loginPage.userLogin(principal, password);
		(new WebDriverWait(getDriver(), UIComponentConstants.TEN)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(textResponseToUnlock)));
		
		find(By.xpath(dropdownSelectQuestion)).click();
		Thread.sleep(2000);
		find(By.xpath(selectingQuestion)).click();
		
		firstAnswer = find(By.xpath(inputFirstAnswer));
		firstAnswer.clear();
		firstAnswer.sendKeys("abc");
		
		secondAnswer = find(By.xpath(inputSecondAnswer));
		secondAnswer.clear();
		secondAnswer.sendKeys("abc");
		
		find(By.xpath(buttonSaveResponeQuestion)).click();
		Thread.sleep(threadSleep);
	
	}
}

