Access Management
========
This project contains design level tests for testing the user access controls using oAuth2.0 framework. These tests are performed by using the sample oAuth client applications.

## Project Structure

```
├── build.gradle        <-- contains tasks and configurations specific to Access management
├── CHANGELOG.md        <-- contains the information about the changes made in each release
├── README.md           <-- overview of the project
├── src                 <-- contains JAVA files for design level test implementation.
└── support             <-- contains oAuth2 framework specific client application.
```

## Module

* src
     * test (automated tests)
          * java(It defines design level tests)
          * resources has features folder(It defines requirements)

NOTE: The automation test runs successfully only when dummy client application running which are located at access_management/support/client_applications.
