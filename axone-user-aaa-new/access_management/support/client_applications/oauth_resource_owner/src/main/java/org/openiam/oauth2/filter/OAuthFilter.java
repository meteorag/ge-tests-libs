package org.openiam.oauth2.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.openiam.oauth2.model.OAuthToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

import com.ge.hc.lcs.axone.oauth.constants.Constants;
import com.ge.hc.lcs.axone.oauth.help.OAuthFilterHelper;

public class OAuthFilter implements Filter {
	
	private OAuthFilterHelper oAuthFilterHelper = new OAuthFilterHelper();
	
	@Value("${openiam.oauth2.client.clientId}")
	private String clientId;
	
	@Value("${openiam.oauth2.client.baseURL}"+"${openiam.oauth2.resource.userInfoUri}")
	private String userInfoEndpoint;

	@Value("${openiam.oauth2.client.baseURL}"+"${openiam.oauth2.client.accessTokenUri}")
	private String accessTokenEndpoinit;
	
	@Value("${openiam.oauth2.client.baseURL}"+"${openiam.oauth2.client.userAuthorizationUri}")
	private String authorizeEndpoint;
	
	@Value("${openiam.oauth2.client.baseURL}"+"${openiam.oauth2.client.token.info.endpoint}")
	private String tokenInfoEndpoint;
	
	@Autowired
	private RestTemplate restTemplate;
	
	public void destroy() {
		
	}
	
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain chain) throws IOException, ServletException {
		final HttpServletRequest request = (HttpServletRequest)arg0;
		final HttpServletResponse response = (HttpServletResponse)arg1;
		final HttpSession session = request.getSession(true);
		final OAuthToken token = (OAuthToken)session.getAttribute(Constants.TOKEN);
		final String localdomain = new StringBuilder("http://").append(request.getHeader("host")).toString();
	
		if(token == null || token.isExpired()) {
			response.sendRedirect(localdomain+"/loginpage");
		} else {
			if (!oAuthFilterHelper.checkUserAccessToResource(token.getAccessToken(), request.getRequestURI(),
					tokenInfoEndpoint, restTemplate)) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
				return;
			}
			chain.doFilter(arg0, arg1);
		}
	}

	public void init(FilterConfig arg0) throws ServletException {
		
	}

}
