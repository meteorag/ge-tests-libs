package com.ge.hc.lcs.axone.oauth.client;

import java.util.HashMap;
import java.util.Map;

import org.apache.oltu.oauth2.client.HttpClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthAccessTokenResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;

public class OAuthCustomClient {

	protected HttpClient httpClient;

	public OAuthCustomClient(HttpClient oauthClient) {
		this.httpClient = oauthClient;
	}

	public <T extends OAuthAccessTokenResponse> T accessToken(OAuthClientRequest oAuthClientRequest, Class<T> responseClass,
			String authorization) throws OAuthSystemException, OAuthProblemException {
		return accessToken(oAuthClientRequest, OAuth.HttpMethod.POST, responseClass, authorization);
	}

	public <T extends OAuthAccessTokenResponse> T accessToken(OAuthClientRequest request, String requestMethod,
			Class<T> responseClass, String authorization) throws OAuthSystemException, OAuthProblemException {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put(OAuth.HeaderType.CONTENT_TYPE, OAuth.ContentType.URL_ENCODED);
		headers.put(OAuth.HeaderType.AUTHORIZATION, authorization);
		return httpClient.execute(request, headers, requestMethod, responseClass);
	}

}
