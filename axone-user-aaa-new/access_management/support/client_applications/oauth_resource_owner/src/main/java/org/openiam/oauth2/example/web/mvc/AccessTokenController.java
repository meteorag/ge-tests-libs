package org.openiam.oauth2.example.web.mvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.openiam.oauth2.model.OAuthToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ge.hc.lcs.axone.oauth.client.OAuthCustomClient;
import com.ge.hc.lcs.axone.oauth.constants.Constants;
import com.ge.hc.lcs.axone.oauth.help.ControllerHelper;

@Controller
public class AccessTokenController {
	
	private ControllerHelper controllerhelper = new ControllerHelper();
	
	@Value("${openiam.oauth2.client.clientSecret}")
	private String clientSecret;
	
	@Value("${openiam.oauth2.client.baseURL}"+"${openiam.oauth2.resource.userInfoUri}")
	private String userInfoEndpoint;

	@Value("${openiam.oauth2.client.baseURL}"+"${openiam.oauth2.client.accessTokenUri}")
	private String accessTokenEndpoint;
	
	@Value("${openiam.oauth2.client.clientId}")
	private String clientId;
	
	@RequestMapping("/authenticate")
	public String login(final HttpServletRequest request) throws Exception {
		final HttpSession session = request.getSession(true);
		org.apache.oltu.oauth2.common.token.OAuthToken accessTokenResponse = getAccessToken(request.getParameter("uname"), request.getParameter("psw"));
		OAuthToken oAuthToken = new OAuthToken(accessTokenResponse.getAccessToken(), accessTokenResponse.getExpiresIn().toString(), "",
				accessTokenResponse.getScope(), accessTokenResponse.getRefreshToken());
		session.setAttribute(Constants.TOKEN, oAuthToken);
		String userName = controllerhelper.getUserNameFromUserInformation(accessTokenResponse.getAccessToken(), userInfoEndpoint);
		request.setAttribute("userInfo", userName);
		return Constants.PAGE_USERHOME;
	}
	
	public org.apache.oltu.oauth2.common.token.OAuthToken getAccessToken(String userName, String pwd) throws OAuthSystemException, OAuthProblemException {
		OAuthClientRequest oauthRequest = OAuthClientRequest.tokenLocation(accessTokenEndpoint)
				.setGrantType(GrantType.PASSWORD).setUsername(userName)
				.setPassword(pwd)
				.setClientId(clientId)
				.setClientSecret(clientSecret).buildBodyMessage();
		OAuthCustomClient oAuthClient = new OAuthCustomClient(new URLConnectionClient());
		org.apache.oltu.oauth2.common.token.OAuthToken accessToken = oAuthClient
				.accessToken(oauthRequest,
						OAuthJSONAccessTokenResponse.class, controllerhelper.getAuthorizationHeader(
								clientId, clientSecret)).getOAuthToken();
		return accessToken;
	}
	
	@RequestMapping("/loginpage")
	public String loginPage(final HttpServletRequest request) {
		return Constants.PAGE_LOGIN;
	}
	
}
