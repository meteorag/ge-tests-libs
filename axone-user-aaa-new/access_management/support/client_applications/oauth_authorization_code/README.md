# Application start-up

1. Start application instance (port - 8083) with **valid client configuration** using below command,
   mvn spring-boot:run -Drun.arguments="valid.properties"

2. Start application instance (port - 8087) with **invalid client ID** using below command,
   mvn spring-boot:run -Drun.arguments="invalid_client_id.properties"

3. Start application instance (port - 8086) with **invalid client secret** using below command,
   mvn spring-boot:run -Drun.arguments="invalid_client_secret.properties"

4. Start application instance (port - 8085) with **invalid redirect-URL** configuration using below command,
   mvn spring-boot:run -Drun.arguments="invalid_redirect_url.properties"
