package org.openiam.oauth2.example.web.mvc;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.openiam.oauth2.model.OAuthToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ge.hc.lcs.axone.oauth.client.OAuthCustomClient;
import com.ge.hc.lcs.axone.oauth.constants.Constants;
import com.ge.hc.lcs.axone.oauth.help.ControllerHelper;

@Controller
public class AccessTokenController {

	private ControllerHelper controllerhelper = new ControllerHelper();

	@Value("${openiam.oauth2.client.clientSecret}")
	private String clientSecret;

	@Value("${openiam.oauth2.client.baseURL}"+"${openiam.oauth2.resource.userInfoUri}")
	private String userInfoEndpoint;

	@Value("${openiam.oauth2.client.clientId}")
	private String clientId;

	@Value("${openiam.oauth2.client.baseURL}"+"${openiam.oauth2.client.accessTokenUri}")
	private String accessTokenEndpoint;

	@Value("${openiam.oauth2.client.redirecturl}")
	private String redirectUrl;

	/**
	 * This method is called by the redirect URL from the OpenIAM after
	 * successfully authenticating.
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/oauthhandler")
	public String handlerOAuthResponse(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
	
		org.apache.oltu.oauth2.common.token.OAuthToken accessTokenResponse = null;
		try {
			accessTokenResponse = getAccesstokenUsingAuthCode(request.getParameter("code"));
		} catch (OAuthSystemException | OAuthProblemException e1) {
			request.setAttribute("errorMessage", e1.getMessage());
			return "error";
		}
		final HttpSession session = request.getSession(true);
		OAuthToken token = new OAuthToken(accessTokenResponse.getAccessToken(),
				accessTokenResponse.getExpiresIn().toString(), "", accessTokenResponse.getScope(),
				accessTokenResponse.getRefreshToken());
		session.setAttribute(Constants.TOKEN, token);
		String userName = controllerhelper.getUserNameFromUserInformation(accessTokenResponse.getAccessToken(),
				userInfoEndpoint);
		request.setAttribute("userInfo", userName);
		return Constants.PAGE_USERHOME;
	}

	private org.apache.oltu.oauth2.common.token.OAuthToken getAccesstokenUsingAuthCode(final String authorizationCode)
			throws OAuthSystemException, OAuthProblemException {
		OAuthClientRequest oAuthRequest = OAuthClientRequest.tokenLocation(accessTokenEndpoint)
				.setGrantType(GrantType.AUTHORIZATION_CODE).setClientId(clientId).setClientSecret(clientSecret)
				.setRedirectURI(redirectUrl).setCode(authorizationCode).buildBodyMessage();
		OAuthCustomClient oAuthClient = new OAuthCustomClient(new URLConnectionClient());
		org.apache.oltu.oauth2.common.token.OAuthToken accessToken = null;
		accessToken = oAuthClient.accessToken(oAuthRequest, OAuthJSONAccessTokenResponse.class,
				controllerhelper.getAuthorizationHeader(clientId, clientSecret)).getOAuthToken();
		return accessToken;
	}

}
