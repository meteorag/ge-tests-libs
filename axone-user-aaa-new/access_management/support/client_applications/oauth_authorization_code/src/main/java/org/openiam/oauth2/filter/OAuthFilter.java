package org.openiam.oauth2.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.openiam.oauth2.model.OAuthToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

import com.ge.hc.lcs.axone.oauth.constants.Constants;
import com.ge.hc.lcs.axone.oauth.help.OAuthFilterHelper;

public class OAuthFilter implements Filter {

	private OAuthFilterHelper oAuthFilterHelper = new OAuthFilterHelper();

	@Value("${openiam.oauth2.client.clientId}")
	private String clientId;

	@Value("${openiam.oauth2.client.baseURL}"+"${openiam.oauth2.client.userAuthorizationUri}")
	private String authorizeEndpoint;

	@Value("${openiam.oauth2.client.baseURL}"+"${openiam.oauth2.client.token.info.endpoint}")
	private String tokenInfoEndpoint;

	@Autowired
	private RestTemplate restTemplate;

	@Value("${openiam.oauth2.client.redirecturl}")
	private String redirectUrl;

	public void destroy() {
	}

	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;
		HttpSession session = request.getSession(true);
		OAuthToken token = (OAuthToken) session.getAttribute(Constants.TOKEN);

		if (token == null || token.isExpired()) {
			StringBuilder sb = new StringBuilder(authorizeEndpoint).append("?response_type=code").append("&client_id=")
					.append(clientId).append("&redirect_uri=").append(redirectUrl);
			response.sendRedirect(sb.toString());
		} else {
			if (!oAuthFilterHelper.checkUserAccessToResource(token.getAccessToken(), request.getRequestURI(),
					tokenInfoEndpoint, restTemplate)) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
				return;
			}
			chain.doFilter(arg0, arg1);
		}
	}

	public void init(FilterConfig arg0) throws ServletException {

	}
}
