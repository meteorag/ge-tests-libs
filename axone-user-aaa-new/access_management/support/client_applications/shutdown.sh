#!/usr/bin/env bash
# Script to shut down the client applications

. env.sh

# Declare the ports where the client applications are running
declare -a ports=(${implicit_client_port} ${authcode_client_port} ${resource_owner_port} ${authcode_client_invalid_clientSecret_port} ${authcode_client_invalid_clientID_port} ${authcode_client_invalid_redirectURL_port})

exit_status=0

# Client application processes running in those ports are killed
for i in "${ports[@]}"
do
    pid=$(lsof -i :$i | tail -n +2 | awk '{print $2}');
    if [[ -z $pid ]]; then
        echo "No process running on port " $i
    fi
    if [ -n "${pid}" ]; then
        echo "Killing application running in port " $i " and PID " $pid
        kill -9 $pid
        if [[ $? -ne 0 ]]; then
            echo "Failed to kill application running on port " $i
            exit_status=1
        fi
    fi
done

exit $exit_status