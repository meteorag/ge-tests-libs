#!/usr/bin/env bash
# Script to start the cient applications for access management tests

. env.sh

echo -e '\n###################################'
echo -e '# Assumption - Maven is installed.'
echo -e '#####################################\n'

usage()
{
    echo "Usage: ./`basename $0` [openiam-host] [client-host]"
    echo -e "Eg, ./startup.sh http://10.177.220.188:8000 http://127.0.0.1\n"
}

if [ "$1" == "-h" ]; then
    usage
    exit 0
fi

if [ "$#" -ne 2 ]; then
    echo -e "Error - Provide valid inputs.\n"
    usage
    exit 1
fi

base_URL=$1
client_host=$2

# Verify an application is running on the provided port
isApplicationRunning()
{
    port=$1
    n=0
    until [ $n -ge 20 ]
    do
        if [ -n "$(lsof -i :$port | tail -n +2 | awk '{print $2}')" ]; then
            return 0
        fi
        n=$[$n+1]
        sleep 3
    done
    exit 1
}

# Create an executable jar to start the spring boot application
createPackage()
{
    mvn clean package
    if [ "$?" -ne "0" ]; then
        exit 1
    fi
}

# Base directory of client applications
base_directory=$(cd "$(dirname "$0")"; pwd)

#OAuth client lib directory
cd ${base_directory}/oauth-client-lib
mvn clean install

# Arguments for Authorization code oauth client
authcode_redirect_url=${client_host}:${authcode_client_port}/oauthhandler
authcode_redirect_url_invalid_secret=${client_host}:${authcode_client_invalid_clientSecret_port}/oauthhandler

# Arguments for Implicit oauth client
implicit_redirect_url=${client_host}:${implicit_client_port}/oauthhandler

# Arguments for Resource owner oauth client
resource_owner_redirect_url=${client_host}:${resource_owner_port}/oauthhandler

# Starting Authorization code oauth client application instances
cd ${base_directory}/oauth_authorization_code
# Starting valid Authorization code oauth client
createPackage
java -jar target/oauth_authorization_code-1.0.jar $authcode_client_id $authcode_client_secret $authcode_redirect_url ${authcode_client_port} $base_URL &>target/authorization_code_log.txt &
isApplicationRunning ${authcode_client_port}

# Starting Authorization code oauth client with Invalid client ID
java -jar target/oauth_authorization_code-1.0.jar invalid_cient_id $authcode_client_secret $authcode_redirect_url ${authcode_client_invalid_clientID_port} $base_URL &>target/authorization_code_invalid_client_id_log.txt &
isApplicationRunning ${authcode_client_invalid_clientID_port}

# Starting Authorization code oauth client with Invalid Redirect URL
java -jar target/oauth_authorization_code-1.0.jar $authcode_client_id $authcode_client_secret invalid_redirect_url ${authcode_client_invalid_redirectURL_port} $base_URL &>target/authorization_code_invalid_client_redirect_url.txt &
isApplicationRunning ${authcode_client_invalid_redirectURL_port}

# Starting Authorization code oauth client with Invalid Client Secret
java -jar target/oauth_authorization_code-1.0.jar $authcode_client_id invalid_client_secret $authcode_redirect_url_invalid_secret ${authcode_client_invalid_clientSecret_port} $base_URL &>target/authorization_code_invalid_client_secret_log.txt &
isApplicationRunning ${authcode_client_invalid_clientSecret_port}

# Starting Implicit oauth client application instance (Default port for implict application is ${implicit_client_port})
cd ${base_directory}/oauth_implicit
createPackage
java -jar target/oauth_implicit-1.0.jar $implicit_client_id $implicit_client_secret $implicit_redirect_url $base_URL &>target/implicit_log.txt &
isApplicationRunning ${implicit_client_port}

# Starting Resource owner oauth client application instance (Default port for implict application is ${resource_owner_port})
cd ${base_directory}/oauth_resource_owner
createPackage
java -jar target/oauth_resource_owner-1.0.jar $resource_owner_client_id $resource_owner_client_secret $resource_owner_redirect_url $base_URL &>target/resource_owner_log.txt &
isApplicationRunning ${resource_owner_port}

echo "Client applications are started successfully"
exit 0
