
toplevel - Blue Arrow Repository - LCS Software / Axone Platform
================================================================

This repository contains three projects, which are oAuth 2 framework client applications. One for each of the below grant types,

1. Authorization code grant flow - /oauth_authorization_code
2. Implicit grant flow - /oauth_implicit
3. Resource Owner Grant flow - /oauth_resource_owner

All these applications can be started and shut down using the below scripts

- startup.sh - to start the below applications

  `http://localhost:8083` , authorization code application with valid configuration  
  `http://localhost:8086` , authorization code application with invalid client secret   
  `http://localhost:8087` , authorization code application with invalid client ID  
  `http://localhost:8088` , authorization code application with invalid redirect URL  
  `http://localhost:8082` , implicit grant application  
  `http://localhost:8084` , resource owner application  

- shutdown.sh - to shut down the applications

### Configurations

startup.sh

- `base_directory` , parent directory containing the projects
- `authcode_client_id` , client ID of the authorization code application
- `authcode_client_secret`, client secret of the authorization code application
- `implicit_client_id` , client ID of the authorization code application
- `implicit_client_secret`, client secret of the authorization code application
- `resource_owner_client_id` , client ID of the authorization code application
- `resource_owner_client_secret`, client secret of the authorization code application
- `authcode_redirect_url` , redirect URL of the authorization code application  
- `authcode_redirect_url_invalid_secret` , redirect URL of the authorization code application with invalid client secret
- `implicit_redirect_url` , redirect URL of the authorization code application  
- `resource_owner_redirect_url` , redirect URL of the authorization code application  

shutdown.sh

- `ports` , all the ports used by the client applications

### Starting the applications

`sh startup.sh`

### Shut down the applications

`sh shutdown.sh`

### Application Configuration

Application configuration is available in the below location for each application,

`src\main\resources\application.properties`

It contains the following configurations,

`openiam.oauth2.client.baseURL` , host name of OpenIAM server

`openiam.oauth2.client.accessTokenUri` , token endpoint

`openiam.oauth2.client.token.info.endpoint` , token info endpoint

`openiam.oauth2.client.userAuthorizationUri` , authorization endpoint

`openiam.oauth2.resource.userInfoUri` , user info endpoint

`resource.owner.username` , OpenIAM administrator user name

`resource.owner.password` , OpenIAM administrator password

