package org.openiam.oauth2.example.web.mvc;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openiam.oauth2.model.OAuthToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.ge.hc.lcs.axone.oauth.constants.Constants;
import com.ge.hc.lcs.axone.oauth.help.ControllerHelper;

@Controller
public class AccessTokenController {

	private ControllerHelper controllerhelper = new ControllerHelper(); 

	@Value("${openiam.oauth2.client.baseURL}"+"${openiam.oauth2.resource.userInfoUri}")
	private String userInfoEndpoint;

	/**
	 * This method is called by the redirect URL from the OpenIAM after
	 * successfully authenticating.
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/oauthhandler")
	public @ResponseBody String handlerOAuthResponse(final HttpServletRequest request,
			final HttpServletResponse response, final @RequestParam(value = "error", required = false) String error,
			final @RequestParam(value = "access_token", required = false) String token,
			final @RequestParam(value = "expires_in", required = false) String expiresIn,
			final @RequestParam(value = "state", required = false) String state,
			final @RequestParam(value = "token_type", required = false) String tokenType,
			final @RequestParam(value = "id_token", required = false) String idToken) throws IOException {
		if (error != null) {
			return error;
		} else {
			final OAuthToken accessToken = new OAuthToken(token, expiresIn, state, tokenType, idToken);
			request.getSession(true).setAttribute(Constants.TOKEN, accessToken);
			response.sendRedirect(state);
			return Constants.PAGE_USERHOME;
		}
	}
	
	@RequestMapping("/oauth-info")
	public String userInfo(final HttpServletRequest request, final @SessionAttribute OAuthToken token)
			throws IllegalArgumentException, IOException {
		String userName = controllerhelper.getUserNameFromUserInformation(token.getAccessToken(), userInfoEndpoint);
		request.setAttribute("userInfo", userName);
		return Constants.PAGE_USERHOME;
	}

}
