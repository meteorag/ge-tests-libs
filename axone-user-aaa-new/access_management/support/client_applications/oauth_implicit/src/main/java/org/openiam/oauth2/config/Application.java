package org.openiam.oauth2.config;

import java.io.InputStream;
import java.util.Properties;

import javax.servlet.Filter;

import org.openiam.oauth2.filter.OAuthFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.ge.hc.lcs.axone.oauth.constants.Constants;
import com.ge.hc.lcs.axone.oauth.help.BeanConfigurationHelper;

@Configuration
@Import(OAuthBeanFactory.class)
@ComponentScan(basePackages = "org.openiam.oauth2.example,com.ge.hc.lcs.axone.oauth.controller")
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class, SecurityAutoConfiguration.class})
@SpringBootApplication
public class Application {
	
	private BeanConfigurationHelper beanConfigurationHelper = new BeanConfigurationHelper();
	
	/**
	 * This method is used to start the spring application. Below arguments are
	 * set on runtime as it can be changed externally and can be used to start
	 * multiple instances with different clients. 
	 * args[0], ClientID - Client ID of the OAuth client
	 * args[1], ClientSecret - Client Secret of the OAuth Client 
	 * args[2], Redirect URL of the OAuth Client
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		SpringApplication application = new SpringApplication(Application.class);
		InputStream is = Application.class.getClassLoader().getResourceAsStream(Constants.APPLICATION_PROPERTIES);
		Properties properties = new Properties();
		properties.load(is);
		properties.setProperty(Constants.CLIENT_ID, args[0]);
		properties.setProperty(Constants.CLIENT_SECRET, args[1]);
		properties.setProperty(Constants.REDIRECT_URL, args[2]);
		properties.setProperty(Constants.BASE_URL, args[3]);
		application.setDefaultProperties(properties);
		application.run(args);
	}

	@Bean
	public Filter oAuthFilter() {
		return new OAuthFilter();
	}

    @Bean
    public FilterRegistrationBean myFilter() {
    	return beanConfigurationHelper.createFilter(oAuthFilter());
    }
    
}
