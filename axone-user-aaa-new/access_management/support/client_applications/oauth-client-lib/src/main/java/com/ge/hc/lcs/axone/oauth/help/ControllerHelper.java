package com.ge.hc.lcs.axone.oauth.help;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.openiam.oauth2.filter.AuthorizedScope;
import org.openiam.oauth2.filter.OAuthTokenInfoResponse;
import org.openiam.oauth2.model.OAuthToken;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ge.hc.lcs.axone.oauth.constants.Constants;

public class ControllerHelper {

	/**
	 * Get Encrypted Client ID and secret for authentication
	 * 
	 * @param clientID
	 * @param clientSecret
	 * @return
	 */
	public String getAuthorizationHeader(String clientID, String clientSecret) {
		return "Basic " + new Base64().encodeAsString(new String(clientID + ":" + clientSecret).getBytes());
	}

	public Map<String, Object> getMapFromJson(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> map = new HashMap<String, Object>();
		map = mapper.readValue(json, new TypeReference<Map<String, String>>() {
		});
		return map;
	}

	public String userInformation(final HttpServletRequest request, final @SessionAttribute OAuthToken token,
			final String userInfoEndpoint) throws Exception {
		URL url = new URL(userInfoEndpoint + Constants.TOKEN_PARAM + token.getAccessToken());
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		if (conn.getResponseCode() != 200) {
			conn.disconnect();
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode() + ", Message : "
					+ conn.getResponseMessage());
		}
		BufferedReader br1 = new BufferedReader(new InputStreamReader((conn.getInputStream())));
		String output;
		StringBuilder userInformation = new StringBuilder();
		while ((output = br1.readLine()) != null) {
			userInformation.append(output);
		}
		request.setAttribute("userinformation", constructUserInformationResponse(userInformation));
		return Constants.PAGE_USERINFORMATION;
	}

	private String constructUserInformationResponse(StringBuilder userInformation) {
		StringBuilder builder = new StringBuilder();
		builder.append("<div id='userinformation'><table border=1 id='userinformation_table'>");
		builder.append("<tr><th>Field</th><th>Value</th></tr>");
		try {
			Map<String, Object> map = getMapFromJson(userInformation.toString());
			for (Entry<String, Object> entry : map.entrySet()) {
				builder.append("<tr >");
				builder.append("<td>" + entry.getKey() + "</td>");
				builder.append("<td>" + (String) entry.getValue() + "</td>");
				builder.append("</tr>");
			}
		} catch (Exception e) {

		}
		builder.append("</table></div>");
		return builder.toString();
	}

	public String tokenInformation(final HttpServletRequest request, final @SessionAttribute OAuthToken token)
			throws IllegalArgumentException, UnsupportedEncodingException {
		request.setAttribute("tokeninfo", constructTokenInformationResponse(token));
		return "tokeninformation";
	}

	private String constructTokenInformationResponse(final OAuthToken token) {
		StringBuilder builder = new StringBuilder();
		builder.append("<div id='tokeninformation'><table border=1 id='tokeninformation_table'>");
		builder.append("<tr><th>Field</th><th>Value</th></tr>");
		builder.append("<tr><td>Access token</td><td id='access_token_value'>" + token.getAccessToken() + "</td></tr>");
		builder.append("<tr><td>State</td><td id='token_state_value'>" + token.getState() + "</td></tr>");
		builder.append("<tr><td>Token type</td><td id='token_type_value'>" + token.getTokenType() + "</td></tr>");
		builder.append("<tr><td>Expires In</td><td id='expires_in_value'>" + token.getExpiresIn() + "</td></tr>");
		builder.append(
				"<tr><td>Refresh Token</td><td id='refresh_token_value'>" + token.getRefreshToken() + "</td></tr>");
		builder.append("</table></div>");
		return builder.toString();
	}

	public String userScopes(final HttpServletRequest request, final @SessionAttribute OAuthToken token,
			final String tokenInfoEndpoint, final RestTemplate restTemplate)
			throws IllegalArgumentException, UnsupportedEncodingException {
		final String url = new StringBuilder(tokenInfoEndpoint).append(Constants.TOKEN_PARAM)
				.append(token.getAccessToken()).toString();
		final OAuthTokenInfoResponse tokenInfo = restTemplate.getForEntity(url, OAuthTokenInfoResponse.class).getBody();
		request.setAttribute("userscopes", constructUserScopesResponse(tokenInfo));
		return Constants.PAGE_USERSCOPES;
	}

	private String constructUserScopesResponse(final OAuthTokenInfoResponse tokenInfo) {
		StringBuilder userScopes = new StringBuilder();
		List<AuthorizedScope> authorizedScopes = tokenInfo.getAuthorizedScopes();
		userScopes.append("<div id='userscopes'><table border=1 id='userscopes_table'>");
		userScopes.append("<tr><th>No</th><th>Value</th></tr>");
		if (authorizedScopes != null) {
			for (int i = 0; i < authorizedScopes.size(); i++) {
				userScopes
						.append("<tr><td>" + (i + 1) + "</td><td>" + authorizedScopes.get(i).getName() + "</td></tr>");
			}
		}
		userScopes.append("</table></div>");
		return userScopes.toString();
	}

	public String getUserNameFromUserInformation(String accessToken, final String userInfoEndpoint) throws IOException {
		URL url = new URL(userInfoEndpoint + Constants.TOKEN_PARAM + accessToken);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		if (conn.getResponseCode() != 200) {
			conn.disconnect();
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode() + ", Message : "
					+ conn.getResponseMessage());
		}
		BufferedReader br1 = new BufferedReader(new InputStreamReader((conn.getInputStream())));
		String output;
		StringBuilder userInformation = new StringBuilder();
		while ((output = br1.readLine()) != null) {
			userInformation.append(output);
		}
		String userName = "";
		if (userInformation != null) {
			try {
				userName = (String) getMapFromJson(userInformation.toString()).get("name");
			} catch (Exception e) {
			}
		}
		return userName;
	}

	public String refreshtoken(HttpServletRequest request, OAuthToken token, String userInfoEndpoint)
			throws IOException {
		URL url = new URL(userInfoEndpoint + Constants.REFRESH_TOKEN_PARAM + token.getRefreshToken());
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		if (conn.getResponseCode() != 200) {
			conn.disconnect();
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode() + ", Message : "
					+ conn.getResponseMessage());
		}
		BufferedReader br1 = new BufferedReader(new InputStreamReader((conn.getInputStream())));
		String output;
		StringBuilder tokenInformation = new StringBuilder();
		while ((output = br1.readLine()) != null) {
			tokenInformation.append(output);
		}
		request.setAttribute("tokeninfo", constructRefreshTokenResponse(tokenInformation.toString()));
		return "tokeninformation";
	}

	private String constructRefreshTokenResponse(String tokenInformation) {
		try {
			Map<String, Object> map = getMapFromJson(tokenInformation.toString());
			OAuthToken token = new OAuthToken(map.get("access_token") != null ? (String) map.get("access_token") : "",
					map.get("expires_in") != null ? (String) map.get("expires_in") : "",
					map.get("state") != null ? (String) map.get("state") : "",
					map.get("token_type") != null ? (String) map.get("token_type") : "",
					map.get("refresh_token") != null ? (String) map.get("refresh_token") : "");
			return constructTokenInformationResponse(token);
		} catch (IOException e) {
		}
		return "";
	}

	public String revoketoken(HttpServletRequest request, OAuthToken token, String revocationEndpoint) throws IOException {
		URL url = new URL(revocationEndpoint + Constants.TOKEN_PARAM + token.getAccessToken());
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setRequestProperty("content-Type", "application/x-www-form-urlencoded");
		if (conn.getResponseCode() != 200) {
			conn.disconnect();
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode() + ", Message : "
					+ conn.getResponseMessage());
		}
		BufferedReader br1 = new BufferedReader(new InputStreamReader((conn.getInputStream())));
		String output;
		StringBuilder revocationResponse = new StringBuilder();
		while ((output = br1.readLine()) != null) {
			revocationResponse.append(output);
		}
		request.setAttribute("revocationResponse", revocationResponse.toString());
		return "revoke";
	}
}
