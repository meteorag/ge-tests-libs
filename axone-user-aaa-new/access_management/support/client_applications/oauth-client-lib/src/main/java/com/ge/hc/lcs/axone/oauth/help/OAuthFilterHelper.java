package com.ge.hc.lcs.axone.oauth.help;

import org.openiam.oauth2.filter.OAuthTokenInfoResponse;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.ge.hc.lcs.axone.oauth.constants.Constants;

public class OAuthFilterHelper {

	public boolean checkUserAccessToResource(String token, String requestUrl, String tokenInfoEndpoint, final RestTemplate restTemplate) {
		OAuthTokenInfoResponse tokenInfo = getTokenInfo(token, tokenInfoEndpoint, restTemplate);
		if (CollectionUtils.isEmpty(tokenInfo.getAuthorizedScopes()) || tokenInfo.getAuthorizedScopes().stream()
				.filter(e -> e.getName().contains(requestUrl)).count() == 0) {
			return false;
		}
		return true;
	}
	
	private OAuthTokenInfoResponse getTokenInfo(String token, String tokenInfoEndpoint, final RestTemplate restTemplate) {
		String url = new StringBuilder(tokenInfoEndpoint).append(Constants.TOKEN_PARAM)
				.append(token).toString();
		return restTemplate.getForEntity(url, OAuthTokenInfoResponse.class).getBody();
	}
	
}
