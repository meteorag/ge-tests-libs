package org.openiam.oauth2.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ge.hc.lcs.axone.oauth.help.BeanConfigurationHelper;

@Configuration
public class OAuthBeanFactory {

	private BeanConfigurationHelper beanConfigurationHelper = new BeanConfigurationHelper();

	@Bean
	public RestTemplate restTemplate() {
		return beanConfigurationHelper.createRestTemplate(messageConverter());
	}

	@Bean
	public MappingJackson2HttpMessageConverter messageConverter() {
		return beanConfigurationHelper.craeteMessageConverter(jacksonMapper());
	}

	@Bean
	public ObjectMapper jacksonMapper() {
		return new ObjectMapper();
	}

}
