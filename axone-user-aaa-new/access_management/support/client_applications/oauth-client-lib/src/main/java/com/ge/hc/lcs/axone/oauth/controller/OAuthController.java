package com.ge.hc.lcs.axone.oauth.controller;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import org.openiam.oauth2.model.OAuthToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.client.RestTemplate;

import com.ge.hc.lcs.axone.oauth.constants.Constants;
import com.ge.hc.lcs.axone.oauth.help.ControllerHelper;

@Controller
public class OAuthController {
	
	private ControllerHelper controllerhelper = new ControllerHelper();

	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${openiam.oauth2.client.baseURL}"+"${openiam.oauth2.client.token.info.endpoint}")
	private String tokenInfoEndpoint;
	
	@Value("${openiam.oauth2.client.baseURL}"+"${openiam.oauth2.resource.userInfoUri}")
	private String userInfoEndpoint;
	
	@Value("${openiam.oauth2.client.baseURL}"+"${openiam.oauth2.resource.refreshTokenUri}")
	private String refreshTokenEndpoint;

	@Value("${openiam.oauth2.client.baseURL}"+"${openiam.oauth2.resource.revokeTokenUri}")
	private String revocationEndpoint;
	
	@RequestMapping("/hospitalpage")
	public String hospitalPage(final HttpServletRequest request, final @SessionAttribute OAuthToken token)
			throws IllegalArgumentException, UnsupportedEncodingException {
		return Constants.PAGE_HOSPITAL;
	}

	@RequestMapping("/geservicepage")
	public String geServicePage(final HttpServletRequest request, final @SessionAttribute OAuthToken token)
			throws IllegalArgumentException, UnsupportedEncodingException {
		return Constants.PAGE_GESERVICE;
	}

	/**
	 * This Method fetches the user scopes from token info endpoint
	 * @param request
	 * @param token
	 * @return
	 * @throws IllegalArgumentException
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping("/userscopes")
	public String userScopes(final HttpServletRequest request, final @SessionAttribute OAuthToken token)
			throws IllegalArgumentException, UnsupportedEncodingException {
		return controllerhelper.userScopes(request, token, tokenInfoEndpoint, restTemplate);
	}

	/**
	 * Fetches the token information from the access token which has been received from token endpoint
	 * @param request
	 * @param token
	 * @return
	 * @throws IllegalArgumentException
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping("/tokeninformation")
	public String tokenInformation(final HttpServletRequest request, final @SessionAttribute OAuthToken token)
			throws IllegalArgumentException, UnsupportedEncodingException {
		return controllerhelper.tokenInformation(request, token);
	}

	@RequestMapping("/userinformation")
	public String userInformation(final HttpServletRequest request, final @SessionAttribute OAuthToken token)
			throws Exception {
		return controllerhelper.userInformation(request, token, userInfoEndpoint);
	}

	@RequestMapping("/")
	public String indexPage(final HttpServletRequest request) {
		return Constants.PAGE_INDEX;
	}

	@RequestMapping("/refreshtoken")
	public String refreshToken(final HttpServletRequest request, final @SessionAttribute OAuthToken token)
			throws Exception {
		return controllerhelper.refreshtoken(request, token, refreshTokenEndpoint);
	}
	
	@RequestMapping("/revoke")
	public String revokeToken(final HttpServletRequest request, final @SessionAttribute OAuthToken token)
			throws Exception {
		return controllerhelper.revoketoken(request, token, revocationEndpoint);
	}
	
	@RequestMapping("/logout")
	public String logout(final HttpServletRequest request, final @SessionAttribute OAuthToken token)
			throws Exception {
		request.getSession().invalidate();
		return "logout";
	}
	
	
}