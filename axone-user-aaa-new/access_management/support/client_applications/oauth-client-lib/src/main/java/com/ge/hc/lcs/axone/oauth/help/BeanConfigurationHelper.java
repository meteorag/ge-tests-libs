package com.ge.hc.lcs.axone.oauth.help;

import java.util.Arrays;

import javax.servlet.Filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ge.hc.lcs.axone.oauth.constants.Constants;

public class BeanConfigurationHelper {

    public FilterRegistrationBean createFilter(Filter oAuthFilter) {
        final FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(oAuthFilter);
        registration.addUrlPatterns(Constants.URL_OAUTH_INFO);
        registration.addUrlPatterns(Constants.URL_USERINFORMATION);
        registration.addUrlPatterns(Constants.URL_USERSCOPES);
        registration.addUrlPatterns(Constants.URL_TOKENINFORMATION);
        registration.addUrlPatterns(Constants.URL_GE_SERVICE);
        registration.addUrlPatterns(Constants.URL_HOSPITAL);
        return registration;
    }
    
	public MappingJackson2HttpMessageConverter craeteMessageConverter(ObjectMapper objectMapper) {
		final MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter(objectMapper);
		converter.setSupportedMediaTypes(Arrays.asList(new MediaType[] {
			MediaType.APPLICATION_JSON
		}));
		return converter;
	}
	
	public RestTemplate createRestTemplate(MappingJackson2HttpMessageConverter messageConverter) {
		final RestTemplate template = new RestTemplate();
		template.setMessageConverters(Arrays.asList(new HttpMessageConverter<?>[] {
			messageConverter
		}));
		return template;
	}
}
