package com.ge.hc.lcs.axone.oauth.constants;

public interface Constants {

	public static final String CLIENT_ID = "openiam.oauth2.client.clientId";
	public static final String CLIENT_SECRET = "openiam.oauth2.client.clientSecret";
	public static final String REDIRECT_URL = "openiam.oauth2.client.redirecturl";
	public static final String SERVER_PORT = "server.port";
	public static final String BASE_URL="openiam.oauth2.client.baseURL";
	
	public static final String APPLICATION_PROPERTIES = "application.properties";
	
	public static final String URL_OAUTH_INFO = "/oauth-info";
	public static final String URL_GE_SERVICE = "/geservicepage";
	public static final String URL_USERINFORMATION = "/userinformation";
	public static final String URL_USERSCOPES = "/userscopes";
	public static final String URL_TOKENINFORMATION = "/tokeninformation";
	public static final String URL_HOSPITAL = "/hospitalpage";
	
	public static final String PAGE_USERHOME = "userhome";
	public static final String PAGE_HOSPITAL = "hospitalpage";
	public static final String PAGE_GESERVICE = "geservice";
	public static final String PAGE_USERSCOPES = "userscopes";
	public static final String PAGE_TOKENINFORMATION = "tokeninformation";
	public static final String PAGE_USERINFORMATION = "userinformation";
	public static final String PAGE_INDEX = "index";
	public static final String PAGE_LOGIN = "login";
	
	public static final String TOKEN = "token";
	public static final String TOKEN_PARAM = "?token=";
	public static final String REFRESH_TOKEN_PARAM = "?refresh_token=";
}
