package org.openiam.oauth2.model;

import java.io.Serializable;
import java.util.Date;

public class OAuthToken implements Serializable {

	private static final long serialVersionUID = 1L;
	private String accessToken;
	private String state;
	private String tokenType;
	private Date expiresIn;
	private String refreshToken;
	
	public OAuthToken(final String accessToken, final String expiresIn, final String state, final String tokenType, final String refreshToken) {
		this.accessToken = accessToken;
		this.expiresIn = new Date(new Date().getTime() + (Long.valueOf(expiresIn) * 1000));
		this.state = state;
		this.tokenType = tokenType;
		this.refreshToken = refreshToken;
	}
	
	public boolean isExpired() {
		return new Date().after(expiresIn);
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public Date getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(Date expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

}
